import pympe, pm_helpers, wx, time, threading

class ProgressIndicator:
	def __init__(self, label='', start=0, total=100, runOnFinish=None, runOnStop=None, runOnUpdate=None, updateDelta=None):
		self.id = time.time()
		self.baseLabel = label
		self.label = label
		self.current = start
		self.total = total
		self.runOnFinish = runOnFinish
		self.runOnStop = runOnStop
		self.runOnUpdate = runOnUpdate
		self.updateDelta = updateDelta
		self.running = True
		self.startTime = time.time()
		self.lock = threading.RLock()

	def start(self):
		pympe.events.indicator_started(self)

	def update(self, current=None, label=None, total=None, step=1):
		with self.lock:
			if not self.running: return
			if total != None: self.total = total
			if label != None: self.baseLabel = label
			
			
			if current is None:
				self.current += step
			else:
				self.current = current

			if self.current:
				elapsed = (time.time()-self.startTime)
				remainingEstimate = ((self.total/float(self.current))*elapsed)-elapsed
			else:
				remainingEstimate = 0
			
			
			self.label = '(%s/%s) (%s Remaining) %s'%(self.current, self.total, pm_helpers.make_display_time(remainingEstimate), self.baseLabel)

			if self.current >= self.total:
				return self.finish()
			
			if not self.updateDelta or not self.current%self.updateDelta:
				pympe.events.indicator_updated(self)
			
				if self.runOnUpdate: pm_helpers.unthread_function(self.runOnUpdate, self)

	def finish(self, label=None):
		with self.lock:
			if label != None: self.baseLabel = label
			self.label = '(%s/%s) %s'%(self.current, self.total, self.baseLabel)
			self.running = False
			pympe.progress.finished(self)
			pympe.events.indicator_stopped(self)
			if self.runOnFinish: pm_helpers.unthread_function(self.runOnFinish, self)
			return None

	def stop(self, label=None):
		with self.lock:
			if label != None: self.baseLabel = label
			self.label = '(%s/%s) %s'%(self.current, self.total, self.baseLabel)
			
			self.running = False
			pympe.progress.finished(self)
			pympe.events.indicator_stopped(self)
			if self.runOnStop: pm_helpers.unthread_function(self.runOnStop, self)


class ProgressManager:
	def module_start(self):
		self.indicators = []
		self.current = None
		self.index = 0
		
	def module_stop(self): pass

	def new(self, label='', start=0, total=100, runOnFinish=None, runOnStop=None, runOnUpdate=None, updateDelta=None):
		indicator = ProgressIndicator(label, start, total, runOnFinish, runOnStop, runOnUpdate, updateDelta)
		self.indicators.append(indicator)
		self.current = indicator
		self.index = len(self.indicators)-1

		pympe.events.indicator_selected(self.current)
		indicator.start()

		return indicator
		
	def previous(self):
		if self.index == 0: self.index = len(self.indicators)-1
		else: self.index -= 1
		
		self.current = self.indicators[self.index]

		pympe.events.indicator_selected(self.current)
		return self.current
		
	def next(self):
		if self.index == len(self.indicators)-1: self.index = 0
		else: self.index += 1
		
		self.current = self.indicators[self.index]

		pympe.events.indicator_selected(self.current)
		return self.current
		
	def finished(self, indicator):
		#pympe.events.indicator_finished(indicator)

		# It was already deleted
		try:
			if self.index >= self.indicators.index(indicator):
				self.index -= 1
		except ValueError: return
		
		self.indicators.remove(indicator)
		
		if not self.indicators:
			self.index = 0
			self.current = None
		else:
			self.current = self.indicators[self.index]
		
