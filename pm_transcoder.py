import pympe, gst

formats = {"Mp3 (Variable Bit Rate)":
					{"description":"A proprietary and older, but also popular, lossy audio format.\nCBR gives less quality than VBR, but is compatible with any player.",
					"default":5,
					"quality":['32', '48', '64', '96', '128', '160', '192', '224', '256', '320'],
					"command":"lame vbr=4 vbr-mean-bitrate=%i",
					"extension":"mp3"},
			"Mp3 (Constant Bit Rate)":
					{"description":"A proprietary and older, but also popular, lossy audio format.\nCBR gives less quality than VBR, but is compatible with any player.",
					"default":5,
					"quality":['32', '48', '64', '96', '128', '160', '192', '224', '256', '320'],
					"command":"lame bitrate=%i",
					"extension":"mp3"},
			"Ogg Vorbis":
					{"description":"Vorbis is an open source, lossy audio codec with\nhigh quality output at a lower file size than MP3.",
					"default":5,
					"quality":['0.0', '0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9'],
					"command":"vorbisenc quality=%1.1f ! oggmux",
					"extension":"ogg"}
				}
				

class Transcoder:
	def __init__(self, callbackEnd=None, callbackError=None):
		self.input = None
		self.output = None
		self.callbackEnd = callbackEnd
		self.callbackError = callbackError
		self.lastPosition = 0.0
		self.running = False
		self.pipeline = None
		
	def set_raw_input(self, input):
		self.input = input
		
	def set_input(self, uri):
		self.input = 'filesrc location="%s"'%uri
		
	def set_raw_output(self, output):
		self.output = output
		
	def set_output(self, uri):
		self.output = 'filesink location="%s"'%uri
		
	def setup_encoder(self):
		importFormat = formats[pympe.config.get("import", "format")]
		self.encoder = importFormat["command"]%importFormat["quality"][pympe.config.get("import", "quality")]
		
	def start(self):
		self.setup_encoder()
		pipelineString = " ! ".join([self.input, 'decodebin name="decoder"', "audioconvert", self.encoder, self.output])
		self.pipeline = gst.parse_launch(pipelineString)
		
		self.bus = self.pipeline.get_bus()
		self.bus.add_signal_watch()
		self.bus.connect('message::error', self.message_error)
		self.bus.connect('message::eos', self.message_eos)
		
		self.pipeline.set_state(gst.STATE_PLAYING)
		self.running = True
		return self.pipeline
		
	def stop(self):
		if self.pipeline is None: return
		self.pipeline.set_state(gst.STATE_NULL)
		self.running = False
		if self.callbackEnd: self.callbackEnd()

	def message_error(self, *args):
		self.pipeline.set_state(gst.STATE_NULL)
		self.running = False
		if self.callbackError: self.callbackError()
		
	def message_eos(self, *args):
		self.stop()

	def get_position(self):
		if not self.running: return 0.0
		try: self.lastPosition = self.pipeline.query_position(gst.FORMAT_TIME)[0]/gst.SECOND
		except: pass
		return self.lastPosition
