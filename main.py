#!/usr/bin/env python2
# Pympe Media Player
# By: Jason Richards (scabootssca)
# At: scabootssca@gmail.com
# On: 24.Feb.2011
import sys, os, time, fileinput, re, pympe, logging
pympeVersionNumber = 0.2

def parse_arguments():
	import argparse

	parser = argparse.ArgumentParser(prog='pympe')
	#
	parser.add_argument('--no-library', action='store_true', help='Disables loading or saving of libraries.')
	parser.add_argument('--no-playlist', action='store_true', help='Disables loading or saving of playlists.')
	parser.add_argument('--no-plugins',action='store_true', help='Disables plugins.')

	parser.add_argument('--version', action='store_true', help='Print version then exit.')
	parser.add_argument('-v', '--verbose', default=False, action='count', help='Be verbose.')

	parser.add_argument('--debug', action='store_true', help='Helps when debugging.')
	parser.add_argument('--portable', action='store_true', help='Save to data directory instead of user directory.')
	parser.add_argument('--new', default=False, action='store_true', help='Spawn a new instance.')

	parser.add_argument('--library', nargs=1, help='Library file to use.')
	parser.add_argument('--config', nargs=1, help='Config file to use.')


	# Control
	parser.add_argument('-p', '--play', nargs='?', const=True, default=False, help='Play or play uri.')
	parser.add_argument('--pause', action='store_true', help='Pauses playback.')
	parser.add_argument('--next', action='store_true', help='Goes to the next song.')
	parser.add_argument('--previous', action='store_true', help='Goes to the previous song.')

	return dict(parser.parse_args()._get_kwargs())

class AssetManager:
	def __init__(self):
		self.libraries = {}
		self.nextLbraryUid = 0

	def add(self, assetType, asset):
		if assetType == 'library':
			self.libraries[self.nextLbraryUid] = asset
			asset.uid = self.nextLbraryUid
			self.nextLibraryUid += 1

	def remove(self, assetType, assetUid):
		try:
			if assetType == 'library':
				self.libraries.remove(assetUid)

		except ValueError:
			return False

		return True

class StdoutRedirect:
	def __init__(self, stdout):
		self.stdout = stdout
		self.pympe = None
		self.ansiEscape = re.compile(r'\x1b[^m]*m')

	def write(self, *args, **kwargs):
		if not args[0].strip():
			message = args[0].replace('\n', '\\n').strip().replace('\\n', '\n')
			if message == '\n': return
		else:
			message = time.strftime('[%H:%M:%S]\033[39m {}{}\n'.format(args[0][:-1].replace('\n', '\n            '), args[0][-1].strip()))
		self.stdout.write(message)

		try:
			self.pympe.stdout.write(self.ansiEscape.sub('', message))
			logging.info(message)
		except:
			pass

class StderrRedirect:
	def __init__(self, stderr):
		self.stderr = stderr
		self.pympe = None
		self.ansiEscape = re.compile(r'\x1b[^m]*m')

	def write(self, *args, **kwargs):
		if not args[0].strip():
			message = args[0].replace('\n', '\\n').strip().replace('\\n', '\n')
			if message == '\n': return
		else:
			message = time.strftime('[%H:%M:%S]\033[31m [ERROR]\033[39m {}{}\n'.format(args[0][:-1].replace('\n', '\n                   '), args[0][-1].strip()))
		self.stderr.write(message)

		# yeah..
	#	self.pympe.error(message, True, False, True)

		# Stuffs?
		try:
			self.pympe.stderr.write(self.ansiEscape.sub('', message))
			logging.error(message)
		except:
			pass

# Output redirection and logging
def alert(message):
	print('\033[33m[ALERT] {}\033[39m'.format(message))

	try: pympe.ui.show_message(message)
	except: pass

def output(message, *args, **kwargs):
	alert(message)

def debug(message):
	if pympe.arguments['verbose']:
		print('\033[32m[DEBUG]\033[39m {}'.format(message))

#need to make the stderr and this somewhat more linked
def error(message, traceback=False, showUiError=True):


	if showUiError:
		try: pympe.ui.show_error(message)
		except: pass

	sys.stderr.write(message)

	if traceback:
		from traceback import format_exc
		traceback = format_exc()
		sys.stdout.write('\033[36m[TRACE]\033[39m {}'.format(traceback))

	if pympe.arguments['debug']:
		import pdb; pdb.set_trace() # BREAKPOINT


#####################################################33
# Initilize all the parts
def init():
	print('Initalizing Pympe')

	pympe.running = True
	pympe.libraries = {}
	pympe.libraryUid = 0

	pympe.debug('Initalizing Log')
	pympe.log      = __import__("pm_log")
	pympe.log.init()

	pympe.debug('Initalizing Interface')
	pympe.ui        = __import__("pm_ui").Ui()
	pympe.debug('Initalizing Mainloop')
	pympe.mainloop = __import__("pm_mainloop").MainLoop()
	pympe.debug('Initalizing Config')
	pympe.config    = __import__("pm_config").Config("settings.ini" if not pympe.arguments['config'] else '%s.ini'%pympe.arguments['config'], mainConfig=True)
	pympe.debug('Initalizing Progress Manager')
	pympe.progress  = __import__("pm_status").ProgressManager()
	pympe.debug('Initalizing Cache')
	pympe.cache     = __import__('pm_cache').Cache()
	pympe.debug('Initalizing Mpris')
	pympe.mpris2    = __import__("pm_mpris2").Mpris2Interface()
	# Already started Dbus in main.py
	pympe.debug('Initalizing Device Manager')
	pympe.devices   = __import__("pm_devices").DeviceManager()
	pympe.debug('Initalizing Player Backend')
	pympe.player    = __import__("pm_player" ).Player()
	pympe.debug('Initalizing Playlist Manager')
	pympe.playlists = __import__("pm_playlists").PlaylistManager()
	pympe.debug('Initalizing Library')
	pympe.library   = __import__("pm_library").Library()
	pympe.debug('Initalizing Plugin Manger')
	pympe.plugins   = __import__("pm_plugins").PluginManager()

	return True

# Run all the parts
def start():
	print('Starting Pympe')

	pympe.debug('Starting Config')
	pympe.config.module_start()

	# Check for one instance lock
	#if config.get('main', 'oneInstanceLock'):
	#	import dbus
	#
	pympe.debug('Starting Mainloop')
	pympe.mainloop.module_start()
	pympe.debug('Starting Progress Manager')
	pympe.progress.module_start()
	pympe.debug('Starting Cache')
	pympe.cache.module_start()
	pympe.debug('Starting Player Backend')
	pympe.player.module_start()
	pympe.debug('Starting Mpris2')
	pympe.mpris2.module_start()
	pympe.debug('Starting Device Manager')
	pympe.devices.module_start()
	pympe.debug('Starting Playlist Manager')
	pympe.playlists.module_start()
	pympe.debug('Starting Library')
	pympe.library.start('database' if not pympe.arguments['library'] else pympe.arguments['library'])
	pympe.debug('Starting Interface')
	pympe.ui.module_start()
	pympe.debug('Starting Plugin Manager')
	pympe.plugins.module_start()

# Stop all the parts
def stop():
	if not pympe.running: return
	print('Shutting Down Pympe')
	pympe.events.pympe_shutdown()

	# Close the log
	#shutdownProgress = progress.new('Shutting Down', 0, 12, updateDelta=1)

	pympe.debug('Shutting Down Plugin Manager')
	#shutdownProgress.update(1, 'Shutting Down Plugin Manager')
	pympe.plugins.module_stop()
	pympe.debug('Shutting Down Playlist Manager')
	pympe.playlists.module_stop()
	pympe.debug('Shutting Down Player Backend')
	pympe.player.module_stop()
	pympe.debug('Shutting Down Library')
	pympe.library.shutdown()
	pympe.debug('Shutting Down Device Manager')
	pympe.devices.module_stop()
	pympe.debug('Shutting Down Dbus')
	pympe.dbus.module_stop()
	pympe.debug('Shutting Down Mpris2')
	pympe.mpris2.module_stop()
	pympe.debug('Shutting Down Cache')
	pympe.cache.module_stop()
	pympe.debug('Shutting Down Interface')
	pympe.ui.module_stop()
	pympe.debug('Shutting Down Config')
	pympe.config.module_stop()
	pympe.debug('Shutting Down Progress Manager')
	pympe.progress.module_stop()
	#debug('Shutting Down Event Manager')
	#events.module_stop()
	pympe.debug('Stopping Mainloop')
	pympe.mainloop.module_stop()
	pympe.running = False

	pympe.debug('Killing Existing Threads')
	import threading

	for thread in threading.enumerate():
		if thread.ident == threading._get_ident(): continue

		pympe.debug('Killing thread: %s'%thread.name)
		try: thread.join()
		except AssertionError: output('Dummy Thread, Nevermind')
		pympe.debug('Finished Killing: %s'%thread.name)


	pympe.log.stop()
	
	print('Fin. Thank You Come Again.')

def run():
	import sys
	# Parse any command line options
	arguments = parse_arguments()

	# Prints
	if arguments['version']:
		print 'Pympe %s'%pympeVersionNumber
		return

	# Set the process name
	if sys.platform == "linux2":
		try:
			import ctypes
			libc = ctypes.CDLL("libc.so.6")
			libc.prctl(15, "pympe", 0, 0, 0)
		except:
			pass

	# Find the actual working path
	# Stolen from pithos
	realPath = os.path.realpath(sys.argv[0])  # If this file is run from a symlink, it needs to follow the symlink
	if os.path.dirname(realPath) != ".":
		if realPath[0] == os.sep:
			fullPath = os.path.dirname(realPath)+os.sep
		else:
			fullPath = os.sep.join(os.getcwd(), os.path.dirname(realPath))
	else:
		fullPath = os.getcwd() + os.sep

	sys.path.insert(0, fullPath)
	sys.path.insert(0, fullPath+'include')
	sys.path.insert(0, fullPath+'plugins')

	# We need this for the gstreamer backend do not delete threads init
	import gobject, glib
	gobject.set_application_name('Pympe')
	gobject.set_prgname('Pympe')
	gobject.threads_init() # DO NOT DELETE THIS

	# Change our cwd if needed
	if os.sep in sys.argv[0]:
		os.chdir(os.path.dirname(sys.argv[0]))

	# Override stdout and stderr
	sys.stdout = StdoutRedirect(sys.stdout)
	sys.stderr = StderrRedirect(sys.stderr)

	#############################################################################
	# Start Pympe
	#############################################################################
	import pm_dbus, pm_event

	# Store the version
	pympe.version = pympeVersionNumber

	# Store the args (Needs to be before dbus)
	pympe.arguments = arguments

	# If not in debug mode set a reference back to pympe from the std redirects
	sys.stdout.pympe = pympe
	sys.stderr.pympe = pympe

	# Output Redirection and Logging
	pympe.alert = alert
	pympe.debug = debug
	pympe.error = error
	pympe.output = output

	# Start Dbus Service And Check For Running Instances
	pympe.debug('Initalizing Dbus')
	pympe.dbus = pm_dbus.Dbus(pympe)

	if not arguments['new']:
		if pympe.dbus.try_handoff() == True:
			print('Handing Off')
			return
		else:
			pympe.debug('Starting Dbus')
			pympe.dbus.module_start()

	# Init Needed Variables, Make directories
	pympe.dataDirectory = 'data' if arguments['portable'] else glib.get_user_data_dir()+'/pympe'
	pympe.logDirectory = os.sep.join((pympe.dataDirectory, 'logs'))
	pympe.configDirectory = 'config' if arguments['portable'] else glib.get_user_config_dir()+'/pympe'
	pympe.cacheDirectory = 'cache' if arguments['portable'] else glib.get_user_cache_dir()+'/pympe'
	pympe.cwd = fullPath
	pympe.dragData = []

	# Make the directories if needed
	## .local
	if not os.path.exists(pympe.dataDirectory):
		os.mkdir(pympe.dataDirectory)
		os.mkdir(pympe.logDirectory)
	## .local/logs
	elif not os.path.exists(pympe.logDirectory):
		os.mkdir(pympe.logDirectory)

	## .config
	if not os.path.exists(pympe.configDirectory):
		pympe.firstRun = True
		os.mkdir(pympe.configDirectory)

	## .cache
	if not os.path.exists(pympe.cacheDirectory):
		os.mkdir(pympe.cacheDirectory)

	import logging
	logLevel=logging.DEBUG if pympe.arguments['debug'] else logging.INFO
	logging.basicConfig(filename=time.strftime("{}{}%Y%m%d-%H%M%S.log".format(pympe.logDirectory,os.sep)),
						filemode='a',
						level=logLevel,
						format='%(asctime)s %(levelname)s:%(filename)s:%(lineno)s\n	%(message)s',
						datefmt="%Y/%m/%d %H:%M:%S")



	# Save the ident of the current main thread
	import threading
	pympe.mainThread = threading.current_thread().ident

	# Start Event Manager
	pympe.events = pm_event

	# Setup and Start Pympe
	init()
	start()
	pympe.stop = stop

	# Send an event that signals the player is finished loading
	pympe.events.pympe_initilized()

	# The Startup, If we're playing anything from command line
	if arguments['play']:
		pympe.playlists.main.set_index(pympe.playlists.main.get_length())
		pympe.playlists.main.enqueue(None, arguments['play'])
		pympe.player.play(entry=pympe.playlists.main.get_current())

	# The interface MainLoop
	print('Started Pympe')
	pympe.ui.app.MainLoop()

	# Finally, clean up
	stop()

if __name__ == '__main__':
	run()
