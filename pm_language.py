
# -*- coding: utf-8 -*-
language = "english"

import os
if os.name == "posix":
	uiPrevious = "◂◂"
	uiNext = "▸▸"
	uiPlay = "▶"
	uiPause = "❚❚"
	uiStop = "█"
else:
	uiPrevious = "|<"
	uiNext = ">|"
	uiPlay = ">"
	uiPause = "||"
	uiStop = "[_]"

uiPreviousText = "Previous"
uiNextText = "Next"
uiPlayText = "Play"
uiPauseText = "Pause"
uiStopText = "Stop"

if language == "english":
	readableTags = {
		"artist":"Artist",
		"album":"Album",
		"title":"Title",
		"tracknumber":"Track Number",
		"tracktotal":"Track Total",
		"albumartist":"Album Artist",
		"genre":"Genre",
		"year":"Year",
		"bitrate":"Bitrate",
		"uri":"Uri",
		"length":"Length",
		"comment":"Comment",
		"discnumber":"Disc",
		"disctotal":"Disc Total",
		"compilation":"Compilation",
		"timemodified":"Time Modified",
		"playcount":"Play Count",
		"art":"Album Art",
		"rating":"Rating",
		"timeplayed":"Last Played",
		"timeadded":"Time Added",
		"lyrics":"Lyrics",
		"skipcount":"Skip Count",
		'composer':'Composer',
		'episodenumber':'Episode Number',
		'episodetotal':'Episode Total',
		'series':'Series',
		'season':'Season',
		'trackId':'Track Id',
		'musicbrainzid':'Musicbrainz Id'}

	libraryColumnHeaders = {
		"artist":"Artist",
		"album":"Album",
		"title":"Title",
		"tracknumber":"#",
		"tracktotal":"Track Total",
		"albumartist":"Album Artist",
		"genre":"Genre",
		"year":"Year",
		"bitrate":"Bitrate",
		"uri":"Uri",
		"length":"Length",
		"comment":"Comment",
		"discnumber":"Disc",
		"disctotal":"Disc Total",
		"compilation":"Comp.",
		"timemodified":"Time Modified",
		"playcount":"Plays",
		"art":"Art",
		"rating":"Rating",
		"timeplayed":"Last Played",
		"timeadded":"Time Added",
		"lyrics":"Lyrics",
		"skipcount":"Skips",
		'composer':'Composer',
		'episodenumber':'Episode',
		'episodetotal':'Episode Total',
		'series':'Series',
		'season':'Season',
		'trackId':'Track Id',
		'musicbrainzid':'Musicbrainz Id'}

	uiShuffle = "SHUF"
	uiLoop = ["LOOP OFF", "LOOP ONE", "LOOP ALL"]
	uiLoopShort = ['', '1', '']


	uiWindowTitle = "Pympe"

	uiTabNowPlaying = 'Now Playing'
	uiTabLibrary = 'Library'
	uiTabDebug = 'Debug'
	uiTabPlaylist = 'Playlist'
