import pympe, dbus, tempfile, os, pm_mediafile
import dbus.service
from hashlib import md5

class MprisInterface(dbus.service.Object):
	def __init__(self):
		self.bus = dbus.service.BusName('org.mpris.MediaPlayer2.pympe', bus=dbus.SessionBus())
		dbus.service.Object.__init__(self, self.bus, '/org/mpris/MediaPlayer2')
		
		# Register with natty
		self.populate('org.mpris.MediaPlayer2', 'DesktopEntry')

		# Register with maverick
		#server = indicate.indicate_server_ref_default()
		#server.set_type('music.pympe')
		#server.set_desktop_file('/usr/share/applications/pympe.desktop')
		#server.show()
		
		self.trackIds = []
		self.tracks = []
		self.trackId = ''
		
		#pympe.event.add_listener("player-song-changed", self.listener)
		#pympe.event.add_listener("currentfile-updated-tags", self.listener)
		#pympe.event.add_listener("player-paused", self.listener)
		#pympe.event.add_listener("player-playing", self.listener)
		pympe.event.subscribe('player-playback-started', self.event_player_playback_started)
		pympe.event.subscribe('player-playback-paused', self.event_player_playback_paused)
		pympe.event.subscribe('player-playback-stopped', self.event_player_playback_stopped)

		#pympe.newevent.subscribe('playlist-renamed', self.event_playlist_renamed)
		#pympe.newevent.subscribe('playlist-replaced', self.event_playlist_replaced)
		#pympe.newevent.subscribe('playlist-item-added', self.event_playlist_item_added)
		#pympe.newevent.subscribe('playlist-item-removed', self.event_playlist_item_removed)
		#pympe.newevent.subscribe('playlist-item-updated', self.event_playlist_item_updated)

	

	def listener(self, eventType, eventSource, *info):
		if eventType == 'player-song-changed': self.populate("org.mpris.MediaPlayer2.Player", 'CanGoNext', 'CanGoPrevious')
		if eventType == "player-paused" or eventType == 'player-playing': self.populate("org.mpris.MediaPlayer2.Player", 'PlaybackStatus')
		elif eventType == "currentfile-updated-tags": self.populate("org.mpris.MediaPlayer2.Player", 'Metadata')
		
	def populate(self, interface, *propNames):
		props = {}
		for p in propNames: props[p] = getattr(self, p)()
		self.PropertiesChanged(interface, props, [])

	@dbus.service.method(dbus.PROPERTIES_IFACE, in_signature='ss', out_signature='v')
	def Get(self, interface, prop):
		if hasattr(self, prop):
			result = getattr(self, prop)()
			return result
		return None

	@dbus.service.signal(dbus.PROPERTIES_IFACE, signature='sa{sv}as')
	def PropertiesChanged(self, interface, updated, invalid): pass
	
	def open_mediafile(self, uri):
		m = pm_mediafile.MediaFile(uri)
		return (m if m.open() else None)
	
	def generate_mpris_metadata(self, mediaFile):
		if mediaFile is None: return dbus.types.Dictionary({}, signature='sv', variant_level=1)
		mediaFile.read()
		## mpris2.0 meta map, defined at http://xmms2.org/wiki/MPRIS_Metadata
		meta = {}

		meta['xesam:title'] = mediaFile.display('title', True)
		meta['xesam:album'] = mediaFile.display('album', True)
		meta['xesam:artist'] = dbus.types.Array(mediaFile['artist'], signature='s')
		meta['mpris:length'] = dbus.types.Int64(int(mediaFile['length'] or 0)*1000)

		cacheFile = pympe.cache.has(mediaFile['uri'])
		if cacheFile is None and mediaFile['art']:
			cacheFile = pympe.cache.set(mediaFile['uri'], mediaFile['art'])
		if cacheFile is not None: meta['mpris:artUrl'] = 'file://'+cacheFile

		meta['mpris:trackid'] = mediaFile['uri']
		meta['xesam:url'] = mediaFile['uri']

		return dbus.types.Dictionary(meta, signature='sv', variant_level=1)

#######################################################
# org.mpris.MediaPlayer2
#######################################################
	@dbus.service.method('org.mpris.MediaPlayer2')
	def Raise(self): pympe.ui.uiMain.bring_to_front()
	@dbus.service.method('org.mpris.MediaPlayer2')
	def Quit(self): pympe.stop()
	
	def CanRaise(self): return True
	def CanQuit(self): return True
	def HasTrackList(self): return True
	def Identity(self): return "Pympe Media Player"
	def DesktopEntry(self): return "pympe"
	def SupportedUriSchemes(self): return ['http', 'file']
	def SupportedMimeTypes(self): return ['audio/mpeg']
	
#######################################################
# org.mpris.MediaPlayer2.TrackList
#######################################################
	@dbus.service.method('org.mpris.MediaPlayer2.TrackList', 'ao', 'aa{sv}')
	def GetTracksMetadata(self, trackIds): return [self.generate_mpris_metadata(self.open_mediafile(pympe.playlists.current.entryInfo[trackId][0])) for trackId in trackIds]
	@dbus.service.method('org.mpris.MediaPlayer2.TrackList', 'sob')
	def AddTrack(self, uri, afterTrack, setAsCurrent): pympe.playlists.current.enqueue([uri], None if not afterTrack else pympe.playlists.uids.index(afterTrack))
	@dbus.service.method('org.mpris.MediaPlayer2.TrackList', 'o')
	def RemoveTrack(self, trackId): pympe.playlists.current.dequeue([trackId])
	@dbus.service.method('org.mpris.MediaPlayer2.TrackList', 'o')
	def GoTo(self, trackId): pympe.playlists.current.set_index(pympe.playlists.current.uids.index(trackId))
	@dbus.service.signal("org.mpris.MediaPlayer2.TrackList", 'aoo')
	def TrackListReplaced(self, tracks, currentTrack): pass
	@dbus.service.signal("org.mpris.MediaPlayer2.TrackList", 'a{sv}o')
	def TrackAdded(self, metadata, afterTrack): pass
	@dbus.service.signal("org.mpris.MediaPlayer2.TrackList", 'o')
	def TrackRemoved(self, trackId): pass
	@dbus.service.signal("org.mpris.MediaPlayer2.TrackList", 'oa{sv}')
	def TrackMetadataChanged(self, trackId, metadata): pass
	def Tracks(self): return pympe.playlists.current.uids
	def CanEditTracks(self): return True
	
	def event_playlist_replaced(self, eventType, eventObject):
		if eventObject == pympe.playlists.current:
			self.TrackListReplaced(eventObject.uids, eventObject.index)
	def event_playlist_item_added(self, eventType, eventObject, itemUid, index):
		if eventObject == pympe.playlists.current:
			print eventType, eventObject, itemUid, index
			self.TrackAdded(self.generate_mpris_metadata(self.open_mediafile(eventObject.entryInfo[itemUid][0])), '' if index == 0 else eventObject.uids[index-1])
	def event_playlist_item_removed(self, eventType, eventObject, itemUid):
		if eventObject == pympe.playlists.current:
			self.TrackRemoved(itemUid)
	def event_playlist_item_updated(self, eventType, eventObject, itemUid):
		if eventObject == pympe.playlists.current:
			self.TrackMetadataChanged(itemUid, self.generate_mpris_metadata(self.open_mediafile(eventObject.entryInfo[itemUid][0])))
	
#######################################################
# org.mpris.MediaPlayer2.Player
#######################################################
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def Next(self): pympe.player.next()
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def Previous(self): pympe.player.previous()
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def Pause(self): pympe.player.pause()
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def PlayPause(self): pympe.player.pause() if (pympe.player.playing and not pympe.player.paused) else pympe.player.play()
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def Stop(self): pympe.player.stop()
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def Play(self): pympe.player.play()
	@dbus.service.method('org.mpris.MediaPlayer2.Player', 'x')
	def Seek(self, microseconds): pympe.player.seek(microseconds*1000)
	@dbus.service.method('org.mpris.MediaPlayer2.Player', 'ox')
	def SetPosition(self, trackId, microseconds):
		if trackId == self.trackId: pympe.player.position(microseconds*1000)
	@dbus.service.method('org.mpris.MediaPlayer2.Player', 's')
	def OpenUri(self, uri): pympe.player.play(uri)
	@dbus.service.signal("org.mpris.MediaPlayer2.TrackList", 'x')
	def Seeked(self, microseconds): pass
	def PlaybackStatus(self): return 'Stopped' if not pympe.player.playing else ('Paused' if pympe.player.paused else 'Playing')
	def LoopStatus(self, loop=None):
		if loop is None:
			loop = pympe.config.get("player", "loop")
			return 'None' if loop == 'LOOP OFF' else ('Track' if loop == 'LOOP ONE' else 'Playlist')
		else:
			pympe.event.send("player-loop", [0 if loop == 'None' else (1 if loop == 'Track' else 2)])
	def Rate(self, rate=None):
		if rate is None:
			return 1.0
		else:
			pass
	def Shuffle(self, shuffle=None):
		if shuffle is None:
			return True if pympe.config.get('player', 'shuffle') else False
		else:
			pympe.event.send('player-shuffle', [shuffle])
	def Metadata(self): return self.generate_mpris_metadata(pympe.player.currentFile)
	def Volume(self, volume=None):
		if volume is None: return pympe.player.volume()
		else: pympe.player.volume(volume if volume > 0 else 0)
	def Position(self): return pympe.player.position()*1000
	def MinimumRate(self): return 1.0
	def MaximumRate(self): return 1.0
	def CanGoNext(self): return True if pympe.playlists.current.get_next() else False
	def CanGoPrevious(self): return True if pympe.playlists.current.get_previous() else False
	def CanPlay(self): return True if pympe.playlists.current.get_length() else False
	def CanPause(self): return pympe.player.playing
	def CanSeek(self): return True
	def CanControl(self): return True
	
#######################################################
# org.mpris.MediaPlayer2.Playlists
#######################################################
	@dbus.service.method('org.mpris.MediaPlayer2.Playlist', 'o')
	def ActivatePlaylist(self, playlistUid): pympe.newevent.send("playlist-select", self, playlistUid)
	@dbus.service.method('org.mpris.MediaPlayer2.Playlist', 'uusb')
	def GetPlaylists(self, index, maxCount, order, reverseOrder):
		if maxCount+index > len(pympe.playlists.playlists): maxCount = len(pympe.playlists.playlists)-index
		# This is where we would do our ordering
		playlists = pympe.playlists.playlists[index:index+maxCount]
		if reverseOrder: playlists.reverse()
		return [(p.uid, p.name, '') for p in playlists]
	@dbus.service.signal("org.mpris.MediaPlayer2.Playlist", '(oss)')
	def PlaylistChanged(self, playlist): pass
	def PlaylistCount(self): return len(pympe.playlists.playlists)
	def Orderings(self): return ['Alphabetical']
	def ActivePlaylist(self): return pympe.playlists.current.uid
	
	def event_playlist_renamed(self, eventType, eventObject, playlist): self.PlaylistChanged((playlist.uid, playlist.name, None))
	
