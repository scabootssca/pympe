#import pympe, pm_helpers, dbus, pm_mpris
import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop

class Dbus(dbus.service.Object):
	def __init__(self, pympe):
		self.bus = None
		self.pympe = pympe

		# Set the default mainloop
		DBusGMainLoop(set_as_default=True)

		try:
			self.bus = dbus.SessionBus()
			self.systemBus = dbus.SystemBus()
		except: print 'Unable to connect to Session Bus, is the dbus server running?'

	def module_start(self):
		self.busName = dbus.service.BusName("org.pympe.Main", bus=self.bus)
		dbus.service.Object.__init__(self, self.busName, '/org/pympe/Main')

	def try_handoff(self):
		# Single instance check
		obj = self.bus.get_object('org.freedesktop.DBus', '/org/freedesktop/DBus')
		dbus_iface = dbus.Interface(obj, 'org.freedesktop.DBus')
		if not 'org.pympe.Main' in dbus_iface.ListNames(): return

		# Connect to the existing
		remote_object = self.bus.get_object('org.pympe.Main', '/org/pympe/Main')
		iface = dbus.Interface(remote_object, 'org.pympe.Main')

		#try:
		if 1:
			if self.pympe.arguments['play']:
				iface.Play(self.pympe.arguments['play'])
			elif self.pympe.arguments['pause']:
				iface.Pause()
			elif self.pympe.arguments['next']:
				iface.Next()
			elif self.pympe.arguments['previous']:
				iface.Previous()
			else:
				iface.Raise()


		#except:
		#
		#	print 'Existing pympe dbus server running but recieved invalid or no response'
		#	return
		return True

	def module_stop(self):
		self.bus.close()

	@dbus.service.method("org.pympe.Main")
	def Raise(self):
		self.pympe.ui.uiMain.bring_to_front()

	#@dbus.service.method("org.pympe.Pympe", None, "s")
	#def GetCurrent(self):
		#if not pympe.player.currentFile: return ""
		#return pympe.player.currentFile["uri"]
	@dbus.service.method("org.pympe.Main", 'as')
	def Play(self, uris=[]):
		uris = [('file://'+x if not '://' in x else x) for x in uris]

		if uris:
			self.pympe.playlists.main.set_index(self.pympe.playlists.main.get_length())
			self.pympe.playlists.main.enqueue(None, [str(uri) for uri in uris])

		self.pympe.player.play(entry=self.pympe.playlists.main.get_current())
	@dbus.service.method("org.pympe.Main")
	def Next(self):
		self.pympe.events.player_next()
	@dbus.service.method("org.pympe.Main")
	def Previous(self):
		self.pympe.events.player_previous()
	@dbus.service.method("org.pympe.Main")
	def Pause(self):
		self.pympe.events.player_pause()
	@dbus.service.method('org.pympe.Main', 'as')
	def Enqueue(self, uris=[]):
		uris = [('file://'+x if not '://' in x else x) for x in uris]

		self.pympe.playlists.main.enqueue(None, [str(uri) for uri in uris])
	@dbus.service.method('org.pympe.Main', 'as')
	def Replace(self, uris=[]):
		uris = [('file://'+x if not '://' in x else x) for x in uris]

		self.pympe.playlists.main.replace(None, [str(uri) for uri in uris])
	#@dbus.service.method("org.pympe.Pympe")
	#def Pause(self): pympe.player.pause()
	#@dbus.service.method("org.pympe.Pympe")
	#def PlayPause(self):
		#if pympe.player.paused or not pympe.player.playing: pympe.player.play()
		#else: pympe.player.pause()
	#@dbus.service.method("org.pympe.Pympe")
	#def Stop(self): pympe.player.stop()
	#@dbus.service.method("org.pympe.Pympe")
	#def Next(self): pympe.player.next()
	#@dbus.service.method("org.pympe.Pympe")
	#def Previous(self): pympe.player.previous()
	#@dbus.service.method("org.pympe.Pympe", None, "i")
	#def GetVolume(self): return pympe.player.volume()*100
	#@dbus.service.method("org.pympe.Pympe", "i")
	#def SetVolume(self, volume): pympe.player.volume(volume*.01)
	#@dbus.service.method("org.pympe.Pympe")
	#def IncreaseVolume(self):
		#volume = pympe.player.volume()
		#if volume <= 0.9: volume += 0.1
		#else: volume = 1.0
		#pympe.player.volume(volume)
	#@dbus.service.method("org.pympe.Pympe")
	#def DecreaseVolume(self):
		#volume = pympe.player.volume()
		#if volume >= 0.1: volume -= 0.1
		#else: volume = 0.0
		#pympe.player.volume(volume)
	#@dbus.service.method("org.pympe.Pympe", None, "i")
	#def GetPosition(self): return pympe.player.position()
	#@dbus.service.method("org.pympe.Pympe", "i")
	#def SetPosition(self, seconds): pympe.player.position(seconds)
	#@dbus.service.method("org.pympe.Pympe", None, "v")
	#def GetProgress(self): return (pympe.player.position()/(pympe.player.length+0.0))*100
	#@dbus.service.method("org.pympe.Pympe", "v")
	#def SetProgress(self, percent): pympe.player.position(int(pympe.player.length*(percent*0.01)))
	#@dbus.service.method("org.pympe.Pympe", None, "i")
	#def GetLength(self): return pympe.player.length
	#@dbus.service.method("org.pympe.Pympe", None, "b")
	#def IsPlaying(self): return pympe.player.playing
	#@dbus.service.method("org.pympe.Pympe", None, "b")
	#def IsPaused(self): return pympe.player.paused
	#@dbus.service.method("org.pympe.Pympe", "s", "s")
	#def GetTag(self, tag):
		#if pympe.player.currentFile:
			#return pympe.player.currentFile.display(tag, True)
	#@dbus.service.method("org.pympe.Pympe", "s", "as")
	#def GetMultiTag(self, tags):
		#if pympe.player.currentFile:
			#if not type(x) == type(list()): x = [x]
			#return [pympe.player.currentFile[x] for x in x]
	#@dbus.service.method("org.pympe.Pympe", "sv")
	#def SetTag(self, tag, value):
		#if pympe.player.currentFile: pympe.player.currentFile[tag] = value
	#@dbus.service.method("org.pympe.Pympe", None, "v")
	#def GetVersion(self): return pympe.config.get("main", "version")
	#@dbus.service.method("org.pympe.Pympe", "s")
	#def PlayUri(self, uri):
		#if not "://" in uri: uri = "file://"+uri
		#pympe.player.play(uri)
	#@dbus.service.method("org.pympe.Pympe", "s")
	#def ExportPlaylist(self, location):
		#pympe.playlists.save_playlist_to_file(location, pympe.playlists.current.uid)
	#@dbus.service.method("org.pympe.Pympe", None, "ay")
	#def GetArtwork(self):
		#if pympe.player.currentFile: return pympe.player.currentFile["art"]
	#@dbus.service.method("org.pympe.Pympe", None, "s")
	#def GetState(self):
		#return "stopped" if not pympe.player.playing else ("paused" if pympe.player.paused else "playing")
	#@dbus.service.method("org.pympe.Pympe", None, "b")
	#def GetShuffle(self):
		#return True if pympe.config.get("player", "shuffle") else False
	#@dbus.service.method("org.pympe.Pympe", None, "s")
	#def GetLoop(self):
		#loop = pympe.config.get("player", "loop")
		#return 'None' if loop == 0 else ('Track' if loop == 1 else 'Playlist')
	#@dbus.service.method("org.pympe.Pympe", "b")
	#def SetShuffle(self, shuffle):
		#pympe.events.player-shuffle(bool(shuffle))
	#@dbus.service.method("org.pympe.Pympe", "x")
	#def SetLoop(self, loop):
		#pympe.event("player-loop", self, int(loop))
	#@dbus.service.method("org.pympe.Pympe")
	#def SetLoopNext(self):
		#l = pympe.config.get("player", "loop")
		#l += 1
		#if l == 3: l = 0
		#pympe.event("player-loop", [l])


	#@dbus.service.signal("org.pympe.Pympe")
	#def TrackChanged(self): pass
	#@dbus.service.signal("org.pympe.Pympe", 's')
	#def StateChanged(self, state): pass
	#@dbus.service.signal("org.pympe.Pympe", 's')
	#def PlayModeChanged(self, mode): pass
