import pympe, pm_helpers, os
functions = ['clean_library', 'rebuild_uri_lookup', 'albart_artist_diff']

def albart_artist_diff():
	different = []

	for trackId in pympe.library.tracks.iterkeys():
		info = pympe.library.get_single_track_multi_values(trackId, ['artist', 'albumartist', 'compilation'], 'unmodified')
		if info[0][0] != info[1][0] and not info[2]:
			different.append(trackId)

	pympe.ui.uiMain.uiTabLibrary.display_subset(different)
	print different

def save_independant():
	print '#'*80
	print '#'*80
	print '#'*80

	import struct

	possibleKeys = []


	for track in pympe.libraries[0].tracks.itervalues():
		types = {}

		packString = ''
		packValues = []
		lengths = []

		for key, value in track.iteritems():
			if not key in possibleKeys:
				possibleKeys.append(key)
				keyIndex = len(possibleKeys)-1
			else:
				keyIndex = possibleKeys.index(key)

			# If it's a list
			if type(value) == list:
				packString += 'HH'
				packValues += [keyIndex, len(value)]
				valueAsList = value
			else:
				valueAsList = [value]

			for value in valueAsList:
				if type(value) == int:
					packString += 'Hi'
					packValues += [keyIndex, value]
				elif type(value) == bool:
					packString += 'H?'
					packValues += [keyIndex, value]
				elif type(value) == float:
					packString += 'Hf'
					packValues += [keyIndex, value]
				elif type(value) in (unicode, str):
					encoded = value.encode('UTF-8')
					if encoded:
						packString += 'H%is'%len(encoded)
						packValues += [keyIndex, encoded]

		# Pack it
		pInfo = struct.pack('H%is'%len(packString), len(packString), packString)
		packed = struct.pack(packString, *packValues)

		print '~~~~~~~~~~~~~~ Saved ~~~~~~~~~~~~~~'
		break

	packString = ''
	for key in possibleKeys:
		packString += '%is'%len(key)
	print struct.pack(packString, *possibleKeys)

	print '~~~~~~~~~~~~~~ Now Loading ~~~~~~~~~~~~~~'
	stringLength = struct.unpack('H', pInfo[:2])
	packString = struct.unpack('%is'%stringLength, pInfo[2:])[0]

	results = struct.unpack(packString, packed)

	trackInfo = {}
	keyIndex = None
	for result in results:
		if keyIndex is None:
			keyIndex = result
			continue

		trackInfo[possibleKeys[keyIndex]] = result
		keyIndex = None

	print trackInfo



def rebuild_uri_lookup():
	print 'Rebuilding uri lookup'
	pympe.library.tracksByUri = {}

	for trackInfo in pympe.library.tracks.values():
		if type(trackInfo['uri']) == str:
			print trackInfo['uri']
		if trackInfo['uri'] in pympe.library.tracksByUri:
			print 'Duplicate track', trackInfo['uri']
		pympe.library.tracksByUri[trackInfo['uri']] = trackInfo

	print 'Finished'

def clean_library():
	print 'Updating tag ids'
	for tag in pympe.library.sharedTagIds.keys():
		existingIds = sorted(pympe.library.sharedTags[tag].keys())
		if existingIds:
			pympe.library.sharedTagIds[tag] = existingIds[-1]+1
		else:
			pympe.library.sharedTagIds[tag] = 0

	print 'Checking tracks for existance, tag links, duplicates'
	inUseTagIds = {}
	for tag in pympe.library.multiTagsShared: inUseTagIds[tag] = set()

	for trackId, trackInfo in pympe.library.tracks.items():
		if not os.path.exists(trackInfo['uri'].split('://', 1)[1]):
			print 'Missing track from filesystem %s'%trackId
			pympe.library.remove_single_track(trackId)
			continue

		if not trackInfo['uri'] in pympe.library.tracksByUri:
			print 'Track %s was missing uri reference: %s'%(trackId, trackInfo['uri'])
			pympe.library.tracksByUri[trackInfo['uri']] = trackInfo

		elif pympe.library.tracksByUri[trackInfo['uri']]['trackId'] != trackId:
			print 'Track %s is a duplicate: %s'%(trackId, trackInfo['uri'])
			pympe.library.remove_single_track(trackId)
			continue

		for tag in pympe.library.multiTagsShared:
			inUseTagIds[tag].update(trackInfo[tag])
			for tagId in trackInfo[tag]:
				# Make sure it links to a valid tag
				if not tagId in pympe.library.sharedTags[tag]:
					print 'Track %s links to invalid tag %s:%s:%s'%(trackId, tag, tagId, pympe.library.sharedTags[tag][tagId][1])
					pympe.library.tracks[trackId][tag].remove(tagId)
				# And if it does then be sure that the tag links back to the track
				elif not trackId in pympe.library.sharedTags[tag][tagId][3:]:
					print 'Tag %s:%s:%s no link back to track %s'%(tag, tagId, pympe.library.sharedTags[tag][tagId][1], trackId)
					pympe.library.sharedTags[tag][tagId].append(trackId)


	print 'Checking for abandoned tags'
	for tag, usedTagIds in inUseTagIds.iteritems():
		for tagId in set(pympe.library.sharedTags[tag].keys()).difference(usedTagIds):
			name = pympe.library.sharedTags[tag][tagId][1]
			print 'Tag %s:%s:%s abandoned'%(tag, tagId, name)

			if name in pympe.library.sharedTagsByName[tag]:
				del pympe.library.sharedTagsByName[tag][name]
			del pympe.library.sharedTags[tag][tagId]


	print 'Removing links to missing tracks from tags'
	trackIds = pympe.library.tracks.keys()
	for tagName, tagEntries in pympe.library.sharedTags.iteritems():
		for tagId, tagInfo in tagEntries.iteritems():
			missingTrackIds = set(tagInfo[3:]).difference(trackIds)

			for missingId in missingTrackIds:
				del pympe.library.sharedTags[tagName][tagId][pympe.library.sharedTags[tagName][tagId][3:].index(missingId)+3]

			if missingTrackIds:
				print 'Tag %s:%s:%s bad track references: %s'%(tagName, tagId, 	pympe.library.sharedTags[tagName][tagId][1], ', '.join((str(x) for x in missingTrackIds)))


	print 'Finished'

def cleanup_library():
	trackIds = pympe.library.tracks.keys()
	uris = [pympe.library.tracks[x]['uri'] for x in trackIds]

	unmatched = []
	for trackId, uri in zip(trackIds, uris):
		if pympe.library.trackDict[uri][1] != trackId:
			unmatched.append(trackId)

	print ' | '.join(['id:%i'%x for x in unmatched])

def find_duplicates():
	total = len(pympe.library.tracks.keys())
	indicator = pympe.progress.new("Removing Duplicate Tracks" , 0, total)

	counts = {}

	for index, (trackId, track) in enumerate(pympe.library.tracks.iteritems()):
		indicator.update(index, 'Checking Track %i/%i'%(index, total))
		try:
			counts[track['uri']][0] += 1
			counts[track['uri']][1].append(trackId)
		except KeyError:
			counts[track['uri']] = [1, [trackId]]


	dupes = []
	for index, dupe in enumerate([x for x in counts.values() if x[0] > 1]):
		dupes += sorted(dupe[1])[1:]

	#pympe.library.remove_multi_tracks(dupes)
	print dupes
	indicator.finish('Finished Removing Duplicates')

def audio_curve():
	size = 100

	grid = [0]*size

	for x in range(size):
		grid[x] = x*(x*0.0123)

	print grid[0], grid[24], grid[49], grid[74], grid[99]
	print ' '.join(('%03f'%x for x in grid))
	#print '\n\n'
	#print '\n'.join((' '.join(('%03i'%x for x in y)) for y in grid))

#@pm_helpers.threaded
def update_library_format():
	for tag, tagInfo in pympe.library.sharedTags.items():
		newById = {}
		newByName = {}

		for uid, value in tagInfo.items():
			entry = [value[1], value[1].lower()]+value[2]
			newById[uid] = entry
			newByName[value[1]] = entry

		pympe.library.sharedTags[tag] = newById
		pympe.library.sharedTagsByName[tag] = newByName

	print 'Finished Updating Format'

	#import time

	#start = time.time()
	#pympe.library.save_database()
	#print time.time()-start
	#from hashlib import sha1
	#import os, struct

	#indicator = pympe.progress.new('Generating file hashes', 0, len(pympe.library.tracks), updateDelta=7)

	#for index, track in enumerate(pympe.library.tracks.values()):
		#filename = track['uri'].split('//', 1)[1]

		#indicator.update(index, 'Generating for %s'%filename)

		## only hash the first 16k
		#hash = sha1()
		#size = os.path.getsize(filename)
		#hash.update(struct.pack("<L", size))
		#hash.update(open(filename).read(16384))
		#track['hash'] = hash.hexdigest()

		#track['hash'] =
	#	track['compilation'] = False
		#pympe.library.tagNames['composer'] = {}
		#pympe.library.tagNames['tags'] = {}
		#pympe.library.tagNames['series'] = {}
		#pympe.library.tagNames['season'] = {}
		#pympe.library.tagIds['composer'] = {}
		#pympe.library.tagIds['tags'] = {}
		#pympe.library.tagIds['series'] = {}
		#pympe.library.tagIds['season'] = {}
		#pympe.library.currentTagIds += [0,0,0,0]
		#try:
			#track['timeadded']
		#except KeyError:
			#track['timeadded'] = time.time()
		#track['bookmarks'] = []
		#track['usePlaybackPosition'] = False
		#track['playbackPosition'] = 0
		#track['useStartTime'] = False
		#track['startTime'] = 0
		#track['useStopTime'] = False
		#track['stopTime'] = 0
		#track['skipOnShuffle'] = False
		#track['useEqualizerPreset'] = False
		#track['equalizerPreset'] = 0
	#indicator.finish()

def struct_save_list():
	import struct, time
	start = time.time()
	library = pympe.library
	libraryLength = len(pympe.library.tracks)

	entries = []

#### self.version, self.trackId, self.currentTagIds
	entries.append(struct.pack('HH9H', library.version, library.trackId, *library.currentTagIds))


#### self.tracks
	entries.append(struct.pack('H', len(library.tracks)))

	for trackId, track in library.tracks.iteritems():
		tempValues = []

		packString = 'H16HI3f7?'
		packValues = [trackId]+[track[key] for key in ('tracknumber', 'tracktotal', 'discnumber', 'disctotal', 'episodenumber', \
			'episodetotal', 'bitrate', 'samplerate', 'length', 'rating', 'playcount', 'skipcount', 'equalizerPreset', 'stopTime', \
			'startTime', 'playbackPosition', 'filesize', 'timeplayed', 'timemodified', 'timeadded', 'compilation', 'hasCustomRating', \
			'usePlaybackPosition', 'useStartTime', 'useStopTime', 'skipOnShuffle', 'useEqualizerPreset')]

		for key in ('hash', 'musicbrainzid', 'uri', 'mimetype', 'lyrics'):
			encoded = track[key].encode('UTF-8')
			packString += '%is'%len(encoded)
			packValues.append(encoded)

		packString += '9H'
		for key in ('artist', 'album', 'year', 'genre', 'albumartist', 'composer', 'tags', 'series', 'season'):
			packValues.append(len(track[key]))

			if len(track[key]):
				packString += '%iH'%len(track[key])
				tempValues += track[key]

		packValues += tempValues




		packString += 'H'
		packValues.append(len(track['title']))
		for encoded in (value.encode('UTF-8') for value in track['title']):
			packString += '%is'%len(encoded)
			packValues.append(encoded)

		packString += 'H'
		packValues.append(len(track['comment']))
		for encoded in (value.encode('UTF-8') for value in track['comment']):
			packString += '%is'%len(encoded)
			packValues.append(encoded)

		# For trackDict
		packString += 'f'
		packValues.append(library.trackDict[track['uri']][0])

		entries.append(struct.pack('50s', packString))
		entries.append(struct.pack(packString, *packValues))

#### self.tagIds
	#{tagName:{tagId:[tagId, tagName, [trackId, trackId, trackId, etc..]]}}
	for tagName in ("artist", "album", "year", "genre", "albumartist", "composer", "tags", "series", 'season'):
		packValues = [len(library.tagIds[tagName])]
		packString = 'H%iH'%(packValues[0]*3) # tagIds, tagNameLength, tagTrackIdsLength

		tempValues = []
		tempValues2 = []
		tempValues3 = []
		tempValues4 = []

		for tag in library.tagIds[tagName].values():
			encodedTag = tag[1].encode('UTF-8')
			packValues.append(tag[0])
			tempValues.append(len(encodedTag))
			tempValues2.append(len(tag[2]))
			tempValues3.append(encodedTag)
			tempValues4 += tag[2]
			packString += '%is'%len(encodedTag)

		entries.append(struct.pack(packString+'%iH'%len(tempValues4), *packValues+tempValues+tempValues2+tempValues3+tempValues4))

	with open('structLibraryTest', 'wb') as db:
		db.write(struct.pack('L', len(entries)))
		db.write(struct.pack('%iL'%len(entries), *[len(x) for x in entries]))
		db.write(struct.pack('H', pympe.library.version))
		for entry in entries: db.write(entry)

	print time.time()-start
	print 'Finished Packing'

def struct_load_list():
	import struct, time

	startTime = time.time()

	pympe.library.blank_database()

	with open('structLibraryTest', 'rb') as db:
		entryCount = struct.unpack('L', db.read(8))[0]
		entryLengths = list(reversed(struct.unpack('%iL'%entryCount, db.read(entryCount*8))))
		databaseVersion = struct.unpack('H', db.read(2))[0]

#### self.version, self.trackId, self.currentTagIds
		entryLengths.pop()
		version, trackId = struct.unpack('HH', db.read(4))
		currentIds = struct.unpack('9H', db.read(18))

		pympe.library.version, pympe.library.trackId, pympe.library.currentTagIds = version, trackId, currentIds

#### self.tracks
		trackCount = struct.unpack('H', db.read(entryLengths.pop()))[0]

		print 'Maybe dont do two entries do only one joined with two .pack() entries.append(packString.pack()+values.pack()) the first 50 would be pack string etc..'
	# Track
		for x in range(trackCount):
			packString = struct.unpack('50s', db.read(entryLengths.pop()))[0]
			unpacked = struct.unpack(packString, db.read(entryLengths.pop()))

			track = {}
			trackId, track['tracknumber'], track['tracktotal'], track['discnumber'], track['disctotal'], track['episodenumber'], \
			track['episodetotal'], track['bitrate'], track['samplerate'], track['length'], track['rating'], track['playcount'], \
			track['skipcount'], track['equalizerPreset'], track['stopTime'], track['startTime'], track['playbackPosition'], \
			track['filesize'], track['timeplayed'], track['timemodified'], track['timeadded'], track['compilation'], \
			track['hasCustomRating'], track['usePlaybackPosition'], track['useStartTime'], track['useStopTime'], track['skipOnShuffle'], \
			track['useEqualizerPreset'], track['hash'], track['musicbrainzid'], track['uri'], track['mimetype'], \
			track['lyrics'] = unpacked[:33]

			track['hash'] = track['hash'].decode('UTF-8')
			track['musicbrainzid'] = track['musicbrainzid'].decode('UTF-8')
			track['uri'] = track['uri'].decode('UTF-8')
			track['mimetype'] = track['mimetype'].decode('UTF-8')
			track['lyrics'] = track['lyrics'].decode('UTF-8')

			lengths = unpacked[33:42]
			unpackedIndex = 42

			for index, key in enumerate(('artist', 'album', 'year', 'genre', 'albumartist', 'composer', 'tags', 'series', 'season')):
				if lengths[index]:
					track[key] = list(unpacked[unpackedIndex:unpackedIndex+lengths[index]])
					unpackedIndex += lengths[index]
				else:
					track[key] = []

			count = unpacked[unpackedIndex]
			end = unpackedIndex+1+count
			track['title'] = [value.decode('UTF-8') for value in unpacked[unpackedIndex+1:end]]
			count = unpacked[end]
			track['comment'] = [value.decode('UTF-8') for value in unpacked[end+1:-1]]

			pympe.library.tracks[trackId] = track
			pympe.library.trackDict[track['uri']] = [unpacked[-1], trackId]

#### self.tagIds
		#{tagName:{tagId:[tagId, tagName, [trackId, trackId, trackId, etc..]]}}
		for tagName in ("artist", "album", "year", "genre", "albumartist", "composer", "tags", "series", 'season'):
			packed = db.read(entryLengths.pop())
			count = struct.unpack('H', packed[:2])[0]
			if not count: continue
			integers = struct.unpack('%iH'%count*3, packed[2:2+count*6])#packString = 'H%iH'%(packValues[0]*3) # tagIds, tagNameLength, tagTrackIdsLength
			tagIds, tagNameLengths, tagTrackIdLengths = integers[:count], integers[count:count*2], integers[count*2:]
			packedIndex = 2+count*6

			split = struct.unpack('s'.join(str(x) for x in tagNameLengths)+'s%iH'%sum(tagTrackIdLengths), packed[packedIndex:])
			tagNames, trackIds = [value.decode('UTF-8') for value in split[:count]], split[count:]

			currentIndex = 0

			for index in range(count):
				pympe.library.tagNames[tagName][tagNames[index]] = tagIds[index]
				pympe.library.tagIds[tagName][tagIds[index]] = [tagIds[index], tagNames[index], list(trackIds[currentIndex:currentIndex+tagTrackIdLengths[index]])] # END RESULT
				currentIndex += tagTrackIdLengths[index]

		print time.time()-startTime



