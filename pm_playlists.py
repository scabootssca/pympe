import pympe, random, os, re, hashlib, pm_helpers
import cPickle as pickle, time, struct

class Playlist:
	def __init__(self, name, uid):
		self.name = name
		self.uid = uid

		self.clear(False)

		self.userModifiable = True
		self.allowDragDrop = True
		self.multiSelect = True
		self.smart = False
		self.smartSeedData = []

		# Custom Right Click Menus
		# Weird spot I know..
		# = [('Title', 1), None, ('Title 2', 2), ('Title 3', 4)]
		# None == Seperator
		self.singleItemMenu = []
		self.multiItemMenu  = []

		self.shuffle = pympe.config.get("player", "shuffle")
		self.repeat = pympe.config.get("player", "loop")

		pympe.events.player_set_shuffle.subscribe(self.mpevent_player_set_shuffle)
		pympe.events.player_set_repeat.subscribe(self.mpevent_player_set_repeat)
		pympe.events.player_current_updated.subscribe(self.mpevent_player_current_updated)
		pympe.events.library_entries_updated.subscribe(self.mpevent_library_entries_updated)

		pympe.events.playlist_update_entries.subscribe(self.mpevent_playlist_update_entries)

	def handle_custom_menu(self, eventId, selected): return

	def clear(self, sendEvent=True):
		self.index = -1
		self.nextIndex = False
		self.changed = False
		self.nextUid = 0
		self.uris = []
		self.uids = []
		self.titles = []
		self.libraryIds = []
		self.libraries = []

		self.shuffleOrder = []
		self.previouslyPlayed = []

		# Maybe not needed ?
		self.selected = []
		if sendEvent: pympe.events.playlist_replaced(self.uid)

	def replace(self, libraries, uris, titles=[], libraryIds=[]):
		self.index = 0
		self.uids = range(len(uris))
		self.nextUid = self.uids[-1]+1
		self.uris = list(uris)
		if titles: self.titles = list(titles)
		else: self.titles = list(uris[:])

		if libraries == None or libraries == -1:
			self.libraries = list([-1]*len(uris))
		elif type(libraries) == int:
			self.libraries = list([libraries]*len(uris))
		else:
			self.libraries = list(libraries)

		if libraryIds:
			self.libraryIds = list(libraryIds)
		else:
			self.libraryIds = [-1]*len(uris)

		self.previouslyPlayed = []
		self.shuffleOrder = range(len(self.uids))
		random.shuffle(self.shuffleOrder)

		pympe.events.playlist_replaced(self.uid)

	def enqueue(self, libraries, uris, titles=[], libraryIds=[], index=None):
		if titles == []: titles = uris[:]
		if libraryIds == []: libraryIds = ([-1]*len(uris))
		if libraries == None or libraries == -1:
			libraries = ([-1]*len(uris))
		elif type(libraries) == int:
			libraries = ([libraries]*len(uris))

		libraries, uris, titles, libraryIds = list(libraries), list(uris), list(titles), list(libraryIds)

		# If the index isn't at the end then store the shuffle and previous for after the insert position
		if index == None: index = len(self.uris)
		else:
			# If we are inserting back the shuffle indexes up if they are after the insert position
			self.shuffleOrder = sorted(self.shuffleOrder)
			for i, x in enumerate(self.shuffleOrder):
				if x >= index:
					length = len(self.uris)
					self.shuffleOrder = self.shuffleOrder[:i]+[x+length for x in self.shuffleOrder[i:]]
					break

			# Also for previously played
			self.previouslyPlayed = sorted(self.previouslyPlayed)
			for i, x in enumerate(self.previouslyPlayed):
				if x >= index:
					length = len(self.uris)
					self.previouslyPlayed = self.previouslyPlayed[:i]+[x+length for x in self.previouslyPlayed[i:]]
					break

		self.uris = self.uris[:index]+uris+self.uris[index:]
		self.titles = self.titles[:index]+titles+self.titles[index:]
		self.libraryIds = self.libraryIds[:index]+libraryIds+self.libraryIds[index:]
		self.libraries = self.libraries[:index]+libraries+self.libraries[index:]
		self.uids = self.uids[:index]+range(self.nextUid, self.nextUid+len(uris))+self.uids[index:]
		self.nextUid += len(self.uris)

		# Add the new uids and reshuffle
		self.shuffleOrder += range(len(self.uids), len(self.uids)+len(uris))
		random.shuffle(self.shuffleOrder)

		# If it is the main playlist set this as active
		if self.name is None: pympe.events.playlist_selected(self.uid)
		pympe.events.playlist_entries_added(self.uid, uris, titles, libraryIds, libraries)

	def dequeue(self, uids):
		lessThanIndexCount = 0
		removedIndexes = []
		removedIndex = False

		for index in reversed([self.uids.index(uid) for uid in uids]):
			try: self.previouslyPlayed.remove(index)
			except ValueError: pass

			try: self.shuffleOrder.remove(index)
			except ValueError: pass

			del self.uris[index]
			del self.titles[index]
			del self.libraryIds[index]
			del self.libraries[index]
			del self.uids[index]
			removedIndexes.append(index)

			if index == self.index: removedIndex = True
			if lessThanIndexCount < self.index: lessThanIndexCount += 1

		# If there's nothing left then just run the clear
		if not self.uids:
			return self.clear()

		# The remove event
		pympe.events.playlist_entries_removed(self.uid, uids, removedIndexes)

		# The playlist ui self.currentEntryIndex needs to be updated because we changed the index so it updates the wrong one
		self.index -= lessThanIndexCount
		if removedIndex: self.index = -1

		# Make sure we haven't removed them all
		pympe.events.playlist_entries_updated(self.uid, [self.uids[self.index]])

	def get_item_info(self, uid=None, uri=None, index=None):
		if uid is not None:
			try: index = self.uids.index(uid)
			except: return None

		if uri is not None:
			try: index = self.uris.index(uid)
			except: return None

		if index is not None:
			return (self.uris[index], self.titles[index], (None if self.libraryIds[index] == -1 else self.libraryIds[index]), pympe.libraries[self.libraries[index]])

	def set_item_info(self, uid=None, uri=None, index=None, info={}):
		if uid is not None:
			try: index = self.uids.index(uid)
			except: return False

		if uri is not None:
			try: index = self.uris.index(uid)
			except: return False

		if index is not None:
			try:
				for k, v in info.iteritems():
					if k == 'uri': self.uris[index] = v
					elif k == 'title': self.titles[index] = v
					elif k == 'libraryId': self.libraryIds[index] = -1 if v == None else v
					elif k == 'library': self.libraries[index] = v.uid

				pympe.events.playlist_entries_updated(self.uid, [self.uids[index]])
				return True
			except: return False


	def get_current(self, key=None):
		# This is only called when a song is played
		if not self.uids: return False
		if key == 'uri': return self.uris[self.index]
		elif key == 'title': return self.titles[self.index]
		elif key == 'libraryId': return (None if self.libraryIds[self.index] == -1 else self.libraryIds[self.index])
		elif key == 'library': return pympe.libraries[self.libraries[self.index]]
		elif key == None: return (self.uris[self.index], self.titles[self.index], (None if self.libraryIds[self.index] == -1 else self.libraryIds[self.index]), None if self.libraries[self.index] == -1 else pympe.libraries[self.libraries[self.index]])
		# Uri, Title, LibraryId, Library

	def get_item(self, index, key=None):
		if index >= len(self.uids): return None

		if key == 'uri': return self.uris[index]
		elif key == 'title': return self.titles[index]
		elif key == 'libraryId': return (None if self.libraryIds[index] == -1 else self.libraryIds[index])
		elif key == 'library': return pympe.libraries[libraries[self.index]]
		elif key == None: return (self.uris[index], self.titles[index], (None if self.libraryIds[index] == -1 else self.libraryIds[index]), (None if self.libraries[index] == -1 else pympe.libraries[self.libraries[index]]))

	def get_previous(self):
		i = self.previous()
		if not i is None: self.next()
		return i

	def get_next(self, forcedNext=False):
		'''
		0, 1, 2 == No Repeat, Repeat One, Repeat All


		'''
		# If we already have one then return it
		if self.nextIndex != False:
			return self.nextIndex

		# If we are on repeat one return what we're on
		if self.repeat == 1:
			self.nextIndex = self.index
			return self.nextIndex

		# On direct user next we always go to repeat
		# Else we don't

		if self.shuffle:
			# If we're out of tracks then reshuffle if repeat else return done
			if not self.shuffleOrder:
				if self.repeat == 0 and not forcedNext:
					self.nextIndex = None
					return self.nextIndex

				self.generate_shuffle()

				if not self.shuffleOrder:
					self.nextIndex = None
					return self.nextIndex

			self.nextIndex = self.shuffleOrder.pop()
		else:
			if forcedNext or self.repeat == 2: # Force next or Repeat all
				self.nextIndex = self.index+1
				if self.nextIndex == len(self.uids):
					self.nextIndex = 0

			elif self.repeat == 0: # No repeat
				if self.index != len(self.uids)-1:
					self.nextIndex = self.index+1
				else:
					self.nextIndex = None
					return self.nextIndex

		# If it's a new index then go ahead, if it's the same then stop unless we're at repeat one
		#if self.index != self.nextIndex:
		self.previouslyPlayed.append(self.index)
		#elif self.index == self.nextIndex and self.repeat != 1: self.nextIndex = None

		return self.nextIndex

	def next(self):
		self.get_next(forcedNext=True)

		#while self.shuffle:
			## If we skip this on shuffle
			#if self.libraryIds[self.index] != None and self.libraries[self.index].get_single_track_single_value(self.libraryIds[self.index], 'skipOnShuffle'):
				## Then find the next index and try again till we have one or we run out
				#self.get_next()
				##doesn't break properly
				#if self.nextIndex == self.index:
					#self.nextIndex = False
					#return None

				#continue

			#break

		if self.nextIndex == None:
			self.nextIndex = False
			return None

		self.index = self.nextIndex
		self.nextIndex = False
		return self.index

	def previous(self):
		if self.previouslyPlayed:
			self.index = self.previouslyPlayed.pop()

		return self.index

	def get_length(self):
		return len(self.uids)

	def get_index(self, uid=None):
		if uid is not None: return self.uids.index(uid)
		return self.index

	def set_index(self, index):
		self.previouslyPlayed.append(self.index)
		self.index = index

	def generate_shuffle(self):
		self.shuffleOrder = range(len(self.uids))

		# Remove all the ones we skip on shuffle
		for x in reversed(self.shuffleOrder):
			# The != None is a temporary (probably permanant) solution to why None gets in the libraries
			if self.libraries[x] != -1 and self.libraries[x] != None and pympe.libraries[self.libraries[x]].get_single_track_single_value(self.libraryIds[x], 'skipOnShuffle'):
				self.shuffleOrder.pop(x)

		if self.shuffleOrder:
			try: self.shuffleOrder.remove(self.index)
			except ValueError: pass

		random.shuffle(self.shuffleOrder)

	def mpevent_library_entries_updated(self, event, libraryUid, changedUids, changedKeys):
		# If it has uri or title updated and the library is in pympe.libraries then continue
		if not 'uri' in changedKeys and not 'title' in changedKeys: return
		updatedEntries = []

		# Go through all the updated entries
		for entryUid, changed in zip(changedUids, changedKeys):
			# Through all the occurances of that entryId
			for x in range(self.libraryIds.count(entryUid)):
				# Find the trackid in ours and get the library for that trackid
				trackIndex = self.libraryIds.index(entryUid)
				library = self.libraries[trackIndex]

				# If it's the correct library update the values and send item updated event
				if library == libraryUid:
					if 'title' in changed or 'uri' in changed:
						self.uris[trackIndex], self.titles[trackIndex] = pympe.libraries[libraryUid].get_single_track_multi_values(entryUid, ['uri', 'title'])
						updatedEntries.append(self.uids[trackIndex])

		if updatedEntries:
			pympe.events.playlist_entries_updated(self.uid, updatedEntries)

	def mpevent_playlist_update_entries(self, event, playlistUid, entryUids, titles=None, uris=None, libraryIds=None, libraries=None):
		if playlistUid != self.uid: return

		for index, entryUid in enumerate(entryUids):
			entryIndex = self.uids.index(entryUid)

			if titles: self.titles[entryIndex] = titles[index]
			if uris: self.uris[entryIndex] = uris[index]
			if libraryIds: self.libraryIds[entryIndex] = libraryIds[index]
			if libraries: self.libraries[entryIndex] = libraries[index]

		pympe.events.playlist_entries_updated(self.uid, entryUids)

	def mpevent_player_set_shuffle(self, event, shuffle):
		if not self.shuffle and shuffle: # Changing to shuffle
			self.generate_shuffle()

		self.shuffle = shuffle

		if self == pympe.playlists.main:
			pympe.events.player_shuffle_changed(shuffle)

	def mpevent_player_set_repeat(self, event, repeat):
		self.repeat = repeat

		if self == pympe.playlists.main:
			pympe.events.player_repeat_changed(repeat)

	def mpevent_player_current_updated(self, event, tags):
		if pympe.player.currentPlaylist != self.uid: return

		try: entryIndex = self.uris.index(pympe.player.currentInfo['uri'])
		except ValueError: return

		if 'title' in tags: self.titles[entryIndex] = pm_helpers.make_display_tag(pympe.player.currentInfo['title'])
		pympe.events.playlist_entries_updated(self.uid, [self.uids[entryIndex]])

	def update(self, smartSeedData):
		self.smartSeedData = smartSeedData
		self.smart = True

	def refresh(self):
		''' Smart playlist only works on pympe internal library for now '''
		self.index = 0
		plugin = pympe.plugins.get_plugin(1323703595)
		if not plugin: return
		trackIds = plugin.generate_playlist(self.smartSeedData)
		if not trackIds: return self.clear()

		self.replace(pympe.library, pympe.library.get_multi_track_single_value(trackIds, 'uri'),
					pympe.library.get_multi_track_single_value(trackIds, 'title'),
					trackIds)

	def copy_playlist(self, playlist):
		self.index = playlist.index
		self.nextIndex = playlist.nextIndex
		self.changed = playlist.changed
		self.nextUid = playlist.nextUid
		self.uris = playlist.uris
		self.uids = playlist.uids
		self.titles = playlist.titles
		self.libraryIds = playlist.libraryIds
		self.libraries = playlist.libraries

		self.shuffleOrder = playlist.shuffleOrder
		self.previouslyPlayed = playlist.previouslyPlayed

		self.selected = playlist.selected

class PlaylistManager:
	def __init__(self):
		self.nextUid = 0
		self.playlists = []

		self.current = None
		self.main = None

		pympe.events.playlist_create.subscribe(self.mpevent_playlist_create)
		pympe.events.playlist_delete.subscribe(self.mpevent_playlist_delete)
		pympe.events.playlist_select.subscribe(self.mpevent_playlist_select)
		pympe.events.playlist_rename.subscribe(self.mpevent_playlist_rename)

	def get_playlist(self, uid):
		if uid == -1: return self.main
		playlist = [playlist for playlist in self.playlists if playlist.uid == uid]
		if playlist: return playlist[0]

	def get_playlists(self, smart=None, main=False):
		if smart is None: return ([self.main] if main else [])+self.playlists
		elif smart is True: return [p for p in self.playlists if p.smart]
		elif smart is False: return [p for p in self.playlists if not p.smart]

	def mpevent_playlist_create(self, event, playlistName): self.create_playlist(playlistName)
	def create_playlist(self, name, playlistClass=Playlist):
		playlist = playlistClass(name, self.nextUid)
		self.playlists.append(playlist)
		pympe.events.playlist_created(playlist.uid, name)
		self.nextUid += 1
		return playlist

	def mpevent_playlist_delete(self, event, playlistUid): self.delete_playlist(playlistUid)
	def delete_playlist(self, playlistUid):
		for playlist in self.playlists:
			if playlist.uid == playlistUid:
				self.playlists.remove(playlist)
				self.select_playlist()
				pympe.events.playlist_deleted(playlist.uid)

	def mpevent_playlist_select(self, event, playlistUid=None): self.select_playlist(playlistUid)
	def select_playlist(self, playlistUid=-1):
		if playlistUid == -1:
			self.current = self.main
		else:
			playlists = [p for p in self.playlists if p.uid == playlistUid]
			if not playlists: return False
			self.current = playlists[0]
		pympe.events.playlist_selected(self.current.uid)

	def mpevent_playlist_rename(self, event, playlistUid, playlistName): self.rename_playlist(playlistUid, playlistName)
	def rename_playlist(self, playlistUid, playlistName):
		playlist = [p for p in self.playlists if p.uid == playlistUid]
		if playlist is []: return False
		playlist[0].name = playlistName
		if playlist[0].smart: playlist[0].smartSeedData[0] = playlistName
		pympe.events.playlist_renamed(playlistUid, playlistName)

	def blank_playlists(self, selectPlaylist=True):
		self.main = Playlist('[Main Playlist]', -1)
		if selectPlaylist: self.select_playlist()

	def module_start(self):
		filename = 'playlists.pmp'

		if not os.path.exists(pympe.dataDirectory+os.sep+filename):
			self.blank_playlists()
			return

		def load_playlists(filename):
			# If we elected not to load or save playlists
			if pympe.arguments['no_playlist']:
				self.blank_playlists()
				return

			self.blank_playlists(selectPlaylist=False)

			with open(filename, "rb") as f:
				indexCount = struct.unpack('=H', f.read(2))[0]
				entryLengths = list(reversed(struct.unpack('=%iL'%indexCount, f.read(indexCount*4))))

				fileVersion = struct.unpack('=H', f.read(entryLengths.pop()))[0]
				self.nextUid, currentUid, self.main.index, self.main.nextUid, mainLength = struct.unpack('=hhhhh', f.read(entryLengths.pop()))
				self.main.uids = list(struct.unpack('=%iH'%mainLength, f.read(entryLengths.pop())))
				self.main.libraryIds = list(struct.unpack('=%ih'%mainLength, f.read(entryLengths.pop())))
				self.main.libraries = list(struct.unpack('=%ih'%mainLength, f.read(entryLengths.pop())))

				lengths = struct.unpack('=%iH'%mainLength, f.read(entryLengths.pop()))
				self.main.uris = [f.read(x).decode('UTF-8') for x in lengths]
				entryLengths.pop()

				lengths = struct.unpack('=%iH'%mainLength, f.read(entryLengths.pop()))
				self.main.titles = [f.read(x).decode('UTF-8') for x in lengths]
				entryLengths.pop()


				numPlaylists = struct.unpack('=H', f.read(entryLengths.pop()))[0]


				for x in range(numPlaylists):
					playlist = Playlist(f.read(entryLengths.pop()).decode('UTF-8'), 0)
					playlist.uid, playlist.nextUid, playlist.index, playlist.smart, playlistLength = struct.unpack('=HHH?H', f.read(entryLengths.pop()))
					playlist.uids = list(struct.unpack('=%iH'%playlistLength, f.read(entryLengths.pop())))
					playlist.libraryIds = list(struct.unpack('=%ih'%playlistLength, f.read(entryLengths.pop())))
					playlist.libraries = list(struct.unpack('=%ih'%playlistLength, f.read(entryLengths.pop())))

					lengths = struct.unpack('=%iH'%playlistLength, f.read(entryLengths.pop()))
					playlist.uris = [f.read(x).decode('UTF-8') for x in lengths]
					entryLengths.pop()

					lengths = struct.unpack('=%iH'%playlistLength, f.read(entryLengths.pop()))
					playlist.titles = [f.read(x).decode('UTF-8') for x in lengths]
					entryLengths.pop()

					if playlist.smart:
						playlist.smartSeedData = pickle.loads(f.read(entryLengths.pop()))

					self.playlists.append(playlist)
					pympe.events.playlist_created(playlist.uid, playlist.name)

			self.select_playlist(currentUid)

		try: load_playlists(pympe.dataDirectory+os.sep+filename)
		except ValueError:
			try:
				pympe.error('Could not load playlist file trying backup.')
				load_playlists(pympe.dataDirectory+os.sep+filename+".bkup")
			except:
				pympe.error('Could not load playlist file or backup, starting fresh.')
				self.blank_playlists()
				return

	def module_stop(self):
		# If we elected not to load or save playlists
		if pympe.arguments['no_playlist']: return

		filename = pympe.dataDirectory+os.sep+"playlists.pmp"
		entries = [struct.pack('=H', 1)]

		entries.append(struct.pack('=hhhhh', self.nextUid, self.current.uid, self.main.index, self.main.nextUid, len(self.main.uids)))
		entries.append(struct.pack('=%iH'%len(self.main.uids), *self.main.uids))
		entries.append(struct.pack('=%ih'%len(self.main.libraryIds), *self.main.libraryIds))
		entries.append(struct.pack('=%ih'%len(self.main.libraries), *self.main.libraries))

		# List of lengths, then the joined
		encoded = [x.encode('UTF-8') for x in self.main.uris]
		entries.append(struct.pack('=%iH'%len(encoded), *(len(x) for x in encoded)))
		entries.append(''.join(encoded))

		encoded = [x.encode('UTF-8') for x in self.main.titles]
		entries.append(struct.pack('=%iH'%len(encoded), *(len(x) for x in encoded)))
		entries.append(''.join(encoded))

		entries.append(struct.pack('=H', len(self.playlists)))

		for playlist in self.playlists:
			entries.append(playlist.name.encode('UTF-8'))
			entries.append(struct.pack('=HHH?H', playlist.uid, playlist.nextUid, playlist.index, playlist.smart, len(playlist.uids)))
			entries.append(struct.pack('=%iH'%len(playlist.uids), *playlist.uids))
			entries.append(struct.pack('=%ih'%len(playlist.libraryIds), *playlist.libraryIds))
			entries.append(struct.pack('=%ih'%len(playlist.libraries), *playlist.libraries))

			encoded = [x.encode('UTF-8') for x in playlist.uris]
			entries.append(struct.pack('=%iH'%len(encoded), *(len(x) for x in encoded)))
			entries.append(''.join(encoded))

			encoded = [x.encode('UTF-8') for x in playlist.titles]
			entries.append(struct.pack('=%iH'%len(encoded), *(len(x) for x in encoded)))
			entries.append(''.join(encoded))

			if playlist.smart:
				entries.append(pickle.dumps(playlist.smartSeedData))

		with open(filename+'.bkup', 'wb') as f:
			f.write(struct.pack('=H', len(entries)))
			f.write(struct.pack('=%iL'%len(entries), *[len(x) for x in entries]))
			for entry in entries: f.write(entry)

		if os.path.exists(filename): os.remove(filename)
		os.rename(filename+'.bkup', filename)

	def load_playlist_from_file(self, filename):
		if not os.path.exists(filename): return False

		playlist = Playlist('Playlist: #%i'%self.nextUid, self.nextUid)
		self.playlists.append(playlist)

		with open(filename, "rb") as playlistFile:
			for line in playlistFile.readlines():
				line = line.strip()

				# Get the playlist name
				match = re.match('\[(.+)\]', line)
				if match: playlist.name = match.group(1)

				# Get the playlist entries
				match = re.match('(File|Title)(\d+)\=(.+)', line)
				if match:
					key, number, value = match.groups()

					#if not number in info: info[number] = ['', '', 0]

					if key == "File":
						playlist.uris.append(value)

					elif key == "Title":
						playlist.titles.append(value)

		playlist.uids = range(len(playlist.uris))
		playlist.nextUid = playlist.uids[-1]+1

		trackInfo = []
		for uri in playlist.uris:
			trackId = None

			for library in sorted(pympe.libraries.values(), key=lambda x: x.uid):
				trackId = library.get_trackid_from_uri(uri)
				if trackId != None:
					trackInfo.append((library.uid, trackId))
					break

			if trackId == None:
				trackInfo.append((-1, -1))

		playlist.libraries, playlist.libraryIds = zip(*trackInfo)

		playlist.previouslyPlayed = []
		playlist.shuffleOrder = playlist.uids[:]
		random.shuffle(playlist.shuffleOrder)

		pympe.events.playlist_created(playlist.uid, playlist.name)
		self.nextUid += 1
		return playlist

	def save_playlist_to_file(self, filename, playlistUid=-1, playlistName=None):
		if playlistUid is -1:
			playlist = self.main
		else:
			playlist = [p for p in self.playlists if p.uid == playlistUid]
			if not playlist: return False
			playlist = playlist[0]

		#try:
		if 1:
			with open(filename, "w") as pls:
				pls.write("[%s]\n"%(playlist.name if playlistName is None else playlistName))
				pls.write("NumberOfEntries=%i\n"%len(playlist.uids))

				for index in range(len(playlist.uids)):
					pls.write("File%i=%s\nTitle%i=%s\nLength%i=-1\n"%(index+1,playlist.uris[index],index+1,playlist.titles[index],index+1))

				pls.write("Version=2\n\n")
		#except: return False
		return True
