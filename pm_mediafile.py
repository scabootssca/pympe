


#import re, os
#from mutagen.mp3 import MP3
	#def set(self, tag, value, add=False):
		#info = None
		#if not tag in self.tags: return None
		#elif add:
			#self.file[self.tags[tag]].append(value)
		#else:
			#self.file[self.tags[tag]].text = [unicode(value)]

	#def save(self, cropV1Tags=False):
		#self.file.tags.save(v1=2 if not cropV1Tags else 0)

from mutagen.id3 import ID3
from mutagen.mp3 import MP3
from mutagen.mp4 import MP4
from mutagen.flac import FLAC
import mutagen.id3 as id3
import os, pm_network, urllib
import pympe
from mimetypes import types_map as mimeTypes
from time import time as current_time

supportedFormats = ["mp3", 'mp4', 'm4a', 'm4b', 'flac']
mimeTypes['.m4a'] = 'audio/mp4'
mimeTypes['.m4b'] = 'audio/x-m4b'
#mimeTypes['.pls'] = 'audio/x-scpls'
mimeTypes['.flac'] = 'audio/flac'

class GenericFile:
	def __init__(self, base):
		self.base = base
		self.info = {}

	def read(self):
		if self.base["scheme"] != "file": return True

		# Set the title as the filename for now
		self.info["title"] = [self.base["filename"].rsplit(os.sep, 1)[-1]]

		return True

	def write(self, info, compatibility=False):
		pass

class FlacFile:
	def __init__(self, base):
		self.base = base
		self.info = {'artist':[''], 'album':[''], 'title':[self.base['uri']]}

	def read(self):
		if self.base["scheme"] != "file": return True

		try: audioObject = FLAC(self.base["filename"])
		except: return False

		friendlyNames = {
						'title':'title',
						'comment':'comment',
						'tracknumber':'tracknumber',
						'discnumber':'discnumber',
						'rating':'rating',
						'compilation':'compilation',
						'playcounter':'playcount',
						'unsyncedlyrics':'lyrics',
						'artist':'artist',
						'album':'album',
						'date':'year',
						'genre':'genre',
						'ensemble':'albumartist',
						'composer':'composer'
						}



		for key, value in audioObject.tags.iteritems():
			if key in friendlyNames: key = friendlyNames[key]
			if key in pympe.library.blankTrack and pympe.library.blankTrack[key][1] == list and type(value) != list:
				self.info[key] = [value]
			else:
				self.info[key] = value


		if audioObject.pictures:
			self.info['art'] = audioObject.pictures[0].data

		self.info['length'] = int(round(audioObject.info.length))
		self.info["bitrate"] = audioObject.info.bits_per_sample
		self.info["samplerate"] =audioObject.info.sample_rate

		return True

	def write(self, info, compatibility=False):
		return False
		#try: id3Tags = ID3(self.base["filename"])
		#except id3.error: id3Tags = ID3()

		#if 'tracknumber' in info or 'tracktotal' in info:
			#id3Tags.delall("TRCK")
			#tracknumber = info['tracknumber'] if 'tracknumber' in info else (self.info['tracknumber'] if 'tracknumber' in self.info else 0)
			#tracktotal = info['tracktotal'] if 'tracktotal' in info else (self.info['tracktotal'] if 'tracktotal' in self.info else 0)
			#id3Tags["TRCK"] = id3.TRCK(encoding=3, text=[unicode('%s/%s'%(tracknumber,tracktotal))])

		#if 'discnumber' in info or 'disctotal' in info:
			#id3Tags.delall("TPOS")
			#discnumber = info['discnumber'] if 'discnumber' in info else (self.info['discnumber'] if 'discnumber' in self.info else 0)
			#disctotal = info['disctotal'] if 'disctotal' in info else (self.info['disctotal'] if 'disctotal' in self.info else 0)
			#id3Tags["TPOS"] = id3.TPOS(encoding=3, text=[unicode('%s/%s'%(discnumber,disctotal))])
			##self.info["TPOS"] = id3.TPOS(encoding=3, text=[unicode('%s/%s'%((info['discnumber'] if 'discnumber' in info else 0),(info['disctotal'] if 'disctotal' in info else 0)))])

		#for key in info:
			#if key == "title":
				#if not info[key]:      id3Tags.delall("TIT2")
				#else:                  id3Tags["TIT2"] = id3.TIT2(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			#elif key == "artist":
				#if not info[key]:      id3Tags.delall("TPE1")
				#else:                  id3Tags["TPE1"] = id3.TPE1(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			#elif key == "albumartist":
				#if not info[key]:      id3Tags.delall("TPE2")
				#else:                  id3Tags["TPE2"] = id3.TPE2(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			#elif key == "composer":
				#if not info[key]:      id3Tags.delall("TCOM")
				#else:                  id3Tags["TCOM"] = id3.TCOM(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			#elif key == "album":
				#if not info[key]:      id3Tags.delall("TALB")
				#else:                  id3Tags["TALB"] = id3.TALB(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			#elif key == "year":        id3Tags["TDRC"] = id3.TDRC(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			#elif key == "genre":
				#if not info[key]:      id3Tags.delall("TCON")
				#else:                  id3Tags["TCON"] = id3.TCON(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			#elif key == "compilation": id3Tags["TCMP"] = id3.TCMP(encoding=3, text=["1" if info[key] else "0"])
			#elif key == "playcount":   id3Tags["PCNT"] = id3.PCNT(encoding=3, text=[unicode(info[key])])

			#elif key == "comment":
				#if not info[key]:      id3Tags.delall("COMM")
				#else:                  id3Tags["COMM"] = id3.COMM(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			#elif key == "art":
				##if info[key] == None:  id3Tags.delall("APIC")
				##else:                  id3Tags["APIC"] = id3.APIC(encoding=3, type=3, data=info[key])
				#id3Tags.delall("APIC")
				#if info[key] != None: id3Tags["APIC"] = id3.APIC(encoding=3, type=3, data=info[key])
			#elif key == "lyrics":
				#id3Tags.delall("USLT::'eng'")
				#id3Tags["USLT::'eng'"] = id3.USLT(encoding=3, lang='eng', desc='English Lyrics', text=info[key])

		#if compatibility:
			##self.info.update_to_v23()
			#id3Tags.save(self.base["filename"], v2=3)
		#else:
			#id3Tags.save(self.base["filename"])


class Mp4File:
	def __init__(self, base):
		self.base = base
		self.info = {}

	def read(self):
		if self.base["scheme"] != "file": return True

		# Set the title as the filename for now
		self.info["title"] = [self.base["filename"].rsplit(os.sep, 1)[-1]]

		try: tags = MP4(self.base["filename"])
		except: tags = MP4()

		friendlyNames = {"\xa9ART":"artist",
						 "\xa9alb":"album",
						 "\xa9nam":"title",
						 "aART":"albumartist",
						 "\xa9cmt":"comment",
						 "\xa9day":"year",
						 "\xa9gen":"genre"
						 }

		for key, value in tags.iteritems():
			if key in friendlyNames:
				self.info[friendlyNames[key]] = [unicode(x) for x in value]
			elif key == "cpil":
				self.info["compilation"] = bool(int(value))
			elif key == "trkn":
				(self.info["tracknumber"], self.info["tracktotal"]) = value[0]
			elif key == "disk":
				(self.info["discnumber"], self.info["disctotal"]) = value[0]
			elif key == "covr":
				self.info["art"] = value[0]

		# And other info
		self.info["length"]  = int(tags.info.length)
		self.info["bitrate"] = int(tags.info.bitrate)/1024
		self.info["samplerate"] = tags.info.sample_rate

		return True

	def write(self, info, compatibility=False):
		pass

#class PlsFile:
	#def __init__(self, base):
		#self.base = base
		#self.info = {}

	#def read(self):
		#if self.base["scheme"] != "file":
			#def callback(progress, downloaded, total):
				#pympe.ui.uiGaugeBuffer.SetValue(progress*100)

			#filename = pm_network.open_url(self.base["uri"], callback, read=False)
			#if not filename: return False
			#self.base["filename"] = filename

		#file = open(self.base["filename"])

		#for line in file.readlines():
			#line = line.lower().split("=", 1)
			#if len(line) > 1:
				#if line[0].startswith("title"):
					#self.info["title"] = line[1].strip()

				#elif line[0].startswith("file"):
					#uri = line[1].strip()

					#split = uri.split(":")
					#if len(split) > 1:
						#self.info["port"] = split.pop()
						#self.info["uri"] = ":".join(split)
					#else:
						#self.info["uri"] = line[1]

					#if not line[1].startswith("file://"):
						#self.info["local"] = False

		#file.close()

		#return self.info

	#def write(self, info): pass

class Mp3File:
	def __init__(self, base):
		self.base = base
		self.info = {'artist':[''], 'album':[''], 'title':[self.base['uri']]}

	def read(self):
		if self.base["scheme"] != "file": return True

		#try: tags = id3.ID3(self.base["filename"])
		#except id3.error: tags= id3.ID3()

		try: self.tags = MP3(self.base["filename"])
		except:
			pympe.error('Cannot Read File Tags', True)
			return False

		#self.info["version"] = tags.version
		self.info["version"] = [0, 0, 0]

		friendlyNames = {"TPE1":"artist",
						 "TALB":"album",
						 "TIT2":"title",
						 "TPE2":"albumartist",
						 "COMM":"comment",
						 "TDRC":"year",
						 "TCOM":"composer"
						 }

		for frame in self.tags.values():
			id = frame.FrameID

			if id == "APIC" and len(frame.data):
				# Multiple pictures simply have multiple APIC frames
				# Mutagen does not support multiple frames with the same name because it stores them in a dictionary and they overwrite eachother
				# We cannot have multiple album art unless we mod mutagen or make our own APIC reader
				if type(frame.data) == type(list):
					self.info["art"] = frame.data[0]
				else:
					self.info['art'] = frame.data

			if id == "TCON":
				self.info["genre"] = [unicode(x) for x in frame.genres]
			elif id == "TCMP":
				self.info["compilation"] = bool(int(unicode(frame.text[0])))
			elif id == "TRCK":
				split = unicode(frame.text[0]).split("/")
				try: self.info["tracknumber"] = int(split[0]) if split[0] else 0
				except: self.info["tracknumber"] = 0
				if len(split) > 1:
					try: self.info["tracktotal"] = int(split[-1]) if split[-1] else 0
					except: self.info["tracktotal"] = 0
			elif id == "TPOS":
				split = unicode(frame.text[0]).split("/")
				try: self.info["discnumber"] = int(split[0]) if split[0] else 0
				except: self.info["discnumber"] = 0
				if len(split) > 1:
					try: self.info["disctotal"] = int(split[-1]) if split[-1] else 0
					except: self.info["disctotal"] = 0
			#elif id == "TDRC":
			#	if unicode(frame.text[0]).strip():
			#		self.info["year"] = int(unicode(frame.text[0]))
			elif id == "PCNT":
				self.info["playcount"] = int(frame.count)
			elif id == "USLT":
				self.info["lyrics"] = frame.text
			elif id in friendlyNames:
				self.info[friendlyNames[id]] = [unicode(x) for x in frame.text]

		# And other info
		self.info["length"]  = int(self.tags.info.length)
		self.info["bitrate"] = int(self.tags.info.bitrate)/1024
		self.info["samplerate"] = self.tags.info.sample_rate

		return True

	def write(self, info, compatibility=False):
		try: id3Tags = ID3(self.base["filename"])
		except id3.error: id3Tags = ID3()

		if 'tracknumber' in info or 'tracktotal' in info:
			id3Tags.delall("TRCK")
			tracknumber = info['tracknumber'] if 'tracknumber' in info else (self.info['tracknumber'] if 'tracknumber' in self.info else 0)
			tracktotal = info['tracktotal'] if 'tracktotal' in info else (self.info['tracktotal'] if 'tracktotal' in self.info else 0)
			id3Tags["TRCK"] = id3.TRCK(encoding=3, text=[unicode('%s/%s'%(tracknumber,tracktotal))])

		if 'discnumber' in info or 'disctotal' in info:
			id3Tags.delall("TPOS")
			discnumber = info['discnumber'] if 'discnumber' in info else (self.info['discnumber'] if 'discnumber' in self.info else 0)
			disctotal = info['disctotal'] if 'disctotal' in info else (self.info['disctotal'] if 'disctotal' in self.info else 0)
			id3Tags["TPOS"] = id3.TPOS(encoding=3, text=[unicode('%s/%s'%(discnumber,disctotal))])
			#self.info["TPOS"] = id3.TPOS(encoding=3, text=[unicode('%s/%s'%((info['discnumber'] if 'discnumber' in info else 0),(info['disctotal'] if 'disctotal' in info else 0)))])


		#if "tracknumber" in info:
			#number = info["tracknumber"]
			#total = 0 if not "tracktotal" in info else info["tracktotal"]
			#self.info.delall("TRCK")
			#self.info["TRCK"] = id3.TRCK(encoding=3, text=[unicode("%s/%s"%(number, total))])
		#elif "tracktotal" in info:
			#number =  0 if not "tracknumber" in info else info["tracknumber"]
			#total = info["tracktotal"]
			#self.info.delall("TRCK")
			#self.info["TRCK"] = id3.TRCK(encoding=3, text=[unicode("%s/%s"%(number, total))])

		#if "discnumber" in info:
			#number = info["discnumber"]
			#total = 0 if not "disctotal" in info else info["disctotal"]
			#self.info.delall("TPOS")
			#self.info["TPOS"] = id3.TPOS(encoding=3, text=[unicode("%s/%s"%(number, total))])
		#elif "disctotal" in info:
			#number =  0 if not "discnumber" in info else info["discnumber"]
			#total = info["disctotal"]
			#self.info.delall("TPOS")
			#self.info["TPOS"] = id3.TPOS(encoding=3, text=[unicode("%s/%s"%(number, total))])

		for key in info:
			if key == "title":
				if not info[key]:      id3Tags.delall("TIT2")
				else:                  id3Tags["TIT2"] = id3.TIT2(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			elif key == "artist":
				if not info[key]:      id3Tags.delall("TPE1")
				else:                  id3Tags["TPE1"] = id3.TPE1(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			elif key == "albumartist":
				if not info[key]:      id3Tags.delall("TPE2")
				else:                  id3Tags["TPE2"] = id3.TPE2(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			elif key == "composer":
				if not info[key]:      id3Tags.delall("TCOM")
				else:                  id3Tags["TCOM"] = id3.TCOM(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			elif key == "album":
				if not info[key]:      id3Tags.delall("TALB")
				else:                  id3Tags["TALB"] = id3.TALB(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			elif key == "year":        id3Tags["TDRC"] = id3.TDRC(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			elif key == "genre":
				if not info[key]:      id3Tags.delall("TCON")
				else:                  id3Tags["TCON"] = id3.TCON(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			elif key == "compilation": id3Tags["TCMP"] = id3.TCMP(encoding=3, text=["1" if info[key] else "0"])
			elif key == "playcount":   id3Tags["PCNT"] = id3.PCNT(encoding=3, text=[unicode(info[key])])

			elif key == "comment":
				if not info[key]:      id3Tags.delall("COMM")
				else:                  id3Tags["COMM"] = id3.COMM(encoding=3, text=[unicode(x) for x in (info[key].split('\\\\') if type(info[key]) != list else info[key])])
			elif key == "art":
				#if info[key] == None:  id3Tags.delall("APIC")
				#else:                  id3Tags["APIC"] = id3.APIC(encoding=3, type=3, data=info[key])
				id3Tags.delall("APIC")
				if info[key] != None: id3Tags["APIC"] = id3.APIC(encoding=3, type=3, data=info[key])
			elif key == "lyrics":
				id3Tags.delall("USLT::'eng'")
				id3Tags["USLT::'eng'"] = id3.USLT(encoding=3, lang='eng', desc='English Lyrics', text=info[key])

		if compatibility:
			#self.info.update_to_v23()
			id3Tags.save(self.base["filename"], v2=3)
		else:
			id3Tags.save(self.base["filename"])

class MediaFile:
	def __init__(self, uri, read=False):
		split = uri.split("://", 1)

		self.file = None
		self.info = {'title':[],
					'artist':[],
					'album':[],
					'length':0,
					'trackId':str(int(current_time()*1000000))+str(len(uri)),
					'scheme':split[0],
					'filename':urllib.unquote(split[1]),
					'local':(True if split[0] == "file" else False),
					'uri':uri
					} # The basic required tags

		if read:
			if self.read() == False: return

	def open(self):
		try:
			if self.info["local"]:
				self.info["timemodified"] = os.path.getmtime(self.info["filename"])
				self.info["filesize"] = os.path.getsize(self.info["filename"])
		except OSError, e:
			pympe.error('Problem opening file: %s'%e, traceback=True)
			return False

		self.ext = self.info["filename"].rsplit(".", 1)[-1].lower()

		try: self.info['mimetype'] = mimeTypes['.'+self.ext]
		except KeyError: self.info['mimetype'] = 'text/plain'

		#if mediaFile["local"]:
			#if not os.path.exists(mediaFile["filename"]):

		# Try to open the file if it is supported
		if self.ext == "mp3":   self.file = Mp3File(self)
		elif self.ext == "flac":   self.file = FlacFile(self)
		elif self.ext in ["mp4", "m4a", "m4b"]: self.file = Mp4File(self)
		#elif self.ext == "pls": self.file = PlsFile(self)
		elif self.info['mimetype'].startswith('audio'): self.file = GenericFile(self)
		elif self.info['mimetype'].startswith('video'): self.file = GenericFile(self)
		else: return False

		return True

	def read(self):
		# If it's a stream then just return that it was readable
		if not self.info["scheme"] in ["cdda", "file"]:
			self.info["title"] = [self.info["filename"].split("/")[-1]]
			return True

		# See if it exists
		if not os.path.exists(self.info["filename"]): return None

		# Try and access the file
		if not self.open(): return False

		# Try to read from the file
		if not self.file.read(): return False

		# Return the info
		for key in self.file.info:
			self.info[key] = self.file.info[key]

		return self.info

	def write(self, tags=None, compatibility=False):
		if self.file == None:
			if not self.open():
				return

		self.file.write((self.info if tags == None else tags), compatibility)

	def __getitem__(self, key):
		if key in self.info:
			return self.info[key]
		#if key in self.multiTags:
		#	return [""]
		return None

	def __setitem__(self, key, value):
		#if not key in self.info:
		self.info[key] = value

	def display(self, key, singleValue=False):
		if key in self.info:
			if key in self.multiTags:
				if singleValue: return str(self.info[key][0])
				return "; ".join(self.info[key])
			return str(self.info[key])
		return ""

	def keys(self):
		return self.info.keys()

	def values(self):
		return self.info.values()

	def get_format_string(self, newFormat, tags=None):
		fileInfo = self.info

		for x in fileInfo:
			if x != "art":
				if x in self.multiTags: value = fileInfo[x][0]#"-".join(fileInfo[x])
				else: value = str(fileInfo[x])
				newFormat = newFormat.replace("%"+str(x)+"%", value)

		return newFormat+"."+str(self.ext)
