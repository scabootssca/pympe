import pympe, os
from hashlib import md5

class Cache:
	def __init__(self):
		self.items = {}

	def __getitem__(self, key):
		return self.get(key)

	def __setitem__(self, key, value):
		return self.set(key, value)

	def module_start(self):
		self.dataDirectory = pympe.cacheDirectory+'/data/'
		self.infoDirectory = pympe.cacheDirectory+'/info/'


		for directory in [self.dataDirectory, self.infoDirectory]:
			if not os.path.exists(directory):
				os.mkdir(directory)

	def module_stop(self): pass

	def filename(self, uri):
		''' Gets what the filename would be set to '''
		return self.dataDirectory+md5(uri).hexdigest()
		#if os.path.exists(filename): return filename
		#return None

	def write_info(self, uri, sendEvent=True):
		with open(self.infoDirectory+os.sep+md5(uri).hexdigest(), 'w') as f:
			f.write(uri)

		if sendEvent:
			pympe.events.cache_entry_added(uri)

	def get(self, uri):
		path = self.dataDirectory+md5(uri).hexdigest()
		try:
			with open(path, 'r') as f: return f.read()
		except:
			return None

	def set(self, uri, data, sendEvent=True):
		filename = md5(uri).hexdigest()

		newEntry = not self.has(uri)
		with open(self.dataDirectory+filename, 'w') as f: f.write(data)
		with open(self.infoDirectory+filename, 'w') as f: f.write(uri)

		if sendEvent:
			if newEntry: pympe.events.cache_entry_added(uri)
			else: pympe.events.cache_entry_updated(uri)

		return self.dataDirectory+filename

	def has(self, uri):
		path = self.dataDirectory+md5(uri).hexdigest()
		return path if os.path.exists(path) else None

	def list(self):
		listing = []

		for root, dirs, files in os.walk(self.infoDirectory):
			for z in files:
				try:
					with open(root+os.sep+z, 'r') as f:
						listing.append(f.read())
				except:
					continue

		return listing

	def delete(self, uri, sendEvent=True):
		filename = md5(uri).hexdigest()
		try:
			os.remove(self.dataDirectory+filename)
			os.remove(self.infoDirectory+filename)
			if sendEvent: pympe.events.cache_entry_removed(uri)
			return True
		except:
			return False

	def clear(self):
		[self.delete(uri) for uri in self.list()]
		pympe.events.cache_cleared()

