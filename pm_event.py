from pm_helpers import unthread
import pympe

class Event:
	def __init__(self, x=0):
		self.subscribers = []

	@unthread
	def __call__(self, *args, **kwargs):
		#import pympe
		#for x in dir(pympe.events):
			#if getattr(pympe.events, x) == self:
				#print 'Calling Event',x#,args,kwargs

		for subscriber in self.subscribers:
			try:
				subscriber(self, *args, **kwargs)
			except:
				module = __import__('pm_event')
				pympe.error('\033[31m[EVENT FAILED]:\n\033[34mEvent:\n	\033[39m{}\n\033[34mSubscriber:\n	\033[39m{}\n\033[34mArgs:\n	\033[39m{}\n\033[34mKwargs:\n	\033[39m{}\n'.format(\
					', '.join([x for x in dir(module) if getattr(module, x) == self]),
					repr(subscriber),
					'\n	'.join([str(x) for x in args]),
					'\n	'.join(['%s:%s'%(str(k),str(v)) for k, v in kwargs.iteritems()])), True, False)


	def subscribe(self, function):
		self.subscribers.append(function)

	def unsubscribe(self, function):
		try: self.subscribers.remove(function)
		except ValueError: return

	## OLD should be depreciated
	def send(self, *args, **kwargs):
		self(*args,**kwargs)


# Playlist Events
	#### Control Events
playlist_create             = Event()   # playlistName
playlist_delete             = Event()   # playlistUid
playlist_select             = Event()   # playlistUid
playlist_rename             = Event()   # playlistUid, playlistName

playlist_add_entries        = Event()   # playlistUid, titles=None, uris=None, libraryIds=None, libraries=None, index=None
playlist_update_entries     = Event()   # playlistUid, uids, titles=None, uris=None, libraryIds=None, libraries=None
playlist_remove_entries     = Event()   # playlistUid, uids

playlist_replace            = Event()   # playlistUid, titles=None, uris=None, libraryIds=None, libraries=None

	#### Notification Events
playlist_created            = Event()   # playlistUid, playlistName
playlist_deleted            = Event()   # playlistUid
playlist_selected           = Event()   # playlistUid
playlist_renamed            = Event()   # playlistUid, playlistName

playlist_entries_added      = Event()   # playlistUid, [uids]
playlist_entries_updated    = Event()   # playlistUid, [uids]
playlist_entries_removed    = Event()   # playlistUid, [uids], [oldIndexes]

playlist_replaced           = Event()   # playlistUid, uris, titles, libraryIds, libraries

# Player Engine Events
	#### Control Events
player_play                 = Event()    #
player_pause                = Event()    #
player_stop                 = Event()    #
player_next                 = Event()    #
player_previous             = Event()    #

player_set_volume           = Event()    # volume
player_set_shuffle          = Event()    # shuffleMode
player_set_repeat           = Event()    # repeatMode

	#### Notification Events
player_playback_started     = Event()    #
player_playback_paused      = Event()    #
player_playback_stopped     = Event()    #
player_playback_next        = Event()    #
player_playback_previous    = Event()    #

player_volume_changed       = Event()    # volume
player_shuffle_changed      = Event()    # shuffleMode
player_repeat_changed       = Event()    # repeat Mode

player_playback_resumed     = Event()    #
player_playback_buffering   = Event()    # bufferPercent

player_current_updated      = Event()    # tags={tagKey:tagValue}
player_current_changed      = Event()    # info, luibraryId, library
player_previous_changed     = Event()    # info, libraryId, library, playbackPosition, playTime

player_video_mode_changed   = Event()    # True|False

player_gst_error            = Event()    # error, debug

# Library Events
	#### Control Events
library_clear                = Event()   # libraryUid
library_search               = Event()   # libraryUid, searchTerms

library_add_entries          = Event()   # libraryUid, info
library_remove_entries       = Event()   # libraryUid, entryUids
library_update_entries       = Event()   # libraryUid, entryUids, changes

	#### Nofication Events
library_cleared              = Event()   # libraryUid
library_replaced             = Event()   # libraryUid

library_entries_added        = Event()   # libraryUid, entryUids
library_entries_updated      = Event()   # libraryUid, entryUids, keys, values
library_entries_removed      = Event()   # libraryUid, entryUids

library_key_added            = Event()   # libraryUid, key, value
library_keys_removed          = Event()   # libraryUid, key, value

library_keys_added           = Event()   # libraryUid, {key:[values], key:[values]}

# Device Events
	#### Control Events
device_sync              = Event()   # deviceUid              # Sent to start the sync process of a device

device_connect           = Event()   # deviceUid              # Sent to start connecting to a detected device
device_disconnect        = Event()   # deviceUid              # Sent to start disconnecting from a device
device_rename            = Event()   # deviceUid, deviceName  # Sent to rename device to deviceName

	#### Nofication Events
device_added             = Event()   # device                 # Sent when a new device is first detected
device_removed           = Event()   # device                 # Sent when a device was removed regardless of connection state
device_ready             = Event()   # device                 # Sent when a device is mounted or otherwise ready to connect to

device_connected         = Event()   # device                 # Sent when a device is connected and scanned/read etc...
device_disconnected      = Event()   # device                 # Sent when a device is disconnected but maybe not physically
device_renamed           = Event()   # device, deviceName     # Sent when a device has been renamed

# Cache Events
	#### Control Events
cache_clear              = Event()   #

cache_remove_entry       = Event()   # entryUid

	#### Nofication Events
cache_cleared            = Event()   #

cache_entry_added        = Event()   # entryUri
cache_entry_removed      = Event()   # entryUri
cache_entry_updated      = Event()   # entryUri

# Plugin Events
	#### Control Events
plugins_plugin_start     = Event()   # pluginUid
plugins_plugin_stop      = Event()   # pluginUid

	#### Nofication Events
plugins_plugin_started   = Event()   # pluginUid
plugins_plugin_stopped   = Event()   # pluginUid

plugins_plugin_added     = Event()   # pluginUid
plugins_plugin_removed   = Event()   # pluginUid

# Config Events
	#### Control Events
config_add_key           = Event()   # sectionName, keyName, value
config_remove_key        = Event()   # sectionName, keyName
config_change_key        = Event()   # sectionName, keyName, value

	#### Nofication Events
config_key_added         = Event()   # sectionName, keyName, value
config_key_removed       = Event()   # sectionName, keyName
config_key_changed       = Event()   # sectionName, keyName, value
config_changed           = Event()   # sectionName, info

# Interface Events
	#### Control Events
#ui_change_setting        = Event()   # settingName, settingValue
ui_change_view_mode      = Event()   # mode=(normal, fullscreen, theatre)

	#### Nofication Events
#ui_setting_changed       = Event()   # settingName, settingValue
ui_view_mode_changed     = Event()   # mode=(normal, fullscreen, theatre)
ui_iconized              = Event()

# Indicator Events
	#### Control Events

	#### Nofication Events
indicator_started      = Event(True) # indicatorId
indicator_updated      = Event(True) # indicatorId
indicator_stopped      = Event(True) # indicatorId
indicator_selected     = Event(True) # indicatorId

# Pympe Events
	#### Control Events
pympe_shutdown           = Event()   # Called to shutdown the player and all listeners
pympe_raise              = Event()   # Raises the player to the front

	#### Nofication Events
pympe_initilized         = Event()   # Called once the entirety of the player is started
pympe_output             = Event()   # message
