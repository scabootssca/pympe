import pympe, os, imp, sys, pm_config, traceback

class PluginManager:
	def __init__(self):
		self.plugins = {}

	def __repr__(self):
		return '\n'.join(('%s:%s'%(plugin['info']['uid'], plugin['path'].rsplit(os.sep, 1)[-1]) for plugin in self.plugins.itervalues()))

	def __getitem__(self, uid):
		return self.plugins.get(uid)

	def module_start(self):
		self.scan()

	def module_stop(self):
		# Unload all the plugins that are enabled
		for pluginUid in self.plugins.keys():
			if self.plugins[pluginUid]["running"]:
				self.unload_plugin(pluginUid, True)

	def scan(self):
		if pympe.arguments['no_plugins']: return
		existingPlugins = pympe.config.get("plugins", "list")
		pluginDirectory = pympe.cwd+pympe.config.get("plugins", "directory")

		# Walk through the directory and find whats there now
		newlyLoaded = []
		for root, dirs, files in os.walk(pluginDirectory):
			# Only use paths that have a plugin.ini file
			if "plugin.ini" in files:
				info = self.read_plugin(root)

				# If a plugin with the same uid already exists check to be sure they're different
				if info['info']['uid'] in self.plugins:
					# If it's different then unload the old and load the new
					if info['info'] != self.plugins[info['info']['uid']]['info']:
						previouslyRunning = self.plugins[info['info']['uid']]['running']
						self.unload_plugin(info['info']['uid'])
						self.load_plugin(info=info)
						pympe.debug('Reloaded: {} v{}'.format(info['info']['name'], info['info']['version']))
						if previouslyRunning:
							self.start_plugin(info['info']['uid'])
				# New plugins just load them
				else:
					plugin = self.load_plugin(info=info)
					pympe.debug('Loaded: {} v{}'.format(info['info']['name'], info['info']['version']))
					if info['info']['uid'] in existingPlugins and existingPlugins[info['info']['uid']]:
						self.start_plugin(info['info']['uid'])


				# Make a list of what's there for pruning
				newlyLoaded.append(info["info"]["uid"])

		# Prune any ones that were loaded but aren't anymore
		for loadedPlugin in self.plugins.keys():
			if not loadedPlugin in newlyLoaded:
				if self.plugins[loadedPlugin]["running"]:
					self.unload_plugin(loadedPlugin)
					pympe.debug('Unloaded: {} v{}'.format(self.plugins[loadedPlugin]['info']['name'], self.plugins[loadedPlugin]['info']['version']))

		# Write the settings file
		pluginList = {}
		for pluginUid in self.plugins:
			pluginList[pluginUid] = True if self.plugins[pluginUid]["running"] else False
		pympe.config.set("plugins", "list", pluginList)

	def read_plugin(self, path):
		cfg = pm_config.Config(path=path+"/plugin.ini")
		cfg.load()

		settings = cfg.get("plugin")[1]
		creator = cfg.get("creator")[1]
		variables = cfg.get("variables")[1]

		return {"running":False, "info":settings, "creator":creator, "variables":variables, "path":path, "plugin":None, "listeners":[]}

	def load_plugin(self, path=None, info=None):
		if info == None:
			if not path: return
			info = self.read_plugin(path)
		self.plugins[info['info']['uid']] = info
		pympe.events.plugins_plugin_added(info['info']['uid'])

	def unload_plugin(self, pluginUid, shutdown=False):
		if pluginUid in self.plugins:
			result = True if self.stop_plugin(pluginUid, False, False, shutdown) else False#True if self.stop_plugin(pluginUid, False, False, shutdown) else False
			del self.plugins[pluginUid]

			# Don't send the event on shutdown
			if not shutdown:
				pympe.events.plugins_plugin_removed(pluginUid)

			return result
		return False

	def reload_plugin(self, pluginUid):
		if pluginUid in self.plugins:
			path = self.plugins[pluginUid]["path"]
			running = self.plugins[pluginUid]["running"]
			pluginPath = self.plugins[pluginUid]['path']

			if running: self.unload_plugin(pluginUid)
			result = self.load_plugin(path)

			if running: self.start_plugin(pluginUid, False)
			return result
		return False

	def start_plugin(self, pluginUid, writeToSettings=True):
		if not pluginUid in self.plugins: return False
		plugin = self.plugins[pluginUid]
		if plugin["running"]: return False
		path = plugin["path"]

		# Check for prerequisites first
		#plugin_requirements = [pluginUid, pluginName, pluginVersion]
		missing = []
		if "plugin_requirements" in plugin["info"]:
			reqs = plugin["info"]["plugin_requirements"]
			for req in reqs:
				for x in self.plugins.values():
					if x["info"]["uid"] == req[0]:
						if x["info"]["version"] < req[2] or not x["running"]:
							missing.append(req)
		if missing:
			pympe.debug("Plugin '%s' requires the following plugin%s to be installed and running: %s"%(pluginUid, ("s" if len(missing) > 1 else ""), ", ".join(["%s v%s"%(x[1], x[2]) for x in missing])))
			return False

		try:
			name = plugin['path'].rsplit(os.sep, 1)[1]
			module = imp.load_module(name, *imp.find_module(name))#imp.load_source(path, path+os.sep+"__init__.py")
			if not module: return False

			# Create the initial variables if they aren't set
			for key, value in plugin["variables"].iteritems():
				pympe.config.set_first(plugin["path"], key, value)

			plugin["plugin"] = module.Plugin()
			module.cfg_key = plugin["path"]
			module.cfg_get = lambda value: pympe.config.get(plugin["path"], value)
			module.cfg_set = lambda key, value: pympe.config.set(plugin["path"], key, value)
			module._config = pympe.config[plugin["path"]]
			module.subscribe = lambda event, function: self.subscribe(plugin, event, function)
			module.unsubscribe = lambda event, function: self.unsubscribe(plugin, event, function)
			if hasattr(plugin['plugin'], 'plu_start'):
				plugin['plugin'].plu_start()
			else:
				plugin["plugin"].__plugin_start__()
			plugin["running"] = True

			if writeToSettings:
				pluginList = pympe.config.get("plugins", "list")
				pluginList[pluginUid] = True
				pympe.config.set("plugins", "list", pluginList)

			pympe.debug('Started: {} v{}'.format(plugin['info']['name'], plugin['info']['version']))
			pympe.events.plugins_plugin_started(pluginUid)
			return True
		except ImportError, e:
			pympe.error("Problem starting plugin: %s\n%s"%(plugin["info"]["name"], e), True)
		except:
			pympe.error("Problem starting plugin: %s"%plugin["info"]["name"], True)
			return False

	def stop_plugin(self, pluginUid, writeToSettings=True, cascade=True, shutdown=False):
		#import ipdb; ipdb.set_trace()
		if not pluginUid in self.plugins: return False
		plugin = self.plugins[pluginUid]
		if not plugin["running"]: return False

		# Disable the plugins listeners
		for listener in plugin["listeners"]:
			listener[0].unsubscribe(listener[1])

		try:
			if hasattr(plugin['plugin'], 'plu_stop'):
				plugin["plugin"].plu_stop(shutdown)
			else:
				plugin["plugin"].__plugin_stop__(shutdown)
			result = True
		except:
			pympe.error('Plugin \'%s\' Shut Down Incorrectly'%self.plugins[pluginUid]['info']['name'], True)
			result = False

		plugin["running"] = False
		plugin["listeners"] = []

		# Disable any other plugin that requires this one
		if cascade:
			for plugin in self.plugins:
				if pluginUid in self.plugins[plugin]["info"]["plugin_requirements"]:
					self.stop_plugin(plugin)

		if writeToSettings:
			pluginList = pympe.config.get("plugins", "list")
			pluginList[pluginUid] = False
			pympe.config.set("plugins", "list", pluginList)

		# Only send the event on shutdown
		if not shutdown:
			pympe.events.plugins_plugin_stopped(pluginUid)
		return result

	def show_settings(self, pluginUid, parent=None):
		if not pluginUid in self.plugins: return False
		plugin = self.plugins[pluginUid]

		if not plugin["running"]: return False
		if hasattr(plugin['plugin'], 'plu_settings'):
				plugin['plugin'].plu_settings(parent)
		else:
			plugin["plugin"].__plugin_settings__(parent)
		return True

	def get_plugin(self, pluginUid, returnDict=False):
		if not pluginUid in self.plugins: return False
		plugin = self.plugins[pluginUid]

		if not plugin["running"]: return False
		if returnDict: return plugin
		else: return plugin["plugin"]

	def get_plugin_by_name(self, name):
		for plugin in self.plugins.values():
			if plugin['info']['name'] == name:
				return plugin['info']['uid']
		return False

	def subscribe(self, plugin, event, function):
		if not (event, function) in plugin["listeners"]:
			event.subscribe(function)
			plugin["listeners"].append((event, function))

	def unsubscribe(self, plugin, event, function):
		if (event, function) in plugin["listeners"]:
			event.unsubscribe(function)
			plugin["listeners"].remove((event, function))
