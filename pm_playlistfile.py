class FauxPlaylist:
	def export(playlist, filename):
		''' Writes playlist to filename '''

	def import(filename):
		''' Tries to return a tuple of playlist goodness '''
		return ('Playlist Title', 'TrackTitle1', 'Uri1', 'TrackTitle2', 'Uri2')


class PlsPlaylist(filename):
	wildcard = "PLS files (*.pls)|*.pls"
	extension = '.pls'

	def __init__(self, filename):
		self.filename = filename

	def load(self):
		if not os.path.exists(self.filename): return False

		playlist = Playlist('Playlist: #%i'%pympe.playlists.nextUid, pympe.playlists.nextUid)
		pympe.playlists.playlists.append(playlist)

		with open(pympe.playlists.filename, "rb") as playlistFile:
			for line in playlistFile.readlines():
				line = line.strip()

				# Get the playlist name
				match = re.match('\[(.+)\]', line)
				if match: playlist.name = match.group(1)

				# Get the playlist entries
				match = re.match('(File|Title)(\d+)\=(.+)', line)
				if match:
					key, number, value = match.groups()

					#if not number in info: info[number] = ['', '', 0]

					if key == "File":
						playlist.uris.append(value)

					elif key == "Title":
						playlist.titles.append(value)

		playlist.uids = range(len(playlist.uris))
		playlist.nextUid = playlist.uids[-1]+1

		trackInfo = []
		for uri in playlist.uris:
			trackId = None

			for library in sorted(pympe.libraries.values(), key=lambda x: x.uid):
				trackId = library.get_trackid_from_uri(uri)
				if trackId != None:
					trackInfo.append((library.uid, trackId))
					break

			if trackId == None:
				trackInfo.append((-1, -1))

		playlist.libraries, playlist.libraryIds = zip(*trackInfo)

		playlist.previouslyPlayed = []
		playlist.shuffleOrder = playlist.uids[:]
		random.shuffle(playlist.shuffleOrder)

		pympe.events.playlist_created(playlist.uid, playlist.name)
		pympe.playlists.nextUid += 1
		return playlist

	def save(self, playlistUid):
		if playlistUid is -1:
			playlist = pympe.playlists.main
		else:
			playlist = [p for p in pympe.playlists.playlists if p.uid == playlistUid]
			if not playlist: return False
			playlist = playlist[0]

		#try:
		if 1:
			with open(filename, "w") as pls:
				pls.write("[%s]\n"%(playlist.name if playlistName is None else playlistName))
				pls.write("NumberOfEntries=%i\n"%len(playlist.uids))

				for index in range(len(playlist.uids)):
					pls.write("File%i=%s\nTitle%i=%s\nLength%i=-1\n"%(index+1,playlist.uris[index],index+1,playlist.titles[index],index+1))

				pls.write("Version=2\n\n")
		#except: return False
		return True

class M3uPlaylist:
	wildcard = "M3U files (*.m3u)|*.m3u"
	extension = '.m3u'

	def __init__(self, filename):
		self.filename = filename

types = [M3uPlaylist, PlsPlaylist]

def match(filename):
	for t in types:
		if filename.lower().endswith(t.extension):
			return t
	return None


