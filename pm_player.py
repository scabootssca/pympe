import gst, pympe, pm_mediafile, pm_helpers, time, os
from urllib import quote as urllibQuote

class VideoBin(gst.Bin):
	def __init__(self, name):
		gst.Bin.__init__(self, name)

		self.blocked = False
		self.state = None

		self.setup_pipeline()

	def setup_pipeline(self):
		self.identity = gst.element_factory_make('identity')
		self.color = gst.element_factory_make('ffmpegcolorspace')
		self.sink = gst.element_factory_make('xvimagesink')


		self.add(self.identity, self.color, self.sink)
		gst.element_link_many(self.identity, self.color, self.sink)

		self.blockPad = self.identity.get_static_pad('src')
		self.add_pad(gst.GhostPad("sink", self.identity.get_static_pad("sink")))

	def block(self, runFunc=None, *args):
		self.state = self.get_state()[1]
		def now_blocked(e, blocked):
			self.blocked = True
			if runFunc is not None: pm_helpers.unthread_function(runFunc, *args)

		if pympe.player.playing: self.blockPad.set_blocked_async(True, now_blocked)
		else: now_blocked(None, True)

	def update_size(self, size):
		pass

	def set_window_handle(self, handle):
		self.sink.set_property("force-aspect-ratio", True)
		self.sink.set_xwindow_id(handle)

	def unblock(self):
		self.sink.set_state(self.state)
		if self.blocked: self.blockPad.set_blocked_async(False, lambda *args: False)
		self.blocked = False

class AudioBin(gst.Bin):
	def __init__(self, name=None):
		self.state = None
		self._src = None
		self._sink = None
		self.srcPad = None
		self.sinkPad = None
		self.blockPad = None
		self.blocked = False

		gst.Bin.__init__(self, name)

		self._setup()

	def _setup(self):
		# Get the elements
		self.setup()
		elements = list(self.elements)
		if len(elements) == 0: elements.append(gst.element_factory_make('identity'))

		# Add and link them
		self.add_many(*elements)
		if len(elements) > 1: gst.element_link_many(*elements)

		# Get the src and sink
		if not self.srcPad:
			self.srcPad = elements[-1].get_static_pad('src')

		if self.srcPad:
			self._src = gst.GhostPad('src', self.srcPad)
			self.add_pad(self._src)
			self.blockPad = self.srcPad

		if not self.sinkPad:
			self.sinkPad = elements[0].get_static_pad("sink")

		self._sink = gst.GhostPad('sink', self.sinkPad)
		self.add_pad(self._sink)

	def setup(self):
		print 'Need to override setup for custom audiobins'

	def block(self, runFunc=None, *args):
		self.state = self.get_state()[1]

		def now_blocked(e, blocked):
			self.blocked = True

			if runFunc is not None:
				runFunc(*args)
				self.unblock()

		if pympe.player.playing and not self.blocked and self.blockPad:
			self.blockPad.set_blocked_async(True, now_blocked)
		else:
			now_blocked(None, True)

	def unblock(self):
		self.set_state(self.state)
		if self.blocked and self.blockPad:
			self.blockPad.set_blocked_async(False, lambda *args: False)
		self.blocked = False

	#def set_state(self, state):
		#if state == gst.STATE_PLAYING and self.get_state() == gst.STATE_NULL:
			#self._setup()
		#gst.Bin.set_state(self, state)

class MainAudioBin(AudioBin):
	postProcessors = []

	def setup(self):
		self.identity = gst.element_factory_make('audioconvert')
		#self.tee = gst.element_factory_make('tee')
		#self.queue = gst.element_factory_make('queue')
		self.sink = gst.element_factory_make('autoaudiosink')

		#self.elements = [self.identity, self.tee, self.queue, self.sink]
		self.elements = [self.identity, self.sink]

		self.blockPad = self.identity.get_static_pad('src')

	def get_volume(self):
		return self.sink.get_volume()

	def set_volume(self, value):
		self.sink.set_volume(value)

	def add_element(self, element=None):
		def now_blocked():
			before = self.identity if not self.postProcessors else self.postProcessors[-1]

			before.unlink(self.sink)
			self.add(element)
			gst.element_link_many(before, element, self.sink)

			self.postProcessors.append(element)
			element.sync_state_with_parent()

		self.block(now_blocked)

	def remove_element(self, element=None):
		def now_blocked():
			if element in self.postProcessors:
				index = self.postProcessors.index(element)
				#@debug index

				if index == 0: before = self.identity
				else: before = self.postProcessors[index-1]
				if index == len(self.postProcessors)-1: after = self.sink
				else: after = self.postProcessors[index+1]

				before.unlink(element)
				element.unlink(after)
				before.link(after)

				element.set_state(gst.STATE_NULL)
				self.postProcessors.pop(index)
				self.remove(element)

		self.block(now_blocked)

class Player:
	def __init__(self):
		self.currentFile = None
		self.currentId = None
		self.currentLibrary = None
		self.currentPlaylist = None
		self.currentInfo = None

		self.enqueuedFile = None
		self.enqueuedId = None
		self.enqueuedLibrary = None
		self.enqueuedPlaylist = None
		self.enqueuedInfo = None

		self.settled = False
		self.resumedPosition = False
		self.resumePosition = None
		self.stopTime = False

		#self.startPoint = [False, 0]
		#self.stopPoint = [False, 0]

		self.bufferIndicator = None
		self.playing = False
		self.paused = False

		self.playingVideo = False

		self.length = 0

		self.playStartTime = 0
		self.playTime = 0

		self.eventStorageKey = 0

		self.lastPlayTime = 0

		pympe.events.player_play.subscribe(self.mpevent_player_controls)
		pympe.events.player_pause.subscribe(self.mpevent_player_controls)
		pympe.events.player_stop.subscribe(self.mpevent_player_controls)
		pympe.events.player_next.subscribe(self.mpevent_player_controls)
		pympe.events.player_previous.subscribe(self.mpevent_player_controls)
		pympe.events.player_set_volume.subscribe(self.mpevent_player_controls)
		pympe.events.pympe_initilized.subscribe(self.mpevent_pympe_initilized)
		pympe.events.library_entries_removed.subscribe(self.mpevent_library_entries_removed)

		#pympe.events.playlist_entries_updated.subscribe(self.mpevent_playlist_entries_updated)
		pympe.events.library_entries_updated.subscribe(self.mpevent_library_entries_updated)

	def module_start(self):
		self.create_playbin()
		self.volume(pympe.config.get("player", "volume"))

	def module_stop(self):
		# If there is a current file try and save the play position if needed
		if self.currentId != None:
			position = self.position()
			if position != None:
				self.currentLibrary.set_single_track_single_value(self.currentId, 'playbackPosition', position)

			state = 2 if self.paused else (1 if self.playing else 0)
			pympe.config['player']['savedPlayState'] = (state, position)

		self.playbin.set_state(gst.STATE_NULL)
		self.bus.remove_signal_watch()

	def mpevent_library_entries_removed(self, event, libraryUid, libraryIds):
		if libraryUid == self.currentLibrary:
			if self.currentId in libraryIds:
				self.currentLibrary = None
				self.currentId = None

		if libraryUid == self.enqueuedLibrary:
			if self.enqueuedId in libraryIds:
				self.enqueuedLibrary = None
				self.enqueuedId = None


	def mpevent_pympe_initilized(self, event):
		if pympe.config.get('player', 'resumePlayState') or pympe.arguments['play']:
			playState = pympe.config.get('player', 'savedPlayState')

			# If we weren't stopped or cmd line options say to play then resume!
			if playState[0] != 0:
				# 0:Stopped, 1:Playing, 2:Paused
				if pympe.config.get('player', 'resumePlayPosition'): # If we resume the position as well
					self.resumePosition = playState[1]

				self.play()

				if playState[0] == 2:
					self.pause()

	#def mpevent_playlist_entries_updated(self, event, playlistUid, entryIds):
	#	pass
		#if playlistUid != pympe.playlists.current.uid: return
		#if not self.currentInfo: return

		#for index in [pympe.playlists.current.uids.index(entryId) for entryId in entryIds]:
			#if pympe.playlists.current.uris[index] == self.currentInfo['uri']:
				#self.currentInfo['title'] = [pympe.playlists.current.titles[index]]
				#pympe.events.player_current_updated({'title':self.currentInfo['title']})

	def mpevent_library_entries_updated(self, event, libraryUid, entryUids, changedKeys):
		if not self.currentLibrary or libraryUid != self.currentLibrary.uid: return
		if not self.currentInfo: return

		for entryUid, changed in zip(entryUids, changedKeys):
			if entryUid == self.currentId:
				if self.currentLibrary.get_single_track_multi_values(entryUid, changed, 'unmodified') == [self.currentInfo[key] for key in changed if key in self.currentInfo]:
					return

				#info = dict(zip(changed, self.currentLibrary.get_single_track_multi_values(entryUid, changed, 'unmodified')))
				#for k, v in info.iteritems(): self.currentInfo[k] = v
				pympe.events.player_current_updated(changed)
				return

	def swap_video(self, newBin=None, swapSink=False):
		#self.videoBin = newBin if newBin is not None else VideoBin()
		#self.playbin.set_property('video-sink', self.videoBin)
		self.videoBin = newBin if newBin is not None else self.defaultVideoBin
		if swapSink: self.playbin.set_property('video-sink', self.videoBin)

	def create_playbin(self):
		# Create the playbin
		self.playbin = gst.element_factory_make('playbin2')
		self.bus = self.playbin.get_bus()
		self.bus.add_signal_watch()
		self.bus.enable_sync_message_emission()
		self.bus.connect("message", self.handle_bus_message)
		self.bus.connect("sync-message::element", self.handle_sync_message)

		# Create the audio sink override
		self.audioBin = MainAudioBin('AudioBin')
		self.playbin.set_property("audio-sink", self.audioBin)

		# Create the video sink override
		self.videoBin = self.defaultVideoBin = VideoBin('VideoBin')
		self.playbin.set_property('video-sink', self.videoBin)

		# Connect to signals
		self.playbin.connect("about-to-finish", self.handle_about_to_finish)
		self.playbin.connect("audio-changed", self.handle_audio_changed)

	def enqueue(self, entry):
		if not entry: return pympe.debug('Nothing to enqueue, aborting')
		if len(entry) > 4: mediaFile = entry[4]
		else: mediaFile = pm_mediafile.MediaFile(entry[0])
		uri = mediaFile['uri']

		#self.playbin.set_property('uri', uri.encode('UTF-8'))

		# For some have a special exemption on mediafile reading else read the file
		if uri.startswith('cdda://') or uri.startswith('http://'): pass
		elif not mediaFile.read(): return pympe.debug('Couldn\'t load file to enqueue, aborting')

		if mediaFile['local']:
			self.playbin.set_property('uri', urllibQuote(uri.encode('utf8'), '/: '))
		else:
			self.playbin.set_property('uri', uri.encode('utf8'))

		#self.playbin.set_property('uri', urllibQuote(uri.encode('utf8'), '/: '))

		self.enqueuedFile, self.enqueuedId, self.enqueuedLibrary, self.enqueuedPlaylist = mediaFile, entry[2], entry[3], pympe.playlists.current.uid

		# Read from the library if available else from the media file
		if self.enqueuedId:
			self.enqueuedInfo = self.enqueuedLibrary.get_single_track_multi_values(self.enqueuedId, self.enqueuedLibrary.dbKeys, 'dict')
			self.enqueuedInfo['art'] = self.enqueuedFile['art']

		else:
			self.enqueuedInfo = self.enqueuedFile.info

	def post_enqueue(self):
		# If we're playing a new file and the old one was in a library reset the playback position
		if self.currentId != self.enqueuedId and self.currentId != None:
			try:
				self.currentLibrary.set_single_track_single_value(self.currentId, 'playbackPosition', 0)
				pympe.events.player_previous_changed(self.currentInfo, self.currentId, self.currentLibrary, self.position(), self.play_time())
			except:
				pympe.debug('Previous track doesn\'t exist anymore.')

			# Reset the play time
			self.playTime = 0

		# For getting how long we've been playing instead of just using the playback position
		self.playStartTime = time.time()

		self.settled = False
		self.currentFile, self.currentId, self.currentLibrary, self.currentInfo, self.currentPlaylist = self.enqueuedFile, self.enqueuedId, self.enqueuedLibrary, self.enqueuedInfo, self.enqueuedPlaylist
		self.enqueuedFile = self.enqueuedId = self.enqueuedLibrary = self.enqueuedInfo = self.enqueuedPlaylist = None

		# Set the new video mode if needed
		# We set as video if the type is a video file or there is an alternative videoBin
		if ('mimetype' in self.currentInfo and self.currentInfo['mimetype'].startswith('video')) or self.videoBin != self.defaultVideoBin:
			newVideoType = True
		else:
			newVideoType = False

		if newVideoType != self.playingVideo:
			self.playingVideo = newVideoType
			pympe.events.player_video_mode_changed(self.playingVideo)
			#pympe.ui.uiMain.uiTabNowPlaying.set_video_mode(self.playingVideo)

		pympe.events.player_current_changed(self.currentInfo, self.currentId, self.currentLibrary)

		# Set the eq preset if enabled else set to default
		if self.currentId:
			eqPlugin = pympe.plugins.get_plugin(1323703538)
			if eqPlugin != False:
				if self.currentId != None and self.currentLibrary.get_single_track_single_value(self.currentId, 'useEqualizerPreset'):
					eqPlugin.set_preset(self.currentLibrary.get_single_track_single_value(self.currentId, 'equalizerPreset'))
				else:
					eqPlugin.set_preset(pympe.config.get('plugins/plu_equalizer', 'current'))

		# Write the folder.jpg if needed
		if self.currentFile['local'] and self.currentLibrary and pympe.config.get(self.currentLibrary.configKey, 'writeFolderImage'):
			pm_helpers.write_folder_art(self.currentFile)

		# Stop the buffering indicator if we don't need it
		if self.currentFile["local"] and self.bufferIndicator: self.bufferIndicator.stop()

		self.resumedPosition = False
		self.paused = False
		self.playing = True

	def play(self, uri=None, entry=None, mediaFile=None):
		newFile = newLibrary = newId = newInfo = newPlaylist = None

		# Get the first of the active playlist if we aren't inputting any data
		if not uri and not entry and not mediaFile:
			# Resume if we're paused
			if self.paused:
				self.playbin.set_state(gst.STATE_PLAYING)
				self.paused = False
				pympe.events.player_playback_resumed()
				#pympe.events.player_playback_started()
				return
			# Restart if we're stopped
			if self.currentFile:
				newFile, newId, newLibrary, newInfo, newPlaylist = self.currentFile, self.currentId, self.currentLibrary, self.currentInfo, self.currentPlaylist
			# Else play something
			else:
				entry = pympe.playlists.current.get_current()
				if not entry: return

		# For data handed to us
		if entry:
			uri = entry[0]
			newId = entry[2]
			newLibrary = entry[3]
			if len(entry) > 4: newFile = entry[4]
			newPlaylist = pympe.playlists.current.uid

			if newId:
				newInfo = newLibrary.get_single_track_multi_values(newId, newLibrary.dbKeys, 'dict')
			elif newFile:
				newInfo = newFile.info
			else:
				newInfo = {}

		# If we're playing a new file and the old one was in a library save the playback position
		if self.currentId != newId and self.currentId != None:
			try:
				position = self.position()
				if position != None:
					self.currentLibrary.set_single_track_single_value(self.currentId, 'playbackPosition', position)

				pympe.events.player_previous_changed(self.currentInfo, self.currentId, self.currentLibrary, self.position(), self.play_time())
			except:
				pympe.debug('Previous track doesn\'t exist anymore.')

		# For getting how long we've been playing instead of just using the playback position
		self.playStartTime = time.time()

		# Reset the play time
		self.playTime = 0

		# Create the new mediaFile if needed and try to read it
		if not newFile:
			# Create the mediafile if it's a new track
			newFile = pm_mediafile.MediaFile(uri)

			# For some have a special exemption on mediafile reading else read the file
			if uri.startswith('cdda://') or uri.startswith('http://'): pass
			elif not newFile.read():
				self.stop()
				return pympe.error('Couldn\'t load file to play, aborting', False, False)

			# Try and read the info if there is none
			if not newInfo: newInfo = newFile.info

		# Write the folder.jpg if needed
		if newFile['local'] and self.currentLibrary and pympe.config.get(self.currentLibrary.configKey, 'writeFolderImage'): pm_helpers.write_folder_art(newFile)

		# Set the info art
		if 'art' in newFile.info: newInfo['art'] = newFile.info['art']

		# If it's a new song then mark to send event
		playingNewSong = newFile != self.currentFile


		# The split for enqueueing or not
		self.currentFile, self.currentId, self.currentLibrary, self.currentInfo, self.currentPlaylist = newFile, newId, newLibrary, newInfo, newPlaylist

		# If we have no title then use the uri
		if not self.currentInfo['title']: self.currentInfo['title'] = [self.currentInfo['uri'].rsplit(os.sep, 1)[-1]]

		# Set the new video mode if needed
		# We set as video if the type is a video file or there is an alternative videoBin
		if ('mimetype' in self.currentInfo and self.currentInfo['mimetype'].startswith('video')) or self.videoBin != self.defaultVideoBin:
			newVideoType = True
		else:
			newVideoType = False

		if newVideoType != self.playingVideo:
			self.playingVideo = newVideoType
			pympe.events.player_video_mode_changed(self.playingVideo)
			#pympe.ui.uiMain.uiTabNowPlaying.set_video_mode(self.playingVideo)

		# Set the uri
		self.playbin.set_state(gst.STATE_READY)
		self.settled = False

		#try:
		if newFile['local']:
			self.playbin.set_property('uri', urllibQuote(newFile['uri'].encode('utf8'), '/: '))
		else:
			self.playbin.set_property('uri', newFile['uri'].encode('utf8'))
		self.playbin.set_state(gst.STATE_PLAYING)
		#except:
		#	print 'PLAY ERROR'
		#	return


		# Set the eq preset if enabled else set to default
		if self.currentId:
			eqPlugin = pympe.plugins.get_plugin(1323703538)
			if eqPlugin != False:
				if self.currentId != None and self.currentLibrary.get_single_track_single_value(self.currentId, 'useEqualizerPreset'):
					eqPlugin.set_preset(self.currentLibrary.get_single_track_single_value(self.currentId, 'equalizerPreset'))
				else:
					eqPlugin.set_preset(pympe.config.get('plugins/plu_equalizer', 'current'))

		# Stop the buffering indicator if we don't need it
		if self.currentFile["local"] and self.bufferIndicator: self.bufferIndicator.stop()

		previouslyPlaying = self.playing

		self.resumedPosition = False
		self.paused = False
		self.playing = True

		# Send the events
		if playingNewSong:
			pympe.events.player_current_changed(self.currentInfo, self.currentId, self.currentLibrary)

		# If we weren't already playing send playback event
		if not previouslyPlaying:
			pympe.events.player_playback_started()

		# If it's not in a library send the update event for the info
		if not self.currentId: pympe.events.player_current_updated(self.currentInfo.keys())

	def pause(self):
		if self.playing and not self.paused and not self.bufferIndicator:
			# For getting how long we've been playing instead of just using the playback position
			self.playTime += time.time()-self.playStartTime

			self.playbin.set_state(gst.STATE_PAUSED)
			self.paused = True
			pympe.events.player_playback_paused()

	def stop(self):
		if self.playing:
			# For getting how long we've been playing instead of just using the playback position
			self.playTime = 0

			self.playbin.set_state(gst.STATE_NULL)
			self.playing = self.paused = False
			pympe.events.player_playback_stopped()

	def next(self):
		nextIndex = pympe.playlists.current.next()

		if nextIndex == None:
			self.stop()
			return None

		pympe.events.player_playback_next()
		self.play(entry=pympe.playlists.current.get_item(nextIndex))


	def previous(self):
		pympe.events.player_playback_previous()
		self.play(entry=pympe.playlists.current.get_item(pympe.playlists.current.previous()))

	def volume(self, value=None):
		if value == None:
			return self.playbin.get_property("volume")
		else:
			return self.playbin.set_property("volume", value)

	def position(self, value=None):
		if value == None:
			try: return int(round(self.playbin.query_position(gst.FORMAT_TIME, None)[0]/gst.SECOND))
			except gst.QueryError: pass
		else:
			#result = self.playbin.seek_simple(gst.FORMAT_TIME, gst.SEEK_FLAG_FLUSH|gst.SEEK_FLAG_KEY_UNIT, value*gst.SECOND)#gst.SEEK_FLAG_FLUSH|gst.SEEK_FLAG_KEY_UNIT, value*gst.SECOND)
			self.playbin.get_state()
			event = gst.event_new_seek(1.0, gst.FORMAT_TIME, gst.SEEK_FLAG_FLUSH, gst.SEEK_TYPE_SET, int(value*gst.SECOND), gst.SEEK_TYPE_NONE, 0)
			result = self.playbin.send_event(event)

			if result:
				self.playbin.set_new_stream_time(0L)

			return result

	def duration(self, update=False):
		if self.length != 0 and not update: return self.length
		try: self.length = self.playbin.query_duration(gst.FORMAT_TIME, None)[0]/gst.SECOND
		except gst.QueryError: return
		return self.length

	def play_time(self):
		if pympe.player.paused:
			return int(self.playTime)
		if pympe.player.playing:
			return int(self.playTime+(time.time()-self.playStartTime))
		return 0

	def seek(self, value): pass

	def handle_about_to_finish(self, data):
		if not pympe.config.get('player', 'gapless'): return

		nextIndex = pympe.playlists.current.get_next()
		if nextIndex != None:
			self.enqueue(pympe.playlists.current.get_item(nextIndex))

	def handle_audio_changed(self, data):
		if self.enqueuedFile:
			#self.currentFile, self.currentId = self.enqueuedFile, self.enqueuedId
			#self.enqueuedFile = self.enqueuedId = None
			#if self.eventStorageKey:
			#	pympe.newevent.send_stored(self.eventStorageKey)
			#	self.eventStorageKey = 0
	#DO all the set eq and shit like that here

			pympe.events.player_video_mode_changed(self.playingVideo)
			#pympe.ui.uiMain.uiTabNowPlaying.set_video_mode(False)
			pympe.playlists.current.next() # Needs this to update the current.index value
			self.post_enqueue()

		#if self.playing:
		#	pympe.events.player-current-changed(self.currentFile, self.currentId)
		#else:
		#	print self.playing
		#print self.playing

	def handle_bus_message(self, bus, message):
	# Error, the application should stop
		if message.type == gst.MESSAGE_ERROR:
			error, debug = message.parse_error()
			pympe.events.player_gst_error(error, debug)
			pympe.error('GST Error: %s\n%s'%(str(error), str(debug)), False, False)
			self.stop()
	# End of stream reached
		elif message.type == gst.MESSAGE_EOS: self.next()
	# A tag was found
		elif message.type == gst.MESSAGE_TAG and (pympe.config.get('player', 'useTagsFromFile') or \
												not self.currentLibrary or \
												pympe.config.get(self.currentLibrary.configKey, 'saveMissingTags')):
			tags = dict(message.parse_tag())

			#print tags
			mapping = {"album-artist":"albumartist",
						"album-disc-count":"disctotal",
						"album-disc-number":"discnumber",
						"track-count":"tracktotal",
						"track-number":"tracknumber",
						"date":"year",
						"image":"art",
						"genre":"genre",
						"artist":"artist",
						"title":"title",
						"album":"album",
						'extended-comment':'comment'}

			useInfo = self.currentInfo if not self.enqueuedInfo else self.enqueuedInfo
			modifiedTags = []

			for tag in tags:
				if tag in mapping:
					key, value = mapping[tag], tags[tag]
					#if key in self.currentFile.multiTags and type(value) != type([]): value = [value]
					if key == 'art' and type(value) == list: value = value[0]

					# Ugly hack?
					# Perhaps test for all type value matches
					if key in pympe.library.blankTrack and pympe.library.blankTrack[key][1] == list and type(value) != list:
						value = [value]

					# If the key doesn't exist or it's different
					if not key in useInfo or useInfo[key] != value:
						useInfo[key] = value
						modifiedTags.append(key)

			if modifiedTags and not self.enqueuedFile:
				pympe.events.player_current_updated(modifiedTags)

	# Pipeline is buffering, pause while buffering resume when finished
		elif message.type == gst.MESSAGE_BUFFERING:
			percent = message.parse_buffering()

			if self.length == 0:
				if self.duration():
					if not 'length' in self.currentInfo or self.currentInfo['length'] != self.length:
						self.currentInfo['length'] = self.length
						pympe.events.player_current_updated(['length'])

			if percent >= 100 and self.bufferIndicator:
				self.bufferIndicator = self.bufferIndicator.finish()
			elif self.bufferIndicator:
				self.bufferIndicator.update(percent)
			else: self.bufferIndicator = pympe.progress.new('Buffering')

	# Element specific message
		elif message.type == gst.MESSAGE_ELEMENT: pass
	# Duration of the stream has changed
		elif message.type == gst.MESSAGE_DURATION:
			#print 'Duration Message', int(message.parse_duration()[1]/gst.SECOND)
			parsed = message.parse_duration()

			if parsed[0] == gst.FORMAT_TIME: length = int(parsed[1]/gst.SECOND)
			elif parsed[0] == gst.FORMAT_BYTES: length = 0 # Maybe bitrate/bytes?
			else: length = 0

			if length != 0: self.length = length

			if not self.settled:
				self.settled = True

				if self.enqueuedFile: useFile, useId, useLibrary, useInfo = (self.enqueuedFile, self.enqueuedId, self.enqueuedLibrary, self.enqueuedInfo)
				else: useFile, useId, useLibrary, useInfo = (self.currentFile, self.currentId, self.currentLibrary, self.currentInfo)

				# Update the length if needed
				if not 'length' in useInfo or useInfo['length'] != self.length:
					useInfo["length"] = self.length

					if not self.enqueuedFile:
						pympe.events.player_current_updated(['length'])

				# Resume where the track left off (Can also put start at XX:XX)
				if useId != None and not self.resumedPosition:
					self.resumedPosition = True
					useStart, useStop, usePlaybackPosition, startTime, stopTime, playbackPosition = useLibrary.get_single_track_multi_values(useId, ['useStartTime', 'useStopTime', 'usePlaybackPosition', 'startTime', 'stopTime', 'playbackPosition'])
					self.stopTime = stopTime if useStop else False

					# If it's first start
					if self.resumePosition:
						self.position(self.resumePosition)
						self.resumePosition = None

					# Start from where we left off if stored
					elif usePlaybackPosition: self.position(playbackPosition)
					# Else start from start point if set
					elif useStart: self.position(startTime)

		else: return gst.BUS_PASS

	def mpevent_player_controls(self, event, *args):
		if event == pympe.events.player_play: self.play()
		elif event == pympe.events.player_pause: self.pause()
		elif event == pympe.events.player_stop: self.stop()
		elif event == pympe.events.player_next: self.next()
		elif event == pympe.events.player_previous: self.previous()
		elif event == pympe.events.player_set_volume: self.volume(args[0])

	#def add_audio_element(self, element):
		#self.audioBin.add_element(element)

	#def remove_audio_element(self, element):
		#self.audioBin.remove_element(element)

	#def swap_audio_element(self,

	# This needs to run in the main thread
	def handle_sync_message(self, bus, message):
		if message.structure is None: return
		messageName = message.structure.get_name()

		if messageName == 'missing-plugin':
			pympe.error('GST Missing Plugin: %s'%str(message.structure['name']), False, False)


		elif messageName == "prepare-xwindow-id":
			if self.playingVideo == False:
				self.playingVideo = True
				pympe.events.player_video_mode_changed(self.playingVideo)

			print pympe.ui.uiMain.videoWindowHandle
			self.videoBin.set_window_handle(pympe.ui.uiMain.videoWindowHandle)
