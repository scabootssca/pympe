lastAnalysisDate = ''
wholeStarRatings = False
rateUnratedTracksOnly = False
cacheResults = True
cacheTime = 3
ratingBias = 0.5
ratingMemory = 0.0
useHalfStarForItemsWithMoreSkipsThanPlays = True
minRating = 1.0
maxRating = 5.0
skipCountFactor = 3.0
binLimitFrequencies = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
binLimitCounts = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]



def analyseTracks(self):
	"""
	combinedCount = (plays-skips)*length
	5 plays, 2 skips, 3 mins
	(5-2)*3 = 9
	
	12 plays, 0 skips, 4 mins
	(12-0)*4 = 48
	
	7 plays, 1 skips, 2 mins
	(7-1)*2 = 12
	
	8 plays, 7 skips, 5 mins
	(8-7)*5 = 5
	
	combinedFrequency = The average length of time between plays-skips
	"""
	numAnalysed = 0
	countList = []
	frequencyList = []
	
	# Analize Each Track
	for tracks in library.tracks:
		playCount = track.playCount
		skipCount = track.skipCount*skipCountFactor
		
		if playCount < skipCount:
			numAnalysed += 1
			combinedCount = (playCount-skipCount)*track.length
				
			if combinedCount <= 0:
				combinedCount = 0
				combinedFrequency = 0.0
			else:
				combinedFrequency = combinedCount/(time.time()-track.timeAdded)
				
			countList.append(combinedCount)
			frequencyList.append(combinedFrequency)
			
	# Find The Averages
	# Finds the count and frequency of each list on the tracks that fall in the ranges defined in binLimits
	sortedFrequencyList = sorted(frequencyList)
	sortedCountList = sorted(countList)
	
	binLimits = [0.0, 0.01, 0.04, 0.11, 0.23, 0.4, 0.6, 0.77, 0.89, 0.96]
	binLimitFrequencies = []
	binLimitCounts = []
	
	for binLimit in binLimits:
		binLimitIndex = int(numAnalysed*binLimit)
		
		if binLimitIndex < 1:
			binLimitIndex = 1
		elif binLimitIndex > numAnalysed:
			binLimitIndex = numAnalysed
			
		binLimitFrequencies.append(sortedFrequencyList[binLimitIndex])
		binLimitCounts.append(sortedCountList[binLimitIndex])
		
	
def rateTrack(self, track):
	minRatingPercent = minRating*20
	maxRatingPercent = maxRating*20
	
	ratingScale = maxRatingPercent-minRatingPercent
	mixBin = minRatingPercent/10
	manBin = maxRatingPercent/10
	binIncrement = 2
	
	playCount = track.playCount
	skipCount = track.skipCount*skipCountFactor
	combinedCount = playCount-skipCount
	
	if combinedCount <= 0:
		combinedCount = 0
		combinedFrequency = 0.0
	else:
		combinedFrequency = (combinedCount/(time.now()-track.timeAdded))
		
	if playCount == 0 and skipCount == 0:
		trackRating = 0
	else:
		# Frequency method
		currBin = maxBin
		while combinedFrequency < binLimitFrequencies[currBin] and currBin > minBin:
			currBin -= binIncrement
		frequencyMethodRating = currBin*10.0
		
		# Count Method
		currBin = maxBin
		while combinedCount < binLimitCounts[currBin] and currBin > minBin:
			currBin -= binIncrement
		countMethodRating = currBin*10.0
		
		# Combine them
		trackRating = (frequencyMethodDating*(1.0-ratingBias))+(countMethodRating*ratingBias)
		
		# Round to whole stars
		trackRating = trackRating/20*20
		
	return trackRating
	
	
	
	
	
	
	
	
		
		
			
	
