lastAnalysisDate = ''
wholeStarRatings = False
rateUnratedTracksOnly = False
cacheResults = True
cacheTime = 3
ratingBias = 0.5
ratingMemory = 0.0
useHalfStarForItemsWithMoreSkipsThanPlays = True
minRating = 1.0
maxRating = 5.0
skipCountFactor = 3.0
binLimitFrequencies = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
binLimitCounts = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]










# Analize track
playcount
skipcount
tracklength

countlist = []
frequencylist = []
numanalized  = 0

for track in library.tracks:
	if track.playcount > track.skipcount:
		numanalized += 1
		combinedcount = (track.playcount-track.skipcount)*track.length
		
		if combinedcount <= 0:
			combinedcount = 0
			combinedfrequency = 0.0
		else:
			combindedfrequency = combinedcount/(timenow-track.dateadded)
			
		countlist.append(combinedCount)
		frequencylist.append(combinedfrequency)
	


sortedfrequencylist = sorted(frequencylist)
sortedcountlist = sorted(countlist)

binlimits = [0.0, 0.01, 0.04, 0.11, 0.23, 0.4, 0.6, 0.77, 0.89, 0.96]

binlimitfrequencies = []
binlimitcounts = []

for binlimit in binlimits:
	binlimitindex = int(numanalized*binlimit)
	
	if binlimitindex < 1:
		binlimitindex = 1
	elif binlimitindex > numanalized:
		binlimitindex = numanalized
		
	binlimitfrequencies.append(sortedFrequencylist[binlimitindex])
	binlimitcounts.append(sortedcountlist[binlimitindex])
	
lastAnalysisDate = time.now()

#--Correct minimum rating value if user selects whole-star ratings or to reserve 1/2 star for disliked songs
#--0 star ratings are always reserved for songs with no skips and no plays
minRatingPercent = 20
maxRatingPercent = 100

theTrackCount = 0
ratingScale = maxRatingPercent-minRatingPercent

minBin = minRatingPercent/10.0
maxBin = maxRatingPErcent/10.0

numTracksToRate = len(library.tracks)
timeoutCount = 0

for theTrackNum in range(numTracksToRate):
	track = tracks[theTrackNum]
	
	playCount = track.playcount
	skipcount = track.skipcount
	dateAdded = track.dateadded
	
	combinedCount = int(playcount-skipCount)
	if combinedCount <= 0:
		combinedCount = 0
		combinedFrequency = 0.0
	else:
		combinedFrequency = combinedCount/(time.now()-dateAdded)
		
	if playCount == 0 and skipCount == 0:
		theRating = 0
	else:
		# Frequency Method
		bin = maxBin
		while combinedFrequency < binLimitFrequencies[bin] and bin > minBin:
			bin -= binIncrement
		frequencyMethodDating = bin*10.0
		
		# Count Method
		bin = maxBin
		while combinedCount < binLimitCounts[bin] and bin > minBin:
			bin -= binIncrement
		countMethodRating = bin*10.0
		
		# Combine the ratings
		theRating = (frequencyMethodRating*(1.0-ratingBias)) + (countMethodRating*ratingBias)
		
		# Factor previous Rating
		if ratingMemory > 0.0:
			theRating = (theOldRating*ratingMemory)+(theRating*(1.0-ratingMemory))
			
	# Round to whole stars
	theRating = (theRating/20)*20
	
	
		


