#update_tee_visualizer (RBVisualizerPlugin *plugin,
		       #const char *vis_override,
		       #int quality,
		       #GError **error)
#{
update_tee_visualizer(plugin, vis_override, quality, error):
	#GstElement *old_vis_plugin = NULL;
	#GstPad *blocked_pad = NULL;
	#gboolean add_tee;
	old_vis_plugin = None
	blocked_pad = None
	add_tee = False

	#/* if we're not active, just make sure the bin isn't in the pipeline. */
	if (plugin->active == FALSE) {
		if (GST_ELEMENT_PARENT (plugin->visualizer)) {
			rb_debug ("removing visualizer bin from the pipeline");
			rb_player_gst_tee_remove_tee (RB_PLAYER_GST_TEE (plugin->player),
						      plugin->visualizer);
		} else {
			rb_debug ("visualizer bin isn't in the pipeline");
		}
		return;
	}
	if plugin.active == False:
		if plugin.visualizer.get_parent() != None:
			add_tee = False
			(ret, state, pending) = plugin.visualizer.get_state()
			if ret == gst.STATE_CHANGE_SUCCESS and state == gst.STATE_PLAYING):
				



			/* probably should do this async.. */
			rb_debug ("blocking visualizer bin sink pad");
			pad = gst_element_get_static_pad (plugin->visualizer, "sink");
			blocked_pad = gst_ghost_pad_get_target (GST_GHOST_PAD (pad));
			gst_pad_set_blocked (blocked_pad, TRUE);
			gst_object_unref (pad);
			rb_debug ("blocked visualizer bin sink pad");
		}
	} else {
		GstStateChangeReturn state_ret;
		GstBus *bus;
		gboolean new_bus = FALSE;
		gboolean failed = FALSE;

		add_tee = TRUE;

		/* put the sink in READY state so it grabs XV ports etc.
		 * if it refuses to change state, disable visualization.
		 */
		bus = gst_element_get_bus (plugin->visualizer);
		if (bus == NULL) {
			bus = gst_bus_new ();
			new_bus = TRUE;
			gst_element_set_bus (plugin->visualizer, bus);
		}

		state_ret = gst_element_set_state (plugin->video_sink, GST_STATE_READY);
		if (state_ret == GST_STATE_CHANGE_FAILURE) {
			/* look for error messages on the bus */
			while (gst_bus_have_pending (bus)) {
				GstMessage *msg;

				msg = gst_bus_pop (bus);
				if (GST_MESSAGE_TYPE (msg) == GST_MESSAGE_ERROR) {
					char *debug;

					gst_message_parse_error (msg, error, &debug);
					failed = TRUE;
				}

				gst_message_unref (msg);
			}

			if (failed == FALSE) {
				g_set_error (error,
					     RB_PLAYER_ERROR,
					     RB_PLAYER_ERROR_GENERAL,
					     _("Unable to start video output"));
				failed = TRUE;
			}
		}

		if (new_bus) {
			gst_element_set_bus (plugin->visualizer, NULL);
		}
		gst_object_unref (bus);

		if (failed) {
			rb_debug ("sink failed to change state");
			return;
		}

		add_tee = TRUE;
	}

	/* otherwise, update the visualizer element */
	if (plugin->vis_plugin != NULL) {
		/* clean up the element once we've unblocked the pipeline;
		 * otherwise it seems to deadlock.
		 */
		old_vis_plugin = g_object_ref (plugin->vis_plugin);
		gst_bin_remove (GST_BIN (plugin->visualizer), plugin->vis_plugin);
	}

	plugin->vis_plugin = create_visualizer_element (vis_override);
	gst_bin_add (GST_BIN (plugin->visualizer), plugin->vis_plugin);

	if (gst_element_link_many (plugin->identity, plugin->vis_plugin, plugin->capsfilter, NULL) == FALSE) {
		rb_debug ("failed to link in new visualizer element");
		g_set_error (error,
			     RB_PLAYER_ERROR,
			     RB_PLAYER_ERROR_GENERAL,
			     _("Failed to link new visual effect into the GStreamer pipeline"));

		return;
	}

	/* update the capsfilter */
	fixate_vis_caps (plugin, plugin->vis_plugin, plugin->capsfilter, quality);

	/* make sure the visualizer is in the pipeline, and sync its state
	 * with the rest of the pipeline.
	 */
	if (add_tee) {
		rb_debug ("adding visualizer bin to the pipeline");
		rb_player_gst_tee_add_tee (RB_PLAYER_GST_TEE (plugin->player),
					   plugin->visualizer);
	} else if (blocked_pad != NULL) {
		gst_element_set_state (plugin->vis_plugin, GST_STATE_PLAYING);
		gst_pad_set_blocked (blocked_pad, FALSE);
		gst_object_unref (blocked_pad);
	} else {
		gst_element_set_state (plugin->vis_plugin, GST_STATE_PAUSED);
	}

	if (old_vis_plugin != NULL) {
		rb_debug ("cleaning up old visualizer element");
		gst_element_set_state (old_vis_plugin, GST_STATE_NULL);
		g_object_unref (old_vis_plugin);
	}
}
