import threading, wx, pympe
from cStringIO import StringIO
from gtk.gdk import PixbufLoader

def tryexcept(func):
	def wrapper(*args, **kwargs):
		try:
			func(*args, **kwargs)
		except:
			pympe.error("Something Broke!", traceback=True)

	return wrapper

#def run_return_thread(func, after, *args, **kwargs):
	#def run_return_thread(self, func, after, *args, **kwargs):
		#after(*func(*args, **kwargs))

	#t = threading.Thread(target=run_return_thread, args=(after, *args), kwargs=kwargs, name=repr(func))
	#t.daemon = True
	#t.start()
	#return t

def thread_name():
	return threading._get_ident()

def threaded(func):
	threadName = func.__doc__.split('\n')[0][2:] if func.__doc__ and func.__doc__.startswith('@=') else repr(func)

	def wrapper(*args, **kwargs):
		t = threading.Thread(target=func, args=args, kwargs=kwargs, name=threadName)
		t.daemon = True
		t.start()

	return wrapper

def run_thread(func, *args, **kwargs):
	threadName = func.__doc__.split('\n')[0][2:] if func.__doc__ and func.__doc__.startswith('@=') else repr(func)
	#print 'Run Thread', func, threading.current_thread()
	#func(*args, **kwargs)
	t = threading.Thread(target=func, args=args, kwargs=kwargs, name=threadName)
	t.daemon = True
	t.start()
	return t

def unthread(func):
	def wrapper(*args, **kwargs):
		#current = threading.current_thread()
		#ident = current.ident
		#if type(current) = threading._DummyThread: del current

		#print 'Current Thread:\n	%s\n	%s\n'%(repr(func),current)

		if threading._get_ident() == pympe.mainThread:
			func(*args, **kwargs)
		else:
			wx.CallAfter(func, *args, **kwargs)


	return wrapper

def unthread_function(func, *args, **kwargs):
	#current = threading.current_thread()
	#ident = current.ident
	#if type(current) = threading._DummyThread: del current

	#print 'Current Thread:\n	%s\n	%s\n'%(repr(func),threading.current_thread())
	if threading._get_ident() == pympe.mainThread:
		func(*args, **kwargs)
	else:
		wx.CallAfter(func, *args, **kwargs)

def debug(function):
	def wrapped(*args, **kwargs):
		print "Function Called: %s\n"%(str(function))
		return function(*args, **kwargs)

	return wrapped

def inspect(function):
	def wrapped(*args, **kwargs):
		print "--------------------------------------\nName: %s\nArgs: %s\nKwArgs: %s\n--------------------------------------\n"%(str(function), str(args), str(kwargs))

		return function(*args, **kwargs)

	return wrapped

# These need to be down here
import pympe, re, htmlentitydefs, re, webbrowser, os, pm_mediafile, subprocess, wx

def has_focus():
	return True if wx.GetActiveWindow() else False

def sanitize_path(path, *args):
	# Linux and such
	if os.name == 'posix':
		return path.replace('>','_').replace('<','_').replace('|','_').replace(':','_').replace('&','_')

	# Windows
	if os.name == 'nt':
		# args[0] will then be special chars or something?
		# I don't remember and dont want to worry about decoding this right now
		# Welcome to the terrible dregs of rushed and undirected programming from years past
		return re.sub('[\\\\\:\;\*\?\"\<\>\|%s]'%(('\\%s'%'\\'.join(args[0])) if args else ''), '_', path).encode('UTF-8')

	# Other
	return path

def winsafe(path, *args):
	print 'pm_helpers.winsafe() is depreciated please refrain from using this and use pm_helpers.sanitize_path() directly'
	return sanitize_path(path, *args)

def make_display_time(seconds, showDays=False, showHours=True):
	try: seconds = int(seconds)
	except ValueError:
		return ''

	if showDays:
		return '%i:%02i:%02i:%02i'%(seconds/86400, seconds%86400/3600, seconds%86400%3600/60, seconds%86400%3600%60)
	elif showHours:
		return '%i:%02i:%02i'%(seconds/3600, seconds%3600/60, seconds%3600%60)
	else:
		return '%i:%02i'%(seconds/60, seconds%60)

def make_display_filesize(b):

	gb = b/1073741824.0
	b -= int(gb)*1073741824

	if int(gb): return "%.2f Gigabytes"%gb

	mb = b/1048576.0
	b -= int(mb)*1048576

	if int(mb): return "%.2f Megabytes"%mb

	kb = int(b)/1024.0

	if int(kb): return "%.2f Kilobytes"%kb

	b -= 1024*kb

	return "%.2f Bytes"%b

def make_display_tag(value, singleValue=False, quote=True):
	if type(value) == list:
		result = '; '.join([str(entry) for entry in value]) if not singleValue else str(value[0])
	else:
		result = str(value)

	if quote:
		result = result.replace('&', '&&')

	return result.decode('UTF-8')

def make_alternating_list_attributes(alternative=False):
	light = wx.ListItemAttr()
	light.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_3DSHADOW if alternative else wx.SYS_COLOUR_LISTBOX))

	dark = wx.ListItemAttr()
	dark.SetBackgroundColour([x-5 for x in wx.SystemSettings.GetColour(wx.SYS_COLOUR_3DSHADOW if alternative else wx.SYS_COLOUR_LISTBOX)])
	return (light, dark)

def split_string(string, chunkSize):
	chunks = []

	while True:
		chunks.append(string[:chunkSize])
		string = string[chunkSize:]
		if not string: break

	return chunks

def identify_event(event):
	result = [x for x in dir(pympe.events) if getattr(pympe.events, x) == event]
	if result: return result[0]

def db_to_percent(dB, base=20.0):
	return 10**(dB / base)*100

def percent_to_db(percent, base=20.0):
	if percent == 0: return -1
	from math import log10
	return base*log10(percent/100.0)

def slider_to_log(value):
	return db_to_percent(percent_to_db(value, 60.0), 20.0)

def log_to_slider(value):
	return db_to_percent(percent_to_db(value, 20.0), 60.0)

def unencode(text):
		def fixup(m):
			text = m.group(0)
			if text[:2] == "&#":
				# character reference
				try:
					if text[:3] == "&#x":
						return unichr(int(text[3:-1], 16))
					else:
						return unichr(int(text[2:-1]))
				except ValueError:
					pass
			else:
				# named entity
				try:
					text = unichr(htmlentitydefs.name2codepoint[text[1:-1]])
				except KeyError:
					pass
			return text # leave as is
		return re.sub("&#?\w+;", fixup, text)

def open_location(uri):
	uriType, location = uri.split("://")
	if uriType == "file":
		location = location.rsplit(os.sep, 1)[0]
		if os.name == "nt": os.startfile(location)
		elif os.name == "posix": subprocess.Popen(['xdg-open', location])
	elif uriType == "http":
		webbrowser.open(uri)

def clipboard_write(text):
	clipdata = wx.TextDataObject()
	clipdata.SetText(text)
	wx.TheClipboard.Open()
	wx.TheClipboard.SetData(clipdata)
	wx.TheClipboard.Close()

def clipboard_read():
	clipdata = wx.TextDataObject()
	wx.TheClipboard.Open()
	result = wx.TheClipboard.GetData(clipdata)
	wx.TheClipboard.Close()
	if result: return clipdata.GetText()
	return None

def get_mediafile_format_string(mediaFile, newFormat, tags=None):
	for tag in (tags if tags else mediaFile.info.keys()):
		if '%'+tag+'%' in newFormat:
			newFormat = newFormat.replace('%'+tag+'%', mediaFile.display(tag))
	return newFormat#+'.'+mediaFile.ext

def get_info_format_string(info, newFormat, tags=None):
	for tag in (tags if tags else info.keys()):
		if '%'+tag+'%' in newFormat:
			newFormat = newFormat.replace('%'+tag+'%', str(info[tag]).replace(os.sep, ''))
	#if "uri" in info: newFormat += '.'+info["uri"].rsplit(".", 1)[-1].lower()
	return newFormat

def write_folder_art(mediaFile, writeRegardless=False, root=None):
	if root is None: root = mediaFile["filename"].rsplit(os.sep, 1)[0]
	if (writeRegardless or not os.path.exists(root+os.sep+"folder.jpg")) and mediaFile["art"]:
		with open(root+os.sep+"folder.jpg", "w") as f: f.write(mediaFile["art"])

def write_folder_art_uri(uri, writeRegardless=False, root=None):
	if root is None: root = uri.rsplit(os.sep, 1)[0][7:]
	if (writeRegardless or not os.path.exists(root+os.sep+"folder.jpg")):
		mediaFile = pm_mediafile.MediaFile(uri)
		mediaFile.read()
		if mediaFile["art"]:
			with open(root+os.sep+"folder.jpg", "w") as f: f.write(mediaFile["art"])

def write_folder_art_library(trackId, writeRegardless=False, root=None):
	mediaFile = pm_mediafile.MediaFile(pympe.library.get_single_track_single_value(trackId, 'uri'))
	mediaFile.read()
	write_folder_art(mediaFile, writeRegardless, root)

def get_stock_media_icon():
	return 'audio-x-generic' if not pympe.player.playingVideo else 'video-x-generic'


# Try to use PIL for image quality if it is installed
try:
	from PIL import Image
	from PIL import ImageOps
	usePIL = True
except:
	usePIL = False
	print('PIL not detected, falling back.')

class PmImage:
	def __init__(self, path=None, stream=None, image=None, stock=None, string=None, pil=None, stockSize=(500, 500)):
		self.usePIL = pil if pil != None else usePIL

		if self.usePIL:
			try:
				if path: self.image = Image.open(path)
				elif stream: self.image = Image.open(stream)
				elif image:
					self.image = image.copy()
				elif stock:
					myWxImage = wx.ArtProvider.GetBitmap(stock, size=stockSize).ConvertToImage()
					self.image = Image.new( 'RGB', (myWxImage.GetWidth(), myWxImage.GetHeight()) )
					self.image.frombytes( myWxImage.GetData() )
				elif string:
					self.image = Image.open(StringIO(string))
				else: self.image = Image.new('RGB', (1, 1))
			except IOError:
				self.image = Image.new('RGB', (1, 1))
		else:
			if path: self.image = wx.Image(path)
			elif stream: self.image = wx.ImageFromStream(stream)
			elif image: self.image = image
			elif stock: self.image = wx.ArtProvider.GetBitmap(stock, size=stockSize).ConvertToImage()
			elif string: self.image = wx.ImageFromStream(StringIO(string))
			else: self.image = wx.EmptyBitmapRGBA(1, 1, alpha=0).ConvertToImage()

	def scaled(self, x, y):
		if self.usePIL: return PmImage(image=self.image.resize((x, y), Image.ANTIALIAS), pil=self.usePIL)
		return PmImage(image=self.image.Scale(x, y, wx.IMAGE_QUALITY_HIGH), pil=self.usePIL)

	def scale(self, x, y):
		if self.usePIL:
			try: self.image = self.image.resize((x, y), Image.ANTIALIAS)
			except IOError: pympe.debug('Unable to load artwork')
		else: self.image.Rescale(x, y, wx.IMAGE_QUALITY_HIGH)

	def mirror(self):
		if self.usePIL: self.image = self.image.transpose(Image.FLIP_TOP_BOTTOM)
		else: self.image = self.image.Mirror(False)

	def mirrored(self):
		if self.usePIL: return PmImage(image=self.image.transpose(Image.FLIP_TOP_BOTTOM), pil=self.usePIL)
		return PmImage(image=self.image.Mirror(False), pil=self.usePIL)

	def get_size(self):
		if self.usePIL: return self.image.size
		else: return (self.image.GetWidth(), self.image.GetHeight())

	def get_width(self):
		if self.usePIL: return self.image.size[0]
		return self.image.GetWidth()

	def get_height(self):
		if self.usePIL: return self.image.size[1]
		return self.image.GetHeight()

	def apply_mask(self, mask):
		if not self.usePIL:
			self.image.SetAlphaData(mask.image.GetAlphaData())
		else:
			try:
				self.image = self.image.convert("RGBA")
				self.image.putalpha(mask.image.split()[-1])
			except ValueError: pass

	def apply_overlay(self, overlay):
		if not self.usePIL:
			pass
		else:
			self.image = self.image.convert("RGBA")
			self.image.putdata(overlay.image.getdata(), 1.0, 0.0)

	def or_alpha(self, overlay):
		''' ors the alpha '''
		if not self.usePIL: return

		newAlpha = []

		for x, y in zip(self.image.split()[-1].getdata(), overlay.image.split()[-1].getdata()):
			if x < y: newAlpha.append(x)
			else: newAlpha.append(y)

		alphaImage = Image.new('L', self.image.size)
		alphaImage.putdata(newAlpha)

		self.image.putalpha(alphaImage)

	def and_alpha(self, overlay):
		''' and the alpha '''
		if not self.usePIL: return

		newAlpha = []

		for x, y in zip(self.image.split()[-1].getdata(), overlay.image.split()[-1].getdata()):
			if x > y: newAlpha.append(x)
			else: newAlpha.append(y)

		alphaImage = Image.new('L', self.image.size)
		alphaImage.putdata(newAlpha)

		self.image.putalpha(alphaImage)

	def paste(self, otherImage, offset):
		if not self.usePIL: return
		self.image.paste(otherImage.image, offset)

	def blit(self, overlay, position, cutoff=20):
		if not self.usePIL:
			return

		# Use the bottom image unless the top image has total opacity and then use the top image
		myColor = list(self.image.getdata())
		overlayColor = list(overlay.image.getdata())

		overlayWidth = overlay.image.size[0]
		myWidth = self.image.size[0]

		for i, pixel in enumerate(overlayColor):
			#if pixel[-1] > 200:
			#	myColor[myPosition] = pixel

			if pixel[-1] <= cutoff: continue

			myPosition = (((i/overlayWidth)+position[1])*myWidth)+(i%overlayWidth)+position[0]

			p2 = pixel
			p1 = myColor[myPosition]

			# Full opacity then use the overlay
			if pixel[-1] >= 200:
				myColor[myPosition] = pixel
				continue


			alpha = p1[-1]/255.0

			myColor[myPosition] = (int(p1[0]*(1.0-alpha)+p2[0]*alpha), int(p1[1]*(1.0-alpha)+p2[1]*alpha), int(p1[2]*(1.0-alpha)+p2[2]*alpha), p1[3])


		self.image.putdata(myColor)


	def set_mask_color(self, r, g, b):
		if not self.usePIL:
			self.image.SetMaskColour(r, g, b)
		return self

	def crop(self, box):
		if not self.usePIL:
			self.image = self.image.Resize((box[2],box[3]), (0, 0))
		else:
			self.image = self.image.crop(box)

	def convert_to_wx_bitmap(self, copyAlpha=True):
		if not self.usePIL: return self.image.ConvertToBitmap()

		image = wx.EmptyImage(*self.image.size)
		image.SetData(self.image.convert('RGB').tobytes())

		# Copy the alpha if it exists and is wanted
		if copyAlpha and self.image.mode[-1] == 'A':
			image.SetAlphaData(self.image.convert('RGBA').tobytes()[3::4])

		return image.ConvertToBitmap()

	def convert_to_pixbuf(self):
		if not self.usePIL: return False


		pixbufLoader = PixbufLoader()
		pixbufLoader.write(self.image.convert('RGBA').tobytes('jpeg', 'RGBA'))
		pixbuf = pixbufLoader.get_pixbuf()
		pixbufLoader.close()
		return pixbuf

	def copy(self):
		return PmImage(image=self.image)

class StaticWrapText(wx.StaticText):
    """A StaticText-like widget which implements word wrapping."""

    def __init__(self, *args, **kwargs):
        wx.StaticText.__init__(self, *args, **kwargs)

        # store the initial label
        self.__label = super(StaticWrapText, self).GetLabel()

        # listen for sizing events
        self.Bind(wx.EVT_SIZE, self.OnSize)

    def SetLabel(self, newLabel):
        """Store the new label and recalculate the wrapped version."""
        self.__label = newLabel
        self.__wrap()

    def GetLabel(self):
        """Returns the label (unwrapped)."""
        return self.__label

    def __wrap(self):
        """Wraps the words in label."""
        words = self.__label.split()
        lines = []

        # get the maximum width (that of our parent)
        max_width = self.GetParent().GetVirtualSizeTuple()[0]

        print max_width # DEBUG

        index = 0
        current = []

        for word in words:
            current.append(word)

            if self.GetTextExtent(" ".join(current))[0] > max_width:
                del current[-1]
                lines.append(" ".join(current))

                current = [word]

        # pick up the last line of text
        lines.append(" ".join(current))

        # set the actual label property to the wrapped version
        super(StaticWrapText, self).SetLabel("\n".join(lines))

        # refresh the widget
        self.Refresh()

    def OnSize(self, event):
        # dispatch to the wrap method which will
        # determine if any changes are needed
        self.__wrap()

