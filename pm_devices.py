import pympe, pm_library, os, pm_helpers, threading

class DeviceManager:
	def __init__(self):
		self.handlers = []
		self.devices = {}
		self.nextUid = 0
		self.DeviceClass = Device

		pympe.events.device_connect.subscribe(self.mpevent_device_connect)
		pympe.events.device_disconnect.subscribe(self.mpevent_device_disconnect)
		pympe.events.device_rename.subscribe(self.mpevent_device_rename)

	def module_start(self): pass

	def module_stop(self):
		for device in self.devices.values():
			device.disconnect()

		self.devices = {}

	def change_type(self, device, toType):
		self.devices[device.uid] = toType(device)
		return self.devices[device.uid]

	# For ease of access
	def __getitem__(self, key):
		try: return self.devices[key]
		except KeyError: return None

	def __repr__(self):
		return ', '.join(('%s:(\'%s\' at \'%s\')'%(uid, device.name, device.mountPoint) for uid, device in self.devices.iteritems()))

	def keys(self):
		return self.devices.keys()

	def values(self):
		return self.devices.values()

	def iteritems(self):
		return self.devices.iteritems()

	def __len__(self):
		return len(self.devices)

	def __contains__(self, key):
		return key in self.devices

	# Event handlers
	def mpevent_device_connect(self, event, deviceUid):
		if deviceUid in self.devices:
			self.devices[deviceUid].connect()

	def mpevent_device_disconnect(self, event, deviceUid):
		if deviceUid in self.devices:
			self.devices[deviceUid].disconnect()

	def mpevent_device_rename(self, event, deviceUid, name):
		if deviceUid in self.devices:
			self.devices[deviceUid].set_name(name)

	# Functions
	def new_device(self, deviceClass, *args, **kwargs):
		device = deviceClass(self.nextUid, *args, **kwargs)
		self.nextUid += 1

		self.devices[device.uid] = device

		pympe.events.device_added(device)
		return device

	def remove_device(self, uid):
		try: device = self.devices[uid]
		except KeyError: return False

		device.disconnect()

		pympe.events.device_removed(device)
		del self.devices[uid]

		return True

	# For finding them
	def get_device(self, uid):
		try: return self.devices[uid]
		except KeyError: return None

	def get_device_by(self, key, value):
		for device in self.devices.values():
			if getattr(device, key) == value:
				return device

	def get_ready(self):
		return [device for device in self.devices.values() if device.isReady]

class Device:
	def __init__(self, uid, *args, **kwargs):
		self._setup()

		self.lock = threading.RLock()

		# If uid is a device to copy
		if type(uid) != int:
			self.copy(uid)
			return

		self.uid = uid
		self.connected = False
		self.library = None

		self.isReady = False

		# Info
		self.devPath = kwargs['devPath'] if 'devPath' in kwargs else None
		self.mountPoint = kwargs['mountPoint'] if 'mountPoint' in kwargs else None
		self.uuid = kwargs['uuid'] if 'uuid' in kwargs else None
		self.name = kwargs['name'] if 'name' in kwargs else None
		self.capacity = kwargs['capacity'] if 'capacity' in kwargs else None

	def copy(self, device):
		self.uid = device.uid
		self.connected = device.connected
		self.library = device.library
		self.isReady = device.isReady
		self.devPath = device.devPath
		self.mountPoint = device.mountPoint
		self.uuid = device.uuid
		self.name = device.name
		self.capacity = device.capacity

	def ready(self):
		if self.isReady: return
		self.isReady = True
		pympe.events.device_ready(self)

	@pm_helpers.unthread
	def connect(self):
		print 'Connecting device %s'%self.name
		indicator = pympe.progress.new('Connecting %s'%self.name)

		# Already connected
		if self.connected:
			print 'Device already connected not connecting'
			return True

		# No mountpoint
		if self.mountPoint is None:
			print 'Unable to connect device, it is not mounted.'
			return False

		# Setup the library
		self.library = pm_library.Library('Device_%s'%self.mountPoint.replace(os.sep, '_'))
		self.library.device = self

		@pm_helpers.threaded
		def connect_threaded(self, indicator):
			try:
				self._connect(indicator)
			except:
				pympe.error('There was a problem connecting', True, True)
				self.finished_connecting(indicator, False)

		connect_threaded(self, indicator)


	@pm_helpers.unthread
	def finished_connecting(self, indicator, result):
		self.connected = result
		if result: pympe.events.device_connected(self)
		indicator.finish()

	def read(self):
		print 'Reading Device %s'%self.name
		self._read()

	@pm_helpers.unthread
	def disconnect(self):
		# Not connected
		if not self.connected:
			return True

		self.connected = False

		self.library.shutdown()

		# Disconnect the subtype
		self._disconnect()

		#
		self.library = None


		pympe.events.device_disconnected(self)

	def write(self, *args, **kwargs):
		return self._write(*args, **kwargs)
	def delete(self, *args, **kwargs):
		return self._delete(*args, **kwargs)


	#def save(self, *args, **kwargs):
	#	return self._save(*args, **kwargs)

	def initalize(self, *args, **kwargs):
		return self._initalize(*args, **kwargs)
	def syncronize(self, *args, **kwargs):
		return self._syncronize(*args, **kwargs)

	# These will be overridden
	def _setup(self, *args, **kwargs): pass
	def _connect(self, *args, **kwargs): pass
	def _disconnect(self, *args, **kwargs): pass

	def _write(self, *args, **kwargs): pass
	def _delete(self, *args, **kwargs): pass

	#def _save(self, *args, **kwargs): pass

	def _initalize(self, *args, **kwargs): pass
	def _syncronize(self, *args, **kwargs): pass
