import os, pympe

class Config:
	def __init__(self, fileName=None, path=None, mainConfig=False):
		if fileName: self.fileName = pympe.configDirectory+os.sep+fileName
		elif path: self.fileName = path

		self.mainConfig = mainConfig

		self.sectionOrder = []
		self.sections     = {}

		self.eventSendQueue = {}

	def module_start(self):
		self.load()

	def module_stop(self):
		self.save()

	# Change from getters and setters
	def __getitem__(self, section):
		if section in self.sections:
			return self.sections[section][1]

	def keys(self):
		return self.sections.keys()

	###

	def load(self, sendEvents=False):
		self.sectionOrder = []
		self.sections = {}

		if os.path.exists(self.fileName):
			with open(self.fileName, "r") as f:
				for line in [x.strip() for x in f.readlines()]:
					if line.startswith("[") and line.endswith("]"):
						self.sections[line[1:-1]] = [[],{}]
						self.sectionOrder.append(line[1:-1])

					elif self.sectionOrder:
						split = [x.strip() for x in line.split("=",1)]

						if len(split) > 1:
							self.sections[self.sectionOrder[-1]][0].append(split[0])
							self.sections[self.sectionOrder[-1]][1][split[0]] = eval(split[1])

							if sendEvents:
								if self.sectionOrder[-1] in self.eventSendQueue:
									self.eventSendQueue[self.sectionOrder[-1]][split[0]] = self.sections[self.sectionOrder[-1]][1][split[0]]
								else:
									self.eventSendQueue[self.sectionOrder[-1]] = {split[0]:self.sections[self.sectionOrder[-1]][1][split[0]]}


			self.check_defaults()

			if sendEvents:
				self.send_events()

		else:
			self.reset()
			self.save()

		pympe.debug('Loaded Config {}'.format(self.fileName))

	def save(self):
		output = []

		for section in self.sectionOrder:
			output.append("[%s]"%section)

			for key in self.sections[section][0]:
				output.append("%s = %s"%(key, repr(self.sections[section][1][key])))

			output.append("")

		with open(self.fileName, "w") as f:
			f.write("\n".join(output))

		pympe.debug('Saved Config {}'.format(self.fileName))

	def get(self, section, key=None):
		if section in self.sections:
			if key == None: return self.sections[section]
			if key in self.sections[section][1]:
				return self.sections[section][1][key]

	def set(self, section, key, value, sendEvent=True):
		if section in self.sections:
			if key in self.sections[section][1] and self.sections[section][1][key] == value:
				return

			self.sections[section][1][key] = value

			if not key in self.sections[section][0]:
				self.sections[section][0].append(key)

		else:
			self.sections[section] = [[key],{key:value}]
			self.sectionOrder.append(section)

		if self.mainConfig:
			if sendEvent:
				pympe.events.config_key_changed(section, key, value)
				pympe.events.config_changed(section, {key:value})
			else:
				if section in self.eventSendQueue:
					self.eventSendQueue[section][key] = value
				else:
					self.eventSendQueue[section] = {key:value}

	def set_first(self, section, key, value):
		if not section in self.sections:
			self.sections[section] = [[key],{key:value}]
			self.sectionOrder.append(section)
			if self.mainConfig: pympe.events.config_key_added(section, key, value)
		else:
			if not key in self.sections[section][0]:
				self.sections[section][0].append(key)
				self.sections[section][1][key] = value
				if self.mainConfig: pympe.events.config_key_added(section, key, value)

	def send_events(self):
		if not self.mainConfig:
			return

		for section, values in self.eventSendQueue.iteritems():
			pympe.events.config_changed(section, values)

			for k,v in values.iteritems():
				pympe.events.config_key_changed(section, k, v)

		self.eventSendQueue = {}

	def reset(self):
		self.sectionOrder = []
		self.sections = {}

		self.check_defaults()

		pympe.debug('Reset Config {}'.format(self.fileName))

	def check_defaults(self):
		if not self.mainConfig: return

		# Main settings
		self.set_first("main", "name", "Pympe")
		self.set_first("main", "version", pympe.version)
		self.set_first('main', 'oneInstanceLock', True)

		# Db settings
		self.set_first("database", "locations", [])

		# Import settings
		self.set_first("import", "format", "Mp3 (Variable Bit Rate)")
		self.set_first("import", "quality", 5)
		self.set_first("import", "renamingRules", "%albumartist%/%year% %album%/%tracknumber% %title%")
		self.set_first("import", "renamingTags", ["artist", "albumartist", "album", "title", "genre", "tracknumber", "year", "discnumber"])

		# Window settings
		self.set_first("window", "size", (800, 600))
		self.set_first("window", "position", (50, 50))
		self.set_first("window", "maximized", False)

		# Ui settings
		self.set_first("ui", "tabs_left_page", 0)
		self.set_first("ui", "tabs_right_page", 0)
		self.set_first("ui", "tabs_config_page", 0)
		self.set_first("ui", "seperate_playlist", False)
		self.set_first("ui", "right_panel_size", 300)
		self.set_first('ui', 'use_theatre_unlock_password', False)
		self.set_first('ui', 'theatre_unlock_password', '')
		self.set_first('ui', 'theatre_lock_mode', 0)
		self.set_first('ui', 'settingsDialogSelectedTab', 0)
		self.set_first('ui', 'show_debug_tab', False)

		# Np tab settings
		self.set_first('np', 'enabled', True)
		self.set_first("np", "use_custom_background", False)
		self.set_first("np", "background_color", (255, 255, 255))
		self.set_first("np", "enable_reflection", True)
		self.set_first("np", "rounded_edges", True)
		self.set_first("np", "display_mode", 0)
		self.set_first("np", "artwork_buffer", .5)
		self.set_first('np', 'show_title_and_artist', False)
		self.set_first('np', 'enable_shadow', False)

		# Player settings
		self.set_first("player", "shuffle", 0)
		self.set_first("player", "loop",  0)
		self.set_first("player", "volume",  1.0)
		self.set_first("player", "gapless", True)
		self.set_first('player', 'useTagsFromFile', False)
		self.set_first('player', 'resumePlayState', False)
		self.set_first('player', 'resumePlayPosition', False)
		self.set_first('player', 'savedPlayState', (0, 0)) # (state, position) {0:stopped, 1:playing, 2:paused}

		# Plugin settings
		self.set_first("plugins", "directory", "plugins")
		self.set_first("plugins", "list", {}) # {"name":ENABLED}
		self.set_first("plugins", "tab_left_page", None)
		self.set_first("plugins", "tab_right_side", None)

		# Metadata editor settings
		self.set_first("metadata", "enabled", [(1, "title"), (1, "artist"), (1, "album"), (1, "albumartist"), (1, "tracknumber"), (1, "disc"), (1, "genre"), (1, "year"), (1, "art")])

