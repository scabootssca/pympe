#!/bin/sh

if [ "$(id -u)" != "0" ]; then
	echo "This install script needs to be ran as root to continue."
	exit 1
fi

# First install the .desktop file
while true; do
	read -p 'Would you like to install the .desktop file? [Y/n]: ' yn
	case $yn in
		[Yy]* ) xdg-desktop-menu install --novendor ./pympe.desktop; break;;
		[Nn]* ) break;;
		* ) continue;;
	esac
done

# Them install the binary script
echo 'The pympe bash script needs to have PROGRAM_DIR changed to the current';
cp ./pympe /usr/bin
chmod +xr /usr/bin/pympe

# Install the Moka if wanted
while true; do
	read -p 'Install Moka icons? [Y/n]: ' yn
	case $yn in
		[Yy]* )
			for SIZE in "16" "22" "24" "32" "48" "64" "96" "256"; do
				cp "Moka/"$SIZE".png" "/usr/share/icons/Moka/"$SIZE"x"$SIZE"/apps/media-player-pympe.png";
				ln -s "/usr/share/icons/Moka/"$SIZE"x"$SIZE"/apps/media-player-pympe.png" "/usr/share/icons/Moka/"$SIZE"x"$SIZE"/apps/pympe.png";
			done
			gtk-update-icon-cache /usr/share/icons/Moka/
			echo 'Moka icons sucessfully installed.';
			break;;
		[Nn]* ) break;;
		* ) continue;;
	esac
done

## Install the Faenza if wanted
#while true; do
#xx
	#read -p 'Install Faenza icons? [Y/n]: ' yn
	#case $yn in
		#[Yy]* )
			#for SIZE in "16" "22" "24" "32" "48" "64" "96" "256"; do
				#cp "Moka/"$SIZE".png" "/usr/share/icons/Moka/"$SIZE"x"$SIZE"/apps/media-player-pympe.png";
				#ln -s "/usr/share/icons/Moka/"$SIZE"x"$SIZE"/apps/media-player-pympe.png" "/usr/share/icons/Moka/"$SIZE"x"$SIZE"/apps/pympe.png";
			#done
			#echo 'Moka icons sucessfully installed.';
			#gtk-update-icon-cache /usr/share/icons/Faenza/
			#break;;
		#[Nn]* ) break;;
		#* ) continue;;
	#esac
#done

# Install the hicolor default icons
for SIZE in "16" "22" "24" "32" "36" "48" "64" "72" "96" "128" "192" "256"; do
	cp "icon$SIZE.png" "/usr/share/icons/hicolor/"$SIZE"x"$SIZE"/apps/media-player-pympe.png";
	ln -s "/usr/share/icons/hicolor/"$SIZE"x"$SIZE"/apps/media-player-pympe.png" "/usr/share/icons/hicolor/"$SIZE"x"$SIZE"/apps/pympe.png";
done

cp "iconScalable.svg" "/usr/share/icons/hicolor/scalable/apps/media-player-pympe.svg"
ln -s "/usr/share/icons/hicolor/scalable/apps/media-player-pympe.svg" "/usr/share/icons/hicolor/scalable/apps/pympe.svg"
gtk-update-icon-cache /usr/share/icons/hicolor/

##xdg-icon-resource install --size 48 --theme Faenza iconname.png

#gtk-update-icon-cache
