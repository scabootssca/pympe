#!/usr/bin/env python 

import atexit
import gobject
import glib
import sys
import os
import dbus
import gtk

try:
	from dockmanager.dockmanager import DockManagerItem, DockManagerSink, DOCKITEM_IFACE, RESOURCESDIR
	from signal import signal, SIGTERM
	from sys import exit
except ImportError, e:
	exit()
	
		
pympebus = "org.pympe.Pympe"
playerpath = "/org/pympe/Pympe"
playeriface = "org.pympe.Pympe"
	
albumArtTempFile = "/tmp/dockmanager_%s_pympe_helper"%os.getenv('USERNAME')
overlayStyle = 1
showTooltipPos = True
showBadge = False

class PympeItem(DockManagerItem):
 	def __init__(self, sink, path):
 		DockManagerItem.__init__(self, sink, path)

		self.bus.add_signal_receiver(self.handle_name_owner_changed, dbus_interface='org.freedesktop.DBus', signal_name='NameOwnerChanged')
		busInterface = dbus.Interface(self.bus.get_object("org.freedesktop.DBus", "/org/freedesktop/DBus"), "org.freedesktop.DBus")
		busInterface.ListNames(reply_handler=self.handle_list_names, error_handler=lambda error: None)
		
 		self.bus.add_signal_receiver(self.SignalTrackChanged, "TrackChanged",  playeriface, pympebus, playerpath)
		self.bus.add_signal_receiver(self.SignalStateChanged, "StateChanged",  playeriface, pympebus, playerpath)
		self.bus.add_signal_receiver(self.SignalPlayModeChanged, "PlayModeChanged",  playeriface, pympebus, playerpath)

		#self.player = dbus.Interface(self.bus.get_object(pympebus, playerpath), playeriface)
		self.timer = 0
		self.tooltip = ""
		
		#self.update_menu()
		#
		#if self.player.GetCurrent():
		#	self.update_badge()
		#	self.update_track_info()
		#
		#if self.player.GetState() == "playing":
		#	self.start_timer()

	def handle_list_names(self, names):
		if pympebus in names:
			self.init_pympe_dbus()

	def handle_name_owner_changed(self, name, oldOwner, newOwner):
		if name == pympebus:
			if newOwner:
				self.init_pympe_dbus()
			else:
				self.player = None
				self.stop_timer()
				self.update_menu()		
				self.update_badge()
				self.update_track_info()
		
	def init_pympe_dbus(self):
		self.player = dbus.Interface(self.bus.get_object(pympebus, playerpath), playeriface)
		
		self.update_menu()
		
		if self.player.GetCurrent():
			self.update_badge()
			self.update_track_info()
		
		if self.player.GetState() == "playing":
			self.start_timer()

	def update_menu(self):
		for k in self.id_map.keys(): self.remove_menu_item(k)
		if not self.player: return
		
		self.add_menu_item("Previous", "media-skip-backward", "Playback Controls")
		if self.player.GetState() == "playing": self.add_menu_item("Pause", "media-playback-pause", "Playback Controls")
 		else: self.add_menu_item("Play", "media-playback-start", "Playback Controls")
 		self.add_menu_item("Next", "media-skip-forward", "Playback Controls")
 		
 		loop = self.player.GetLoop()
 		self.add_menu_item("Repeat Mode (%s)"%('Off' if loop == 'None' else ('One' if loop == 'Track' else 'All')), "media-playlist-repeat", "Play Order Controls")
 		self.add_menu_item("Shuffle (%s)"%('On' if self.player.GetShuffle() else 'Off'), "media-playlist-shuffle", "Play Order Controls")
		
	def update_badge(self):
		if not self.player:
			self.reset_badge()
			return False
		
		if self.player.IsPlaying():
			position = self.player.GetPosition()
			string = '%i:%02i' % (position / 60, position % 60)
			
			if showBadge: self.set_badge(string)
			if showTooltipPos and self.tooltip:
				if self.tooltip[-1] == ")": self.set_tooltip(self.tooltip[:-1]+"/"+string+")")
				else: self.set_tooltip(self.tooltip+" ("+string+")")
		else:
			self.reset_badge()
			
		return True
		
	def update_track_info(self):
		if not self.player:
			self.reset_tooltip()
			self.reset_icon()
			return
		
		# The song info tooltip
		(artist, title, length) = (self.player.GetTag("artist"), self.player.GetTag("title"), self.player.GetLength())
		if length > 0: self.tooltip = '%s - %s (%i:%02i)' % (artist, title, length/60, length%60)
		else: self.tooltip = '%s - %s' % (artist, title)
		if self.player.IsPlaying(): self.set_tooltip(self.tooltip)
		else: self.reset_tooltip()
		
		# The album art
		if self.player.IsPlaying():
			artwork = self.player.GetArtwork()
			if artwork:
				with open(albumArtTempFile, "wb") as f: f.write( "".join([str(x) for x in artwork]))
				self.add_art_overlay(overlayStyle)
			
				# Add overlay to cover
				if os.path.isfile(albumArtTempFile): self.set_icon(albumArtTempFile)
				else: self.reset_icon()
			else:
				self.reset_icon()
		else:
			self.current_arturl = ""
			self.reset_icon()
		return True
		
	def add_art_overlay(self, overlay=1):
		if overlay == 0: return

		try: pb = gtk.gdk.pixbuf_new_from_file(albumArtTempFile)
		except Exception, e: return
		
		pb_result = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB, True, 8, 250, 250)
		pb_result.fill(0x00000000)

		if overlay == 1:
			overlayfile = os.path.join(RESOURCESDIR, "albumoverlay_jewel.png")
			pb.composite(pb_result, 30, 21, 200, 200, 30, 21, 200.0/pb.get_width(), 200.0/pb.get_height(), gtk.gdk.INTERP_BILINEAR, 255)
		elif overlay == 2:
			overlayfile = os.path.join(RESOURCESDIR, "albumoverlay_vinyl.png")
			pb.composite(pb_result, 3, 26, 190, 190, 3, 26, 190.0/pb.get_width(), 190.0/pb.get_height(), gtk.gdk.INTERP_BILINEAR, 255)
		else:
			return

		pb_overlay = gtk.gdk.pixbuf_new_from_file_at_size(overlayfile, 250, 250)
		pb_overlay.composite(pb_result, 0, 0, 250, 250, 0, 0, 1, 1, gtk.gdk.INTERP_BILINEAR, 255)
		pb_result.save(albumArtTempFile, "png", {})

	def menu_pressed(self, menu_id):
		if self.id_map[menu_id] == "Play": self.player.Play()
		elif self.id_map[menu_id] == "Pause": self.player.Pause()
		elif self.id_map[menu_id] == "Next": self.player.Next()
		elif self.id_map[menu_id] == "Previous": self.player.Previous()
		elif self.id_map[menu_id].startswith("Shuffle"): self.player.SetShuffle(not self.player.GetShuffle())
		elif self.id_map[menu_id].startswith("Repeat"): self.player.SetLoopNext()

	def start_timer(self):
		if not self.timer > 0:
			self.timer = gobject.timeout_add(1000, self.update_badge)
	
	def stop_timer(self):
		if self.timer > 0:
			gobject.source_remove(self.timer)
			self.timer = 0

 	def SignalTrackChanged(self):
		self.update_track_info()
		self.update_badge()
 	
 	def SignalStateChanged(self, state):
		self.update_menu()
		self.update_badge()
		
		if state == "playing": self.start_timer()
		else: self.stop_timer()
		
	def SignalPlayModeChanged(self, mode):
		self.update_menu()

class PympeSink(DockManagerSink):
 	def item_path_found(self, pathtoitem, item):
 		if item.Get(DOCKITEM_IFACE, "DesktopFile", dbus_interface="org.freedesktop.DBus.Properties").endswith("pympe.desktop"):
 			self.items[pathtoitem] = PympeItem(self, pathtoitem)
 
pympesink =  PympeSink()
 
def cleanup():
	pympesink.dispose()

if __name__ == "__main__":
	mainloop = gobject.MainLoop(is_running=True)

	atexit.register(cleanup)
	signal(SIGTERM, lambda signum, stack_frame: exit(1))

	while mainloop.is_running():
		mainloop.run()
