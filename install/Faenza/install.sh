#!/bin/sh


cp iconFaenza16.png /usr/share/icons/Faenza/apps/16/media-player-pympe.png
cp iconFaenza22.png /usr/share/icons/Faenza/apps/22/media-player-pympe.png
cp iconFaenza24.png /usr/share/icons/Faenza/apps/24/media-player-pympe.png
cp iconFaenza32.png /usr/share/icons/Faenza/apps/32/media-player-pympe.png
cp iconFaenza48.png /usr/share/icons/Faenza/apps/48/media-player-pympe.png
cp iconFaenza64.png /usr/share/icons/Faenza/apps/64/media-player-pympe.png
cp iconFaenza96.png /usr/share/icons/Faenza/apps/96/media-player-pympe.png
cp iconFaenzaScalable.svg /usr/share/icons/Faenza/apps/scalable/media-player-pympe.svg

ln -s /usr/share/icons/Faenza/apps/16/media-player-pympe.png /usr/share/icons/Faenza/apps/16/pympe.png
ln -s /usr/share/icons/Faenza/apps/22/media-player-pympe.png /usr/share/icons/Faenza/apps/22/pympe.png
ln -s /usr/share/icons/Faenza/apps/24/media-player-pympe.png /usr/share/icons/Faenza/apps/24/pympe.png
ln -s /usr/share/icons/Faenza/apps/32/media-player-pympe.png /usr/share/icons/Faenza/apps/32/pympe.png
ln -s /usr/share/icons/Faenza/apps/48/media-player-pympe.png /usr/share/icons/Faenza/apps/48/pympe.png
ln -s /usr/share/icons/Faenza/apps/64/media-player-pympe.png /usr/share/icons/Faenza/apps/64/pympe.png
ln -s /usr/share/icons/Faenza/apps/96/media-player-pympe.png /usr/share/icons/Faenza/apps/96/pympe.png
ln -s /usr/share/icons/Faenza/apps/scalable/media-player-pympe.svg /usr/share/icons/Faenza/apps/scalable/pympe.svg

gtk-update-icon-cache