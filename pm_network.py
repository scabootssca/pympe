import pympe
import os, urllib, pm_helpers, gzip
from hashlib import md5

#class MyURLOpener(urllib.FancyURLopener):
	#def __init__(self):
		##self.version = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.899.0 Safari/535.6"
		#self.version = "Pympe/1.3"
		#urllib.FancyURLopener.__init__(self)

def get_url(url, finishedCallback=None, failureCallback=None, progressCallback=None, refresh=False, returnData=False, data=None, callbackArgs=None, fromThread=False):
	"""Returns a link to the cache file and is updated if needed.
	url      = http://somewebsite.com or http://somewebsite.com/somefile.jpg
	callback = function to call when file is ready
	report   = function to recieve periodic updates about the download progress
	refresh  = True to force redownload of file
	returnData = True to return file data False to return filename
	"""

	def callback_wrapper(callback, result, callbackArgs):
		@pm_helpers.unthread
		def unthread(callback, result, callbackArgs):
			#if result == None:
				#if callbackArgs: callback(*callbackArgs)
				#else: callback()
			#else:
			if callbackArgs: callback(result, *callbackArgs)
			else: callback(result)

		if fromThread: unthread(callback, result, callbackArgs)
		else:
			#if result == None:
				#if callbackArgs: callback(*callbackArgs)
				#else: callback()
			#else:
			if callbackArgs: callback(result, *callbackArgs)
			else: callback(result)

	urlToFetch = url

	if data:
		data = urllib.urlencode(sorted(data.items()))
		url += data

	urlIsCached = pympe.cache.has(url)

	if (not urlIsCached) or refresh:
		reportFunc = lambda numblocks, blocksize, filesize: progressCallback((numblocks*blocksize+0.0)/filesize, numblocks*blocksize, filesize)

		# Setup the cache filename and the opener
		cacheFilename = pympe.cache.filename(url)
		#opener = MyURLOpener()

		try:
			reply = urllib.urlretrieve(urlToFetch, cacheFilename, reportFunc if progressCallback else None, data)
			pympe.cache.write_info(url)
		except:
			if failureCallback: callback_wrapper(failureCallback, None, callbackArgs)
			pympe.error('Problem Fetching Url: {}'.format(url), False, False)

			pympe.debug('url: {0}\nfailureCallback: {1}\nprogressCallback: {2}\nrefresh: {3}\nreturnData: {4}\ndata: {5}\ncallbackArgs: {6}'.format(\
						url, finishedCallback, failureCallback, progressCallback, refresh, returnData, data, callbackArgs))

			return False

		# Uncompress it if needed
		encoding = reply[1].getheader("Content-Encoding")
		if encoding == "gzip":
			unzipped = gzip.GzipFile(reply[0]).read()
			with open(reply[0], "w") as f: f.write(unzipped)

	if returnData: result = pympe.cache.get(url)
	else: result = pympe.cache.filename(url)

	if finishedCallback: callback_wrapper(finishedCallback, result, callbackArgs)

	return result

@pm_helpers.threaded
def get_url_threaded(url, finishedCallback=None, failureCallback=None, progressCallback=None, refresh=False, returnData=False, callbackArgs=None, fromThread=True):
	get_url(url, finishedCallback, failureCallback, progressCallback, refresh, returnData, None, callbackArgs, fromThread)

def post_url(url, data, finishedCallback=None, failureCallback=None, progressCallback=None, refresh=False, returnData=False, callbackArgs=None):
	return get_url(url, finishedCallback, failureCallback, progressCallback, refresh, returnData, data, callbackArgs)

@pm_helpers.threaded
def post_url_threaded(url, data, finishedCallback=None, failureCallback=None, progressCallback=None, refresh=False, returnData=False, callbackArgs=None):
	return get_url(url, finishedCallback, failureCallback, progressCallback, refresh, returnData, data, callbackArgs, fromThread=True)
