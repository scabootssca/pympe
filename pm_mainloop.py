import threading, pm_helpers, time

class Timer:
	def __init__(self, seconds, repeat, function, *args, **kwargs):
		self.seconds = seconds
		self.repeat = repeat
		self.function = function
		self.args = args
		self.kwargs = kwargs

		self.executeCount = 0
		self.executeTime = time.time()+seconds

	def stop(self):
		pm_mainloop.timers.remove(self)

class MainLoop:
	def __init__(self):
		self.timers = []
		self.running = False

	def add_timer(self, seconds, repeat, function, *args, **kwargs):
		timer = Timer(seconds, repeat, function, *args, **kwargs)
		self.timers.append(timer)
		return timer

	def remove_timer(self, timer):
		if timer in self.timers:
			self.timers.remove(timer)
		else:
			pympe.debug('Tried to remove timer that didn\'t exist: %s'%repr(timer))

	def one_iteration(self):
		loopTime = time.time()

		for timer in self.timers:
			if loopTime >= timer.executeTime:
				pm_helpers.unthread_function(timer.function, *timer.args, **timer.kwargs)
				timer.executeCount += 1

				# Push the execute time forward
				timer.executeTime += timer.seconds

				# For infinite repeat
				if timer.repeat == -1: continue

				# If it's executed enough then kill the timer
				if timer.executeCount >= timer.repeat:
					self.timers.remove(timer)



	@pm_helpers.threaded
	def iterate(self):
		'''@=Pympe MainLoop'''
		while self.running:
			self.one_iteration()
			time.sleep(.5)

	def module_start(self):
		self.running = True
		self.iterate()

	def module_stop(self):
		self.running = False

#class Mainloop:
	#def __init__(self):
		#self.timers = {}
		#self.ticking = False
		#self.timer = threading.Timer(1.0, self.tick)

	#def module_start(self):
		#self.ticking = True
		#if self.timers: self.timer.start()

	#def module_stop(self):
		#self.ticking = False
		#self.timer.cancel()

	#@pm_helpers.unthread
	#def run_ticks(self, functions):
		#for function in functions:
			#function()

	#def tick(self):
		#print 'Tick'
		#tickTime = time.time()
		#runFunctions = []

		#for function in self.timers.values():
			#if not self.ticking: return
			#if function[5] and tickTime >= function[3]:
				## Run the function and add one to the run count, if repeat set check and see if we're over it
				#runFunctions.append(function[0])
				#function[3] = tickTime+function[1]
				#function[4] += 1
				#if function[2] and function[4] >= function[2]:
					#self.timers.remove(function)

		#if runFunctions:
			#self.run_ticks(runFunctions)

		#self.timer.run()
		##if self.ticking and not self.timer.is_alive(): self.timer.start()

	#def pause(self, timerId):
		#self.timers[timerId][5] = False

	#def resume(self, timerId):
		#self.timers[timerId][5] = True

	#def add_timer(self, function, interval=1, repeat=0, runNow=False):
		#timerId = time.time()
		#try:
			#exists = True
			#while exists:
				#exists = self.timers[timerId]
				#timerId += .00001
		#except:
			#pass

		#self.timers[timerId] = [function, interval, repeat, time.time()+interval, 0, runNow]
		#if runNow: function()
		#if self.ticking and not self.timer.is_alive(): self.timer.start()
		#return timerId
