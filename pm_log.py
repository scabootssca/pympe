import logger

def init():
	''' Creates the directory to log if needed and inits logger module '''

def stop():
	''' Cleans up after the logger, closes log files '''

def info(*args, **kwargs):
	''' Writes info to the log '''

def debug(*args, **kwargs):
	''' Writes debug info to the log '''

def error(*args, **kwargs):
	''' Writes an error to the log '''
