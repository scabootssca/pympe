# -*- coding: iso-8859-15 -*-
import wx
import pm_language, pm_helpers
import pympe
import time, threading

from pm_ui.pm_ui_now_playing_tab import TabNowPlaying
from pm_ui.pm_ui_library_tab import TabLibrary
from pm_ui.pm_ui_debug_tab import TabDebug
from pm_ui.pm_ui_playlist_tab import TabPlaylist
from pm_ui.pm_ui_status_tab import TabStatus
from pm_ui.pm_ui_settings import SettingsDialog
from pm_ui.pm_ui_plugins import PluginDialog

class FullscreenFrame(wx.Frame):
	def __init__(self, ui):
		wx.Frame.__init__(self, ui, title='Fullscreen Frame', pos=ui.GetPosition())
		self.ui = ui
		self.timer = wx.Timer(self, 100)
		self.timer.Start(1000, True)
		self.hidden = False
		self.mouseOut = False
		self.lockMode = 0 if not pympe.config.get('ui', 'use_theatre_unlock_password') else pympe.config.get('ui', 'theatre_lock_mode')
		self.unlocked = False

		self.Bind(wx.EVT_TIMER, self.event_on_timer, self.timer)
		self.ui.uiTabNowPlaying.motionFunction = self.event_motion
		self.ui.uiTabNowPlaying.leaveFunction = self.event_mouse_leave

		#self.ui.uiTabsLeft.RemovePage(0)
		self.ui.uiTabNowPlaying.Reparent(self)

		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.Add(self.ui.uiTabNowPlaying, 1, wx.EXPAND)

	# The controls
		#['Allow Playback Controls', 'Allow Only Play/Pause', 'Disallow All']
		self.ui.uiSizerFooter.Detach(self.ui.uiSizerControls)
		self.ui.uiSizerFooter.Detach(self.ui.uiSizerNowPlaying)
		self.ui.uiSizerMain.Detach(self.ui.uiSizerProgress)
		(lti, lar, lal) = (self.ui.uiControls["labelTitle"], self.ui.uiControls["labelArtist"], self.ui.uiControls["labelAlbum"])
		for element in (lti, lar, lal):
			self.ui.uiSizerNowPlaying.Detach(element)
			element.Reparent(self)

		self.elements = ["menu", "play", "previous", "stop", "next", "shuffle", "loop", "volume", "progressCurrent", "progressSlider", "progressTotal"]

		for element in self.elements: self.ui.uiControls[element].Reparent(self)


		self.extras = [wx.StaticText(self, -1, " by "), wx.StaticText(self, -1, " from ")]

		if not pympe.player.currentInfo or pympe.player.playingVideo:
			self.extras[0].Hide()
			self.extras[1].Hide()

		if self.lockMode != 0:
			for element in self.elements[2:]:
				self.ui.uiControls[element].Enable(False)

		if self.lockMode == 2:
			self.ui.uiControls['play'].Enable(False)

		self.controlSizer = wx.GridBagSizer()
		self.controlSizer.AddGrowableCol(5)
		self.controlSizer.Add(self.ui.uiSizerControls, (0, 0), (2, 1), wx.RIGHT, 5)
		self.controlSizer.Add(lti, (0, 1), flag=wx.ALIGN_CENTER_VERTICAL)
		self.controlSizer.Add(self.extras[0], (0, 2), flag=wx.ALIGN_CENTER_VERTICAL)
		self.controlSizer.Add(lar, (0, 3), flag=wx.ALIGN_CENTER_VERTICAL)
		self.controlSizer.Add(self.extras[1], (0, 4), flag=wx.ALIGN_CENTER_VERTICAL)
		self.controlSizer.Add(lal, (0, 5), flag=wx.ALIGN_CENTER_VERTICAL)
		lal.SetFont( wx.Font(9,  wx.DEFAULT, wx.NORMAL, wx.BOLD))
		self.controlSizer.Add(self.ui.uiSizerProgress, (1, 1), (1, 5), flag=wx.EXPAND)
		sizer.Add(self.controlSizer, 0, wx.EXPAND|wx.ALL, 2)

		self.Bind(wx.EVT_MOTION, self.event_mouse_leave)

		box = wx.BoxSizer()
		box.Add(sizer, 1, wx.EXPAND)
		self.SetSizer(box)

		pympe.events.player_current_updated.subscribe(self.mpevent_player_current_updated)

		self.ShowFullScreen(True, style=wx.FULLSCREEN_ALL)

	def mpevent_player_current_updated(self, event, tags):
		if pympe.player.playingVideo:
			self.extras[0].Hide()
			self.extras[1].Hide()
		elif not self.hidden:
			self.extras[0].Show()
			self.extras[1].Show()
		self.controlSizer.Layout()

	def unlock(self):
		self.timer.Stop()
		for element in self.elements:
			self.ui.uiControls[element].Enable()
		self.unlocked = True
		self.event_motion(False, 3000)

	def stop(self):
		self.timer.Stop()
		self.ui.uiTabNowPlaying.motionFunction = None
		self.ui.uiTabNowPlaying.leaveFunction = None

		self.ui.uiTabNowPlaying.Reparent(self.ui.uiTabsLeft)
		self.ui.uiTabsLeft.InsertPage(0, self.ui.uiTabNowPlaying, pm_language.uiTabNowPlaying)


		(lti, lar, lal) = (self.ui.uiControls["labelTitle"], self.ui.uiControls["labelArtist"], self.ui.uiControls["labelAlbum"])
		lal.SetFont( wx.Font(9,  wx.DEFAULT, wx.NORMAL, wx.NORMAL))
		for element in (lti, lar, lal):
			element.Show()
			self.controlSizer.Detach(element)
			element.Reparent(self.ui)
			self.ui.uiSizerNowPlaying.Add(element,  0, wx.EXPAND)

		# Return them
		for element in self.elements:
			self.ui.uiControls[element].Reparent(self.ui)
			self.ui.uiControls[element].Enable()
			self.ui.uiControls[element].Show()
		self.controlSizer.Detach(self.ui.uiSizerControls)
		self.controlSizer.Detach(self.ui.uiSizerNowPlaying)
		self.controlSizer.Detach(self.ui.uiSizerProgress)
		self.ui.uiSizerFooter.Insert(0, self.ui.uiSizerControls, 0, wx.LEFT, 10)
		self.ui.uiSizerFooter.Insert(2, self.ui.uiSizerNowPlaying)
		self.ui.uiSizerMain.Insert(4, self.ui.uiSizerProgress, 0, wx.EXPAND|wx.LEFT|wx.RIGHT, 10)

		self.ui.Layout()
		pympe.events.player_current_updated.unsubscribe(self.mpevent_player_current_updated)

		self.Destroy()


	def event_on_timer(self, event):
		if not self.hidden:
			#print(time.time(), "Hide")
			self.hidden = True
			self.SetCursor(wx.StockCursor(wx.CURSOR_BLANK))
			for element in self.elements: self.ui.uiControls[element].Hide()
			self.extras[0].Hide()
			self.extras[1].Hide()
			[x.Hide() for x in (self.ui.uiControls["labelTitle"], self.ui.uiControls["labelArtist"], self.ui.uiControls["labelAlbum"])]
			#self.SetCursor(wx.StockCursor(wx.CURSOR_BLANK))

			if self.unlocked:
				self.unlocked = False
				if self.lockMode != 0:
					for element in self.elements[1:]:
						self.ui.uiControls[element].Enable(False)

				if self.lockMode == 2:
					self.ui.uiControls['play'].Enable(False)

			self.Layout()

	def event_motion(self, event, timerLength=1000):
		if self.mouseOut: self.timer.Start(timerLength, True)
		self.mouseOut = False
		if self.hidden:
			self.hidden = False
			self.SetCursor(wx.StockCursor(wx.CURSOR_ARROW))
			for element in self.elements: self.ui.uiControls[element].Show()
			if pympe.player.currentInfo:
				self.extras[0].Show()
				self.extras[1].Show()
			[x.Show() for x in (self.ui.uiControls["labelTitle"], self.ui.uiControls["labelArtist"], self.ui.uiControls["labelAlbum"])]
			#pympe.ui.app.GetTopWindow().SetCursor(wx.StockCursor(wx.CURSOR_DEFAULT))
			self.Layout()

		self.timer.Start(timerLength, True)

	def event_mouse_leave(self, event):
		self.mouseOut = True
		self.timer.Stop()

class uiMain(wx.Frame):
	def __init__(self, ui):
		self.ui = ui
		self.fullscreenFrame = None
		self.pluginDialog = None
		self.settingsDialog = None
		self.savedFullscreenTab = 0
		self.theatreFrame = None
		self.videoWindowHandle = None
		self.activeTitlePanel = None
		self.iconized = False
		self.menuHookList = []

	def start(self):
		wx.Frame.__init__(self, None, title=pm_language.uiWindowTitle, size=pympe.config.get("window", "size"), pos=pympe.config.get("window", "position"))
		self.Maximize(pympe.config.get("window", "maximized"))

		# Set the icon
		self.SetIcon(wx.ArtProvider.GetIcon('media-player-pympe', wx.ART_CMN_DIALOG))

		# The main sizer
		self.uiSizerMain = wx.BoxSizer(wx.VERTICAL)

		# For config change events
		pympe.events.config_key_changed.subscribe(self.mpevent_config_key_changed)

		# Create The Tabs And Splitter
		################################
		self.uiSplitterMain = wx.SplitterWindow(self, style=wx.SP_LIVE_UPDATE)
		self.uiTabsLeft = wx.Notebook(self.uiSplitterMain,  0)
		self.uiTabsRight = wx.Notebook(self.uiSplitterMain, 1)
		self.uiTabLibrary = TabLibrary(self.uiTabsLeft, pympe.library)
		self.uiSizerMain.Add(self.uiSplitterMain, 1, wx.EXPAND|wx.LEFT|wx.RIGHT, 10)

		# Set it to the default tab at first
		self.activeTitlePanel = self.uiTabLibrary.uiPanelTracks

		# Now playing tab
		self.uiTabNowPlaying = None
		self.show_now_playing(pympe.config.get('np', 'enabled'))

		# Add the tabs to the notebook
		self.uiTabsLeft.AddPage(self.uiTabLibrary,    pm_language.uiTabLibrary)

		if pympe.config.get('ui', 'show_debug_tab'):
			self.uiTabDebug = TabDebug(self.uiTabsLeft)
			self.uiTabsLeft.AddPage(self.uiTabDebug,     pm_language.uiTabDebug)
		else:
			self.uiTabDebug = None

		# Split it
		self.uiSplitterMain.SetSashGravity(0.8)
		self.uiSplitterMain.SetMinimumPaneSize(1)

		# The playlist
		if not pympe.config.get("ui", "seperate_playlist"):
			self.uiTabPlaylist = TabPlaylist(self.uiTabsLeft)
			self.uiTabsLeft.InsertPage(1, self.uiTabPlaylist,  pm_language.uiTabPlaylist)
			self.uiSplitterMain.Initialize(self.uiTabsLeft)
			self.uiTabsRight.Hide()
		else:
			self.uiTabPlaylist = TabPlaylist(self.uiTabsRight)
			self.uiTabsRight.AddPage(self.uiTabPlaylist,  pm_language.uiTabPlaylist)
			self.uiSplitterMain.SplitVertically(self.uiTabsLeft, self.uiTabsRight, -pympe.config.get("ui", "right_panel_size"))

		# Bind it
		self.uiTabsLeft.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.event_notebook_page_changed)
		self.uiTabsRight.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.event_notebook_page_changed)

		# Create The Controls
		################################
		uiGaugeProgress = wx.Gauge(self, size=(-1, 5))
		if self.ui.useBitmaps:
			import wx.lib.buttons as libb
			uiButtonPrevious = wx.BitmapButton(self, -1, wx.ArtProvider.GetBitmap("gtk-media-previous", wx.ART_MENU), size=(35, 23))
			uiButtonPlay     = wx.BitmapButton(self, -1, wx.ArtProvider.GetBitmap("gtk-media-play", wx.ART_MENU), size=(40, 40))
			uiButtonStop     = wx.BitmapButton(self, -1, wx.ArtProvider.GetBitmap("gtk-media-stop", wx.ART_MENU), size=(40, 40))
			uiButtonNext     = wx.BitmapButton(self, -1, wx.ArtProvider.GetBitmap("gtk-media-next", wx.ART_MENU), size=(35, 23))
			uiToggleShuffle  = libb.ThemedGenBitmapToggleButton(self, -1, wx.ArtProvider.GetBitmap("media-playlist-shuffle", wx.ART_MENU), size=(35, 23)) # media-playlist-shuffle
			#uiToggleShuffle  = wx.BitmapToggleButton(self, -1, wx.ArtProvider.GetBitmap("media-playlist-shuffle", wx.ART_MENU))
			#uiToggleLoop   = wx.ToggleButton(self, -1, pm_language.uiLoop[pympe.config.get("player", "loop")], size=(52, 23)) # media-playlist-repeat
			uiToggleLoop   = libb.ThemedGenBitmapTextToggleButton(self, -1, wx.ArtProvider.GetBitmap("media-playlist-repeat", wx.ART_MENU), size=(52, 23)) # media-playlist-repeat
			uiToggleLoop.SetLabel(pm_language.uiLoopShort[pympe.config.get("player", "loop")])
			uiSliderVolume   = wx.Slider(      self, -1, 100, 0, 100, size=(-1, 20)) # media-playlist-dynamic

			uiToggleShuffle.Bind(wx.EVT_BUTTON, self.event_toggle_shuffle)
			uiToggleLoop.Bind(wx.EVT_BUTTON,    self.event_toggle_loop)
		else:
			uiButtonPrevious = wx.Button(      self, -1, pm_language.uiPrevious, size=(35, 23))
			uiButtonPlay     = wx.Button(      self, -1, pm_language.uiPlay, size=(40, 40))
			uiButtonStop     = wx.Button(      self, -1, pm_language.uiStop, size=(40, 40))
			uiButtonNext     = wx.Button(      self, -1, pm_language.uiNext, size=(35, 23))
			uiToggleShuffle  = wx.ToggleButton(self, -1, pm_language.uiShuffle, size=(35, 23))
			uiToggleLoop   = wx.ToggleButton(self, -1, pm_language.uiLoop[pympe.config.get("player", "loop")], size=(52, 23))
			uiSliderVolume   = wx.Slider(      self, -1, 100, 0,  100, size=(-1, 20))
		uiButtonMenu  = wx.Button(self, -1, size=(35, 17))
		uiLabelTitle  = wx.StaticText(self, -1)
		uiLabelArtist = wx.StaticText(self, -1)
		uiLabelAlbum  = wx.StaticText(self, -1)
		uiLabelProgressCurrent = wx.StaticText(self, -1, "00:00:00")
		uiSliderProgress = wx.Slider(self)#, -1, 0.0, 0.0, 1.0)
		uiLabelProgressTotal = wx.StaticText(self, -1, "00:00:00")
		uiLabelProgress = wx.StaticText(self, -1, 'This Is Something We Should Always Look Forwards To!')
		uiButtonProgressLeft = wx.Button(self, 0, '', size=(11, 11), style=wx.BU_EXACTFIT)
		uiButtonProgressRight = wx.Button(self, 1, '', size=(11, 11), style=wx.BU_EXACTFIT)

		# Modify them
		fontButtons = wx.Font(5, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)
		uiToggleShuffle.SetFont(fontButtons)
		uiToggleLoop.SetFont(fontButtons)
		uiToggleShuffle.SetValue(False if pympe.config.get("player", "shuffle") == 0 else True) # media-playlist-shuffle
		uiToggleLoop.SetValue(pympe.config.get("player", "loop")) # media-playlist-repeat
		uiSliderVolume.SetValue(pympe.config.get("player", "volume")*100)  # audio-volume-high
		uiLabelProgress.SetFont(wx.Font(7,  wx.DEFAULT, wx.NORMAL, wx.BOLD))
		uiLabelTitle.SetFont( wx.Font(11,  wx.DEFAULT, wx.NORMAL, wx.BOLD))
		uiLabelArtist.SetFont(wx.Font(9, wx.DEFAULT, wx.NORMAL, wx.BOLD))
		uiLabelAlbum.SetFont( wx.Font(9,  wx.DEFAULT, wx.NORMAL, wx.NORMAL))
		uiLabelProgressCurrent.SetFont(wx.Font(7, wx.DEFAULT, wx.NORMAL, wx.BOLD))
		uiLabelProgressTotal.SetFont(wx.Font(7, wx.DEFAULT, wx.NORMAL, wx.BOLD))
		uiSliderProgress.grabbed = False
		uiSliderVolume.grabbed = False
		#size = uiLabelProgress.GetSize()[1]
		#uiButtonProgressLeft.SetMaxSize((size, size))
		#uiButtonProgressRight.SetMaxSize((size, size))

		#uiButtonProgressLeft.Hide()
		#uiButtonProgressRight.Hide()

		# Bind shit
		self.Bind(wx.EVT_ICONIZE, self.event_iconize)
		uiButtonPrevious.Bind(wx.EVT_BUTTON, self.event_button_previous)
		uiButtonMenu.Bind(wx.EVT_BUTTON, self.event_button_menu)
		uiButtonPlay.Bind(wx.EVT_BUTTON, self.event_button_play)
		uiButtonStop.Bind(wx.EVT_BUTTON, self.event_button_stop)
		uiButtonNext.Bind(wx.EVT_BUTTON, self.event_button_next)
		uiToggleShuffle.Bind(wx.EVT_TOGGLEBUTTON, self.event_toggle_shuffle)
		uiToggleLoop.Bind(wx.EVT_TOGGLEBUTTON,    self.event_toggle_loop)
		uiSliderVolume.Bind(wx.EVT_SCROLL,      self.event_slider_volume_scroll)
		uiSliderVolume.Bind(wx.EVT_LEFT_DOWN,   self.event_slider_volume_grab)
		uiSliderVolume.Bind(wx.EVT_LEFT_UP,     self.event_slider_volume_release)
		uiSliderProgress.Bind(wx.EVT_LEFT_DOWN, self.event_slider_progress_grab)
		uiSliderProgress.Bind(wx.EVT_LEFT_UP,   self.event_slider_progress_release)
		uiButtonProgressLeft.Bind(wx.EVT_BUTTON, self.event_button_progress)
		uiButtonProgressRight.Bind(wx.EVT_BUTTON, self.event_button_progress)

		# Create a sizer for them
		self.uiSizerMain.Add(uiGaugeProgress, 0, wx.EXPAND|wx.LEFT|wx.RIGHT, 10)
		self.uiSizerFooter = wx.BoxSizer(wx.HORIZONTAL)
		self.uiSizerStatus = wx.BoxSizer(wx.HORIZONTAL)
		self.uiSizerStatus.Add(uiButtonProgressLeft, 0, wx.LEFT, 10)
		self.uiSizerStatus.Add(uiLabelProgress, 1, wx.LEFT|wx.RIGHT, 10)
		self.uiSizerStatus.Add(uiButtonProgressRight, 0, wx.RIGHT, 10)
		self.uiSizerMain.Add(self.uiSizerStatus, 0, wx.EXPAND|wx.TOP, 5)
		self.uiSizerMain.Add(self.uiSizerFooter, 0, wx.EXPAND|wx.TOP, 5)
		self.uiSizerControls = wx.GridBagSizer()
		self.uiSizerFooter.Add(self.uiSizerControls, 0, wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 10)
		self.uiSizerControls.Add(uiButtonPrevious, (0, 0), (1, 1))
		self.uiSizerControls.Add(uiButtonMenu, (1, 0), (1, 1))
		self.uiSizerControls.Add(uiButtonPlay,     (0, 1), (2, 2))
		self.uiSizerControls.Add(uiButtonStop,     (0, 3), (2, 2))
		self.uiSizerControls.Add(uiButtonNext,     (0, 5), (1, 1))
		self.uiSizerControls.Add(uiToggleShuffle,  (0, 6), (1, 1))
		self.uiSizerControls.Add(uiToggleLoop,   (0, 7), (1, 1))
		self.uiSizerControls.Add(uiSliderVolume,   (1, 5), (1, 3), flag=wx.EXPAND)
		self.uiSizerFooter.Add(wx.StaticLine(self, style=wx.LI_VERTICAL), 0, wx.EXPAND|wx.LEFT|wx.RIGHT, 5)
		self.uiSizerNowPlaying = wx.BoxSizer(wx.VERTICAL)
		self.uiSizerFooter.Add(self.uiSizerNowPlaying, 1, wx.EXPAND)
		self.uiSizerNowPlaying.Add(uiLabelTitle,  0, wx.EXPAND)
		self.uiSizerNowPlaying.Add(uiLabelArtist, 0, wx.EXPAND)
		self.uiSizerNowPlaying.Add(uiLabelAlbum,  0, wx.EXPAND)
		#self.uiSizerFooter.AddStretchSpacer()
		# Space for additional buttons from plugins
		self.uiSizerExtras = wx.BoxSizer(wx.HORIZONTAL)
		self.uiSizerFooter.Add(self.uiSizerExtras, 0, wx.RIGHT, 10)
		#self.uiSizerFooter.AddStretchSpacer()

		# Misc gtk-leave-fullscreen gtk-fullscreen
		#s = wx.BoxSizer(wx.VERTICAL)
		#s.Add(wx.BitmapButton(self, -1, wx.ArtProvider.GetBitmap("gtk-fullscreen", wx.ART_MENU), size=(32, -1)), 1, wx.EXPAND)
		#self.uiSizerFooter.Add(s, 0, wx.EXPAND)
		# Sizer for the progress bar
		self.uiSizerProgress = wx.BoxSizer()
		self.uiSizerMain.Add(self.uiSizerProgress, 0, wx.EXPAND|wx.LEFT|wx.RIGHT, 10)
		self.uiSizerProgress.Add(uiLabelProgressCurrent, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 10)
		self.uiSizerProgress.Add(uiSliderProgress, 1, wx.EXPAND)
		self.uiSizerProgress.Add(uiLabelProgressTotal, 0, wx.ALIGN_CENTER_VERTICAL|wx.LEFT, 10)

		# Make Them Easy To Access
		self.uiControls = {"progress":uiGaugeProgress, 'progressLabel':uiLabelProgress, "previous":uiButtonPrevious, "play":uiButtonPlay, "stop":uiButtonStop, "next":uiButtonNext,
						"shuffle":uiToggleShuffle, "loop":uiToggleLoop, "volume":uiSliderVolume, "menu":uiButtonMenu,
						"labelTitle":uiLabelTitle, "labelArtist":uiLabelArtist, "labelAlbum":uiLabelAlbum,
						"progressCurrent":uiLabelProgressCurrent, "progressSlider":uiSliderProgress, "progressTotal":uiLabelProgressTotal}

		# Extra Stuffs
		################################
		#self.interfaceUpdateTimer = wx.Timer(self)
		#self.Bind(wx.EVT_TIMER, self.event_timer_interface_update)
		#TODO# self.interfaceUpdateTimer = pympe.mainloop.add_timer(self.event_timer_interface_update, 0.5)
		pympe.mainloop.add_timer(0.9, -1, self.update_interface)
		self.Bind(wx.EVT_CLOSE, self.event_window_close)

		# Bind events
		pympe.events.player_playback_started.subscribe(self.mpevent_player_playback_started)
		pympe.events.player_playback_resumed.subscribe(self.mpevent_player_playback_started)
		pympe.events.player_playback_paused.subscribe(self.mpevent_player_playback_paused)
		pympe.events.player_playback_stopped.subscribe(self.mpevent_player_playback_stopped)

		pympe.events.player_current_updated.subscribe(self.mpevent_player_current_updated)
		pympe.events.player_current_changed.subscribe(self.mpevent_player_current_changed)

		pympe.events.player_repeat_changed.subscribe(self.mpevent_player_repeat_changed)
		pympe.events.player_shuffle_changed.subscribe(self.mpevent_player_shuffle_changed)

		pympe.events.indicator_selected.subscribe(self.mpevent_indicator_selected)
		pympe.events.indicator_updated.subscribe(self.mpevent_indicator_updated)
		pympe.events.indicator_stopped.subscribe(self.mpevent_indicator_stopped)
		pympe.events.indicator_started.subscribe(self.mpevent_indicator_started)

		pympe.events.pympe_raise.subscribe(self.mpevent_raise)

		pympe.events.ui_change_view_mode.subscribe(self.mpevent_ui_change_view_mode)

		# Finish up
		self.uiSizerStatus.ShowItems(False)
		self.SetSizer(self.uiSizerMain)
		self.Show()

	def stop(self):
		# Destroy the fullscreen frame
		if self.fullscreenFrame: self.fullscreenFrame.stop()

		# Stop the update timer
		#TODO# Comment that out
		#self.interfaceUpdateTimer.Stop()

		# Save the layout
		if self.IsMaximized():
			pympe.config.set("window", "maximized", True)
		else:
			pympe.config.set("window", "size", tuple(self.GetSize()))
			pympe.config.set("window", "position", tuple(self.GetPosition()))
			pympe.config.set("window", "maximized", False)

		pympe.config.set("ui", "right_panel_size", self.uiTabsRight.GetSize()[0])

	def show_now_playing(self, shown):
		if shown and not self.uiTabNowPlaying:
			self.uiTabNowPlaying = TabNowPlaying(self.uiTabsLeft)
			self.uiTabsLeft.InsertPage(0, self.uiTabNowPlaying, pm_language.uiTabNowPlaying)
			self.uiTabsLeft.SetSelection(0)
		elif not shown and self.uiTabNowPlaying:
			for index in range(self.uiTabsLeft.GetPageCount()):
				if self.uiTabsLeft.GetPage(index) == self.uiTabNowPlaying:
					self.uiTabNowPlaying.unsubscribe_from_all()
					self.uiTabsLeft.DeletePage(index)
					break

			self.uiTabNowPlaying = None

	def mpevent_config_key_changed(self, event, section, key, value):
		if section == 'np' and key == 'enabled':
			self.show_now_playing(value)
		elif section == 'ui' and key == 'show_debug_tab':
			# If it was enabled and we're disabling
			if not value and self.uiTabDebug:
				for index in range(self.uiTabsLeft.GetPageCount()):
					if self.uiTabsLeft.GetPage(index) == self.uiTabDebug:
						self.uiTabDebug.unsubscribe_from_all()
						self.uiTabsLeft.DeletePage(index)
						break

				self.uiTabDebug = None
			# Else if it was disabled and we're enabling
			elif value and not self.uiTabDebug:
				self.uiTabDebug = TabDebug(self.uiTabsLeft)
				self.uiTabsLeft.InsertPage(1 if not self.uiTabNowPlaying else 2, self.uiTabDebug, "Debug")

	def restore_selected_tab(self):
		if pympe.config["ui"]["tabs_left_page"] < self.uiTabsLeft.GetRowCount():
			self.uiTabsLeft.SetSelection(pympe.config["ui"]["tabs_left_page"])
		else:
			self.uiTabsLeft.SetSelection(0)

		if pympe.config['ui']['seperate_playlist']:
			if pympe.config["ui"]["tabs_right_page"] < self.uiTabsRight.GetRowCount():
				self.uiTabsRight.SetSelection(pympe.config["ui"]["tabs_right_page"])
			else:
				self.uiTabsRight.SetSelection(0)

	def seperate_playlist(self, seperated=True):
		if seperated:
			if self.uiTabPlaylist.GetParent() == self.uiTabsRight: return

			# Move it to the right
			self.uiTabPlaylist.Reparent(self.uiTabsRight)
			self.uiTabsRight.AddPage(self.uiTabPlaylist, pm_language.uiTabPlaylist)

			# Split it
			self.uiSplitterMain.SplitVertically(self.uiTabsLeft, self.uiTabsRight, -pympe.config.get("ui", "right_panel_size"))
		else:
			if self.uiTabPlaylist.GetParent() == self.uiTabsLeft: return

			pympe.config.set("ui", "right_panel_size", self.uiTabsRight.GetSize()[0])

			# Move it to the left
			self.uiTabPlaylist.Reparent(self.uiTabsLeft)
			self.uiTabsLeft.InsertPage(1, self.uiTabPlaylist, pm_language.uiTabPlaylist)

			# Unsplit it
			self.uiSplitterMain.Unsplit(self.uiTabsRight)




	def fullscreen(self, value, fillscreen=False):
		if fillscreen:
			if self.fullscreenFrame:
				self.fullscreenFrame.stop()
				self.fullscreenFrame = None
				self.Show()

			self.ShowFullScreen(value, style=wx.FULLSCREEN_ALL)
			pympe.events.ui_view_mode_changed('fullscreen' if value else 'normal')
			return



		def toggle_fullscreen():
			# We've got to move it somewhere and then show/destroy them and then move it back
			if pympe.player.playingVideo: pympe.player.videoBin.set_video_frame(0)

			if value:
				self.savedFullscreenTab = self.uiTabsLeft.GetSelection()
				if self.IsFullScreen():
					self.ShowFullScreen(False, style=wx.FULLSCREEN_ALL)
				self.fullscreenFrame = FullscreenFrame(self)

				def paint(event):
					pympe.player.videoBin.set_video_frame(self.uiTabNowPlaying.GetHandle())
					pympe.player.videoBin.unblock()
					self.uiTabNowPlaying.Unbind(wx.EVT_PAINT)
					self.Hide()

				if pympe.player.playingVideo: self.uiTabNowPlaying.Bind(wx.EVT_PAINT, paint)
				pympe.events.ui_view_mode_changed('theatre')

			elif self.fullscreenFrame:
				self.fullscreenFrame.stop()
				self.fullscreenFrame = None
				self.Show()

				# Now Unblock
				def paint(event):
					self.uiTabsLeft.ChangeSelection(0)
					pympe.player.videoBin.set_video_frame(self.uiTabNowPlaying.GetHandle())
					pympe.player.videoBin.unblock()
					self.uiTabsLeft.ChangeSelection(self.savedFullscreenTab)
					self.Unbind(wx.EVT_PAINT)

				if pympe.player.playingVideo: self.Bind(wx.EVT_PAINT, paint)
				pympe.events.ui_view_mode_changed('normal')

		if pympe.player.playingVideo: pympe.player.videoBin.block(toggle_fullscreen)
		else: toggle_fullscreen()


	def show_settings(self):
		import pm_ui.pm_ui_settings
		reload(pm_ui.pm_ui_settings)
		from pm_ui.pm_ui_settings import SettingsDialog
		if self.settingsDialog:
			self.settingsDialog.Raise()
		else:
			self.settingsDialog = SettingsDialog()

	def show_plugins(self):
		import pm_ui.pm_ui_plugins
		reload(pm_ui.pm_ui_plugins)
		from pm_ui.pm_ui_plugins import PluginDialog
		if self.pluginDialog:
			self.pluginDialog.Raise()
		else:
			self.pluginDialog = PluginDialog()

	def bring_to_front(self):
		self.Raise()

	def mpevent_raise(self, event):
		self.bring_to_front()

	def update_title(self, panel):
		if self.activeTitlePanel != panel: return
		self.SetTitle("Pympe | "+self.activeTitlePanel.uiTabLibrary.library.get_stats(self.activeTitlePanel.indices))

	def update_interface(self):#event_timer_interface_update(self, event): #TODO# Comment out event, we don't need it
		position = pympe.player.position()
		if position != None:
			self.uiControls["progressCurrent"].SetLabel(pm_helpers.make_display_time(position))
			if not self.uiControls["progressSlider"].grabbed:
				self.uiControls["progressSlider"].SetValue(position)

		# For if the volume is changed in the system dialog
		self.uiControls['volume'].SetValue(pm_helpers.log_to_slider(pympe.player.volume()*100))

		# This should be in the player not the interface
		if pympe.player.stopTime != False:
			if position >= pympe.player.stopTime:
				pympe.events.player_next()

	def update_playbutton(self):
		#print 'Updating',pympe.player.playing, pympe.player.paused

		if pympe.player.playing and not pympe.player.paused:
			if self.ui.useBitmaps: self.uiControls["play"].SetBitmapLabel(wx.ArtProvider.GetBitmap("gtk-media-pause", wx.ART_MENU))
			self.uiControls["play"].SetLabel(pm_language.uiPause)
		else:
			if self.ui.useBitmaps: self.uiControls["play"].SetBitmapLabel(wx.ArtProvider.GetBitmap("gtk-media-play", wx.ART_MENU))
			else: self.uiControls["play"].SetLabel(pm_language.uiPlay)

	def mpevent_player_playback_started(self, event):
		self.update_playbutton()
		#self.interfaceUpdateTimer.Start(400)
		#TODO# pympe.mainloop.resume(self.interfaceUpdateTimer)

	def mpevent_player_playback_paused(self, event):
		self.update_playbutton()
		#TODO# pympe.mainloop.pause(self.interfaceUpdateTimer)
		#self.interfaceUpdateTimer.Stop()

	def mpevent_player_playback_stopped(self, event):
		self.uiControls["progressCurrent"].SetLabel("00:00:00")
		self.uiControls["progressSlider"].SetValue(0)

		self.update_playbutton()

	def mpevent_player_current_changed(self, event, currentInfo, currentLibraryId, currentLibrary):
		if pympe.player.currentInfo['length'] and pympe.player.currentInfo['length'] > 0:
			self.uiControls["progressSlider"].SetRange(0, pympe.player.currentInfo['length'])
			self.uiControls["progressTotal"].SetLabel(pm_helpers.make_display_time(pympe.player.currentInfo['length']))
		self.uiControls["labelTitle"].SetLabel(pm_helpers.make_display_tag(pympe.player.currentInfo["title"]))
		self.uiControls["labelArtist"].SetLabel(pm_helpers.make_display_tag(pympe.player.currentInfo["artist"]))
		self.uiControls["labelAlbum"].SetLabel(pm_helpers.make_display_tag(pympe.player.currentInfo["album"]))

		self.update_playbutton()
		self.Layout()

	def mpevent_player_current_updated(self, event, tags):
		for tag in tags:
			if tag == 'length' and pympe.player.currentInfo['length'] > 0:
				self.uiControls["progressSlider"].SetRange(0, pympe.player.currentInfo['length'])
				self.uiControls["progressTotal"].SetLabel(pm_helpers.make_display_time(pympe.player.currentInfo['length']))
			elif tag == 'title':
				self.uiControls["labelTitle"].SetLabel(pm_helpers.make_display_tag(pympe.player.currentInfo["title"]))
			elif tag == 'artist':
				self.uiControls["labelArtist"].SetLabel(pm_helpers.make_display_tag(pympe.player.currentInfo["artist"]))
			elif tag == 'album':
				self.uiControls["labelAlbum"].SetLabel(pm_helpers.make_display_tag(pympe.player.currentInfo["album"]))
		self.Layout()

	def mpevent_player_repeat_changed(self, event, repeat):
		button = self.uiControls["loop"]
		button.SetValue(True if repeat else False)
		if self.ui.useBitmaps:
			button.SetLabel(pm_language.uiLoopShort[repeat])
		else:
			button.SetLabel(pm_language.uiLoop[repeat])

	def mpevent_player_shuffle_changed(self, event, shuffle):
		self.uiControls["shuffle"].SetValue(shuffle)

	def mpevent_indicator_started(self, event, indicator):
		if len(pympe.progress.indicators) == 1:
			self.uiSizerStatus.ShowItems(True)
			self.Layout()
			#self.Refresh()
			#self.Update()

		if indicator != pympe.progress.current: return

		self.uiControls['progress'].SetRange(indicator.total)
		self.uiControls['progress'].SetValue(indicator.current)
		self.uiControls['progressLabel'].SetLabel('(%s/%s) %s'%(pympe.progress.index+1, len(pympe.progress.indicators), indicator.label))
		#self.Refresh()
		#self.Update()

	def mpevent_indicator_stopped(self, event, indicator):
		if not pympe.progress.indicators:
			self.uiControls['progress'].SetRange(100)
			self.uiControls['progress'].SetValue(0)
			self.uiSizerStatus.ShowItems(False)
			self.Layout()
			#self.Refresh()
			#self.Update()

			return

		if pympe.progress.current:
			if indicator.total < indicator.current: indicator.total = indicator.current
			self.uiControls['progress'].SetRange(pympe.progress.current.total)
			self.uiControls['progress'].SetValue(pympe.progress.current.current)
			self.uiControls['progressLabel'].SetLabel('(%s/%s) %s'%(pympe.progress.index+1, len(pympe.progress.indicators), pympe.progress.current.label))
		#self.Refresh()
		#self.Update()

	def mpevent_indicator_updated(self, event, indicator):
		if indicator != pympe.progress.current: return
		if indicator.total < indicator.current: indicator.total = indicator.current
		self.uiControls['progress'].SetRange(indicator.total)
		self.uiControls['progress'].SetValue(indicator.current)
		self.uiControls['progressLabel'].SetLabel('(%s/%s) %s'%(pympe.progress.index+1, len(pympe.progress.indicators), indicator.label))
		#self.Refresh()
		#self.Update()


	def mpevent_indicator_selected(self, event, indicator):
		if indicator.total < indicator.current: indicator.total = indicator.current
		self.uiControls['progress'].SetRange(indicator.total)
		self.uiControls['progress'].SetValue(indicator.current)
		self.uiControls['progressLabel'].SetLabel('(%s/%s) %s'%(pympe.progress.index+1, len(pympe.progress.indicators), indicator.label))
		#self.Refresh()
		#self.Update()

	def mpevent_ui_change_view_mode(self, event, mode):
		if mode == 'normal':
			self.fullscreen(False)
		elif mode == 'fullscreen':
			self.fullscreen(True, True)
		elif mode =='theatre':
			self.fullscreen(True, False)

# Events
	def event_iconize(self, event):
		pympe.events.ui_iconized(event.Iconized())

		if not event.Iconized():
			self.Layout()

		event.Skip()

	def minimized(self):
		return self.IsIconized()

	def event_notebook_page_changed(self, event):
		event.Skip()

		if event.GetId() == 0:
			pympe.config.set("ui", "tabs_left_page",  event.Selection)

			# Let the tab know it was selected if it is listening
			try: event.EventObject.GetCurrentPage().selected_by_notebook()
			except AttributeError: pass
		else:
			pympe.config.set("ui", "tabs_right_page", event.Selection)

	def event_slider_volume_scroll(self, event):
		value = event.GetEventObject().GetValue()

		if value !=  0:
			value = pm_helpers.slider_to_log(value)*0.01

		pympe.player.volume(value)
		event.Skip()

	def event_slider_volume_grab(self, event):
		self.uiControls["volume"].grabbed = True
		event.Skip()

	def event_slider_volume_release(self, event):
		self.uiControls["volume"].grabbed = False
		pympe.config.set("player", "volume", pympe.player.volume())
		event.Skip()

	def event_slider_progress_grab(self, event):
		self.uiControls["progressSlider"].grabbed = True
		event.Skip()

	def event_slider_progress_release(self, event):
		self.uiControls["progressSlider"].grabbed = False
		pympe.player.position(event.GetEventObject().GetValue())
		event.Skip()

	def event_window_close(self, event):
		pympe.stop()
		event.Skip()

# Control buttons
	def event_button_previous(self, event):
		pympe.player.previous()

	def event_button_next(self, event):
		pympe.player.next()

	def event_button_play(self, event):
		if pympe.player.playing and not pympe.player.paused:
			pympe.player.pause()
		else:
			pympe.player.play()

	def event_button_stop(self, event):
		pympe.player.stop()

	def event_toggle_shuffle(self, event):
		event.Skip()
		v = self.uiControls["shuffle"].GetValue()
		pympe.config.set("player", "shuffle", v)
		pympe.events.player_set_shuffle(v)

	def event_toggle_loop(self, event):
		v = pympe.config.get("player", "loop")
		v += 1
		if v == 3: v = 0
		pympe.config.set("player", "loop", v)
		pympe.events.player_set_repeat(v)

	def event_button_progress(self, event):
		if event.Id == 0: # Left
			pympe.progress.previous()
		else: # Right
			pympe.progress.next()

	def set_video_frame(self, frame=None):
		''' Set to alternative frame or the default if none '''
		if not frame:
			if self.uiTabNowPlaying:
				frame = self.uiTabNowPlaying
			else:
				self.videoWindowHandle = None
				return False


		wasNone = self.videoWindowHandle == None
		self.videoWindowHandle = frame.GetHandle()
		if not wasNone: pympe.player.videoBin.set_window_handle(frame.GetHandle())
		return True

	def event_menu_main(self, event):
		if event.Id == 0: self.fullscreen(not self.IsFullScreen(), fillscreen=True)
		elif event.Id == 1: self.fullscreen(self.fullscreenFrame is None)
		elif event.Id == 201:
			dialog = wx.TextEntryDialog(self, "Please enter the unlock password.", "Enter unlock password.")
			#dialog = wx.PasswordEntryDialog(self, "Please enter the unlock password.", "Enter unlock password.")
			#/usr/lib/python2.7/site-packages/wx-2.8-gtk2-unicode/wx/_core.py:9431: GtkWarning: IA__gtk_container_remove: assertion `GTK_IS_CONTAINER (container)' failed
			#  return _core_.Window_Reparent(*args, **kwargs)
			#swig/python detected a memory leak of type 'wxPasswordEntryDialog *', no destructor found.

			if dialog.ShowModal() == wx.ID_CANCEL: return
			if dialog.GetValue() == pympe.config.get('ui', 'theatre_unlock_password'):
				self.fullscreenFrame.unlock()
				#self.fullscreen(False)
		#elif event.Id == 100:
		#	pympe.plugins.get_plugin(1323703478).MetadataEditor([pympe.player.currentFile['uri']], [pympe.player.currentId])
		elif event.Id == 660: # Debug
			import pdb; pdb.set_trace()	# BREAKPOINT
		elif event.Id == 666: self.Close()
		elif event.Id == 2900: # Plugins
			self.show_plugins()
		elif event.Id == 3000: # Settings
			self.show_settings()
		elif event.Id >= 8000 and event.Id < 9000:
			library = pympe.libraries[(event.Id-8000)/10]

			if (event.Id-8000)%10 == 1: # Update Library
				library.update_library(pympe.config.get(library.configKey, 'searchPaths'))
			elif (event.Id-8000)%10 == 2: # Scan single directory
				dialog = wx.DirDialog(pympe.ui.uiMain, "Choose a directory to scan.")
				if dialog.ShowModal() == wx.ID_OK:
					library.scan_single_directory_threaded(dialog.GetPath())
			elif (event.Id-8000)%10 == 5: # Remove Dead Links
				library.remove_dead_links_threaded()
			elif (event.Id-8000)%10 == 7: # Save Database
				library.save_database()
			elif (event.Id-8000)%10 == 8: # Load Database
				library.load_database(sendEvent=True)

		elif event.Id == 3600:
			import pm_ui.pm_ui_theatre
			reload(pm_ui.pm_ui_theatre)
			from pm_ui.pm_ui_theatre import TheatreFrame

			if not self.theatreFrame:
				self.theatreFrame = TheatreFrame(self)
			else:
				self.theatreFrame.close()

		# For devices
		elif event.Id >= 9000 and event.Id < 10000:
			device = pympe.devices[event.Id-9000]

			if device.connected:
				pympe.events.device_disconnect(device.uid)
			else:
				pympe.events.device_connect(device.uid)

		# For menu hooks
		elif event.Id >= 20000 and event.Id <= 21000:
			menuItemInfo = event.EventObject.menuItemInfo[event.Id-20000]

			if menuItemInfo[2]:
				menuItemInfo[3] = event.IsChecked()
				menuItemInfo[1](menuItemInfo[3])
			else:
				menuItemInfo[1]()

	def event_button_menu(self, event):
		menu = wx.Menu()

		if pympe.config.get('ui', 'use_theatre_unlock_password') and self.fullscreenFrame and not self.fullscreenFrame.unlocked:
			menu.Append(201, 'Unlock')
		else:
			menu.Append(0, "%s Fullscreen Player"%("Leave" if self.IsFullScreen() else "View"))
			menu.Append(3600, '%s Theatre'%("Enter" if self.theatreFrame is None else "Leave"))
			#menu.Append(1, "%s Theatre Mode [OLD]"%("Enter" if self.fullscreenFrame is None else "Leave"))
			#menu.Enable(1, False)
			menu.AppendSeparator()
			if pympe.player.currentId != None:
				menu.AppendSubMenu(self.uiTabLibrary.uiPanelTracks.menu_tracks([pympe.player.currentId], pympe.player.currentLibrary), 'Current Track')
				menu.AppendSeparator()

			menu.Append(2900, 'Plugins')
			menu.Append(3000, 'Settings')
			menu.AppendSeparator()

			if len(pympe.libraries) == 1:
				menuLibrary = wx.Menu()
				menuLibrary.Append(8001, 'Update Library')
				menuLibrary.Append(8002, 'Scan Single Path')
				menuLibrary.Append(8005, 'Remove Dead Links')

				librarySaveLoad = wx.Menu()
				librarySaveLoad.Append(8007, 'Save Database')
				librarySaveLoad.Append(8008, 'Load Database')
				menuLibrary.AppendMenu(8006, 'Save/Load', librarySaveLoad)

				menuLibrary.Bind(wx.EVT_MENU, self.event_menu_main)
				librarySaveLoad.Bind(wx.EVT_MENU, self.event_menu_main)
				menu.AppendMenu(8000, 'Library', menuLibrary)
			else:
				menuLibrary = wx.Menu()

				for library in sorted(pympe.libraries.values(), key=lambda x: x.dbFilename.rsplit('/', 1)[-1]):
					libraryMenu = wx.Menu()
					libraryMenu.Append(8001+(10*library.uid), 'Update Library')
					libraryMenu.Append(8002+(10*library.uid), 'Scan Single Path')
					libraryMenu.Append(8005+(10*library.uid), 'Remove Dead Links')

					librarySaveLoad = wx.Menu()
					librarySaveLoad.Append(8007+(10*library.uid), 'Save Database')
					librarySaveLoad.Append(8008+(10*library.uid), 'Load Database')
					libraryMenu.AppendMenu(8006+(10*library.uid), 'Save/Load', librarySaveLoad)

					libraryMenu.Bind(wx.EVT_MENU, self.event_menu_main)
					librarySaveLoad.Bind(wx.EVT_MENU, self.event_menu_main)
					menuLibrary.AppendMenu(library.uid, library.dbFilename.rsplit('/', 1)[-1], libraryMenu)

				menuLibrary.Bind(wx.EVT_MENU, self.event_menu_main)
				menu.AppendMenu(8000, 'Libraries', menuLibrary)

			menuDevices = wx.Menu()
			for device in pympe.devices.get_ready():
				menuDevices.Append(9000+device.uid, '%s: %s'%('Unmount' if device.connected else 'Mount', device.name))

			menuDevices.Bind(wx.EVT_MENU, self.event_menu_main)
			menu.AppendMenu(8999, 'Devices', menuDevices)

			# Plugin Menu Hooks
			if self.menuHookList:
				menuHooks = wx.Menu()
				menuHooks.Bind(wx.EVT_MENU, self.event_menu_main)
				menuHooks.menuItemInfo = []

				for index, menuItemInfo in enumerate(sorted(self.menuHookList, key=lambda x: x[0].lower())):
					if menuItemInfo[2]: # Check item
						menuHooks.AppendCheckItem(20000+index, menuItemInfo[0]).Check(menuItemInfo[3])
					elif type(menuItemInfo[1]) == wx._core.Menu: # Sub Menu
						menuHooks.AppendMenu(20000+index, menuItemInfo[0], menuItemInfo[1])
					else:
						menuHooks.Append(20000+index, menuItemInfo[0])
					menuHooks.menuItemInfo.append(menuItemInfo)

				menu.AppendMenu(19999, 'Plugins', menuHooks)

			if not pympe.devices.get_ready():
				menu.Enable(8999, False)

			if pympe.arguments['debug']:
				menu.AppendSeparator()
				menu.Append(660, 'Set Breakpoint')

			menu.AppendSeparator()
			menu.Append(666, "Quit")
		menu.Bind(wx.EVT_MENU, self.event_menu_main)
		event.GetEventObject().PopupMenu(menu)
