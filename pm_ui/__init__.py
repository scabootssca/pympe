import wx, pympe, gtk
from pm_ui.pm_ui_main import uiMain
from pm_helpers import unthread

class Splash(wx.SplashScreen):
    def __init__(self):
		bmp = wx.Image(pympe.cwd+"images/splash.png").ConvertToBitmap()
		wx.SplashScreen.__init__(self, bmp, wx.SPLASH_CENTRE_ON_SCREEN|wx.SPLASH_TIMEOUT, 5000, None, -1)
		self.Bind(wx.EVT_CLOSE, self.OnClose)

    def OnClose(self, evt):
        evt.Skip()
        self.Hide()

class Ui:
	def __init__(self):
		self.app = wx.App(False)
		self.splash = None

		self.useBitmaps = True
		self.drawingArea = None

		pympe.events.pympe_initilized.subscribe(self.mpevent_pympe_initilized)

	def show_splash():
		self.splash = Splash()

	def module_start(self):
		self.uiMain = uiMain(self)
		self.uiMain.start()

		if self.splash and self.splash.IsShown(): self.splash.Hide()

	def mpevent_pympe_initilized(self, event):
		# This is needed cause plugins might add tabs
		if not pympe.config['np']['enabled']:
			self.uiMain.restore_selected_tab()

		# For first run tutorial, add music to library
		if hasattr(pympe, 'firstRun'):
			result = wx.MessageDialog(self.uiMain, 'You seem to have no library directory selected, would you like to set one now?', style=wx.YES_NO|wx.NO_DEFAULT).ShowModal()
			if result == wx.ID_YES:
				self.uiMain.show_settings()

	def module_stop(self):
		self.uiMain.stop()

	@unthread
	def show_error(self, message):
		wx.MessageDialog(None, message, 'Pympe has encountered an error.', wx.ICON_ERROR|wx.OK).ShowModal()
		return False

	@unthread
	def show_message(self, message, title='Information'):
		wx.MessageDialog(None, message, title, wx.ICON_INFORMATION|wx.OK).ShowModal()
		return True

	# For plugin gui hooks
	def set_menu_hook(self, label, functionOrMenu, checkItem=False, checked=False):
		''' label, item
		'somelabel', somefunction                     == Regular menu item
		'somelabel', somefunction, True, DefaultValue == Check Menu Item
		'somelabel', somewxMenu                       == Submenu item
		'''
		self.uiMain.menuHookList.append([label, functionOrMenu, checkItem, checked])

	def remove_menu_hook(self, function):
		for index, menuItemInfo in enumerate(self.uiMain.menuHookList):
			if menuItemInfo[1] == function:
				self.uiMain.menuHookList.pop(index)
				return True
		return False

