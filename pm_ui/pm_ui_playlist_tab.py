import wx
import pympe, pm_helpers, pm_language, cPickle, pm_mediafile, pm_playlists, time, os
import wx.lib.mixins.listctrl as listmix

class PlaylistDropTarget(wx.DropTarget):
	def __init__(self, playlist):
		"""['DataObject', 'DefaultAction', 'GetData', 'GetDataObject', 'GetDefaultAction', 'OnData',
		'OnDragOver', 'OnDrop', 'OnEnter', 'OnLeave', 'SetDataObject', 'SetDefaultAction', '__class__',d
		 '__del__', '__delattr__', '__dict__', '__doc__', '__format__', '__getattribute__', '__hash__',
		 '__init__', '__module__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__',
		  '__sizeof__', '__str__', '__subclasshook__', '__swig_destroy__', '__weakref__', '_setCallbackInfo',
		   'base_OnDragOver', 'base_OnDrop', 'base_OnEnter', 'base_OnLeave', 'do', 'doFile', 'doMedia', 'doText', 'playlist', 'this', 'thisown']
		"""
		wx.PyDropTarget.__init__(self)
		self.playlist = playlist

		self.dropIndicatorIndex = None

		self.do = wx.DataObjectComposite() # The dataobject that gets filled with the appropriate data

		self.doMedia = wx.CustomDataObject("MediaFile")
		self.doText  = wx.TextDataObject()
		self.doFile  = wx.FileDataObject()

		self.do.Add(self.doMedia)
		self.do.Add(self.doText)
		self.do.Add(self.doFile)

		self.SetDataObject(self.do)

	def OnDragOver(self, x, y, d):
		if not pympe.playlists.current.allowDragDrop: return d
		dropIndex = self.playlist.HitTest((x, y))[0]
		if dropIndex == -1: dropIndex = pympe.playlists.current.get_length()
		if self.dropIndicatorIndex == None: self.dropIndicatorIndex = dropIndex
		#pympe.playlists.current.displayQueue.insert(dropIndex, pympe.playlists.current.displayQueue.pop(self.dropIndicatorIndex))
		self.dropIndicatorIndex = dropIndex
		self.playlist.drop(self.dropIndicatorIndex)
		#self.playlist.refresh()
		return d

	def OnEnter(self, x, y, d):
		if not pympe.playlists.current.allowDragDrop: return d
		#pympe.playlists.current.enqueue([">----------------<"], index=0)
		self.dropIndicatorIndex = 0
		self.playlist.drop(self.dropIndicatorIndex)
		return d

	def OnLeave(self):
		if not pympe.playlists.current.allowDragDrop: return
		self.playlist.drop(False)
		#pympe.playlists.current.dequeue([self.dropIndicatorIndex])

	def OnData(self, x, y, d):
		if not pympe.playlists.current.allowDragDrop: return d

		if self.GetData():
			dropFormat = self.do.GetReceivedFormat().GetType()
			##['DF_BITMAP', 'DF_DIB', 'DF_DIF', 'DF_ENHMETAFILE', 'DF_FILENAME', 'DF_HTML', 'DF_INVALID', 'DF_LOCALE', 'DF_MAX', 'DF_METAFILE', 'DF_OEMTEXT', 'DF_PALETTE', 'DF_PENDATA', 'DF_PRIVATE', 'DF_RIFF', 'DF_SYLK', 'DF_TEXT', 'DF_TIFF', 'DF_UNICODETEXT', 'DF_WAVE']

			#for x in ['DF_BITMAP', 'DF_DIB', 'DF_DIF', 'DF_ENHMETAFILE', 'DF_FILENAME', 'DF_HTML', 'DF_INVALID', 'DF_LOCALE', 'DF_MAX', 'DF_METAFILE', 'DF_OEMTEXT', 'DF_PALETTE', 'DF_PENDATA', 'DF_PRIVATE', 'DF_RIFF', 'DF_SYLK', 'DF_TEXT', 'DF_TIFF', 'DF_UNICODETEXT', 'DF_WAVE']:
			#	print(x, getattr(wx, x))

			# Dropped from the library
			if dropFormat == wx.DF_PRIVATE:
				indices = cPickle.loads(self.doMedia.GetData())

				pympe.playlists.current.enqueue(pympe.dragData.uid, pympe.dragData.get_multi_track_single_value(indices, 'uri'),
										pympe.dragData.get_multi_track_single_value(indices, 'title', 'joined'), indices, index=self.dropIndicatorIndex)

			# Dropped a file from the browser
			elif dropFormat & wx.DF_FILENAME|wx.DF_UNICODETEXT|wx.DF_TEXT:
				if dropFormat == wx.DF_FILENAME:
					uris =  ["file://"+filename for filename in self.doFile.GetFilenames()]
				else:
					text = self.doText.GetText()
					if len(text) > 12 and text[:7] in ['http://', 'file://']:
						uris = [text]

				trackInfo = []

				for uri in uris:
					trackId = None

					for library in sorted(pympe.libraries.values(), key=lambda x: x.uid):
						trackId = library.get_trackid_from_uri(uri)
						if trackId != None:
							trackInfo.append((library.uid, library.get_single_track_single_value(trackId, 'title', 'joined'), trackId))
							break

					if trackId == None:
						trackInfo.append((-1, uri, -1))

				libraries, titles, trackIds = zip(*trackInfo)
				pympe.playlists.current.enqueue(libraries, uris, titles, trackIds, index=self.dropIndicatorIndex)

			#elif dropFormat == wx.DF_FILENAME:
				#uris =  ["file://"+filename for filename in self.doFile.GetFilenames()]
				#pympe.playlists.current.enqueue(None, uris, index=self.dropIndicatorIndex)

			#elif dropFormat == wx.DF_UNICODETEXT or dropFormat == wx.DF_TEXT:
				#text = self.doText.GetText()
				#if len(text) > 12:
					#if text[:7] in ['http://', 'file://']:
						#pympe.playlists.current.enqueue(None, [text], index=self.dropIndicatorIndex)

			self.dropIndicatorIndex = None


		return d

class ListPlaylist(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
	def __init__(self, parent):
		wx.ListCtrl.__init__(self, parent, -1, style=wx.LC_REPORT|wx.BORDER_NONE|wx.LC_SORT_ASCENDING|wx.LC_VIRTUAL|wx.LC_VRULES)
		listmix.ListCtrlAutoWidthMixin.__init__(self)

		self.activePlaylistUid = -1
		self.currentEntryIndex = pympe.playlists.current.get_index()

		self.dragging = False
		self.dragIndex = None
		self.draggingCurrent = False

		self.InsertColumn(0, "Title")

		self.dropLocation = None
		self.SetDropTarget(PlaylistDropTarget(self))

		font = wx.SystemSettings.GetFont(wx.SYS_DEFAULT_GUI_FONT)
		font.SetWeight(wx.FONTWEIGHT_BOLD)
		self.activeItemAttr = wx.ListItemAttr()
		self.activeItemAttr.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_3DSHADOW))
		self.activeItemAttr.SetFont(font)

		pympe.events.pympe_initilized.subscribe(self.mpevent_pympe_initilized)

		pympe.events.playlist_selected.subscribe(self.mpevent_playlist_selected)

		pympe.events.playlist_replaced.subscribe(self.mpevent_playlist_entries_altered)
		pympe.events.playlist_entries_added.subscribe(self.mpevent_playlist_entries_altered)
		pympe.events.playlist_entries_removed.subscribe(self.mpevent_playlist_entries_altered)
		pympe.events.playlist_entries_updated.subscribe(self.mpevent_playlist_entries_altered)

		pympe.events.player_current_changed.subscribe(self.mpevent_player_current_changed)


		self.Bind(wx.EVT_LIST_BEGIN_DRAG, self.event_begin_drag)
		self.Bind(wx.EVT_MOTION, self.event_drag)
		self.Bind(wx.EVT_LEFT_UP, self.event_end_drag)

		self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.event_item_activated)
		self.Bind(wx.EVT_RIGHT_UP, self.event_right_up)

		self.Bind(wx.EVT_KEY_DOWN, self.event_key_down)

	def OnGetItemText(self, row, column):
		if row >= pympe.playlists.current.get_length(): return ''
		if row == self.dropLocation: return '>>>>>>>>>>>>>>'
		elif self.dropLocation != None and row >= self.dropLocation: row -= 1
		output = pympe.playlists.current.get_item(row, 'title')
		if row == pympe.playlists.current.get_index(): output = pm_language.uiPlay+' '+output
		if self.dragging and row == self.dragIndex: output = '> '+output
		return output

	def OnGetItemAttr(self, item):
		if self.activePlaylistUid == pympe.playlists.current.uid and item == pympe.playlists.current.get_index():
			return self.activeItemAttr

	def drop(self, index):
		if not pympe.playlists.current.allowDragDrop: return event.Skip()
		if index is False:
			self.dropLocation = None
			self.SetItemCount(pympe.playlists.current.get_length())
		else:
			self.dropLocation = index
			self.SetItemCount(pympe.playlists.current.get_length()+1)

	def mpevent_playlist_entries_altered(self, event, playlistUid, *args):
		if playlistUid != pympe.playlists.current.uid: return



		if event == pympe.events.playlist_replaced:
			# Deselect all first
			for x in self.get_selected():
				self.SetItemState(x, not wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)

			self.SetItemCount(pympe.playlists.current.get_length())
			self.currentEntryIndex = pympe.playlists.current.get_index()
		elif event == pympe.events.playlist_entries_updated:
			for entryUid in args[0]:
				entryIndex = pympe.playlists.current.get_index(entryUid)
				self.RefreshItem(entryIndex)

				## Check for currently playing entry
				#if pympe.playlists.current.libraryIds[entryIndex] == pympe.player.currentId and \
					#pympe.libraries[pympe.playlists.current.libraries[entryIndex]] == pympe.player.currentLibrary:
						#pympe.player.currentInfo['title'] = pympe.playlists.current.titles[entryIndex]
						#pympe.events.player_current_updated({'title':pympe.playlists.current.titles[entryIndex]})

		else: # entries_removed, entries_added
			if event == pympe.events.playlist_entries_removed:
				self.currentEntryIndex -= len([entryIndex for entryIndex in args[1] if entryIndex < self.currentEntryIndex])
			self.SetItemCount(pympe.playlists.current.get_length())

	def mpevent_playlist_selected(self, event, playlistUid):
		#if pympe.playlists.current.multiSelect: self.SetWindowStyle(self.GetWindowStyle()|wx.LC_SINGLE_SEL)
		#else: self.SetWindowStyle(self.GetWindowStyle()^wx.LC_SINGLE_SEL)
		self.SetItemCount(pympe.playlists.current.get_length())


	def event_begin_drag(self, event):
		if not pympe.playlists.current.allowDragDrop: return event.Skip()
		self.dragIndex = self.HitTest(event.GetPosition())[0]
		if self.dragIndex != -1:
			self.dragging = True

	def event_drag(self, event):
		if not pympe.playlists.current.allowDragDrop: return event.Skip()
		if not self.dragging: return event.Skip()
		hoverIndex = self.HitTest((event.m_x, event.m_y))[0]
		if hoverIndex == self.dragIndex: return event.Skip()


		playlist = pympe.playlists.current
		playlist.uids.insert(hoverIndex, playlist.uids.pop(self.dragIndex))
		playlist.uris.insert(hoverIndex, playlist.uris.pop(self.dragIndex))
		playlist.titles.insert(hoverIndex, playlist.titles.pop(self.dragIndex))
		playlist.libraryIds.insert(hoverIndex, playlist.libraryIds.pop(self.dragIndex))

		# If we are on the index about to move move the index with us
		if self.dragIndex == playlist.index:
			playlist.index = hoverIndex
		else:
			# If we are about to move on the index and moving down then move it up one
			if hoverIndex == playlist.index and hoverIndex > self.dragIndex: playlist.index -= 1
			# If we are about to move on the index and moving up then move it down one
			elif hoverIndex == playlist.index and hoverIndex < self.dragIndex: playlist.index += 1

		self.RefreshItem(self.dragIndex)
		self.RefreshItem(hoverIndex)

		self.dragIndex = hoverIndex


	def event_end_drag(self, event):
		if not self.dragging: return event.Skip()
		self.dragging = False
		self.RefreshItem(self.dragIndex)
		self.dragIndex = None

	def event_item_activated(self, event):
		self.activePlaylistUid = pympe.playlists.current.uid
		self.RefreshItem(pympe.playlists.current.get_index())
		pympe.playlists.current.set_index(event.Index)
		pympe.player.play(entry=pympe.playlists.current.get_current())

	def event_right_up(self, event):
		#first = self.GetFirstSelected()
		selected = self.get_selected()
		menu = None

		if selected:
			menu = wx.Menu()

			if len(selected) == 1 and pympe.playlists.current.singleItemMenu:
				for entry in pympe.playlists.current.singleItemMenu:
					if entry == None:
						menu.AppendSeparator()
					else:
						try: menu.Append(abs(entry[1])+10000, entry[0])
						except:
							print 'Problem Adding Custom Single Menu Item: %s'%entry
							continue

				menu.AppendSeparator()

			elif pympe.playlists.current.multiItemMenu:
				for entry in pympe.playlists.current.multiItemMenu:
					if entry == None:
						menu.AppendSeparator()
					else:
						try:
							menu.Append(abs(entry[1])+10000, entry[0])
						except:
							print 'Problem Adding Custom Multi Menu Item: %s'%entry
							continue

				menu.AppendSeparator()

			if pympe.playlists.current.userModifiable:
				# 1000 For 'Clear' if all are selected
				# Else 10 for 'Remove' if not all but keep the name
				menu.Append((1000 if len(selected) >= pympe.playlists.current.get_length() else 10), "Remove From Playlist")
				menu.AppendSeparator()

			menu.Append(20, "Copy File Location%s"%('s' if len(selected) > 1 else ''))
			if len(selected) == 1: menu.Append(25, 'Open Containing Folder')
			menu.AppendSeparator()
			if pympe.playlists.current.userModifiable:
				menu.Append(40, 'Edit Metadata')
				if not pympe.plugins.get_plugin(1323703478): menu.Enable(40, False)

			libraryResults = [(pympe.playlists.current.libraries[x] != -1) for x in selected]
			libraryResults = (True in libraryResults, False in libraryResults)

			if libraryResults[0]:
				menu.Append(50, 'Find %sIn Library'%('Existing ' if libraryResults[1] else ''))

				if libraryResults[1]:
					menu.AppendSeparator()

			if libraryResults[1]:
				menu.Append(200, "Import %sTo Library"%('Missing ' if libraryResults[0] else ''))

			if pympe.playlists.current.userModifiable:
				menu.AppendSeparator()
				menu.Append(990,  'Add Files To Playlist')
				#menu.Append(991,  'Add Folder To Playlist')
				menu.Append(1000, "Clear Playlist")
		elif pympe.playlists.current.userModifiable:
			menu = wx.Menu()
			menu.Append(990,  'Add Files To Playlist')
			#menu.Append(991,  'Add Folder To Playlist')
			menu.Append(1000, "Clear Playlist")



		if menu:
			menu.Bind(wx.EVT_MENU, self.event_menu)
			self.PopupMenu(menu)

	def event_menu(self, event):
		eventId = event.Id
		selected = self.get_selected()
		uris = [pympe.playlists.current.uris[x] for x in selected]
		trackIds = [pympe.playlists.current.libraryIds[x] for x in selected]
		titles = [pympe.playlists.current.titles[x] for x in selected]

		# Custom menu items (should) have a value >= 10000
		if eventId >= 10000:
			pympe.playlists.current.handle_custom_menu(eventId-10000, selected)
			return

		if eventId == 10: # Remove From Playlist
			pympe.playlists.current.dequeue([pympe.playlists.current.uids[x] for x in selected])
			for x in selected: self.SetItemState(x, not wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)
		elif eventId == 200: # Add to library
			data = []


			for index in selected:
				if pympe.playlists.current.libraries[index] is not pympe.library:
					mediaFile = pm_mediafile.MediaFile(pympe.playlists.current.uris[index])
					result = mediaFile.read()
					if result: data.append(result)
					else: pympe.debug('Tried to import file but couldn\'t read: %s'%pympe.playlists.current.uris[index])

			if data:
				# Queue to insert them and then set the callback when finished to organize them
				pympe.library.insert_multi_tracks(data, callback=pympe.library.organize)
		elif eventId == 20:
			pm_helpers.clipboard_write('\n'.join(uris))
		elif eventId == 25:
			pm_helpers.open_location(uris[0])
		elif eventId == 40:
			pympe.plugins.get_plugin(1323703478).MetadataEditor(uris, trackIds, libraries=[pympe.playlists.current.libraries[x] for x in selected])
		elif eventId == 50:
			libraries = {}

			for index, trackId in zip(selected, trackIds):
				library = pympe.playlists.current.libraries[index]
				if not library in libraries: libraries[library] = [trackId]
				else: libraries[library].append(trackId)


			for index in range(pympe.ui.uiMain.uiTabsLeft.GetPageCount()):
				tab = pympe.ui.uiMain.uiTabsLeft.GetPage(index)
				if type(tab) == type(pympe.ui.uiMain.uiTabLibrary):
					if tab.library.uid in libraries:
						tab.display_subset([trackId for trackId in libraries[tab.library.uid] if trackId != None])

			for index in range(pympe.ui.uiMain.uiTabsLeft.GetPageCount()):
				if pympe.ui.uiMain.uiTabsLeft.GetPage(index) == pympe.ui.uiMain.uiTabLibrary:
					pympe.ui.uiMain.uiTabsLeft.SetSelection(index)
					break
		elif event.Id == 990: # Add Files To Playlist
			dialog = wx.FileDialog(self, message="Select Media Files", style=wx.FD_OPEN|wx.FD_MULTIPLE)
			if dialog.ShowModal() == wx.ID_OK:
				uris = ['file://'+x for x in dialog.GetPaths()]
				trackInfo = []

				for uri in uris:
					trackId = None

					for library in sorted(pympe.libraries.values(), key=lambda x: x.uid):
						trackId = library.get_trackid_from_uri(uri)
						if trackId != None:
							trackInfo.append((library.uid, library.get_single_track_single_value(trackId, 'title', 'joined'), trackId))
							break

					if trackId == None:
						trackInfo.append((-1, uri, -1))

				libraries, titles, trackIds = zip(*trackInfo)
				pympe.playlists.current.enqueue(libraries, uris, titles, trackIds)

		elif event.Id == 991: # Add Folder To Playlist
			dialog = wx.DirDialog(self, message='Select A Directory To Enqueue', style=wx.DD_DIR_MUST_EXIST)
			if dialog.ShowModal() == wx.ID_OK:
				print dialog.GetPath()

		elif event.Id == 1000: # Clear playlist
			pympe.playlists.current.clear()

	def event_key_down(self, event):
		if event.GetKeyCode() == 65 and event.ControlDown(): # 65 = a
			for item in range(len(pympe.playlists.current.uris)):
				self.SetItemState(item, wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)
		else:
			event.Skip()

	def mpevent_player_current_changed(self, event, info, libraryId, library):
		self.RefreshItem(self.currentEntryIndex)
		self.currentEntryIndex = pympe.playlists.current.get_index()
		self.RefreshItem(pympe.playlists.current.get_index())

	def mpevent_pympe_initilized(self, event):
		self.SetItemCount(pympe.playlists.current.get_length())

		for item in pympe.playlists.current.selected:
			self.SetItemState(item, wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)

	def get_selected(self):
		selection = self.GetFirstSelected()
		if selection == -1: return []

		selected = []
		while True:
			selected.append(selection)
			selection = self.GetNextSelected(selection)
			if selection == -1: break

		return selected

class TabPlaylist(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)

		uiSizer = wx.BoxSizer(wx.VERTICAL)
		self.SetSizer(uiSizer)

		self.uiList   = ListPlaylist(self)
		self.uiButton = wx.Button(self, -1, "Main Playlist" if pympe.playlists.current.uid is -1 else pympe.playlists.current.name) # Needs this because it selects the playlist before the ui
		#self.uiButton.SetInitialSize((-1, self.uiButton.GetEffectiveMinSize()[1]*.75))

		uiSizer.Add(self.uiList,   1, wx.EXPAND)
		uiSizer.Add(self.uiButton, 0, wx.EXPAND)

		self.uiButton.Bind(wx.EVT_BUTTON, self.event_button)

		pympe.events.playlist_selected.subscribe(self.mpevent_playlist_selected)
		#pympe.events.playlist_deleted.subscribe(self.mpevent_playlist_deleted)
		pympe.events.playlist_renamed.subscribe(self.mpevent_playlist_renamed)

	def selected_by_notebook(self):
		pass

	def event_button(self, event):
	##### Playlist Menu
		playlistMenu = wx.Menu()
		playlistMenu.Bind(wx.EVT_MENU, self.event_playlist_menu)

		# Dumb Playlists
		playlists = sorted(pympe.playlists.get_playlists(False), key=lambda x: x.name)
		if playlists:
			for playlist in playlists: playlistMenu.Append(playlist.uid+1000, str(playlist.name))
			playlistMenu.AppendSeparator()

		playlistMenu.Append(20, "New Playlist")
		playlistMenu.Append(30, "New Smart Playlist")
		playlistMenu.Append(40, "Import")

		# Smart Playlists
		playlists = sorted(pympe.playlists.get_playlists(True), key=lambda x: x.name)
		if playlists:
			playlistMenu.AppendSeparator()
			for playlist in playlists: playlistMenu.Append(playlist.uid+1000, 'Smart: '+playlist.name)

	##### Top Menu
		menu = wx.Menu()
		menu.Bind(wx.EVT_MENU, self.event_menu)

		if pympe.playlists.current.uid == -1:
			menu.AppendSubMenu(playlistMenu, "Playlists")
			if pympe.playlists.current.userModifiable:
				menu.AppendSeparator()
				menu.Append(10, "Clear")
			menu.AppendSeparator()
			menu.Append(20, "Save As")
			menu.Append(22, "Export (.pls)")
		else:
			menu.Append(0, "Main Playlist")
			menu.AppendSubMenu(playlistMenu, "Playlists")
			menu.AppendSeparator()

			if pympe.playlists.current.smart:
				menu.Append(150, "Regenerate")
				menu.Append(151, "Edit Criteria")
			elif pympe.playlists.current.userModifiable:
				menu.Append(0, "Clear")

			menu.Append(30, "Delete")
			menu.AppendSeparator()
			menu.Append(21, "Rename")
			menu.Append(22, "Export (.pls)")

		self.uiButton.PopupMenu(menu)

	def event_playlist_menu(self, event):
		eventId = event.Id

		if eventId == 20: # New Playlist
			# Get the name
			dialog = wx.TextEntryDialog(self, "What shall the playlist be named?", "Create a playlist.")
			dialog.ShowModal()
			name = dialog.GetValue()
			if not name: return

			# Create the playlist
			playlist = pympe.playlists.create_playlist(name)
			pympe.playlists.select_playlist(playlist.uid)
		elif eventId == 30: # New Smart Playlist
			pympe.plugins.get_plugin(1323703595).PlaylistEditor()
		elif eventId == 40: # Import
			dialog = wx.FileDialog(self, message="Select a playlist", wildcard="PLS files (*.pls)|*.pls")
			if dialog.ShowModal() == wx.ID_OK:
				playlist = pympe.playlists.load_playlist_from_file(dialog.GetPath())
				pympe.playlists.select_playlist(playlist.uid)
		else:
			pympe.playlists.select_playlist(event.Id-1000)

	def event_menu(self, event):
		eventId = event.Id

		if eventId == 0: # Select Main
			pympe.playlists.select_playlist()
		elif eventId == 10: # Clear
			pympe.playlists.current.clear()
		elif eventId == 30: # Delete
			result = wx.MessageDialog(self, "Delete playlist: %s?"%pympe.playlists.current.name, style=wx.YES_NO|wx.NO_DEFAULT).ShowModal()
			if result == wx.ID_YES: pympe.playlists.delete_playlist(pympe.playlists.current.uid)
		elif eventId == 20: # Save As
			# Get the name
			dialog = wx.TextEntryDialog(self, "What shall the playlist be named?", "Create a playlist.")
			dialog.ShowModal()
			name = dialog.GetValue()
			if not name: return

			# Create the playlist
			playlist = pympe.playlists.create_playlist(name)
			playlist.copy_playlist(pympe.playlists.main)
			pympe.playlists.main.clear()
			pympe.playlists.select_playlist(playlist.uid)
		elif eventId == 21: # Rename
			dialog = wx.TextEntryDialog(self, "What shall the playlist be renamed to?", "Rename a playlist.", pympe.playlists.current.name)
			if dialog.ShowModal() == wx.ID_CANCEL: return
			name = dialog.GetValue()
			if not name: return
			pympe.playlists.rename_playlist(pympe.playlists.current.uid, name)
		elif eventId == 22: # Export (.pls)
			if pympe.playlists.current == pympe.playlists.main:
				dialog = wx.TextEntryDialog(self, "What shall the playlist be named?", "Export playlist.")
				dialog.ShowModal()
				name = dialog.GetValue()
				if not name: return
			else:
				name = None

			dialog = wx.FileDialog(self, message="Export playlist", wildcard="PLS files (*.pls)|*.pls", style=wx.FD_SAVE|wx.FD_OVERWRITE_PROMPT)
			if dialog.ShowModal() == wx.ID_OK:
				filename = dialog.GetPath()
				if not filename.lower().endswith(".pls"): filename = filename+".pls"
				pympe.playlists.save_playlist_to_file(filename, pympe.playlists.current.uid, name)
		elif eventId == 150: # Regenerate
			pympe.playlists.current.refresh()
		elif eventId == 151: # Edit Criteria
			pympe.plugins.get_plugin(1323703595).PlaylistEditor(pympe.playlists.current)
			pympe.playlists.current.refresh()

	def mpevent_playlist_renamed(self, event, playlistUid, playlistName):
		if playlistUid == pympe.playlists.current.uid:
			self.uiButton.SetLabel(playlistName)

	def mpevent_playlist_selected(self, event, playlistUid):
		self.uiButton.SetLabel("Main Playlist" if playlistUid is -1 else pympe.playlists.current.name)

	#def mpevent_playlist_deleted(self, event, playlistUid):
		#if playlistUid == pympe.playlists.current.uid:
			#pympe.playlists.select_playlist()
