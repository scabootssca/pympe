import wx, pympe, pm_language, pm_transcoder, pm_helpers
from os import sep as osSeperator

class SettingsDialog(wx.Frame):
	#@pm_helpers.threaded
	#def write_artwork_path(self):
		#import pm_mediafile, os
		#index = 0
		#total = len(pympe.library.tracks)
		#for trackId, values in pympe.library.tracks.iteritems():
			#print 'Writing %s/%s'%(index, total)
			#index += 1

			#filename = '%s/artwork/%20i.jpg'%(os.path.expanduser(pympe.config.get('cache', 'directory')), trackId)
			#with open(filename, 'w') as artwork:
				#mediaFile = pm_mediafile.MediaFile(values['uri'])
				#if not mediaFile.read():
					#print 'Problem with %s %s'%(trackId, values['title'])


				#try: artwork.write(mediaFile['art'])
				#except: print 'No Artwork %s %s'%(trackId, values['title'])

	def __init__(self):
		wx.Frame.__init__(self, pympe.ui.uiMain, -1, 'Pympe Settings', style=wx.DEFAULT_FRAME_STYLE|wx.FRAME_FLOAT_ON_PARENT^wx.RESIZE_BORDER)

		#self.write_artwork_path()


		self.uiNotebook = uiNotebook = wx.Notebook(self)

		uiNotebook.AddPage(self.make_library_panel(uiNotebook), "Libraries")
		uiNotebook.AddPage(self.make_interface_panel(uiNotebook), "General Interface")
		uiNotebook.AddPage(self.make_playback_panel(uiNotebook), "Playback")
		uiNotebook.AddPage(self.make_format_panel(uiNotebook), "Format Settings")

		uiNotebook.SetSelection(pympe.config.get('ui', 'settingsDialogSelectedTab'))

		uiButtonClose = wx.Button(self, wx.ID_CLOSE)
		uiButtonClose.Bind(wx.EVT_BUTTON, self.event_button_close)

		uiSizer = wx.BoxSizer(wx.VERTICAL)
		uiSizer.Add(uiNotebook, 1, wx.ALL|wx.EXPAND, 5)
		uiSizer.Add((500, 0))
		uiSizer.Add(uiButtonClose, 0, wx.ALIGN_RIGHT|wx.BOTTOM|wx.RIGHT, 5)
		self.SetSizerAndFit(uiSizer)

		self.Show()
		self.CenterOnParent()

	def make_format_panel(self, parent):
		uiPanel = wx.Panel(parent)
		uiSizer = wx.GridBagSizer(5, 5)
		uiFontHeader = wx.Font(-1, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

		# Organize Format
		uiLabelOrganize = wx.StaticText(uiPanel, -1, "Filename Settings")
		self.uiTextFormat = uiTextFormat = wx.TextCtrl(uiPanel, -1, pympe.config.get("import", "renamingRules"))
		uiLabelExample = wx.StaticText(uiPanel, -1, '3Oh!3/2008 Want/2 Punkb*tch.mp3')
		uiButtonPresets = wx.Button(uiPanel, -1, 'Presets')
		uiButtonAvailable = wx.Button(uiPanel, -1, 'Available Tags')

		uiLabelOrganize.SetFont(uiFontHeader)
		uiLabelExample.Wrap(480)
		uiTextFormat.label = uiLabelExample

		uiTextFormat.Bind(wx.EVT_TEXT, self.event_text_format)
		uiButtonPresets.Bind(wx.EVT_BUTTON, self.event_button_presets)
		uiButtonAvailable.Bind(wx.EVT_BUTTON, self.event_button_available)

		self.event_text_format(None, uiTextFormat, uiLabelExample)


		uiSizer.Add(uiLabelOrganize, (0, 0), (1, 4), wx.TOP, 10)
		uiSizer.Add((10, 10), (1, 0), (1, 1))
		uiSizer.Add(uiLabelExample, (1, 1), (1, 3), wx.EXPAND)
		uiSizer.Add(uiTextFormat, (2, 1), (1, 3), wx.EXPAND)
		uiSizer.Add(uiButtonPresets, (3, 1))
		uiSizer.Add(uiButtonAvailable, (3, 3), flag=wx.ALIGN_RIGHT)

		# Import Format
		formatSettings = [pympe.config.get('import', 'format'), pympe.config.get('import', 'quality')]
		uiLabelFormat = wx.StaticText(uiPanel, -1, "Import Format")
		uiChoiceFormatType = wx.Choice(uiPanel, -1, choices=pm_transcoder.formats.keys())
		uiChoiceFormatQuality = wx.Choice(uiPanel, -1, choices=[str(x) for x in pm_transcoder.formats[formatSettings[0]]['quality']])
		uiLabelFormatDescription = wx.StaticText(uiPanel, -1, pm_transcoder.formats[formatSettings[0]]['description'])

		uiLabelFormat.SetFont(uiFontHeader)
		uiLabelFormatDescription.Wrap(480)
		uiChoiceFormatType.SetStringSelection(formatSettings[0])
		uiChoiceFormatQuality.SetSelection(formatSettings[1])
		uiChoiceFormatType.Bind(wx.EVT_CHOICE, self.event_choice_format_type)
		uiChoiceFormatType.items = (uiChoiceFormatQuality, uiLabelFormatDescription)

		uiSizer.Add(uiLabelFormat, (4, 0), (1, 4), wx.TOP, 10)
		uiSizer.Add(uiChoiceFormatType, (5, 1), (1, 3), wx.EXPAND)
		uiSizer.Add(uiChoiceFormatQuality, (6, 1), (1, 3), wx.EXPAND)
		uiSizer.Add(uiLabelFormatDescription, (7, 1), (1, 3))

		uiSizer.AddGrowableCol(2)

		box = wx.BoxSizer()
		box.Add(uiSizer, 1, wx.EXPAND|wx.ALL, 5)
		uiPanel.SetSizerAndFit(box)

		self.formatPanelObjects = {'textFormat':uiTextFormat,
									'choiceFormatType':uiChoiceFormatType,
									'choiceFormatQuality':uiChoiceFormatQuality
									}

		return uiPanel

	def make_library_panel(self, parent):
		uiPanel = wx.Panel(parent)
		uiSizer = wx.GridBagSizer(5, 5)
		uiFontHeader = wx.Font(-1, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

	# Path Settings
		uiLabelPaths = wx.StaticText(uiPanel, -1, "Library Paths")
		uiLabelLibraryChoice = wx.StaticText(uiPanel, -1, 'Which library would you like to modify?')
		self.uiChoiceLibraryChoice = wx.Choice(uiPanel, -1)
		self.uiCheckBoxPaths = uiCheckBoxPaths = wx.CheckListBox(uiPanel, -1)
		uiButtonAddPath = wx.Button(uiPanel, 0, 'Add Path')
		uiButtonRemovePath = wx.Button(uiPanel, 1, 'Remove Path')
		uiCheckWatchLibrary = wx.CheckBox(uiPanel, -1, 'Automatically watch library for changes')
		uiCheckKeepOrganized = wx.CheckBox(uiPanel, -1, 'Keep my default library path organized')
		uiCheckWriteFolderImage = wx.CheckBox(uiPanel, -1, 'Write folder.jpg to library folders')
		uiCheckSaveMissingTags = wx.CheckBox(uiPanel, -1, "Save missing tags to library during playback")
		uiLabelDefaultPath = wx.StaticText(uiPanel, -1, 'No search paths exist.')
		uiButtonSetDefault = wx.Button(uiPanel, -1, 'Set New Default')

		libraries = [pympe.libraries[uid] for uid in sorted(pympe.libraries.keys())]
		self.uiChoiceLibraryChoice.libraries = libraries
		self.uiChoiceLibraryChoice.library = libraries[0]
		self.uiChoiceLibraryChoice.AppendItems([x.dbFilename.rsplit(osSeperator, 1)[-1] for x in libraries])
		self.uiChoiceLibraryChoice.SetSelection(0)
		self.uiCheckBoxPaths.AppendItems(pympe.config.get(libraries[0].configKey, 'searchPaths'))

		uiLabelDefaultPath.SetLabel(pympe.config.get(libraries[0].configKey, 'defaultPath'))

		uiLabelPaths.SetFont(uiFontHeader)
		uiCheckKeepOrganized.SetValue(pympe.config.get(libraries[0].configKey, 'keepOrganized'))
		uiCheckWatchLibrary.SetValue(pympe.config.get(libraries[0].configKey, 'watchForChanges'))
		uiCheckWriteFolderImage.SetValue(pympe.config.get(libraries[0].configKey, 'writeFolderImage'))
		uiCheckSaveMissingTags.SetValue(pympe.config.get(libraries[0].configKey, 'saveMissingTags'))
		uiButtonSetDefault.Disable()
		uiCheckBoxPaths.button = uiButtonSetDefault
		uiButtonSetDefault.label = uiLabelDefaultPath
		uiLabelDefaultPath.button = uiButtonSetDefault

		self.uiChoiceLibraryChoice.Bind(wx.EVT_CHOICE, self.event_choice_select_library)
		uiButtonAddPath.Bind(wx.EVT_BUTTON, self.event_button_modify_path)
		uiButtonRemovePath.Bind(wx.EVT_BUTTON, self.event_button_modify_path)
		uiButtonSetDefault.Bind(wx.EVT_BUTTON, self.event_button_set_default)
		uiCheckBoxPaths.Bind(wx.EVT_CHECKLISTBOX, self.event_check_paths)
		self.uiCheckBoxPaths.Bind(wx.EVT_RIGHT_UP, self.event_right_check_paths)


		##
		# Cant move these without changing the locations in event_choice_select_library
		#######################################
		uiSizer.Add((10, 10), (0, 0), (1, 1))
		uiSizer.Add(uiLabelLibraryChoice, (0, 1), flag=wx.ALIGN_CENTER_VERTICAL)
		uiSizer.Add(self.uiChoiceLibraryChoice, (0, 2), (1, 2), flag=wx.EXPAND)
		uiSizer.Add(uiLabelPaths, (1, 0), (1, 4), wx.TOP, 10)
		uiSizer.Add(uiCheckBoxPaths, (2, 1), (1, 3), wx.EXPAND)
		uiSizer.Add(uiButtonAddPath, (3, 1), (1, 1))
		uiSizer.Add(uiButtonRemovePath, (3, 3), (1, 1), wx.ALIGN_RIGHT)
		uiSizer.Add(wx.StaticText(uiPanel, -1, 'Default library path:'), (4, 1), (1, 3), wx.TOP, 5)
		uiSizer.Add(uiLabelDefaultPath, (5, 1), (1, 3), wx.EXPAND)
		uiSizer.Add(uiButtonSetDefault, (6, 1))
		uiSizer.Add(uiCheckWatchLibrary, (7, 1), (1, 1), wx.TOP, 5)
		uiSizer.Add(uiCheckKeepOrganized, (8, 1), (1, 1))
		uiSizer.Add(uiCheckWriteFolderImage, (9, 1), (1, 1))
		uiSizer.Add(uiCheckSaveMissingTags, (10, 1), (1, 1))

		# Library Settings
		uiLabelLibrary = wx.StaticText(uiPanel, -1, "Library Interface")
		uiCheckBoxFilters = wx.CheckListBox(uiPanel, -1)
		uiCheckVerticalLayout = wx.CheckBox(uiPanel, -1, "Use vertical library layout")

		uiLabelLibrary.SetFont(uiFontHeader)

		#uiCheckBoxFilters.Clear()
		checked, tags = zip(*pympe.config[self.uiChoiceLibraryChoice.library.configKey]["ui_filters"])
		uiCheckBoxFilters.AppendItems([pm_language.readableTags[tag] for tag in tags])
		for index, check in enumerate(checked): uiCheckBoxFilters.Check(index, check)
		uiCheckBoxFilters.tags = tags
		uiCheckBoxFilters.dragging = False
		uiCheckBoxFilters.dPos = (0, 0)
		uiCheckBoxFilters.dIndex = -1

		uiCheckVerticalLayout.SetValue(pympe.config.get(self.uiChoiceLibraryChoice.library.configKey, 'ui_vertical_layout'))

		uiCheckBoxFilters.Bind(wx.EVT_LEFT_DOWN, self.event_filters_begin_drag)
		uiCheckBoxFilters.Bind(wx.EVT_MOTION, self.event_filters_drag)
		uiCheckBoxFilters.Bind(wx.EVT_LEFT_UP, self.event_filters_end_drag)

		uiSizer.Add(uiLabelLibrary, (11, 0), (1, 4), wx.TOP, 10)
		uiSizer.Add(wx.StaticText(uiPanel, -1, 'Select and drag filter panes:'), (12, 1), (1, 3))
		uiSizer.Add(uiCheckBoxFilters, (13, 1), (1, 3), wx.EXPAND)
		uiSizer.Add(uiCheckVerticalLayout, (14, 1), (1, 3))

		uiSizer.AddGrowableCol(2)

		box = wx.BoxSizer()
		box.Add(uiSizer, 1, wx.EXPAND|wx.ALL, 5)
		uiPanel.SetSizerAndFit(box)

		self.libraryPanelObjects = {'checkBoxPaths':uiCheckBoxPaths,
									'labelDefaultPath':uiLabelDefaultPath,
									'checkWatchLibrary':uiCheckWatchLibrary,
									'checkWriteFolderImage':uiCheckWriteFolderImage,
									'checkKeepOrganized':uiCheckKeepOrganized,
									'saveMissingTags':uiCheckSaveMissingTags,
									'checkBoxFilters':uiCheckBoxFilters,
									'checkVerticalLayout':uiCheckVerticalLayout,
									'buttonAddRemovePath':[uiButtonAddPath,uiButtonRemovePath]
									}

		return uiPanel

	def event_choice_select_library(self, event):
		# Save the old
		#################
		key = event.EventObject.library.configKey
		pympe.config.set(key, 'searchPaths', self.libraryPanelObjects['checkBoxPaths'].GetStrings(), False)
		pympe.config.set(key, 'defaultPath', self.libraryPanelObjects['labelDefaultPath'].GetLabel(), False)
		pympe.config.set(key, 'watchForChanges', self.libraryPanelObjects['checkWatchLibrary'].GetValue(), False)
		pympe.config.set(key, 'keepOrganized', self.libraryPanelObjects['checkKeepOrganized'].GetValue(), False)
		pympe.config.set(key, 'writeFolderImage', self.libraryPanelObjects['checkWriteFolderImage'].GetValue(), False)
		pympe.config.set(key, 'saveMissingTags', self.libraryPanelObjects['saveMissingTags'].GetValue(), False)


		filters = [[self.libraryPanelObjects['checkBoxFilters'].IsChecked(index), tag] for index, tag in enumerate(self.libraryPanelObjects['checkBoxFilters'].tags)]
		pympe.config.set(key, 'ui_filters', filters, False)
		pympe.config.set(key, 'ui_vertical_layout', self.libraryPanelObjects['checkVerticalLayout'].GetValue(), False)

		# Load the new
		################
		library = event.EventObject.libraries[event.GetSelection()]
		event.EventObject.library = library

		self.uiCheckBoxPaths.Clear()
		self.uiCheckBoxPaths.AppendItems(pympe.config.get(library.configKey, 'searchPaths'))

		self.libraryPanelObjects['labelDefaultPath'].SetLabel(pympe.config.get(library.configKey, 'defaultPath'))

		self.libraryPanelObjects['checkWatchLibrary'].SetValue(pympe.config.get(library.configKey, 'watchForChanges'))
		self.libraryPanelObjects['checkKeepOrganized'].SetValue(pympe.config.get(library.configKey, 'keepOrganized'))
		self.libraryPanelObjects['checkWriteFolderImage'].SetValue(pympe.config.get(library.configKey, 'writeFolderImage'))
		self.libraryPanelObjects['saveMissingTags'].SetValue(pympe.config.get(library.configKey, 'saveMissingTags'))

		self.libraryPanelObjects['checkBoxFilters'].Clear()
		checked, tags = zip(*pympe.config[self.uiChoiceLibraryChoice.library.configKey]["ui_filters"])
		self.libraryPanelObjects['checkBoxFilters'].AppendItems([pm_language.readableTags[tag] for tag in tags])
		for index, check in enumerate(checked): self.libraryPanelObjects['checkBoxFilters'].Check(index, check)
		self.libraryPanelObjects['checkBoxFilters'].tags = tags
		self.libraryPanelObjects['checkVerticalLayout'].SetValue(pympe.config.get(library.configKey, 'ui_vertical_layout'))


		self.libraryPanelObjects['checkBoxPaths'].Enable(library.device == None)
		self.libraryPanelObjects['labelDefaultPath'].Enable(library.device == None)
		self.libraryPanelObjects['checkWatchLibrary'].Enable(library.device == None)
		self.libraryPanelObjects['checkWriteFolderImage'].Enable(library.device == None)
		self.libraryPanelObjects['checkKeepOrganized'].Enable(library.device == None)
		self.libraryPanelObjects['saveMissingTags'].Enable(library.device == None)
		self.libraryPanelObjects['buttonAddRemovePath'][0].Enable(library.device == None)
		self.libraryPanelObjects['buttonAddRemovePath'][1].Enable(library.device == None)


	def event_right_check_paths(self, event):
		def event_menu(event):
			if event.Id == 10: # Scan now
				pympe.library.scan_single_directory_threaded(self.uiCheckBoxPaths.GetStrings()[self.uiCheckBoxPaths.GetSelection()])
			elif event.Id == 20: # Remove
				self.uiCheckBoxPaths.Delete(self.uiCheckBoxPaths.GetSelection())

		menu = wx.Menu()
		menu.Append(10, 'Scan Now')
		menu.AppendSeparator()
		menu.Append(20, 'Remove')
		menu.Bind(wx.EVT_MENU, event_menu)
		event.EventObject.PopupMenu(menu)

	def event_button_modify_path(self, event):

		if event.Id == 0: # Add
			dialog = wx.DirDialog(pympe.ui.uiMain, "Choose a directory to add to the library.")

			if dialog.ShowModal() == wx.ID_OK:
				path = dialog.GetPath()

				# If it's a new one then set the default path
				if not self.uiCheckBoxPaths.GetStrings():
					self.event_button_set_default(None, path)

				if not path in self.uiCheckBoxPaths.GetStrings():
					self.uiCheckBoxPaths.Append(path)

			dialog.Destroy()
		else:
			checked = set(self.uiCheckBoxPaths.GetChecked()).union([self.uiCheckBoxPaths.GetSelection()])

			for x in reversed(sorted(self.uiCheckBoxPaths.GetChecked())):
				self.uiCheckBoxPaths.Delete(x)

			# If the default isn't in there anymore then change to any available
			availableStrings = self.uiCheckBoxPaths.GetStrings()
			if not self.libraryPanelObjects['labelDefaultPath'].GetLabel() in availableStrings:
				if availableStrings: self.event_button_set_default(None, availableStrings[0])
				else: self.event_button_set_default(None, '')

	def event_button_set_default(self, event, directory=None):
		if directory == None: directory = self.uiCheckBoxPaths.GetCheckedStrings()[0]
		self.libraryPanelObjects['labelDefaultPath'].SetLabel(directory)
		self.libraryPanelObjects['labelDefaultPath'].button.Disable()
		self.uiCheckBoxPaths.SetChecked(())

	def event_check_paths(self, event):
		event.EventObject.button.Enable(True if event.EventObject.GetChecked() else False)

	def event_button_presets(self, event):
		presets = ["%albumartist%/%year% %album%/%tracknumber% %title%",
					"%genre%/%albumartist%/%year% %album%/%tracknumber% %title%",
					"%albumartist%/%year% %album%/%tracknumber% - %title% - ((%artist%))"]

		def event_menu(event):
			self.uiTextFormat.SetValue(presets[event.Id])
			self.uiTextFormat.SetFocus()
			self.uiTextFormat.SetSelection(0, 0)

		menu = wx.Menu()
		for i, x in enumerate(presets): menu.Append(i, x)
		menu.Bind(wx.EVT_MENU, event_menu)
		event.GetEventObject().PopupMenu(menu)

	def event_button_available(self, event):
		tags = ["artist", "albumartist", "album", "title", "genre", "tracknumber", "year", "discnumber"]

		def event_menu(event):
			text = self.uiTextFormat.GetValue()
			point = self.uiTextFormat.GetInsertionPoint()
			self.uiTextFormat.SetValue(text[:point]+"%%%s%%"%tags[event.Id]+text[point:])
			self.uiTextFormat.SetFocus()
			self.uiTextFormat.SetSelection(0, 0)
			self.uiTextFormat.SetInsertionPoint(point+len(tags[event.Id])+2)

		menu = wx.Menu()
		for index, tag in enumerate(tags):
			menu.Append(index, pm_language.readableTags[tag])
		menu.Bind(wx.EVT_MENU, event_menu)
		self.PopupMenu(menu)

	def event_text_format(self, event, uiTextFormat=None, uiLabelExample=None):
		info = {"genre":"Crunkcore", "artist":"3Oh!3", "albumartist":"3Oh!3", "album":"Want", "title":"Punkb*tch", "tracknumber":2, "year":"2008", "discnumber":1, "uri":".mp3"}

		if event is None:
			uiLabelExample.SetLabel(pm_helpers.get_info_format_string(info, uiTextFormat.GetValue()))
		else:
			event.EventObject.label.SetLabel(pm_helpers.get_info_format_string(info, event.EventObject.GetValue()))

	def event_choice_format_type(self, event):
		transcodeFormat = pm_transcoder.formats[event.String]

		event.EventObject.items[0].Clear()
		event.EventObject.items[0].AppendItems(transcodeFormat['quality'])
		event.EventObject.items[0].SetSelection(transcodeFormat['default'])

		event.EventObject.items[1].SetLabel(transcodeFormat['description'])
		event.EventObject.items[1].Wrap(480)

	def make_playback_panel(self, parent):
		uiPanel = wx.Panel(parent)
		uiSizer = wx.GridBagSizer(5, 5)

		uiFontHeader = wx.Font(-1, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

		# General Playback Settings
		uiLabelGeneral = wx.StaticText(uiPanel, -1, "General")
		uiCheckEmbeddedTags = wx.CheckBox(uiPanel, -1, "Read embedded tags over library tags during playback")
		uiCheckGapless = wx.CheckBox(uiPanel, -1, "Use gapless playback")
		uiResumePlayState = wx.CheckBox(uiPanel, -1, "Resume playback state on startup")
		uiResumePlayPosition = wx.CheckBox(uiPanel, -1, "Resume position in active track on startup ")

		uiLabelGeneral.SetFont(uiFontHeader)
		uiCheckEmbeddedTags.SetValue(pympe.config.get('player', 'useTagsFromFile'))
		uiCheckGapless.SetValue(pympe.config.get('player', 'gapless'))
		uiResumePlayState.SetValue(pympe.config.get('player', 'resumePlayState'))
		uiResumePlayPosition.SetValue(pympe.config.get('player', 'resumePlayPosition'))

		uiSizer.Add(uiLabelGeneral, (0, 0), (1, 4), wx.TOP, 10)
		uiSizer.Add((10, 10), (1, 0), (1, 1))
		uiSizer.Add(uiCheckEmbeddedTags, (1, 1), (1, 3))
		uiSizer.Add(uiCheckGapless, (2, 1), (1, 3))
		uiSizer.Add(uiResumePlayState, (3, 1), (1, 3))
		uiSizer.Add(uiResumePlayPosition, (4, 1), (1, 3))

		self.playbackPanelObjects = {'checkEmbeddedTags':uiCheckEmbeddedTags,
									'checkGapless':uiCheckGapless,
									'resumePlayState':uiResumePlayState,
									'resumePlayPosition':uiResumePlayPosition}

		uiSizer.AddGrowableCol(2)

		box = wx.BoxSizer()
		box.Add(uiSizer, 1, wx.EXPAND|wx.ALL, 5)
		uiPanel.SetSizerAndFit(box)
		return uiPanel

	def make_interface_panel(self, parent):
		uiPanel = wx.Panel(parent)
		uiSizer = wx.GridBagSizer(5, 5)

		uiFontHeader = wx.Font(-1, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

		# General Interface Settings
		uiLabelGeneral = wx.StaticText(uiPanel, -1, "General")
		uiCheckSeperatePlaylist = wx.CheckBox(uiPanel, -1, "Show playlist as seperate panel")
		uiCheckShowDebug = wx.CheckBox(uiPanel, -1, "Show debug tab")

		uiLabelGeneral.SetFont(uiFontHeader)
		uiCheckSeperatePlaylist.SetValue(pympe.config.get('ui', 'seperate_playlist'))
		uiCheckShowDebug.SetValue(pympe.config.get('ui', 'show_debug_tab'))

		uiSizer.Add(uiLabelGeneral, (0, 0), (1, 4), wx.TOP, 10)
		uiSizer.Add((10, 10), (1, 0), (1, 1))
		uiSizer.Add(uiCheckSeperatePlaylist, (1, 1), (1, 3))
		uiSizer.Add(uiCheckShowDebug, (2, 1), (1, 3))

		# Now playing settings
		uiLabelNowPlaying = wx.StaticText(uiPanel, -1, "Now Playing")
		uiCheckEnablePane = wx.CheckBox(uiPanel, -1, "Enable Now Playing pane")
		uiCheckRoundedArt = wx.CheckBox(uiPanel, -1, "Round album art edges")
		uiCheckArtReflection = wx.CheckBox(uiPanel, -1, "Show album art reflection")
		uiCheckArtShadow = wx.CheckBox(uiPanel, -1, "Show album art shadow")
		uiCheckMetadata = wx.CheckBox(uiPanel, -1, "Show current artist and title")
		uiCheckCustomBg = wx.CheckBox(uiPanel, -1, "Use custom background color")
		uiSliderBgColor = wx.Slider(uiPanel, -1, 150, 0, 255)

		uiLabelNowPlaying.SetFont(uiFontHeader)
		uiCheckEnablePane.SetValue(pympe.config.get('np', 'enabled'))
		uiCheckEnablePane.Bind(wx.EVT_CHECKBOX, self.event_check_np_pane)
		uiCheckRoundedArt.SetValue(pympe.config.get('np', 'rounded_edges'))
		uiCheckArtReflection.SetValue(pympe.config.get('np', 'enable_reflection'))
		uiCheckArtShadow.SetValue(pympe.config.get('np', 'enable_shadow'))
		uiCheckMetadata.SetValue(pympe.config.get('np', 'show_title_and_artist'))
		uiCheckCustomBg.SetValue(pympe.config.get('np', 'use_custom_background'))
		uiCheckCustomBg.Bind(wx.EVT_CHECKBOX, self.event_check_custom_bg)
		uiCheckCustomBg.item = uiSliderBgColor
		uiSliderBgColor.SetValue(pympe.config.get('np', 'background_color')[0])
		uiSliderBgColor.Enable(uiCheckCustomBg.GetValue())

		uiCheckRoundedArt.Enable(pympe.config['np']['enabled'])
		uiCheckArtReflection.Enable(pympe.config['np']['enabled'])
		uiCheckArtShadow.Enable(pympe.config['np']['enabled'])
		uiCheckMetadata.Enable(pympe.config['np']['enabled'])
		uiCheckCustomBg.Enable(pympe.config['np']['enabled'])
		uiSliderBgColor.Enable(pympe.config['np']['enabled'])

		uiSizer.Add(uiLabelNowPlaying, (3, 0), (1, 4), wx.TOP, 10)
		uiSizer.Add(uiCheckEnablePane, (4, 1), (1, 3))
		uiSizer.Add(uiCheckRoundedArt, (5, 1), (1, 3))
		uiSizer.Add(uiCheckArtReflection, (6, 1), (1, 3))
		uiSizer.Add(uiCheckArtShadow, (7, 1), (1, 3))
		uiSizer.Add(uiCheckMetadata, (8, 1), (1, 3))
		uiSizer.Add(uiCheckCustomBg, (9, 1), (1, 3))
		uiSizer.Add(uiSliderBgColor, (10, 1), (1, 3), wx.EXPAND)
		uiSizer.Add(wx.StaticText(uiPanel, -1, 'Black'), (11, 1), (1, 1))
		uiSizer.Add(wx.StaticText(uiPanel, -1, 'White'), (11, 3), (1, 1), wx.ALIGN_RIGHT)

		# Theatre mode settings
		uiLabelTheatre = wx.StaticText(uiPanel, -1, "Theatre Mode")
		uiCheckTheatrePassword = wx.CheckBox(uiPanel, -1, "Require password to exit theatre mode")
		uiTextTheatrePassword = wx.TextCtrl(uiPanel, -1, "")
		uiChoiceLockMode = wx.Choice(uiPanel, -1, choices=['Allow All Controls When Locked', 'Allow Only Play/Pause', 'Disallow All Controls'])

		uiLabelTheatre.SetFont(uiFontHeader)
		uiCheckTheatrePassword.SetValue(pympe.config.get('ui', 'use_theatre_unlock_password'))
		uiCheckTheatrePassword.Bind(wx.EVT_CHECKBOX, self.event_check_theatre_password)
		uiCheckTheatrePassword.items = (uiTextTheatrePassword, uiChoiceLockMode)
		uiTextTheatrePassword.SetValue(pympe.config.get('ui', 'theatre_unlock_password'))
		uiChoiceLockMode.SetSelection(pympe.config.get('ui', 'theatre_lock_mode'))
		uiTextTheatrePassword.Enable(uiCheckTheatrePassword.GetValue())
		uiChoiceLockMode.Enable(uiCheckTheatrePassword.GetValue())

		uiSizer.Add(uiLabelTheatre, (12, 0), (1, 4), wx.TOP, 10)
		uiSizer.Add(uiCheckTheatrePassword, (13, 1), (1, 3))
		uiSizer.Add(uiTextTheatrePassword, (14, 1), (1, 3), wx.EXPAND)
		uiSizer.Add(uiChoiceLockMode, (15, 1), (1, 3), wx.EXPAND)

		self.interfacePanelObjects = {'seperatePlaylist':uiCheckSeperatePlaylist,
								'checkDebug':uiCheckShowDebug,
								'checkEnablePane':uiCheckEnablePane,
								'checkRoundedArt':uiCheckRoundedArt,
								'checkArtReflection':uiCheckArtReflection,
								'checkArtShadow':uiCheckArtShadow,
								'checkMetadata':uiCheckMetadata,
								'checkCustomBg':uiCheckCustomBg,
								'sliderBgColor':uiSliderBgColor,
								'checkTheatrePassword':uiCheckTheatrePassword,
								'textTheatrePassword':uiTextTheatrePassword,
								'choiceLockMode':uiChoiceLockMode}

		uiSizer.AddGrowableCol(2)

		box = wx.BoxSizer()
		box.Add(uiSizer, 1, wx.EXPAND|wx.ALL, 5)
		uiPanel.SetSizerAndFit(box)
		return uiPanel

	def event_button_close(self, event):
		pympe.ui.uiMain.settingsDialog = None

		# Library Panel
		pympe.config.set(self.uiChoiceLibraryChoice.library.configKey, 'searchPaths', self.libraryPanelObjects['checkBoxPaths'].GetStrings(), False)
		pympe.config.set(self.uiChoiceLibraryChoice.library.configKey, 'defaultPath', self.libraryPanelObjects['labelDefaultPath'].GetLabel(), False)
		pympe.config.set(self.uiChoiceLibraryChoice.library.configKey, 'watchForChanges', self.libraryPanelObjects['checkWatchLibrary'].GetValue(), False)
		pympe.config.set(self.uiChoiceLibraryChoice.library.configKey, 'keepOrganized', self.libraryPanelObjects['checkKeepOrganized'].GetValue(), False)
		pympe.config.set(self.uiChoiceLibraryChoice.library.configKey, 'writeFolderImage', self.libraryPanelObjects['checkWriteFolderImage'].GetValue(), False)
		pympe.config.set(self.uiChoiceLibraryChoice.library.configKey, 'saveMissingTags', self.libraryPanelObjects['saveMissingTags'].GetValue(), False)

		filters = [[self.libraryPanelObjects['checkBoxFilters'].IsChecked(index), tag] for index, tag in enumerate(self.libraryPanelObjects['checkBoxFilters'].tags)]
		pympe.config.set(self.uiChoiceLibraryChoice.library.configKey, 'ui_filters', filters, False)
		pympe.config.set(self.uiChoiceLibraryChoice.library.configKey, 'ui_vertical_layout', self.libraryPanelObjects['checkVerticalLayout'].GetValue(), False)

		# Format Panel
		pympe.config.set('import', 'renamingRules', self.formatPanelObjects['textFormat'].GetValue(), False)
		pympe.config.set('import', 'format', self.formatPanelObjects['choiceFormatType'].GetStringSelection(), False)
		pympe.config.set('import', 'quality', self.formatPanelObjects['choiceFormatQuality'].GetSelection(), False)

		# Interface Panel
		pympe.config.set('ui', 'seperate_playlist', self.interfacePanelObjects['seperatePlaylist'].GetValue(), False)
		pympe.config.set('ui', 'show_debug_tab', self.interfacePanelObjects['checkDebug'].GetValue(), False)
		pympe.ui.uiMain.seperate_playlist(self.interfacePanelObjects['seperatePlaylist'].GetValue())

		pympe.config.set('np', 'enabled', self.interfacePanelObjects['checkEnablePane'].GetValue(), False)
		pympe.config.set('np', 'rounded_edges', self.interfacePanelObjects['checkRoundedArt'].GetValue(), False)
		pympe.config.set('np', 'enable_reflection', self.interfacePanelObjects['checkArtReflection'].GetValue(), False)
		pympe.config.set('np', 'enable_shadow', self.interfacePanelObjects['checkArtShadow'].GetValue(), False)
		pympe.config.set('np', 'show_title_and_artist', self.interfacePanelObjects['checkMetadata'].GetValue(), False)
		pympe.config.set('np', 'use_custom_background', self.interfacePanelObjects['checkCustomBg'].GetValue(), False)
		pympe.config.set('np', 'background_color', [self.interfacePanelObjects['sliderBgColor'].GetValue()]*3, False)
		pympe.config.set('ui', 'use_theatre_unlock_password', self.interfacePanelObjects['checkTheatrePassword'].GetValue(), False)
		pympe.config.set('ui', 'theatre_unlock_password', self.interfacePanelObjects['textTheatrePassword'].GetValue(), False)
		pympe.config.set('ui', 'theatre_lock_mode', self.interfacePanelObjects['choiceLockMode'].GetSelection(), False)

		# Filter panes
		#pympe.ui.uiMain.uiTabLibrary.update_layout()

		# Playback Panel
		pympe.config.set('player', 'useTagsFromFile', self.playbackPanelObjects['checkEmbeddedTags'].GetValue(), False)
		pympe.config.set('player', 'gapless', self.playbackPanelObjects['checkGapless'].GetValue(), False)
		pympe.config.set('player', 'resumePlayState', self.playbackPanelObjects['resumePlayState'].GetValue(), False)
		pympe.config.set('player', 'resumePlayPosition', self.playbackPanelObjects['resumePlayPosition'].GetValue(), False)


		# Ui Events
		pympe.config.set('ui', 'settingsDialogSelectedTab', self.uiNotebook.GetSelection(), False)

		# Send out all the events
		pympe.config.send_events()

		self.Destroy()

	def event_check_theatre_password(self, event):
		event.EventObject.items[0].Enable(event.Int)
		event.EventObject.items[1].Enable(event.Int)

	def event_check_np_pane(self, event):
		self.interfacePanelObjects['checkRoundedArt'].Enable(event.Int)
		self.interfacePanelObjects['checkArtReflection'].Enable(event.Int)
		self.interfacePanelObjects['checkMetadata'].Enable(event.Int)
		self.interfacePanelObjects['checkArtShadow'].Enable(event.Int)
		self.interfacePanelObjects['checkCustomBg'].Enable(event.Int)
		self.interfacePanelObjects['sliderBgColor'].Enable(event.Int)

	def event_check_custom_bg(self, event):
		event.EventObject.item.Enable(event.Int)

	def event_filters_begin_drag(self, event):
		pos = event.EventObject.dPos = event.GetPosition()
		event.EventObject.dIndex = event.EventObject.HitTest(pos)
		event.EventObject.dragging = None
		event.Skip()

	def event_filters_drag(self, event):
		obj = event.EventObject
		pos = event.GetPosition()
		if obj.dragging == None:
			if abs(pos[0] - obj.dPos[0]) > 8 or abs(pos[1] - obj.dPos[0]) > 8:
				obj.dragging = True

		if obj.dragging == True:
			hoverIndex = obj.HitTest((event.GetX(), event.GetY()))
			if hoverIndex == obj.dIndex: return event.Skip()
			if hoverIndex == -1: return event.Skip()


			item = obj.Items[obj.dIndex]
			checked = obj.IsChecked(obj.dIndex)
			obj.Delete(obj.dIndex)
			obj.Insert(item, hoverIndex)
			obj.Check(hoverIndex, checked)
			obj.filters.insert(hoverIndex, obj.filters.pop(obj.dIndex))

			obj.dIndex = hoverIndex

		else:
			return event.Skip()

	def event_filters_end_drag(self, event):
		event.EventObject.dragging = False







