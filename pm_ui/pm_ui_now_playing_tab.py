import wx, pympe, pm_helpers, pm_network, time

class TabNowPlaying(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		pympe.ui.drawingArea = self

		self.previousWindowSize = (0, 0)
		self.currentArtwork = None
		self.lastShadowGenerateTime = None
		self.lastDrawTime = 0

		self.currentWindowSize = (0,0)
		self.currentArtworkSize = (0,0)

		# Event catchers
		#######
		pympe.events.config_changed.subscribe(self.mpevent_config_changed)

		pympe.events.pympe_initilized.subscribe(self.mpevent_pympe_initilized)
		pympe.events.player_current_changed.subscribe(self.mpevent_player_current_changed)
		pympe.events.player_current_updated.subscribe(self.mpevent_player_current_updated)

		pympe.events.player_video_mode_changed.subscribe(self.mpevent_video_mode_changed)

		# Create Everything
		#######
		self.uiArtwork = wx.StaticBitmap(self)
		self.uiReflection = wx.StaticBitmap(self)

		self.labelTitle = wx.StaticText(self, -1, '')
		self.labelArtist = wx.StaticText(self, -1, '')
		self.labelAlbum = wx.StaticText(self, -1, '')

		self.labelTitle.SetFont(wx.Font(19, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD))
		self.labelArtist.SetFont(wx.Font(16, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
		self.labelAlbum.SetFont(wx.Font(14, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))

		# Layout Everything
		#######
		labelSizer = wx.BoxSizer(wx.VERTICAL)
		labelSizer.AddStretchSpacer()
		labelSizer.Add(self.labelTitle, flag=wx.ALIGN_BOTTOM|wx.ALIGN_RIGHT)
		labelSizer.Add(self.labelArtist, flag=wx.ALIGN_RIGHT)
		labelSizer.Add(self.labelAlbum, flag=wx.ALIGN_RIGHT)
		labelSizer.AddStretchSpacer()

		gridSizer = wx.GridBagSizer()
		gridSizer.Add(labelSizer, (0, 0), flag=wx.ALIGN_CENTER|wx.RIGHT, border=10)
		gridSizer.Add(self.uiArtwork, (0, 1))
		gridSizer.Add(self.uiReflection, (1, 1), flag=wx.ALIGN_TOP|wx.ALIGN_CENTER)
		gridSizer.Add((-1, -1), (1, 0), flag=wx.EXPAND)

		self.sizer = wx.BoxSizer(wx.VERTICAL)
		self.sizer.AddStretchSpacer()
		self.sizer.Add(gridSizer, flag=wx.ALIGN_CENTER)
		self.sizer.AddStretchSpacer()

		self.SetSizer(self.sizer)

		for handler in [self]+list(self.GetChildren()):
			#handler.Bind(wx.EVT_MOUSEWHEEL, self.event_mousewheel)
			handler.Bind(wx.EVT_RIGHT_UP, self.event_right_up)

		self.Bind(wx.EVT_PAINT, self.event_paint)

	#def event_mousewheel(self, event):
	#	print 'Mousewheel'

	def event_paint(self, event):
		if pympe.config.get("np", "use_custom_background"):
			self.SetBackgroundColour(pympe.config.get("np", "background_color"))
		self.store_window_size()
		self.update(force=True)
		if not pympe.ui.uiMain.theatreFrame:
			pympe.ui.uiMain.set_video_frame()
		#self.Unbind(wx.EVT_MOUSEWHEEL)
		self.Unbind(wx.EVT_PAINT)
		event.Skip()
		pympe.ui.uiMain.restore_selected_tab()

	def unsubscribe_from_all(self):
		pympe.events.config_changed.unsubscribe(self.mpevent_config_changed)
		pympe.events.pympe_initilized.unsubscribe(self.mpevent_pympe_initilized)
		pympe.events.player_current_changed.unsubscribe(self.mpevent_player_current_changed)
		pympe.events.player_current_updated.unsubscribe(self.mpevent_player_current_updated)
		pympe.events.player_video_mode_changed.unsubscribe(self.mpevent_video_mode_changed)

	def selected_by_notebook(self):
		pass

	#def event_mouse_leave(self, event):
	#	if self.leaveFunction: self.leaveFunction(event)
	#	event.Skip()

	#def event_motion(self, event):
	#	if self.motionFunction: self.motionFunction(event)
	#	event.Skip()

	def event_right_up(self, event):
		def event_menu(event):
			if event.Id == 11:
				if pympe.player.playing and not pympe.player.paused: pympe.player.pause()
				else: pympe.player.play()
			elif event.Id == 12:
				pympe.player.stop()
			elif event.Id == 13: pympe.player.previous()
			elif event.Id == 14: pympe.player.next()

		menu = wx.Menu()

		item = wx.MenuItem(menu, 11, "Pause" if pympe.player.playing and not pympe.player.paused else "Play")
		if pympe.player.playing and not pympe.player.paused: item.SetBitmap(wx.ArtProvider.GetBitmap("gtk-media-pause", wx.ART_MENU))
		else: item.SetBitmap(wx.ArtProvider.GetBitmap("gtk-media-play", wx.ART_MENU))
		menu.AppendItem(item)

		item = wx.MenuItem(menu, 12, "Stop")
		item.SetBitmap(wx.ArtProvider.GetBitmap("gtk-media-stop", wx.ART_MENU))
		menu.AppendItem(item)

		menu.AppendSeparator()

		item = wx.MenuItem(menu, 13, "Previous")
		item.SetBitmap(wx.ArtProvider.GetBitmap("gtk-media-previous", wx.ART_MENU))
		menu.AppendItem(item)

		item = wx.MenuItem(menu, 14, "Next")
		item.SetBitmap(wx.ArtProvider.GetBitmap("gtk-media-next", wx.ART_MENU))
		menu.AppendItem(item)

		menu.Bind(wx.EVT_MENU, event_menu)
		self.PopupMenu(menu)

	def store_window_size(self, newWindowSize=None):
		if newWindowSize == None: newWindowSize = self.GetClientSizeTuple()

		# If it's the same or less than 50 px difference then redraw
		if newWindowSize == self.currentWindowSize: return
		if abs(newWindowSize[0]-self.currentWindowSize[0])+abs(newWindowSize[1]-self.currentWindowSize[1]) < 50: return
		self.currentWindowSize = newWindowSize


	def event_size(self, event):
		event.Skip()
		newWindowSize = event.GetSize()

		pympe.player.videoBin.update_size(newWindowSize)

		self.store_window_size(newWindowSize)
		self.update_artwork(resized=True)

		# Wrap it if needed
		border = 20
		windowSize = (self.currentWindowSize[0]-border*2)-self.currentArtworkSize[0]-10
		self.labelTitle.Wrap(windowSize)
		self.labelArtist.Wrap(windowSize)
		self.labelAlbum.Wrap(windowSize)

	def mpevent_video_mode_changed(self, event, video=True):
		self.update(shown=not video)

		if video:
			self.SetBackgroundColour((0, 0, 0))
		else:
			self.SetBackgroundColour(pympe.config.get("np", "background_color"))

	def mpevent_config_changed(self, event, section, info):
		if pympe.player.playingVideo: return
		if section != 'np': return

		if 'use_custom_background' in info or 'show_title_and_artist' in info or 'background_color' in info:
			self.SetBackgroundColour(pympe.config.get("np", "background_color"))
			self.draw_text()

		if 'rounded_edges' in info or 'enable_reflection' in info or 'enable_shadow' in info:
			self.update_artwork(styleChanged=True)

		self.Layout()

	def mpevent_player_current_changed(self, event, currentInfo, currentLibraryId, currentLibrary):
		self.update()

	def mpevent_player_current_updated(self, event, tags):
		if 'title' in tags or 'artist' in tags or 'album' in tags or 'art' in tags:
			self.update()

	def mpevent_pympe_initilized(self, event):
		self.Bind(wx.EVT_SIZE, self.event_size)
		#pympe.ui.uiMain.restore_selected_tab()

	def update(self, shown=True, force=True):
		''' This calls update_text and update_artwork '''
		self.draw_text(shown)
		self.update_artwork(shown=shown, force=force)

	def draw_text(self, shown=True):
		if pympe.player.playingVideo: shown = False
		shown = False if not pympe.config.get('np', 'show_title_and_artist') else shown

		# Hide it and return if needed
		if not shown or not pympe.player.currentInfo:
			self.labelTitle.Show(shown)
			self.labelArtist.Show(shown)
			self.labelAlbum.Show(shown)
			return

		# Show it
		self.labelTitle.Show(shown)
		self.labelArtist.Show(shown)
		self.labelAlbum.Show(shown)

		# Set the color
		if pympe.config.get("np", "use_custom_background"):
			if pympe.config.get("np", "background_color")[0] < 100: textColor = wx.WHITE
			else: textColor = wx.BLACK
		else:
			textColor = wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOWTEXT)

		self.labelTitle.SetForegroundColour(textColor)
		self.labelArtist.SetForegroundColour(textColor)
		self.labelAlbum.SetForegroundColour(textColor)

		# Set the text
		if pympe.player.currentInfo['title']:
			self.labelTitle.SetLabel(pm_helpers.make_display_tag(pympe.player.currentInfo['title']))
		else:
			self.labelTitle.SetLabel("")

		if pympe.player.currentInfo['artist']:
			self.labelArtist.SetLabel(pm_helpers.make_display_tag(pympe.player.currentInfo["artist"]))
		else:
			self.labelArtist.SetLabel("")

		if pympe.player.currentInfo['album']:
			self.labelAlbum.SetLabel(pm_helpers.make_display_tag(pympe.player.currentInfo['album']))
		else:
			self.labelAlbum.SetLabel("")

		# Wrap it if needed
		border = 20
		windowSize = (self.currentWindowSize[0]-border*2)-self.currentArtworkSize[0]-10
		self.labelTitle.Wrap(windowSize)
		self.labelArtist.Wrap(windowSize)
		self.labelAlbum.Wrap(windowSize)

		self.Layout()

	def update_artwork(self, styleChanged=False, resized=False, shown=True, force=True):
		if pympe.player.playingVideo:
			self.lastDrawTime = 0
			shown = False

		# Get the time and if it's older than a previous then ignore this call
		lastDrawTime = int(time.time()*100)
		if self.lastDrawTime >= lastDrawTime: return
		self.lastDrawTime = lastDrawTime

		# Show depending and then return if not shown
		self.uiArtwork.Show(shown)
		self.uiReflection.Show(shown)
		if not shown: return

		# Get the artwork and if it's the same then just continue
		currentArtwork = pympe.player.currentInfo['art'] if (pympe.player.currentInfo and 'art' in pympe.player.currentInfo) else None
		if currentArtwork == self.currentArtwork and not resized and not styleChanged and not force: return

		# The window size + border
		border = 20
		windowSize = (self.currentWindowSize[0]-border*2, self.currentWindowSize[1]-border*2)

		# Sometimes it throws an error cause it sets the widow size too low
		if windowSize[0] < 1 or windowSize[1] < 1: return

		# Get the artwork
		########################
		imageArtwork = None
		if currentArtwork:
			imageArtwork = pm_helpers.PmImage(string=currentArtwork)
		if imageArtwork == None:
			imageArtwork = pm_helpers.PmImage(stock=pm_helpers.get_stock_media_icon())

		# Settings
		########################
		(roundedEdges, enableReflection, enableShadow) = (pympe.config.get("np", "rounded_edges"),
														pympe.config.get("np", "enable_reflection"),
														pympe.config.get('np', 'enable_shadow'))

		# Calculate the new size
		########################
		heightModifier = 1.5 if enableReflection else 1
		artworkSize = (imageArtwork.get_width(), imageArtwork.get_height()*heightModifier)

		# If it fits then use the normal size
		if windowSize[0] >= artworkSize[0] and windowSize[1] >= artworkSize[1]:
			newSize = (artworkSize[0], int(artworkSize[1]*(1.0/heightModifier)))
		# Else calculate the new size
		else:
			edge = 1 if windowSize[1] < artworkSize[1] else 0
			scaling = windowSize[edge]/float(artworkSize[edge])
			newSize = (int(artworkSize[0]*scaling), int(artworkSize[1]*scaling*(1.0/heightModifier)))

		# If the window was resized but the artwork is still going to be drawn the same size then just continue
		if resized and newSize == self.currentArtworkSize: return
		self.currentArtworkSize = newSize

		# The basic artwork
		########################
		if imageArtwork.get_size() != newSize:
			imageArtwork.scale(*newSize)

		mask = pm_helpers.PmImage(pympe.cwd+"images/mask%i.png"%(1 if not roundedEdges else 2)).scaled(*newSize)
		imageArtwork.apply_mask(mask)

		# Reflection
		########################
		if enableReflection  and imageArtwork.usePIL:
			imageReflection = imageArtwork.mirrored()

			# Create the reflection mask and or it with the normal mask
			reflectionMask = pm_helpers.PmImage(pympe.cwd+"images/maskReflection.png")
			if reflectionMask.get_size() != newSize:
				reflectionMask.scale(*newSize)
			reflectionMask.or_alpha(mask.mirrored())

			# apply the mask, crop the image and display it
			imageReflection.apply_mask(reflectionMask)
			imageReflection.crop((0, 0, newSize[0], int(newSize[1]*.35)))

			self.uiReflection.SetBitmap(imageReflection.convert_to_wx_bitmap())
		else:
			self.uiReflection.SetBitmap(wx.EmptyBitmapRGBA(1, 1, alpha=0))

		# The shadow
		########################
		if enableShadow and imageArtwork.usePIL:
			shadowSize = (int(newSize[0]*1.1), int(newSize[1]*1.1))
			shadow = pm_helpers.PmImage(pympe.cwd+"images/mask%is.png"%(1 if not roundedEdges else 2)).scaled(*shadowSize)
			pasteOffset = (int((shadow.get_width()-newSize[0])/2.0),int((shadow.get_height()-newSize[1])/2.0))

			# Need to blit instead of overlay
			if roundedEdges:
				self.lastShadowGenerateTime = time.time()
				self.draw_shadow(self.lastShadowGenerateTime, shadow.copy(), imageArtwork, pasteOffset)

			shadow.paste(imageArtwork, pasteOffset)
			self.uiArtwork.SetBitmap(shadow.convert_to_wx_bitmap())
		else:
			self.uiArtwork.SetBitmap(imageArtwork.convert_to_wx_bitmap())

		self.Layout()

	@pm_helpers.threaded
	def draw_shadow(self, generateTime, shadow, imageArtwork, pasteOffset):
		shadow.blit(imageArtwork, pasteOffset, 20)
		if self.lastShadowGenerateTime != generateTime: return
		pm_helpers.unthread_function(self.uiArtwork.SetBitmap, shadow.convert_to_wx_bitmap())
