# -*- coding: iso-8859-15 -*-
import wx
import pympe
import pm_language, pm_helpers, pm_transcoder, pm_mediafile, itertools
import wx.lib.mixins.listctrl as listmix
import time, os, cPickle
from bisect import bisect

class ListFilter(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
	def __init__(self, parent, index, tag, uiTabLibrary):
		wx.ListCtrl.__init__(self, parent, index, style=wx.LC_REPORT|wx.BORDER_NONE|wx.LC_SORT_ASCENDING|wx.LC_VIRTUAL)
		listmix.ListCtrlAutoWidthMixin.__init__(self)

		self.parent = parent
		self.index = index
		self.tag   = tag

		self.uiTabLibrary = uiTabLibrary

		# TODO maybe store it as the artistId instead of the tag name
		# Then on the ongetitemtext we can just get it straight from the library
		# return self.uiTabLibrary.library.tagIds[self.tag][self.ids[row]]
		self.tags = []
		self.lockedTags = []
		self.selectionLockout = False

		# Add the column for the tag
		self.InsertColumn(0, pm_language.readableTags[tag])

		self.alternatingAttrs = pm_helpers.make_alternating_list_attributes()

		# Working in the bindery
		self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.event_item_selected)
		self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.event_item_deselected)
		self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.event_item_activated)
		self.Bind(wx.EVT_LIST_BEGIN_DRAG, self.event_begin_drag)
		self.Bind(wx.EVT_RIGHT_UP, self.event_right_up)
		self.Bind(wx.EVT_KEY_DOWN, self.event_key_down)

		# Block column resizing
		self.Bind(wx.EVT_LIST_COL_BEGIN_DRAG, self.event_x)

	def event_key_down(self, event):
		if event.GetKeyCode() == 65 and event.ControlDown(): # 65 = a
			self.select_all()
		else:
			event.Skip()

	def event_x(self, event):
		event.Veto()

	def unselect_all(self):
		# Unselect all selected ones
		for item in self.get_selected(False):
			self.SetItemState(item, not wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)

	def select_all(self):
		for i in range(len(self.tags)):
			self.SetItemState(i, wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)

	def lock_tags(self, tags):
		self.unselect_all()

		# Unset the filter
		self.parent.set_filter(self.tag, None)

		# Sort and set the tags
		self.tags = self.lockedTags = sorted(tags, key=lambda x: x.lower())

		# Add the all item
		self.tags.insert(0, "All %s%s (%i)"%(pm_language.readableTags[self.tag], 's' if not pm_language.readableTags[self.tag][-1] == 's' else '', len(self.tags)))

		# Set the new item count which updates it
		self.SetItemCount(len(self.tags))

	def unlock_tags(self):
		self.lockedTags = []

	def update(self):
		# Get a full set of tags based on the preceding filters
		nullTagExists, self.tags = self.uiTabLibrary.library.get_tag_keys(self.tag, self.parent.get_pane_display_filters(self.tag), sort=True)
		self.tags = list(self.tags)

		# Unselect all
		self.selectionLockout = True
		for item in self.get_selected(stopOnAll=False):
			self.SetItemState(item, not wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)

		# Filter the tags by the locked ones
		if self.lockedTags: self.tags = [tag for tag in self.tags if tag in self.lockedTags]

		# Update the parent's filters based on what's still showing
		filters = [tag for tag in self.parent.get_filter(self.tag) if tag in self.tags]
		self.parent.set_filter(self.tag, filters, updatePanes=False)

		# Select what still exists and was filtered by
		for index in [self.tags.index(item)+1 for item in filters]:
			# I had one time this try and select an index that was greater than the list length
			try: self.SetItemState(index, wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)
			except: continue

		self.selectionLockout = False

		# Add the all item
		self.tags.insert(0, "All %s%s (%i)"%(pm_language.readableTags[self.tag], 's' if not pm_language.readableTags[self.tag][-1] == 's' else '', len(self.tags)))

		# Set the new item count which updates it
		self.SetItemCount(len(self.tags))

	def get_selected(self, stopOnAll=True):
		selection = self.GetFirstSelected()
		if selection == -1: return []

		selected = []
		while True:
			selected.append(selection)
			if stopOnAll and selection == 0: break
			selection = self.GetNextSelected(selection)
			if selection == -1: break

		return selected

	def OnGetItemText(self, row, column):
		return self.tags[row]

	def OnGetItemAttr(self, item):
		return self.alternatingAttrs[item%2]

	# Selection stuff

	def event_item_selected(self, event):
		if self.selectionLockout: return
		selected = self.get_selected(True)

		if not selected or selected[0] == 0:
			self.parent.set_filter(self.tag, None)
		else:
			self.parent.set_filter(self.tag, [self.tags[x] for x in selected])

	def event_item_deselected(self, event):
		if self.selectionLockout: return
		existingFilter = self.parent.get_filter(self.tag)
		if not event.Index >= len(existingFilter):
			existingFilter.remove(self.tags[event.Index])
		self.parent.set_filter(self.tag, existingFilter)

	def event_item_activated(self, event):
		# Just enqueue what's showing in the track pane cause the filters are already applied (So will searches)
		indices = self.uiTabLibrary.uiPanelTracks.indices
		pympe.playlists.main.replace(self.uiTabLibrary.library.uid, self.uiTabLibrary.library.get_multi_track_single_value(indices, 'uri'),
									self.uiTabLibrary.library.get_multi_track_single_value(indices, 'title'),
									indices)


		# Play the active song
		pympe.player.play(entry=pympe.playlists.main.get_current())

	def event_right_up(self, event):
		uiMenu = wx.Menu()

		uiMenu.Append(0, "Search")
		uiMenu.AppendSeparator()
		uiMenu.Append(10, "Play")
		uiMenu.Append(11, "Enqueue")

		uiMenu.Bind(wx.EVT_MENU, self.event_menu)

		# Show it
		self.PopupMenu(uiMenu)

	def event_menu(self, event):
		selected = self.get_selected(True)

		if event.Id == 0:
			pympe.events.library_search(self.uiTabLibrary.library.uid, ' | '.join(['%s:%s'%(self.tag, self.tags[x].lower()) for x in selected]))
		elif event.Id == 10:
			# Just enqueue what's showing in the track pane cause the filters are already applied (So will searches)
			indices = self.uiTabLibrary.uiPanelTracks.indices
			pympe.playlists.main.replace(self.uiTabLibrary.library.uid, self.uiTabLibrary.library.get_multi_track_single_value(indices, 'uri'),
										self.uiTabLibrary.library.get_multi_track_single_value(indices, 'title'),
										indices)


			# Play the active song
			pympe.player.play(entry=pympe.playlists.main.get_current())
		elif event.Id == 11:
			# Just enqueue what's showing in the track pane cause the filters are already applied (So will searches)
			indices = self.uiTabLibrary.uiPanelTracks.indices
			pympe.playlists.main.enqueue(self.uiTabLibrary.library.uid, self.uiTabLibrary.library.get_multi_track_single_value(indices, 'uri'),
										self.uiTabLibrary.library.get_multi_track_single_value(indices, 'title'),
										indices)

	def event_begin_drag(self, event):
		# Create the data object which holds the info being dragged
		data = wx.CustomDataObject("MediaFile")

		data.SetData(cPickle.dumps(self.uiTabLibrary.uiPanelTracks.indices))
		pympe.dragData = self.uiTabLibrary.library

		# Create a Drop Source Object, which enables the Drag operation
		dropSource = wx.DropSource(self)
		# Associate the Data to be dragged with the Drop Source Object
		dropSource.SetData(data)

		# Intiate the Drag Operation
		dropSource.DoDragDrop(wx.Drag_AllowMove)

class PanelFilters(wx.Panel):
	def __init__(self, parent, uiTabLibrary):
		wx.Panel.__init__(self, parent)

		# Store the library tab
		self.uiTabLibrary = uiTabLibrary

		# Make the sizer
		self.uiSizer = wx.BoxSizer(wx.VERTICAL if pympe.config.get(self.uiTabLibrary.library.configKey, "ui_vertical_layout") else wx.HORIZONTAL)
		self.splitterSplit = True

		# Add the filter panes
		self.filters    = {}
		self.paneKeys   = []
		self.paneValues = []
		self.rebuild_panes()

		self.SetSizer(self.uiSizer)

		pympe.events.library_cleared.subscribe(self.mpevent_library_cleared)
		pympe.events.library_keys_added.subscribe(self.mpevent_library_keys_added)
		pympe.events.library_keys_removed.subscribe(self.mpevent_library_keys_removed)

	def mpevent_library_keys_removed(self, event, libraryUid, keys):
		if libraryUid != self.uiTabLibrary.library.uid: return

		for key, values in keys.iteritems():
			#if key in self.paneKeys:
				#paneIndex = self.paneKeys.index(key)

			if key in self.filters:
				for value in values:
					self.filters[key].remove(value)
				if not self.filters[key]:
					del self.filters[key]

				#self.paneValues[paneIndex].remove_values(values)

		# Update the panes starting from the one highest
		panelIndexes = [self.paneKeys.index(key) for key in keys if key in self.paneKeys]
		if panelIndexes: self.update_panes(index=sorted(panelIndexes)[0])

		self.uiTabLibrary.uiPanelTracks.update_track_list(filters=self.filters)

	def mpevent_library_keys_added(self, event, libraryUid, keys):
		if libraryUid != self.uiTabLibrary.library.uid: return

		# Update from the highest pane possible
		panelIndexes = [self.paneKeys.index(key) for key in keys if key in self.paneKeys]
		if panelIndexes: self.update_panes(index=sorted(panelIndexes)[0])

	def update_panes(self, tag=None, index=None):
		if index == None: index = (self.paneKeys.index(tag)+1) if tag != None else 0

		for index, pane in enumerate(self.paneValues[index:]):
			pane.update()


	def set_filter(self, tag, value, updatePanes=True):
		if not value and tag in self.filters: del self.filters[tag]
		elif not value: return
		else: self.filters[tag] = value

		if updatePanes:
			self.update_panes(tag)
			self.uiTabLibrary.uiPanelTracks.update_track_list(filters=self.filters)

	def get_filter(self, tag):
		if not tag in self.filters: return []
		return self.filters[tag]

	def get_pane_display_filters(self, key):
		paneIndex = self.paneKeys.index(key)
		return dict([[key, self.filters[key]] for key in self.paneKeys[:paneIndex] if key in self.filters])

	#def mpevent_library_track_added(self, event, entryId):
	#	self.uiTabLibrary.uiPanelTracks.update_track_list(filters=self.filters)

	#def mpevent_library_track_removed(self, event, entryId):
	#	self.uiTabLibrary.uiPanelTracks.update_track_list(filters=self.filters)

	#def mpevent_library_track_changed(self, event, entryId):
	#	self.update_panes(retainSelected=True)

	def mpevent_library_cleared(self, event, libraryUid):
		if libraryUid != self.uiTabLibrary.library.uid: return
		self.filters = {}
		self.update_panes()
		self.uiTabLibrary.uiPanelTracks.update_track_list()

	def unlock_tags(self):
		for pane in self.paneValues:
			pane.unlock_tags()

		self.update_panes()

	def rebuild_panes(self):
		filters = pympe.config.get(self.uiTabLibrary.library.configKey, "ui_filters")

		# Find the difference
		newIndex = -1
		for f in filters:
			if not f[0]: # If it's disabled
				if f[1] in self.paneKeys: # If it exists
					oldIndex = self.paneKeys.index(f[1])
					# Unset the filter for the deleted tab
					self.set_filter(self.paneKeys[oldIndex], None)
					self.uiSizer.Remove(self.paneValues[oldIndex])
					self.paneValues[oldIndex].Destroy()
					del self.paneValues[oldIndex]
					del self.paneKeys[oldIndex]
			else: # If it's enabled
				newIndex += 1
				if not f[1] in self.paneKeys: # If it's new
					pane = ListFilter(self, newIndex, f[1], self.uiTabLibrary)
					self.paneKeys.insert(newIndex, f[1])
					self.paneValues.insert(newIndex, pane)
					self.uiSizer.Insert(newIndex, pane, 1, wx.EXPAND)
				else: # If we're moving it
					oldIndex = self.paneKeys.index(f[1])
					if oldIndex != newIndex: # If it's in a different spot
						self.paneKeys.insert(newIndex, self.paneKeys.pop(oldIndex))
						self.paneValues.insert(newIndex, self.paneValues.pop(oldIndex))
						self.uiSizer.Detach(self.paneValues[newIndex])
						self.uiSizer.Insert(newIndex, self.paneValues[newIndex], 1, wx.EXPAND)

		self.update_panes()

		self.Layout()


class PanelTracks(wx.ListCtrl):#, listmix.ListCtrlAutoWidthMixin):
	def __init__(self, parent, uiTabLibrary):
		wx.ListCtrl.__init__(self, parent, -1, style=wx.LC_REPORT|wx.BORDER_NONE|wx.LC_SORT_ASCENDING|wx.LC_VIRTUAL)

		self.uiTabLibrary = uiTabLibrary

		# For the little play pause icon
		self.lastRefreshed = None

		# Index list
		self.indices = []
		self.lockedIndices = []
		self.selected = []

		# Sorting shit
		self.lastColumnSorted = None
		self.sortColumnReversed = 0

		# For double middle click
		self.uiPopupEditor = None
		self.popupType = None
		self.popupIsDirty = False
		self.popupRow = self.popupColumn = 0
		self.scrollOffset = (0, 0)

		# Get the alt row color
		font = wx.SystemSettings.GetFont(wx.SYS_DEFAULT_GUI_FONT)
		font.SetWeight(wx.FONTWEIGHT_BOLD)
		self.alternatingActiveAttrs = pm_helpers.make_alternating_list_attributes(alternative=True)
		self.alternatingActiveAttrs[0].SetFont(font)
		self.alternatingActiveAttrs[1].SetFont(font)

		self.alternatingAttrs = pm_helpers.make_alternating_list_attributes()

		iList = wx.ImageList(16, 16)
		iList.AddIcon(wx.ArtProvider.GetIcon("gtk-media-play-ltr", wx.ART_TOOLBAR, (16, 16)))
		iList.AddIcon(wx.ArtProvider.GetIcon("gtk-media-pause", wx.ART_TOOLBAR, (16, 16)))
		iList.AddIcon(wx.ArtProvider.GetIcon("gtk-media-stop", wx.ART_TOOLBAR, (16, 16)))
		self.AssignImageList(iList, wx.IMAGE_LIST_SMALL)

		# Update us
		self.columns = []
		self.specialColumns = []
		self.update_columns()
		self.update_track_list()

		# Bind that shit yo
		self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.event_item_activated)

		self.Bind(wx.EVT_LIST_COL_CLICK, self.event_column_left_click)
		self.Bind(wx.EVT_LIST_COL_RIGHT_CLICK, self.event_column_right_click)

		self.Bind(wx.EVT_LIST_CACHE_HINT, self.OnCacheHint)

		self.Bind(wx.EVT_RIGHT_UP, self.event_right_up)
		self.Bind(wx.EVT_LIST_BEGIN_DRAG, self.event_begin_drag)

		self.Bind(wx.EVT_KEY_DOWN, self.event_key_down)

		self.Bind(wx.EVT_MIDDLE_DCLICK, self.event_middle_down)

		pympe.events.library_entries_updated.subscribe(self.mpevent_library_entries_updated)
		pympe.events.library_entries_removed.subscribe(self.mpevent_library_entries_removed)
		pympe.events.library_entries_added.subscribe(self.mpevent_library_entries_added)

		pympe.events.player_playback_paused.subscribe(self.mpevent_player_playback)
		pympe.events.player_playback_started.subscribe(self.mpevent_player_playback)
		pympe.events.player_playback_stopped.subscribe(self.mpevent_player_playback)
		pympe.events.player_current_changed.subscribe(self.mpevent_player_playback)

	def OnGetItemAttr(self, item):
		if self.indices[item] == pympe.player.currentId and self.uiTabLibrary.library == pympe.player.currentLibrary:
			return self.alternatingActiveAttrs[item%2]

		return self.alternatingAttrs[item%2]

	def OnGetItemText(self, row, column):
		try: return self.cache[row][1][column]
		except: return '*******'

	def OnGetItemColumnImage(self, item, column):
		if column == 0 and self.indices[item] == pympe.player.currentId and self.uiTabLibrary.library == pympe.player.currentLibrary:
			if pympe.player.paused:
				return 1

			if not pympe.player.playing:
				return 2

			return 0

		return -1


	def OnCacheHint(self, event):
		cacheRange = range(event.CacheFrom-1, event.CacheTo+1)

		# Delete the old entries
		for x in set(self.cache.keys()).difference(cacheRange): del self.cache[x]

		# Add the new ones
		for x in set(cacheRange).difference(self.cache.keys()):
			values = self.uiTabLibrary.library.get_single_track_multi_values(self.indices[x], self.columns, 'joined')

			#print values
			for column, key in self.specialColumns:
				if key == 'rating':
					values[column] = "%s%s"%("★"*(int(float(values[column]))/2), "☆" if int(float(values[column]))%2 else '')
				elif key == 'length':
					seconds = int(values[column])
					hours = seconds/3600
					minutes = seconds%3600/60
					seconds = seconds%3600%60

					if hours: values[column] = '%i:%02i:%02i'%(hours, minutes, seconds)
					else: values[column] = '%i:%02i'%(minutes, seconds)
				elif key == 'timemodified' or key == 'timeplayed' or key == 'timeadded':
					values[column] = ("" if float(values[column]) == 0 else time.strftime("%m/%d/%y %I:%M:%S %p", time.localtime(float(values[column]))))

			self.cache[x] = [self.indices[x], values]

	def lock_indices(self, indices):
		self.unselect_all()
		self.indices = self.lockedIndices = indices
		return self.sort_by_tag("tracknumber")

	def unlock_indices(self):
		self.lockedIndices = []
		self.update_track_list()

	def update_track_list(self, sort=True, refreshOnly=False, filters=None):
		if refreshOnly:
			# Delete only what we need maybe sometime?
			self.cache = {}
			self.SetItemCount(len(self.indices))
			return

		# If we need to sort this
		if sort:
			self.indices = self.uiTabLibrary.library.get_trackids(filters)
			if self.lockedIndices: self.indices = [indice for indice in self.indices if indice in self.lockedIndices]
			return self.sort_by_tag("tracknumber")

		# Unselect all
		self.update_selected_list()
		self.unselect_all()

		self.indices = self.uiTabLibrary.library.get_trackids(filters)
		if self.lockedIndices: self.indices = [indice for indice in self.indices if indice in self.lockedIndices]

		pympe.ui.uiMain.update_title(self)

		self.SetItemCount(len(self.indices))

	def unselect_all(self):
		for i in self.selected:
			self.SetItemState(i, not wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)
		self.selected = []

	def select_all(self):
		self.selected = range(len(self.indices))
		for i in self.selected: self.SetItemState(i, wx.LIST_STATE_SELECTED, wx.LIST_STATE_SELECTED)

	def update_columns(self):
		columnInfo = pympe.config.get(self.uiTabLibrary.library.configKey, "ui_columns")

		# Save all the widths except for the last one
		columnWidths = {}
		for index, column in enumerate(self.columns): columnWidths[column] = self.GetColumnWidth(index)
		for index, column in enumerate(columnInfo):
			if column[1] in columnWidths: column[2] = columnWidths[column[1]]

		pympe.config.set(self.uiTabLibrary.library.configKey, "ui_columns", columnInfo)

		# Update the displayed columns
		self.ClearAll()
		self.columns = []
		self.specialColumns = []

		index = 0
		for column in columnInfo:
			if column[0]:
				self.columns.append(column[1])

				if self.uiTabLibrary.library.blankTrack[column[1]][1] == int:
					self.InsertColumn(index, pm_language.libraryColumnHeaders[column[1]], format=wx.LIST_FORMAT_RIGHT, width=column[2])
				else:
					self.InsertColumn(index, pm_language.libraryColumnHeaders[column[1]], width=column[2])
				self.specialColumns.append((index, column[1]))
				index += 1

		self.cache = {}
		self.SetItemCount(len(self.indices))

	def update_selected_list(self):
		selection = self.GetFirstSelected()
		if selection == -1: return

		self.selected = []
		while True:
			self.selected.append(selection)
			selection = self.GetNextSelected(selection)
			if selection == -1: break

	def sort_by_tag(self, tag, reverse=0):
		sortOrder = pympe.config.get(self.uiTabLibrary.library.configKey, 'sortOrder')

		if tag in sortOrder: columnOrder = sortOrder[tag]
		else: columnOrder = [tag]+sortOrder[None]

		#if tag == "artist": columnOrder = ["artist", "album", "discnumber", "tracknumber"]
		#elif tag == "albumartist": columnOrder = ["albumartist", "album", "discnumber", "tracknumber"]
		#elif tag == "tracknumber": columnOrder = ["albumartist", "album", "discnumber", "tracknumber"]
		#elif tag == "album": columnOrder = ["album", "discnumber", "tracknumber"]
		#elif tag == 'composer': columnOrder = ['composer', 'album', 'discnumber', 'artist', 'tracknumber']
		#else: columnOrder = [tag, "albumartist", "album", "discnumber", "tracknumber"]

		# Unselect all
		self.unselect_all()

		# Sort it
		self.indices = self.uiTabLibrary.library.sort_by_tag(self.indices, columnOrder, reverse)

		# Refresh
		self.cache = {}

		pympe.ui.uiMain.update_title(self)

		self.SetItemCount(len(self.indices))

	def event_begin_drag(self, event):
		self.update_selected_list()

		if self.selected:
			# Create the data object which holds the info being dragged
			data = wx.CustomDataObject("MediaFile")
			data.SetData(cPickle.dumps([self.indices[i] for i in self.selected]))
			pympe.dragData = self.uiTabLibrary.library

			# Create a Drop Source Object, which enables the Drag operation
			dropSource = wx.DropSource(self)
			# Associate the Data to be dragged with the Drop Source Object
			dropSource.SetData(data)

			# Intiate the Drag Operation
			dropSource.DoDragDrop(wx.Drag_AllowMove)

	def event_item_activated(self, event):
		self.update_selected_list()

		# If we only double clicked then enqueue everything visible
		# Otherwise if there are some selected then just enqueue them
		if len(self.selected) == 1:
			pympe.playlists.main.replace(self.uiTabLibrary.library.uid, self.uiTabLibrary.library.get_multi_track_single_value(self.indices, 'uri'),
										self.uiTabLibrary.library.get_multi_track_single_value(self.indices, 'title'), self.indices)
			pympe.playlists.main.set_index(event.Index)
		else:
			libraryIds = [self.indices[i] for i in self.selected]

			pympe.playlists.main.replace(self.uiTabLibrary.library.uid, self.uiTabLibrary.library.get_multi_track_single_value(libraryIds, 'uri'),
										self.uiTabLibrary.library.get_multi_track_single_value(libraryIds, 'title'), libraryIds)
			pympe.playlists.main.set_index(0)

		# Play the active song
		pympe.playlists.select_playlist()
		pympe.player.play(entry=pympe.playlists.main.get_current())

	def event_column_left_click(self, event):
		column = event.GetColumn()

		if self.lastColumnSorted == column: self.sortColumnReversed = (self.sortColumnReversed+1)%2
		else: self.sortColumnReversed = 0
		self.lastColumnSorted = column

		self.sort_by_tag(self.columns[column], self.sortColumnReversed)

	def event_column_right_click(self, event):
		columns = pympe.config.get(self.uiTabLibrary.library.configKey, "ui_columns")
		enabledColumns = [x for x in columns if x[0]]
		column = event.GetColumn()

		uiMenuColumns = wx.Menu()

		if self.columns:
			mainColumnName = [x[1] for x in columns if x[0]][column]
			uiMenuColumns.Append(100, "Remove %s Column"%pm_language.readableTags[mainColumnName])
			uiMenuColumns.AppendSeparator()
			if column != 0: uiMenuColumns.Append(201, "← Move Left")
			if column != len(enabledColumns)-1: uiMenuColumns.Append(202, "Move Right →")
			uiMenuColumns.AppendSeparator()
		else:
			mainColumnName = None

		for index, column in enumerate(columns):
			if column[1] == mainColumnName: uiMenuColumns.column = index
			uiMenuColumns.Append(index+10000, pm_language.readableTags[column[1]], "", wx.ITEM_CHECK)
			if column[0]: uiMenuColumns.Check(index+10000, column[0])

		uiMenuColumns.Bind(wx.EVT_MENU, self.event_column_menu)

		# Show it
		self.PopupMenu(uiMenuColumns)

	def event_middle_down(self, event):
		def start_editor(defaultValue):
			if type(defaultValue) == int:
				self.uiPopupEditor = wx.SpinCtrl(self.GetChildren()[0], wx.ID_ANY, min=0, max=99999999, initial=defaultValue)
				self.uiPopupEditor.GetSelection = lambda: [0,0]
				self.uiPopupEditor.SetInsertionPointEnd = lambda: None
				#self.uiPopupEditor.SetSelection = lambda x, y: None
				self.uiPopupEditor.GetLastPosition = lambda: len(str(self.uiPopupEditor.GetValue()))
				self.uiPopupEditor.SetEditable = lambda x: None
			else:
				self.uiPopupEditor = wx.TextCtrl(self.GetChildren()[0], wx.ID_ANY, str(defaultValue), style=wx.TE_PROCESS_ENTER)
			self.popupType = type(defaultValue)
			self.uiPopupEditor.SetBackgroundColour((255,255,175))
			self.uiPopupEditor.Bind(wx.EVT_KEY_DOWN, event_key_down)
			self.uiPopupEditor.Bind(wx.EVT_KILL_FOCUS, event_kill_focus)
			self.uiPopupEditor.Show()
			self.uiPopupEditor.Raise()
			self.uiPopupEditor.SetFocus()

		def exit_editor():
			value = self.uiPopupEditor.GetValue()
			self.uiPopupEditor.Destroy()
			self.uiPopupEditor = None
			self.popupType = None
			return value

		def show_editor(row, column):
			self.popupRow, self.popupColumn = row, column
			self.popupIsDirty = False

			self.EnsureVisible(row)

			# If we're out of range
			try: columnType = type(self.cache[row][1][column])
			except KeyError: return

			columnTag = [x[1] for x in pympe.config.get(self.uiTabLibrary.library.configKey, "ui_columns") if x[0]][self.popupColumn]

			if columnType != self.popupType and self.uiPopupEditor:
				exit_editor()

			if not self.uiPopupEditor:
				start_editor(self.cache[row][1][column])
			else:
				self.uiPopupEditor.SetValue(self.cache[row][1][column])
				self.uiPopupEditor.SetInsertionPointEnd()
				self.uiPopupEditor.SetSelection(0, self.uiPopupEditor.GetLastPosition())

			# Needs compilation and rating
			if columnTag in ['year', 'artist', 'album', 'albumartist', 'genre', 'title', 'tracknumber',
								'tracktotal', 'discnumber', 'disctotal', 'episodenumber', 'episodetotal',
								'season', 'series', 'comment']:
				self.uiPopupEditor.SetEditable(True)
				self.uiPopupEditor.SetBackgroundColour((255,255,175))
			else:
				self.uiPopupEditor.SetEditable(False)
				self.uiPopupEditor.SetBackgroundColour((255,175,175))

			#if not self.uiPopupEditor:
				#start_editor(str(self.cache[row][1][column]))
			#else:
				#self.uiPopupEditor.SetValue(str(self.cache[self.popupRow][1][self.popupColumn]))
				#self.uiPopupEditor.SetInsertionPointEnd()
				#self.uiPopupEditor.SetSelection(0, self.uiPopupEditor.GetLastPosition())


			#if columnTag in ['year', 'artist', 'album', 'albumartist', 'genre', 'title']:
				#
			#elif [x[1] for x in pympe.config.get(self.uiTabLibrary.configKey, "columns") if x[0]][self.popupColumn] not in []:
				#exit_editor()
				#start_editor(int(self.cache[self.popupRow][1][self.popupColumn]))
			#else:
				#self.uiPopupEditor.SetEditable(True)
				#self.uiPopupEditor.SetBackgroundColour((255,255,175))

			rect = self.GetItemRect(self.popupRow, self.popupColumn)

			extraWidth = 10 if columnType == int else 0
			dimensions = [columnLocations[self.popupColumn]+rect[0], rect[1]-rect[3]-5, self.GetColumnWidth(self.popupColumn)+extraWidth, rect[3]+2]
			if dimensions[1] < 0: dimensions[1] = 0

			self.uiPopupEditor.SetDimensions(*dimensions)
			self.uiPopupEditor.default = self.cache[row][1][column]

		def editor_store_changes(storedValue, defaultValue):
			if storedValue == defaultValue: return

			tag = [x[1] for x in pympe.config.get(self.uiTabLibrary.library.configKey, "ui_columns") if x[0]][self.popupColumn]
			trackId = self.indices[self.popupRow]

			if tag in pympe.library.multiTagsLocal+pympe.library.multiTagsShared:
				storedValue = storedValue.split('\\\\')

			#print 'Change tag %s in trackId %s to %s'%(tag, trackId, storedValue)
			self.uiTabLibrary.library.set_single_track_single_value(trackId, tag, storedValue, writeMediaFile=True)

		def event_key_down(event):
			kCode = event.GetKeyCode()
			storedValue = event.EventObject.GetValue()
			defaultValue = event.EventObject.default

			#if kCode == wx.WXK_RETURN:
				#exit_editor()
			#	editor_store_changes()

			if kCode == wx.WXK_RETURN or kCode == wx.WXK_DOWN or kCode == wx.WXK_NUMPAD_DOWN or kCode == wx.WXK_TAB:
				if kCode == wx.WXK_RETURN:
					editor_store_changes(storedValue, defaultValue)

				if self.popupRow < len(self.indices)-1:
					show_editor(self.popupRow+1, self.popupColumn)
				elif kCode == wx.WXK_TAB:
					if self.popupColumn < self.GetColumnCount()-1:
						show_editor(0, self.popupColumn+1)
						self.EnsureVisible(self.popupRow)
					else:
						return event.Skip()
				else:
					exit_editor()
					return

			elif kCode == wx.WXK_UP or kCode == wx.WXK_NUMPAD_UP:
				if self.popupRow > 0:
					show_editor(self.popupRow-1, self.popupColumn)
				else:
					return event.Skip()

			elif (kCode == wx.WXK_LEFT or kCode == wx.WXK_NUMPAD_LEFT) and event.ControlDown():
				# If we have a selection unselect first and then continue
				selection = event.EventObject.GetSelection()
				if selection[0] != selection[1]: return event.Skip()


				if self.popupColumn > 0:
					show_editor(self.popupRow, self.popupColumn-1)
				else:
					return event.Skip()

			elif (kCode == wx.WXK_RIGHT or kCode == wx.WXK_NUMPAD_RIGHT) and event.ControlDown():
				# If we have a selection unselect first and then continue
				selection = event.EventObject.GetSelection()
				if selection[0] != selection[1]: return event.Skip()

				if self.popupColumn < self.GetColumnCount()-1:
					show_editor(self.popupRow, self.popupColumn+1)
					self.EnsureVisible(self.popupRow)
				else:
					return event.Skip()

			elif kCode == 90 and event.ControlDown(): # Ctrl + Z
				self.popupIsDirty = False
				event.EventObject.SetValue(defaultValue)
				event.EventObject.SetInsertionPointEnd()
				event.EventObject.SetSelection(0, event.EventObject.GetLastPosition())
				return

			elif kCode == wx.WXK_ESCAPE:
				exit_editor()
				return

			else:
				self.popupIsDirty = True
				return event.Skip()

			#if storedValue == defaultValue:
			#	print 'Nothing to change'
			#	return

			if self.popupIsDirty:
				editor_store_changes(storedValue, defaultValue)

		def event_kill_focus(event):
			exit_editor()

		if self.uiPopupEditor != None:
			exit_editor()

		x,y = event.GetPosition()
		row, flags = self.HitTest((x,y))

		if row == -1: return

		columnLocations = [0]
		location = 0
		for n in range(self.GetColumnCount()):
			location = location+self.GetColumnWidth(n)
			columnLocations.append(location)


		#self.scrollOffset = scrolledWindow.GetScrollPixelsPerUnit()[0]*scrolledWindow.GetScrollPos(wx.HORIZONTAL)
		self.scrollOffset = (self.GetViewRect()[2]-self.GetClientSizeTuple()[0])/(self.GetScrollRange(wx.HORIZONTAL)-self.GetScrollThumb(wx.HORIZONTAL))

		column = bisect(columnLocations, x+(self.GetScrollPos(wx.HORIZONTAL)*self.scrollOffset))-1
		show_editor(row, column)

	def event_key_down(self, event):
		if event.GetKeyCode() == 65 and event.ControlDown(): # 65 = a
			self.select_all()
		else:
			event.Skip()

	def generateMenuPlaylist(self, trackIds, library):
		def event_menu_playlist_selection(event):
			menu = event.EventObject
			#indices = menu.indices

			if event.Id == 2000:
				# Get the name
				dialog = wx.TextEntryDialog(self, "What shall the playlist be named?", "Create a playlist.")
				dialog.ShowModal()
				name = dialog.GetValue()
				if not name: return

				# Create the playlist
				playlist = pympe.playlists.create_playlist(name)
			else:
				playlist = event.EventObject.playlists[event.Id]

			pympe.playlists.select_playlist(playlist.uid)

			#if indices is None:
				#self.update_selected_list()
				#indices = [self.indices[x] for x in self.selected]

			playlist.enqueue(library.uid, library.get_multi_track_single_value(trackIds, 'uri'),
							library.get_multi_track_single_value(trackIds, "title"), trackIds)

		menu = wx.Menu()
		#menu.indices = indices
		menu.playlists = []
		for index, playlist in enumerate(sorted(pympe.playlists.get_playlists())):
			menu.Append(index, playlist.name)
			menu.playlists.append(playlist)
		if pympe.playlists.playlists: menu.AppendSeparator()
		menu.Append(20000, "New Playlist")
		menu.Bind(wx.EVT_MENU, event_menu_playlist_selection)
		return menu

	def generateMenuSearch(self, trackIds, library):
		single = len(trackIds) == 1

		def event_menu_search(event):
			if event.Id == 0: # Artist
				values = set(itertools.chain.from_iterable(library.get_multi_track_single_value(trackIds, "artist", 'list')))
				search = " | ".join(["artist=="+x for x in values])
			elif event.Id == 1: # Album
				values = set(itertools.chain.from_iterable(library.get_multi_track_single_value(trackIds, "album", 'list')))
				search = " | ".join(["album=="+x for x in values])
			elif event.Id == 2: # Genre
				values = set(itertools.chain.from_iterable(library.get_multi_track_single_value(trackIds, "genre", 'list')))
				search = " | ".join(["genre=="+x for x in values])
			pympe.events.library_search(self.uiTabLibrary.library.uid, search)

		menu = wx.Menu()
		menu.Append(0, "Artist"+("" if single else "s"))
		menu.Append(1, "Album"+("" if single else "s"))
		menu.Append(2, "Genre"+("" if single else "s"))
		menu.Bind(wx.EVT_MENU, event_menu_search)
		return menu

	def generateMenuRating(self, topMenu, trackIds, library):
		def event_menu_set_rating(event):
			library.set_multi_track_single_value(trackIds, 'rating', event.Id)

			if event.Id == 0:
				library.set_multi_track_single_value(trackIds, 'hasCustomRating', False)
			else:
				library.set_multi_track_single_value(trackIds, 'hasCustomRating', True)

			#del self.cache[self.indices][1][column]

		menu = wx.Menu()

		menu.Append(0, "0: Unrated")
		menu.Append(2, "1: ★☆☆☆☆")
		menu.Append(4, "2: ★★☆☆☆")
		menu.Append(6, "3: ★★★☆☆")
		menu.Append(8, "4: ★★★★☆")
		menu.Append(10, "5: ★★★★★")
		menu.Bind(wx.EVT_MENU, event_menu_set_rating)
		if len(trackIds) == 1:
			rating = library.get_single_track_single_value(trackIds[0], 'rating')
			rating = ": %s%s"%("★"*int(rating/2), "☆" if rating%2 else '')
		else: rating = ''
		topMenu.AppendSubMenu(menu, 'Rating%s'%rating)

	def generateMenuDevices(self, topMenu, trackIds, library):
		def event_menu_device(event):
			event.EventObject.data[event.Id].write(trackIds, library)

		menu = wx.Menu()
		menu.data = {}

		for device in pympe.devices.get_ready():
			if device.connected:
				menu.Append(device.uid, device.name)
				menu.data[device.uid] = device

		menu.Bind(wx.EVT_MENU, event_menu_device)

		if menu.GetMenuItemCount():
			topMenu.AppendSubMenu(menu, "Copy To")
			topMenu.AppendSeparator()

	def generateMenuTranscode(self, trackIds, library):
		menu = wx.Menu()
		menu.items = []
		for name in sorted(pm_transcoder.formats.keys()):
			menu.Append(-1, name)
			menu.items.append(name)
		return menu

	def generateMenuMisc(self, trackIds, library):
		menu = wx.Menu()
		menu.trackIds = trackIds
		menu.library = library

		menu.Append(21, "Remove From Library")
		menu.Append(22, "Delete From Drive")
		menu.AppendSeparator()
		menu.Append(41, "Auto Organize")
		menu.AppendSeparator()
		menu.Append(43, "Check File Paths")
		menu.Append(42, "Update Library Tags From File")
		menu.AppendSeparator()
		menu.AppendSubMenu(self.generateMenuTranscode(trackIds, library), "Convert To")
		menu.Bind(wx.EVT_MENU, self.event_menu_track_list)
		return menu

	def menu_tracks(self, trackIds, library):
		singleItemMenu = len(trackIds) == 1

		#return self.setup_menu()
		menu = wx.Menu()
		menu.trackIds = trackIds
		menu.library = library

		menu.Append(11, "Play Now")
		menu.Append(13, 'Play Next')
		menu.Append(12, "Enqueue")
		menu.AppendSeparator()
		menu.AppendSubMenu(self.generateMenuPlaylist(trackIds, library), 'Add To Playlist')
		self.generateMenuRating(menu, trackIds, library)

		if library == pympe.library:
			menu.AppendSeparator()
			menu.AppendSubMenu(self.generateMenuMisc(trackIds, library), 'Misc')

		menu.AppendSeparator()
		if singleItemMenu:
			menu.Append(31, "Copy File Location")
			menu.Append(32, "Open Containing Folder")
		else:
			menu.Append(31, "Copy File Locations")
		menu.AppendSeparator()

		if not self.uiTabLibrary.library.device:
			self.generateMenuDevices(menu, trackIds, library)
		else:
			menu.Append(90, 'Copy To Local Library')
			menu.Append(91, 'Delete From Device' if not self.uiTabLibrary.library.device.name else 'Delete from %s'%self.uiTabLibrary.library.device.name)
			menu.AppendSeparator()

		menu.AppendSubMenu(self.generateMenuSearch(trackIds, library), 'Search By')
		menu.AppendSeparator()
		menu.Append(51, "Edit Metadata")

		if not pympe.plugins.get_plugin(1323703478): menu.Enable(51, False)

		menu.Bind(wx.EVT_MENU, self.event_menu_track_list)
		return menu

	def event_right_up(self, event):
		self.update_selected_list()

		# Alt right click then enqueue it
		if wx.GetKeyState(wx.WXK_CONTROL):
			trackIds = [self.indices[self.selected[0]]]
			pympe.playlists.current.enqueue(self.uiTabLibrary.library.uid, self.uiTabLibrary.library.get_multi_track_single_value(trackIds, 'uri'),
										self.uiTabLibrary.library.get_multi_track_single_value(trackIds, 'title'), trackIds)
			event.Skip()
			return


		menu = self.menu_tracks([self.indices[i] for i in self.selected], self.uiTabLibrary.library)
		self.PopupMenu(menu)

	def event_menu_track_list(self, event, trackIds=None, library=None):
		if trackIds == None:
			trackIds = event.EventObject.trackIds
			#trackIds = [self.indices[i] for i in self.selected]

		if library == None:
			library = event.EventObject.library
		# Play now
		if event.Id == 11:
			pympe.playlists.select_playlist()
			pympe.playlists.main.replace(library.uid, library.get_multi_track_single_value(trackIds, 'uri'),
										library.get_multi_track_single_value(trackIds, 'title'), trackIds)
			pympe.playlists.main.set_index(0)

			# Play the active song
			pympe.player.play(entry=pympe.playlists.main.get_current())
		# Enqueue
		elif event.Id == 12:
			pympe.playlists.current.enqueue(library.uid, library.get_multi_track_single_value(trackIds, 'uri'),
										library.get_multi_track_single_value(trackIds, 'title'), trackIds)
		# Play next
		elif event.Id == 13:
			pympe.playlists.current.enqueue(library.uid, library.get_multi_track_single_value(trackIds, 'uri'),
										library.get_multi_track_single_value(trackIds, 'title'), trackIds, pympe.playlists.current.index+1)

		elif event.Id == 21:
			result = wx.MessageDialog(self, "Are you sure you want to remove these %i tracks from the library?"%len(self.selected), "Remove tracks from library", style=wx.YES_NO|wx.NO_DEFAULT).ShowModal()
			if result == wx.ID_YES:
				library.remove_multi_tracks(trackIds, False)
				self.unselect_all()
		elif event.Id == 22:
			result = wx.MessageDialog(self, "Are you sure you want to permanantly delete these %i tracks?"%len(self.selected), "Permanantly delete tracks", style=wx.YES_NO|wx.NO_DEFAULT).ShowModal()
			if result == wx.ID_YES:
				library.remove_multi_tracks(trackIds, True)
				self.unselect_all()
		elif event.Id == 31:
			pm_helpers.clipboard_write('\n'.join(library.get_multi_track_single_value(trackIds, "uri")))
		elif event.Id == 32:
			pm_helpers.open_location(library.get_single_track_single_value(trackIds[0], "uri"))
		elif event.Id == 41:
			library.organize(trackIds)
		elif event.Id == 42:
			values = {}

			for trackId in trackIds:
				mediaFile = pm_mediafile.MediaFile(library.get_single_track_single_value(trackId, 'uri'))
				mediaFile.read()
				library.set_single_track_multi_values(trackId, mediaFile.info.keys(), mediaFile.info.values())

		elif event.Id == 43: # Check Paths
			library.remove_dead_links_threaded(trackIds=trackIds)

		elif event.Id == 51:
			pympe.plugins.get_plugin(1323703478).MetadataEditor(trackIds=trackIds, library=self.uiTabLibrary.library)

		# 90 = Copy To Local Library
		elif event.Id == 90:
			result = library.get_multi_track_multi_values(trackIds, library.dbKeys, 'dict')
			print 'Needs a safeguard to prevent wri1ting multiple tracks'
			trackIds = pympe.library.insert_multi_tracks(result)
			pympe.library.organize_threaded(trackIds, keepOriginal=True)

		# 91 = Remove From Device
		elif event.Id == 91:
			library.device.delete(trackIds)

	def event_column_menu(self, event):
		column = event.GetEventObject().column
		columns = pympe.config.get(self.uiTabLibrary.library.configKey, "ui_columns")
		eventId = event.GetId()

		# Remove this column
		if eventId == 100:
			columns[column][0] = 0

			displayIndex = 0
			for x in columns:
				if x == columns[column]:
					columns[column][2] = self.GetColumnWidth(displayIndex)
				if x[0]:
					displayIndex += 1

		elif eventId == 201:
			tmp = columns.pop(column)
			for i, x in enumerate(reversed(columns[:column])):
				if x[0]:
					columns.insert(column-1-i, tmp)
					break

		elif eventId == 202:
			tmp = columns.pop(column)
			for i, x in enumerate(columns[column:]):
				if x[0]:
					columns.insert(column+1+i, tmp)
					break

		else:
			# For events conveying the id are +10000 cause we'll hopefully never get that high
			eventId -= 10000
			columns[eventId][0] = event.GetInt()
			columns.insert(column+(1 if eventId < column else 0), columns.pop(eventId))

		pympe.config.set(self.uiTabLibrary.library.configKey, "ui_columns", columns)

		self.update_columns()

	def mpevent_library_entries_updated(self, event, libraryUid, entryUids, changedKeys):
		if libraryUid != self.uiTabLibrary.library.uid: return

		for entryUid, changed in zip(entryUids, changedKeys):
			try: index = self.indices.index(entryUid)
			except ValueError: continue

			try: del self.cache[index]
			except KeyError: pass

			self.RefreshItem(index)

		#if self.uiTabLibrary.uiSearchCtrl.searchQuery:
		#	pympe.events.library_search(self.uiTabLibrary.library.uid, self.uiTabLibrary.uiSearchCtrl.searchQuery)

	def mpevent_library_entries_removed(self, event, libraryUid, entryUids):
		if libraryUid != self.uiTabLibrary.library.uid: return

		for entryUid in entryUids:
			try:
				index = self.indices.index(entryUid)
				self.indices.remove(entryUid)
			except ValueError:
				continue

			try:
				self.lockedIndices.remove(entryUid)
			except ValueError:
				continue

			try: del self.cache[index]
			except KeyError: pass

		pympe.ui.uiMain.update_title(self)

		self.SetItemCount(len(self.indices))

	def mpevent_library_entries_added(self, event, libraryUid, entryUids):
		if libraryUid != self.uiTabLibrary.library.uid: return
		if self.uiTabLibrary.uiPanelFilters.filters or self.lockedIndices: return

		self.indices += entryUids

		if self.lastColumnSorted == None:
			self.sort_by_tag(pympe.config.get(self.uiTabLibrary.library.configKey, 'defaultColumn'))
		else:
			self.sort_by_tag(self.columns[self.lastColumnSorted], self.sortColumnReversed)

		#if self.uiTabLibrary.uiSearchCtrl.searchQuery:
		#	pympe.events.library_search(self.uiTabLibrary.library.uid, self.uiTabLibrary.uiSearchCtrl.searchQuery)


	def mpevent_player_playback(self, event, *args):
		if self.lastRefreshed != None:
			self.RefreshItem(self.lastRefreshed)

		if pympe.player.currentLibrary == self.uiTabLibrary.library:
			try:
				index = self.indices.index(pympe.player.currentId)
			except ValueError:
				self.lastRefreshed = None
				return

			self.lastRefreshed = index
			self.RefreshItem(index)


class TextCtrlLibrarySearch(wx.SearchCtrl):
	def __init__(self, parent):
		wx.SearchCtrl.__init__(self, parent, -1, "", style=wx.TE_PROCESS_ENTER)
		self.ShowCancelButton(True)
		self.SetDescriptiveText('Search your library')
		self.parent = parent
		self.searchQuery = None

		self.Bind(wx.EVT_TEXT_ENTER, self.event_text_search)
		self.Bind(wx.EVT_TEXT, self.event_text)
		self.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN, self.event_text_search)
		self.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, self.event_text_cancel)

		pympe.events.library_search.subscribe(self.mpevent_library_search)

	def search(self, search):
		self.searchQuery = search
		if not search:
			self.parent.uiPanelTracks.unlock_indices()
			self.parent.uiPanelFilters.unlock_tags()
			return

		self.parent.display_subset(self.parent.library.search(search))

	def mpevent_library_search(self, event, libraryUid, searchTerms):
		if libraryUid != self.parent.library.uid: return

		self.ChangeValue(searchTerms)
		self.search(searchTerms)

	def event_text_search(self, event):
		self.search(self.GetValue())

	def event_text_cancel(self, event):
		self.searchQuery = None
		self.ChangeValue("")
		self.parent.uiPanelTracks.unlock_indices()
		self.parent.uiPanelFilters.unlock_tags()

	def event_text(self, event):
		if not self.GetValue():
			self.event_text_cancel(event)

class TabLibrary(wx.Panel):
	def __init__(self, parent, library):
		wx.Panel.__init__(self, parent)
		self.library = library

		# For config changed?
		pympe.events.config_changed.subscribe(self.mpevent_config_changed)
		pympe.events.pympe_shutdown.subscribe(self.mpevent_pympe_shutdown)

		# The main splitter
		self.uiSplitter = wx.SplitterWindow(self, style=wx.SP_LIVE_UPDATE)
		self.uiSplitter.SetMinimumPaneSize(1)
		self.uiSplitter.SetSashGravity(.2)

		# Filters
		self.uiPanelFilters = PanelFilters(self.uiSplitter, self)

		# Track view
		self.uiPanelTracks  = PanelTracks(self.uiSplitter, self)

		# Search and Options
		self.uiSearchCtrl = TextCtrlLibrarySearch(self)
		self.uiButtonOptions = wx.Button(self, -1, 'Options')
		self.uiButtonOptions.Bind(wx.EVT_BUTTON, self.event_button_options)
		self.uiButtonOptions.Hide()

		# Add the splitter to the library
		box = wx.BoxSizer(wx.VERTICAL)
		box.Add(self.uiSearchCtrl, 0, wx.EXPAND)
		box.Add(self.uiSplitter, 1, wx.EXPAND)
		box.Add(self.uiButtonOptions, 0, wx.EXPAND)
		self.SetSizer(box)


		# If not filters are enabled then don't split it
		if not 1 in [x[0] for x in pympe.config.get(self.library.configKey, "ui_filters")]:
			self.uiSplitter.Initialize(self.uiPanelTracks)
			self.uiPanelFilters.Hide()

		self.Bind(wx.EVT_PAINT, self.event_paint)

	def mpevent_config_changed(self, event, section, info):
		if section != self.library.configKey: return
		elif 'ui_vertical_layout' in info or 'ui_filters' in info:
			self.update_layout()

	def mpevent_pympe_shutdown(self, event):
		if pympe.config.get(self.library.configKey, "ui_vertical_layout"):
			pympe.config.set(self.library.configKey, "ui_panelSizeV", self.uiPanelFilters.GetSize()[0])
		else:
			pympe.config.set(self.library.configKey, "ui_panelSizeH", self.uiPanelFilters.GetSize()[1])

		# Save the column widths
		columns = pympe.config.get(self.library.configKey, "ui_columns")
		columnLookup = dict([(x[1], i) for i, x in enumerate(columns)])

		for index, column in enumerate(self.uiPanelTracks.columns):
			columns[columnLookup[column]][2] = self.uiPanelTracks.GetColumnWidth(index)

		pympe.config.set(self.library.configKey, "ui_columns", columns)


	def enable_menu(self, menu, function, name=''):
		if menu == False:
			self.uiButtonOptions.Hide()
		else:
			self.uiButtonOptions.SetLabel('%s Options'%name)
			self.uiButtonOptions.Show()
			self.uiButtonOptions.menu = menu
			menu.Bind(wx.EVT_MENU, function)
		self.Layout()

	def event_button_options(self, event):
		event.EventObject.PopupMenu(event.EventObject.menu)

	def event_paint(self, event):
		if self.uiPanelFilters.IsShown():
			if pympe.config.get(self.library.configKey, "ui_vertical_layout"):
				self.uiSplitter.SplitVertically(self.uiPanelFilters, self.uiPanelTracks, pympe.config.get(self.library.configKey, "ui_panelSizeV"))
			else:
				self.uiSplitter.SplitHorizontally(self.uiPanelFilters, self.uiPanelTracks, pympe.config.get(self.library.configKey, "ui_panelSizeH"))

		self.Layout()
		event.Skip()
		self.Unbind(wx.EVT_PAINT)


	def selected_by_notebook(self):
		pympe.ui.uiMain.activeTitlePanel = self.uiPanelTracks
		pympe.ui.uiMain.update_title(self.uiPanelTracks)

	def display_subset(self, trackIds):
		# Filter them
		sets = [set() for key in self.uiPanelFilters.paneKeys]
		for track in trackIds:
			[sets[i].update(x) for i, x in enumerate(self.library.get_single_track_multi_values(track, self.uiPanelFilters.paneKeys, 'list'))]

		for i, pane in enumerate(self.uiPanelFilters.paneValues):
			pane.lock_tags(sets[i])

		self.uiPanelTracks.lock_indices(trackIds)

	def update_all(self):
		self.uiPanelTracks.update_columns()
		self.uiPanelTracks.update_track_list()
		self.uiPanelFilters.rebuild_panes()

	def update_layout(self):
		isSplit = True if 1 in [x[0] for x in pympe.config.get(self.library.configKey, "ui_filters")] else False
		vertical = pympe.config.get(self.library.configKey, "ui_vertical_layout")

		# If it was split save the position
		if self.uiSplitter.IsSplit():
			if self.uiPanelFilters.uiSizer.GetOrientation() == wx.HORIZONTAL: pympe.config.set(self.library.configKey, "ui_panelSizeH", self.uiPanelFilters.GetSize()[1])
			else: pympe.config.set(self.library.configKey, "ui_panelSizeV", self.uiPanelFilters.GetSize()[0])

		self.uiSplitter.Unsplit(self.uiPanelFilters)

		if isSplit:
			if vertical:
				self.uiSplitter.SplitVertically(self.uiPanelFilters, self.uiPanelTracks, pympe.config.get(self.library.configKey, "ui_panelSizeV"))
				self.uiPanelFilters.uiSizer.SetOrientation(wx.VERTICAL)
			else:
				self.uiSplitter.SplitHorizontally(self.uiPanelFilters, self.uiPanelTracks, pympe.config.get(self.library.configKey, "ui_panelSizeH"))
				self.uiPanelFilters.uiSizer.SetOrientation(wx.HORIZONTAL)

			self.uiPanelFilters.rebuild_panes()
