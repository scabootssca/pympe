import wx, pympe, pm_language, pm_transcoder, pm_helpers

class TheatreFrame(wx.Frame):
	def __init__(self, parent):
		wx.Frame.__init__(self, parent, title='Theatre Mode', style=wx.DEFAULT_FRAME_STYLE|wx.FRAME_FLOAT_ON_PARENT^wx.RESIZE_BORDER)
		self.timer = wx.Timer(self)
		self.timer.Start(1000, True)
		
		
		self.sizer = wx.BoxSizer(wx.VERTICAL)
		self.controlSizer = wx.BoxSizer(wx.HORIZONTAL)
		
		# For Audio
		#pympe.ui.uiMain.uiTabNowPlaying.Reparent(self)
		#self.sizer.Add(pympe.ui.uiMain.uiTabNowPlaying, 1, wx.EXPAND)

		# For Video
		self.videoPanel = wx.Panel(self)
		self.videoPanel.SetBackgroundColour(wx.BLACK)
		
		self.painted = False
		self.shown = True
		
		self.sizer.Add(self.videoPanel, 1, flag=wx.EXPAND)
		self.sizer.Add(self.controlSizer, 0)

		# Transfer the controls over
		pympe.ui.uiMain.uiSizerFooter.Detach(pympe.ui.uiMain.uiSizerControls)
		for child in pympe.ui.uiMain.uiSizerControls.GetChildren():
			child.GetWindow().Reparent(self)
		self.controlSizer.Add(pympe.ui.uiMain.uiSizerControls, 0, wx.LEFT|wx.TOP, 5)
		
		self.staticLine = wx.StaticLine(self, style=wx.LI_VERTICAL)
		self.controlSizer.Add(self.staticLine, 0, wx.EXPAND|wx.LEFT|wx.RIGHT, 5)
		
		# Now playing info
		pympe.ui.uiMain.uiSizerFooter.Detach(pympe.ui.uiMain.uiSizerNowPlaying)
		for child in pympe.ui.uiMain.uiSizerNowPlaying.GetChildren():
			child.GetWindow().Reparent(self)
		self.controlSizer.Add(pympe.ui.uiMain.uiSizerNowPlaying, 1, wx.EXPAND|wx.RIGHT|wx.TOP, 5)
		
		# And then the progress 
		pympe.ui.uiMain.uiSizerMain.Detach(pympe.ui.uiMain.uiSizerProgress)
		for child in pympe.ui.uiMain.uiSizerProgress.GetChildren():
			child.GetWindow().Reparent(self)
		self.sizer.Add(pympe.ui.uiMain.uiSizerProgress, 0, wx.EXPAND|wx.ALL^wx.TOP, 5)
	
		self.SetSizer(self.sizer)

		self.Bind(wx.EVT_CLOSE, self.event_close)
		self.videoPanel.Bind(wx.EVT_PAINT, self.event_paint)
		self.Bind(wx.EVT_MOUSE_EVENTS, self.event_mouse_motion)
		self.videoPanel.Bind(wx.EVT_MOUSE_EVENTS, self.event_mouse_motion)
		self.Bind(wx.EVT_TIMER, self.event_timer, self.timer)
		
		pympe.events.player_current_changed.subscribe(self.mpevent_player_current_changed)
		self.show_now_playing(not pympe.player.playingVideo)
		
		self.ShowFullScreen(True, style=wx.FULLSCREEN_ALL)
		
	def mpevent_player_current_changed(self, eventType, currentInfo, currentId, currentLibrary):
		self.show_now_playing(not pympe.player.playingVideo)
		
	def show_now_playing(self, shown=True):
		if not pympe.ui.uiMain.uiTabNowPlaying: return
		
		# True, False: Change to audio but videopanel is already disabled
		# False, True: Change to video but videopanel is already enabled
		if shown != self.videoPanel.IsShown(): return
		
		if not shown:
			self.videoPanel.Show()
			self.videoPanel.Bind(wx.EVT_PAINT, self.event_paint)
			
			for handler in [pympe.ui.uiMain.uiTabNowPlaying]+list(pympe.ui.uiMain.uiTabNowPlaying.GetChildren()):
				handler.Unbind(wx.EVT_MOTION)

			pympe.ui.uiMain.uiTabNowPlaying.Reparent(pympe.ui.uiMain.uiTabsLeft)
			pympe.ui.uiMain.uiTabsLeft.InsertPage(0, pympe.ui.uiMain.uiTabNowPlaying, pm_language.uiTabNowPlaying)
			return

		pympe.ui.uiMain.uiTabNowPlaying.Reparent(self)
		self.sizer.Insert(0, pympe.ui.uiMain.uiTabNowPlaying, 1, wx.EXPAND)
		
		for handler in [pympe.ui.uiMain.uiTabNowPlaying]+list(pympe.ui.uiMain.uiTabNowPlaying.GetChildren()):
			handler.Bind(wx.EVT_MOTION, self.event_mouse_motion)

		# Hide the video tab
		self.videoPanel.Hide()

		# And layout
		self.Layout()
		
	def event_timer(self, event):
		self.show_controls(False)
		
	def event_mouse_motion(self, event):
		self.show_controls(True)
		self.timer.Start(-1, True)
		
	def show_controls(self, shown=True):
		if shown == self.shown: return
		self.shown = shown
		for child in pympe.ui.uiMain.uiSizerControls.GetChildren():
			child.GetWindow().Show(shown)
		
		for child in pympe.ui.uiMain.uiSizerNowPlaying.GetChildren():
			child.GetWindow().Show(shown)
			
		for child in pympe.ui.uiMain.uiSizerProgress.GetChildren():
			child.GetWindow().Show(shown)	
			
		if shown: wx.SetCursor(wx.NullCursor)
		else: wx.SetCursor(wx.StockCursor(wx.CURSOR_BLANK))
			
		self.staticLine.Show(shown)

		self.Layout()
		
	def event_paint(self, event):
		#if self.painted: return
		#self.painted = True
		self.videoPanel.Unbind(wx.EVT_PAINT)
		pympe.ui.uiMain.set_video_frame(self.videoPanel)
		event.Skip()
		

	def event_close(self, event):
		self.show_controls()
		
		pympe.events.player_current_changed.unsubscribe(self.mpevent_player_current_changed)
		
		# Move the now playing tab back
		self.show_now_playing(False)
		
		# Transfer all back to the main panel
		self.controlSizer.Detach(pympe.ui.uiMain.uiSizerControls)
		for child in pympe.ui.uiMain.uiSizerControls.GetChildren():
			child.GetWindow().Reparent(pympe.ui.uiMain)
		pympe.ui.uiMain.uiSizerFooter.Insert(0, pympe.ui.uiMain.uiSizerControls, 0, wx.LEFT, 10)
		
		self.controlSizer.Detach(pympe.ui.uiMain.uiSizerNowPlaying)
		for child in pympe.ui.uiMain.uiSizerNowPlaying.GetChildren():
			child.GetWindow().Reparent(pympe.ui.uiMain)
		pympe.ui.uiMain.uiSizerFooter.Insert(2, pympe.ui.uiMain.uiSizerNowPlaying)
		
		self.sizer.Detach(pympe.ui.uiMain.uiSizerProgress)
		for child in pympe.ui.uiMain.uiSizerProgress.GetChildren():
			child.GetWindow().Reparent(pympe.ui.uiMain)
		pympe.ui.uiMain.uiSizerMain.Insert(4, pympe.ui.uiMain.uiSizerProgress, 0, wx.EXPAND|wx.LEFT|wx.RIGHT, 10)

		wx.SetCursor(wx.NullCursor)

		pympe.ui.uiMain.Layout()
		
		pympe.ui.uiMain.theatreFrame = None
		pympe.ui.uiMain.set_video_frame()
		if event: event.Skip()
		
	def close(self):
		self.Close()
		
