import wx, pympe, pm_language, pm_helpers, pm_transcoder, pm_debug
import wx.lib.scrolledpanel as scrolled
import wx.lib.mixins.listctrl as listmix
from wx.lib.wordwrap import wordwrap
import os, time, sys

class DebugLog(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
	def __init__(self, parent):
		wx.ListCtrl.__init__(self, parent, -1, size=(500, 300), style=wx.LC_REPORT|wx.LC_VIRTUAL|wx.VSCROLL|wx.HSCROLL)
		listmix.ListCtrlAutoWidthMixin.__init__(self)
		self.log = []

		self.InsertColumn(0, 'Log')
		self.SetFont(wx.Font(8, wx.FONTFAMILY_MODERN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, faceName='monospace'))

	def OnGetItemText(self, r, c):
		return self.log[r]

	def write(self, message):
		self.log += wordwrap(message, self.GetRect()[2], wx.WindowDC(self)).strip().split('\n')

		self.SetItemCount(len(self.log))
		self.EnsureVisible(len(self.log)-1)





#class TabCache(wx.Panel):#scrolled.ScrolledPanel):
	#def __init__(self, configTab, parent):
		#wx.Panel.__init__(self, parent)#scrolled.ScrolledPanel.__init__(self, parent)
		##self.SetupScrolling()

		## Make the sizer
		#uiSizer = wx.GridBagSizer(5, 5)
		#uiSizer.AddGrowableCol(1)
		#uiSizer.AddGrowableRow(1)

		#box = wx.BoxSizer()
		#box.Add(uiSizer, 1, wx.EXPAND|wx.ALL, 10)
		#self.SetSizer(box)

		## The widgets
	## Play uri
		#uiLabel = wx.StaticText(self, -1, "Cached Uris:")
		#uiLabel.SetFont(configTab.fontHeader)
		#uiSizer.Add(uiLabel, (0, 0), (1, 1))

		#self.uiListCache = wx.ListBox(self, style=wx.LB_EXTENDED|wx.LB_SORT)
		#uiSizer.Add(self.uiListCache, (1, 0), (1, 3), wx.EXPAND)
		#self.uiListCache.InsertItems(pympe.cache.list(), 0)

		#uiButtonDelete = wx.Button(self, -1, "Delete Selected")
		#uiSizer.Add(uiButtonDelete, (2, 0), (1, 1), wx.EXPAND)
		#uiButtonDelete.Bind(wx.EVT_BUTTON, self.event_button_delete)

		#uiButtonClear = wx.Button(self, -1, "Clear Cache")
		#uiSizer.Add(uiButtonClear, (2, 2), (1, 1), wx.EXPAND)
		#uiButtonClear.Bind(wx.EVT_BUTTON, self.event_button_clear)

		#pympe.events.cache_cleared.subscribe(self.mpevent_cache_cleared)
		#pympe.events.cache_entry_added.subscribe(self.mpevent_cache_entry_added)
		#pympe.events.cache_entry_removed.subscribe(self.mpevent_cache_entry_removed)

	#def event_button_delete(self, event):
		#for i in reversed(self.uiListCache.GetSelections()):
			#pympe.cache.delete(self.uiListCache.GetString(i))
			#self.uiListCache.Delete(i)

	#def event_button_clear(self, event):
		#dialog = wx.MessageDialog(self, "Are you sure you want to clear the cache? This operation cannot be undone.", "Clear Cache", wx.YES_NO | wx.ICON_QUESTION)
		#if dialog.ShowModal() == wx.ID_NO: return

		#pympe.cache.clear()

	#def mpevent_cache_cleared(self, event):
		#self.uiListCache.Clear()

	#def mpevent_cache_entry_added(self, event, uri):
		#self.uiListCache.InsertItems([uri], 0)

	#def mpevent_cache_entry_removed(self, event, uri):
		#index = self.uiListCache.FindString(uri)
		#if index != wx.NOT_FOUND: self.uiListCache.Delete(index)


class TabDebug(scrolled.ScrolledPanel):
	def __init__(self, parent):
		scrolled.ScrolledPanel.__init__(self, parent)
		self.SetupScrolling()

		self.scrollback = []
		self.scrollbackIndex = -1
		self.debugButtons = {}

		self.fontHeader = wx.Font(-1, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

		# Make the sizer
		uiSizer = wx.GridBagSizer(5, 5)

		box = wx.BoxSizer()
		box.Add(uiSizer, 1, wx.EXPAND|wx.ALL, 10)
		self.SetSizer(box)

		# The widgets
	# Play uri
		uiLabel = wx.StaticText(self, -1, "Play URI:")
		uiLabel.SetFont(self.fontHeader)
		uiSizer.Add(uiLabel, (0, 0), (1, 1))

		uiText = wx.TextCtrl(self, -1, "http://tridity.org/~achernak/Music/Infected%20Mushroom/Infected%20Mushroom%20-%202007%20-%20Vicious%20Delicious/Infected%20Mushroom%20-%2002%20-%20Artillery.mp3", style=wx.TE_PROCESS_ENTER)#http://ubuntu.hbr1.com:19800/trance.ogg
		uiText.Bind(wx.EVT_TEXT_ENTER, self.event_text_play_uri)
		uiSizer.Add(uiText, (1, 1), (1, 2), wx.EXPAND)

		# This is stupid
		uiSizer.AddGrowableCol(1)

		uiButton = wx.Button(self, -1, "Browse")
		uiButton.uiTextPlayUri = uiText
		uiButton.Bind(wx.EVT_BUTTON, self.event_button_browse)
		uiSizer.Add(uiButton, (1, 0), (1, 1), wx.EXPAND)
#print pympe.ui.uiMain.uiTabConfig.GetSizer().GetChildren()[0].Window.uiTextDebugLog
	# Debug log
		uiLabel = wx.StaticText(self, -1, "Debug Log:")
		uiLabel.SetFont(self.fontHeader)
		uiSizer.Add(uiLabel, (3, 0), (1, 1))

		# To Disable Debug Log
		self.uiTextDebugLog = DebugLog(self)
		#if not pympe.arguments['debug']:
		pympe.stdout = pympe.stderr = self.uiTextDebugLog
		uiSizer.Add(self.uiTextDebugLog, (4, 0), (1, 3), wx.EXPAND)
		#else:
		#	uiText = wx.TextCtrl(self, -1, 'Disabled by command switch.', size=(500, 300))
		#	uiText.Disable()
		#	uiSizer.Add(uiText, (4, 0), (1, 3), wx.EXPAND)
		#	self.uiTextDebugLog.Hide()

		uiText = wx.TextCtrl(self, -1, "", style=wx.TE_PROCESS_ENTER)
		uiText.Bind(wx.EVT_TEXT_ENTER, self.event_text_python_console_enter)
		uiText.Bind(wx.EVT_KEY_UP, self.event_text_python_console)
		uiSizer.Add(uiText, (5, 0), (1, 3), wx.EXPAND)


	# Debug Buttons
		uiLabel = wx.StaticText(self, -1, "Other:")
		uiLabel.SetFont(self.fontHeader)
		uiSizer.Add(uiLabel, (7, 0), (1, 2))

		self.uiSizer2 = wx.GridBagSizer(5, 5)
		uiSizer.Add(self.uiSizer2, (8, 0), (1, 3), wx.EXPAND)

		uiButtonCreateBreakpoint = wx.Button(self, -1, "Create Breakpoint")
		uiButtonCreateBreakpoint.Enable(pympe.arguments['debug'])
		self.uiSizer2.Add(uiButtonCreateBreakpoint, (0, 0), (1, 1), wx.EXPAND)
		uiButtonCreateBreakpoint.Bind(wx.EVT_BUTTON, self.event_button_breakpoint)

		#uiButton = wx.Button(self, 0, "Edit Config")
		#self.uiSizer2.Add(uiButton, (0, 1), (1, 1), wx.EXPAND)
		#uiButton.Bind(wx.EVT_BUTTON, self.event_button_misc)

		uiButton = wx.Button(self, 1, "Save Config To File")
		self.uiSizer2.Add(uiButton, (1, 1), (1, 1), wx.EXPAND)
		uiButton.Bind(wx.EVT_BUTTON, self.event_button_misc)

		uiButton = wx.Button(self, 2, "Load Config From File")
		self.uiSizer2.Add(uiButton, (2, 1), (1, 1), wx.EXPAND)
		uiButton.Bind(wx.EVT_BUTTON, self.event_button_misc)

		uiButton = wx.Button(self, 4, "Refresh Debug Functions")
		self.uiSizer2.Add(uiButton, (0, 2), (1, 1), wx.EXPAND)
		uiButton.Bind(wx.EVT_BUTTON, self.event_button_misc)

		for index, function in enumerate(pm_debug.functions):
			uiButton = wx.Button(self, index, "%s()"%function)
			self.uiSizer2.Add(uiButton, (index+1, 2), (1, 1), wx.EXPAND)
			uiButton.Bind(wx.EVT_BUTTON, self.event_button_debug_function)

			self.debugButtons[uiButton] = function

	## Config Editor
		#uiLabel = wx.StaticText(self, -1, "Config Editor:")
		#uiLabel.SetFont(self.fontHeader)
		#uiSizer.Add(uiLabel, (9, 0), (1, 1))

		#uiSizer3 = wx.GridBagSizer(5, 5)
		#uiSizer.Add(uiSizer3, (10, 0), (1, 3), flag=wx.EXPAND)
		#uiSizer3.AddGrowableCol(2)

		#fontSectionName = wx.Font(10, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

		#index = 0
		#sectionIndex = 0
		#for sectionName in pympe.config.sectionOrder:
			#sectionNameLabel = wx.StaticText(self, -1, sectionName)
			#sectionNameLabel.SetFont(fontSectionName)

			#uiSizer3.Add(sectionNameLabel, (index, 0), (1, 4), flag=wx.EXPAND)
			#uiSizer3.Add((10, -1), (index, 0))
			#index += 1


			#for keyName in pympe.config.sections[sectionName][0]:
				#value = pympe.config.sections[sectionName][1][keyName]

				#if type(value) == int:
					#valueCtrl = wx.SpinCtrl(self, index, min=0, max=999999999, initial=value, style=wx.TE_PROCESS_ENTER)
					#valueCtrl.Bind(wx.EVT_TEXT_ENTER, self.event_config_enter)
					#valueCtrl.Bind(wx.EVT_TEXT, self.event_modified)
				#elif type(value) == bool:
					#valueCtrl = wx.Choice(self, index, choices=['False', 'True'], style=wx.TE_PROCESS_ENTER)
					#valueCtrl.SetSelection(int(value))
					#valueCtrl.Bind(wx.EVT_CHOICE, self.event_config_choice)
				##elif type(value) == list or type(value) == tuple:
				##	valueCtrl = wx.ComboBox(self, index, str(value[0] if value else ''), choices=[str(x) for x in value], style=wx.TE_PROCESS_ENTER)
				##	valueCtrl.Bind(wx.EVT_TEXT_ENTER, self.event_config_enter)
				#else:
					#valueCtrl = wx.TextCtrl(self, index, str(value), style=wx.TE_PROCESS_ENTER)
					#valueCtrl.Bind(wx.EVT_TEXT, self.event_modified)
					#valueCtrl.Bind(wx.EVT_TEXT_ENTER, self.event_config_enter)

				#valueCtrl.sectionIndex = sectionIndex
				#valueCtrl.keyName = keyName
				#valueCtrl.type = type(value)


				#uiSizer3.Add(wx.StaticText(self, -1, keyName), (index, 1), flag=wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
				#uiSizer3.Add(valueCtrl, (index, 2), (1, 2), flag=wx.EXPAND)
				#index += 1

			#sectionIndex += 1
			#index += 1

	def unsubscribe_from_all(self):
		pass

	def event_modified(self, event):
		result = event.EventObject.GetValue()

		if result == pympe.config.sections[pympe.config.sectionOrder[event.EventObject.sectionIndex]][1][event.EventObject.keyName]:
			event.EventObject.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))
		else:
			event.EventObject.SetBackgroundColour(wx.RED)
		event.Skip()

	def event_config_enter(self, event):
		result = event.EventObject.GetValue()

		if event.EventObject.type == list or event.EventObject.type == tuple:
			result = eval(result)
		elif event.EventObject.type == int:
			result = int(result)
		elif event.EventObject.type == float:
			result = float(result)
		elif event.EventObject.type == str:
			result = str(result)
		#elif event.EventObject.type == list:
		#	print event.EventObject.GetCurrentSelection()
		#	return
		#elif event.EventObject.type == tuple:
		#	print event.EventObject.GetCurrentSelection()
		#	return
		else:
			print 'Unhandled result type', event.EventObject.type
			return


		print result
		event.EventObject.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))
		pympe.config.set(pympe.config.sectionOrder[event.EventObject.sectionIndex], event.EventObject.keyName, result)

		event.Skip()

	def event_config_choice(self, event):
		result = bool(event.EventObject.GetSelection())
		print result
		pympe.config.set(pympe.config.sectionOrder[event.EventObject.sectionIndex], event.EventObject.keyName, result)
		event.Skip()

	def event_button_breakpoint(self, event):
		import ipdb; ipdb.set_trace() # BREAKPOINT

	def event_button_debug_function(self, event):
		reload(pm_debug)
		getattr(pm_debug, self.debugButtons[event.EventObject])()

	def event_button_misc(self, event):
		#if event.Id == 0: # Edit config
		if event.Id == 1: # Save
			pympe.config.save()
		elif event.Id == 2: # Load
			pympe.config.load(sendEvents=True)
		#elif event.Id == 3:
		#	pympe.library.blank_database()
		elif event.Id == 4:
			reload(pm_debug)

			for button in self.debugButtons.iterkeys():
				self.uiSizer2.Detach(button)
				button.Destroy()

			self.debugButtons = {}

			for index, function in enumerate(pm_debug.functions):
				uiButton = wx.Button(self, index, "%s()"%function)
				self.uiSizer2.Add(uiButton, (index+1, 2), (1, 1), wx.EXPAND)
				uiButton.Bind(wx.EVT_BUTTON, self.event_button_debug_function)
				self.debugButtons[uiButton] = function

			self.Layout()
			#self.SetupScrolling()

	#def event_button_fullscreen(self, event):
		##value = event.GetEventObject().GetValue()
		##print "Fullscreen",value
		#pympe.ui.uiMain.fullscreen(True)

	def event_text_play_uri(self, event):
		uri = event.GetEventObject().GetValue()
		pympe.playlists.main.enqueue(None, [uri])
		pympe.player.play(uri=uri)

	def event_button_browse(self, event):
		dialog = wx.FileDialog(self, message="Choose a file to play", wildcard='|'.join(['%s files (*.%s|*.%s'%(x.upper(), x, x) for x in pm_mediafile.supportedFormats]), style=wx.FD_MULTIPLE)
		if dialog.ShowModal() == wx.ID_OK:
			uris = ["file://"+x for x in dialog.GetPaths()]
			event.GetEventObject().uiTextPlayUri.ChangeValue(uris[0])

			pympe.playlists.main.enqueue(None, [uris])
			pympe.player.play(uri=uris[-1])

	def event_text_python_console(self, event):
		code = event.GetKeyCode()
		obj = event.GetEventObject()
		if not self.scrollback: return

		if code == 315: # Up
			if self.scrollbackIndex != len(self.scrollback)-1:
				self.scrollbackIndex += 1

			obj.SetValue(self.scrollback[self.scrollbackIndex])
			obj.SetInsertionPointEnd()

		elif code == 317: # Down
			if self.scrollbackIndex > 0:
				self.scrollbackIndex -= 1

				obj.SetValue(self.scrollback[self.scrollbackIndex])
				obj.SetInsertionPointEnd()

			else:
				self.scrollbackIndex = -1
				obj.SetValue('')

		else:
			event.Skip()

	def event_text_python_console_enter(self, event):
		value = event.GetEventObject().GetValue()
		if not value: return
		self.scrollback.insert(0, value)
		self.scrollbackIndex = -1

		try:
			eval(compile(value, 'debugTagTerminal', 'single'))
		except:
			pympe.error("Runtime Console Error", True)

		event.GetEventObject().ChangeValue("")

	def selected_by_notebook(self):
		pass


class TabConfig(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)

		self.fontHeader = wx.Font(-1, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

		#uiTabsMain = wx.Notebook(self)
		#uiTabsMain.AddPage(TabCache(self, uiTabsMain), "Cache")
		#uiTabsMain.AddPage(TabDebug(self, uiTabsMain), "Debug")

		# Finish up the tabs
		#uiTabsMain.SetSelection(pympe.config.get("ui", "tabs_config_page"))
		#uiTabsMain.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.event_notebook_page_changed)

		# Add them to the panel
		uiSizerMain = wx.BoxSizer()
		#uiSizerMain.Add(uiTabsMain, 1, wx.EXPAND|wx.ALL, 10)
		uiSizerMain.Add(TabDebug(self, self), 1, wx.EXPAND|wx.ALL, 10)
		self.SetSizerAndFit(uiSizerMain)

	def selected_by_notebook(self):
		pass

	def event_notebook_page_changed(self, event):
		pympe.config.set("ui", "tabs_config_page",  event.Selection)
		event.Skip()
