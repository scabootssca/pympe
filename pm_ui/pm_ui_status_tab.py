import wx
import pympe, pm_helpers, pm_language, cPickle, pm_mediafile, pm_playlists, time

class TabStatus(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		
		self.sizer = wx.BoxSizer(wx.VERTICAL)

		box = wx.BoxSizer()
		box.Add(self.sizer, 1, wx.EXPAND|wx.ALL, 10)
		self.SetSizer(box)
		
		self.indicators = {}
		
		self.fontHeader = wx.Font(10, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)
		self.fontFooter = wx.Font(6, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)
		
		pympe.event.add_listener("progress-indicator-created", self.listener)
		pympe.event.add_listener("progress-indicator-removed", self.listener)
		pympe.event.add_listener("progress-indicator-updated", self.listener)
		
	def listener(self, eventType, eventSource, *info):
		indicator = info[0]

		if eventType == "progress-indicator-created":
			gauge = wx.Gauge(self, -1, size=(-1, 10))
			gauge.SetValue(indicator.current)
			gauge.SetRange(indicator.total)
			parts = (wx.StaticText(self, -1, indicator.label), gauge, wx.StaticText(self, -1, ''))
			parts[0].SetFont(self.fontHeader)
			parts[2].SetFont(self.fontFooter)
			self.indicators[indicator] = parts
			self.sizer.Add(parts[0], 0, wx.EXPAND|wx.BOTTOM, 5)
			self.sizer.Add(parts[1], 0, wx.EXPAND|wx.BOTTOM, 5)
			self.sizer.Add(parts[2], 0, wx.EXPAND|wx.BOTTOM, 5)
			self.Layout()
		elif eventType == "progress-indicator-updated":
			self.indicators[indicator][1].SetValue(info[1])
			self.indicators[indicator][2].SetLabel(info[2])
		else:
			parts = self.indicators[indicator]
			self.sizer.Detach(parts[0])
			self.sizer.Detach(parts[1])
			self.sizer.Detach(parts[2])
			parts[0].Destroy()
			parts[1].Destroy()
			parts[2].Destroy()
			del self.indicators[indicator]
			self.Layout()

	
