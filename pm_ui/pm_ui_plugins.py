import wx, pympe, pm_language, pm_transcoder, pm_helpers

class PluginDialog(wx.Frame):
	def __init__(self):
		wx.Frame.__init__(self, pympe.ui.uiMain, -1, 'Plugin Manager', style=wx.DEFAULT_FRAME_STYLE|wx.FRAME_FLOAT_ON_PARENT^wx.RESIZE_BORDER)
		self.plugin = None
		self.justEnabled = None

		# Scan for new plugins on start
		pympe.events.plugins_plugin_started.subscribe(self.mpevent_plugins_plugin_state_changed)
		pympe.events.plugins_plugin_stopped.subscribe(self.mpevent_plugins_plugin_state_changed)

		uiButtonScan = wx.Button(self, -1, 'Scan For New Plugins')
		uiButtonScan.Bind(wx.EVT_BUTTON, self.event_button_scan)

		uiButtonClose = wx.Button(self, wx.ID_CLOSE)
		uiButtonClose.Bind(wx.EVT_BUTTON, self.event_button_close)

		#uiSizerVertical = wx.BoxSizer(wx.VERTICAL)
		#uiSizerHorizontal = wx.BoxSizer(wx.HORIZONTAL)
		uiSizer = wx.GridBagSizer(5, 5)
		uiSizerPlugin = wx.BoxSizer(wx.VERTICAL)

		self.uiCheckBoxPlugins = wx.CheckListBox(self, -1)

		self.plugins = sorted(pympe.plugins.plugins.keys(), key=lambda x: pympe.plugins.plugins[x]['info']['name'])
		self.uiCheckBoxPlugins.SetItems([pympe.plugins.plugins[pid]['info']['name'] for pid in self.plugins])
		for i, pid in enumerate(self.plugins):
			if pympe.plugins.plugins[pid]['running']:
				self.uiCheckBoxPlugins.Check(i)

		self.uiCheckBoxPlugins.Bind(wx.EVT_LISTBOX, self.event_select_box_plugins)
		self.uiCheckBoxPlugins.Bind(wx.EVT_CHECKLISTBOX, self.event_check_box_plugins)

		self.uiLabelName = wx.StaticText(self, -1, 'Please Select A Plugin')
		self.uiLabelName.SetFont(wx.Font(-1, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		self.uiLabelDescription = wx.StaticText(self, -1, '')
		self.uiLabelDescription.Wrap(500)
		self.uiLabelVersion = wx.StaticText(self, -1, '')
		self.uiButtonPreferences = wx.Button(self, wx.ID_PREFERENCES)
		self.uiButtonPreferences.Disable()
		self.uiButtonPreferences.Bind(wx.EVT_BUTTON, self.event_button_preferences)
		self.uiButtonRefresh = wx.Button(self, wx.ID_REFRESH)
		self.uiButtonRefresh.Disable()
		self.uiButtonRefresh.Bind(wx.EVT_BUTTON, self.event_button_refresh)
		uiSizerPlugin.Add(self.uiLabelName, flag=wx.EXPAND)
		uiSizerPlugin.Add(self.uiLabelDescription, flag=wx.EXPAND)
		uiSizerPlugin.Add(self.uiLabelVersion, flag=wx.EXPAND)
		uiSizerPlugin.Add(self.uiButtonPreferences, 0, wx.TOP, 10)
		uiSizerPlugin.Add(self.uiButtonRefresh, 0)
		uiSizerPlugin.Add((500, 0))
		uiSizer.Add(self.uiCheckBoxPlugins, (0, 0))
		uiSizer.Add(uiSizerPlugin, (0, 1), flag=wx.EXPAND)
		#uiSizerHorizontal.Add(self.uiCheckBoxPlugins, 0)
		#uiSizerHorizontal.Add(uiSizerPlugin, 1, wx.EXPAND|wx.ALL, 5)

		#uiSizerVertical.Add(uiSizerHorizontal, 1, wx.EXPAND)


		#uiSizerButtons = wx.BoxSizer(wx.HORIZONTAL)
		uiSizer.Add(uiButtonScan, (1,0), flag=wx.EXPAND|wx.BOTTOM, border=5)
		#uiSizerButtons.AddStretchSpacer()
		uiSizer.Add(uiButtonClose, (1, 1), flag=wx.ALIGN_RIGHT|wx.BOTTOM|wx.RIGHT, border=5)

		#uiSizerVertical.Add(uiSizerButtons, 0, wx.EXPAND|wx.RIGHT|wx.LEFT|wx.BOTTOM, 5)
		#self.SetSizerAndFit(uiSizerVertical)
		self.SetSizerAndFit(uiSizer)

		self.Show()
		self.CenterOnParent()

	def mpevent_plugins_plugin_state_changed(self, event, pluginUid):
		for i, pid in enumerate(self.plugins):
			try: self.uiCheckBoxPlugins.Check(pympe.plugins.plugins[pid]['running'])
			except KeyError: pass

	def event_button_scan(self, event):
		pympe.plugins.scan()

		self.uiCheckBoxPlugins.Clear()

		self.plugins = sorted(pympe.plugins.plugins.keys(), key=lambda x: pympe.plugins.plugins[x]['info']['name'])
		self.uiCheckBoxPlugins.SetItems([pympe.plugins.plugins[pid]['info']['name'] for pid in self.plugins])
		for i, pid in enumerate(self.plugins):
			self.uiCheckBoxPlugins.Check(pympe.plugins.plugins[pid]['running'])

		self.Layout()

	def event_button_preferences(self, event):
		pympe.plugins.show_settings(self.plugin['info']['uid'], self)

	def event_button_refresh(self, event):
		pympe.plugins.reload_plugin(self.plugin['info']['uid'])

	def event_check_box_plugins(self, event):
		if event.EventObject.IsChecked(event.Int):
			if not pympe.plugins.start_plugin(pympe.plugins.plugins[self.plugins[event.Int]]['info']['uid']):
				event.EventObject.Check(event.Int, False)
		else:
			pympe.plugins.stop_plugin(pympe.plugins.plugins[self.plugins[event.Int]]['info']['uid'])

		self.event_select_box_plugins(event, True)
		event.Skip()

	def event_select_box_plugins(self, event, forced=False):
		self.plugin = pympe.plugins.plugins[self.plugins[event.Int]]

		if not forced:
			self.uiLabelName.SetLabel(self.plugin['info']['name'])
			self.uiLabelDescription.SetLabel(self.plugin['info']['description'])
			self.uiLabelVersion.SetLabel('v%s'%self.plugin['info']['version'])

		self.uiButtonPreferences.Enable(self.plugin['info']['configurable'] and self.plugin['running'])
		self.uiButtonRefresh.Enable()

		#self.uiSizerPlugin.Layout()

		self.Layout()

	def event_button_close(self, event):
		pympe.ui.uiMain.pluginDialog = None
		pympe.events.plugins_plugin_started.unsubscribe(self.mpevent_plugins_plugin_state_changed)
		pympe.events.plugins_plugin_stopped.unsubscribe(self.mpevent_plugins_plugin_state_changed)
		self.Destroy()
