import pympe, os, dbus



class Plugin:
	def __plugin_settings__(self): pass

	def __plugin_start__(self):
		try:
			bus = dbus.Bus(dbus.Bus.TYPE_SESSION)
			self.obj = bus.get_object('org.gnome.SettingsDaemon', '/org/gnome/SettingsDaemon/MediaKeys')
			self.obj.GrabMediaPlayerKeys('Pympe', 0, dbus_interface='org.gnome.SettingsDaemon.MediaKeys')
			self.obj.connect_to_signal('MediaPlayerKeyPressed', self.handle_mediakey)
			self.bindType = 0
		except:
			try:
				import keybinder

				self.bindResults = (keybinder.bind('XF86AudioPlay', self.kbd_playpause, None),
									keybinder.bind('XF86AudioStop', self.kbd_stop, None),
									keybinder.bind('XF86AudioNext', self.kbd_next, None),
									keybinder.bind('XF86AudioPrev', self.kbd_previous, None),
									keybinder.bind('XF86AudioRaiseVolume', self.kbd_volume_raise, None),
									keybinder.bind('XF86AudioLowerVolume', self.kbd_volume_lower, None))

				self.bindType = 1
			except:
				self.bindType = None
				return

	def kbd_playpause(self, x):
		if pympe.player.paused or not pympe.player.playing:
			pympe.events.player_play.send()
		else:
			pympe.events.player_pause.send()

	def kbd_stop(self, x):
		pympe.events.player_stop.send()

	def kbd_next(self, x):
		pympe.events.player_next.send()

	def kbd_previous(self, x):
		pympe.events.player_previous.send()

	def kbd_volume_raise(self, x):
		volume = pympe.player.volume()+0.05
		if volume > 1.0: volume = 1.0
		pympe.events.player_set_volume.send(volume)

	def kbd_volume_lower(self, x):
		volume = pympe.player.volume()-0.05
		if volume < 0.0: volume = 0.0
		pympe.events.player_set_volume.send(volume)

	def __plugin_stop__(self, shutdown):
		if self.bindType == 0:
			self.obj.ReleaseMediaPlayerKeys("Pympe")
		elif self.bindType == 1:
			import keybinder

			if self.bindResults[0]: keybinder.unbind('XF86AudioPlay')
			if self.bindResults[1]: keybinder.unbind('XF86AudioStop')
			if self.bindResults[2]: keybinder.unbind('XF86AudioNext')
			if self.bindResults[3]: keybinder.unbind('XF86AudioPrev')
			if self.bindResults[4]: keybinder.unbind('XF86AudioRaiseVolume')
			if self.bindResults[5]: keybinder.unbind('XF86AudioLowerVolume')

	def handle_mediakey(self, app, *mmkeys):
		if app != 'Pympe': return

		for key in mmkeys:
			if key == "Play":
				if pympe.player.paused or not pympe.player.playing:
					pympe.events.player_play.send()
				else:
					pympe.events.player_pause.send()
			elif key == "Stop":
				pympe.events.player_stop.send()
			elif key == "Next":
				pympe.events.player_next.send()
			elif key == "Previous":
				pympe.events.player_previous.send()
