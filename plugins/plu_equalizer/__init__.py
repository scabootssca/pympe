import pympe, gst, wx
from pm_player import AudioBin

class EqualizerBin(AudioBin):
	def setup(self):
		self.preamp = gst.element_factory_make('volume')
		self.equalizer = gst.element_factory_make("equalizer-10bands")

		self.elements = [gst.element_factory_make('audioconvert'), self.preamp, self.equalizer]

		self.equalizer.set_property('band0', 6)
		self.equalizer.set_property('band1', 6)
		self.equalizer.set_property('band2', 6)

class EqualizerDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, wx.ID_ANY, "Equalizer Settings")

		sizer = wx.GridBagSizer(5, 5)

		self.controlList = []
		self.sliderList = []
		self.plugin = plugin

		# Store what we need locally
		self.currentValues = _config['presets'][_config['current']]
		self.choicesByValues = dict([(v[0], k) for k, v in _config['presets'].iteritems()])
		self.sortedChoices = sorted(self.choicesByValues.keys())

		# Preset Selection
		##############
		buttonNewPreset = wx.Button(self, wx.ID_NEW)
		self.comboPresets = wx.ComboBox(self, -1, self.currentValues[0], choices=self.sortedChoices, style=wx.CB_DROPDOWN|wx.CB_SORT)
		buttonDeletePreset = wx.Button(self, wx.ID_DELETE)

		box = wx.BoxSizer()
		sizer.Add(box, (0, 0), (1, 12), flag=wx.EXPAND)
		box.Add(buttonNewPreset)
		box.Add(self.comboPresets,  wx.EXPAND)
		box.Add(buttonDeletePreset)

		buttonNewPreset.Bind(wx.EVT_BUTTON, self.event_button_new_preset)
		buttonDeletePreset.Bind(wx.EVT_BUTTON, self.event_button_delete_preset)
		self.comboPresets.Bind(wx.EVT_COMBOBOX, self.event_combo_presets)

		self.controlList += [buttonNewPreset, self.comboPresets, buttonDeletePreset]

		# Line
		##############
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), (1, 0), (1, 12), flag=wx.EXPAND)

		## Preamp
		###############
		labelTop = wx.StaticText(self, -1, "%.1f%%"%self.currentValues[1])
		self.sliderPreamp = wx.Slider(self, -1, self.currentValues[1], 0, 100, size=(50, 200), style=wx.SL_VERTICAL|wx.SL_INVERSE)
		labelBottom = wx.StaticText(self, -1, 'Preamp')

		self.sliderPreamp.index = -1
		self.sliderPreamp.label = labelTop
		self.sliderPreamp.desc = labelBottom

		self.sliderPreamp.Bind(wx.EVT_SCROLL, self.event_slider)

		sizer.Add(labelTop, (2, 0), flag=wx.ALIGN_CENTER)
		sizer.Add(self.sliderPreamp, (3, 0), flag=wx.ALIGN_CENTER|wx.EXPAND)
		sizer.Add(labelBottom, (4, 0), flag=wx.ALIGN_CENTER)

		sizer.Add(wx.StaticLine(self, style=wx.LI_VERTICAL), (2, 1), (3, 1), flag=wx.EXPAND|wx.LEFT|wx.RIGHT, border=5)

		## Sliders
		###############
		labels = ["29Hz", "59Hz", "119Hz", "227Hz", "474Hz", "947Hz", "1.9KHz", "3.8KHz", "7.5KHz", "15KHz"]

		for x in range(10):
			labelTop = wx.StaticText(self, -1, "%+.1f"%self.currentValues[2][x])
			slider = wx.Slider(self, -1, self.currentValues[2][x], -240, 120, size=(50, 200), style=wx.SL_VERTICAL|wx.SL_INVERSE)
			labelBottom = wx.StaticText(self, -1, labels[x])

			slider.index = x
			slider.label = labelTop
			slider.desc = labelBottom

			sizer.Add(labelTop, (2, x+2), flag=wx.ALIGN_CENTER)
			sizer.Add(slider, (3, x+2), flag=wx.ALIGN_CENTER|wx.EXPAND)
			sizer.Add(labelBottom, (4, x+2), flag=wx.ALIGN_CENTER)

			slider.Bind(wx.EVT_SCROLL, self.event_slider)

			self.sliderList.append(slider)
			self.controlList += [labelTop, slider, labelBottom]

		# Line
		##############
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), (5, 0), (1, 12), flag=wx.EXPAND)

		# Close/Enable
		##############
		toggleEnabled = wx.ToggleButton(self, -1, "Equalizer Enabled")
		buttonClose = wx.Button(self, wx.ID_CLOSE)
		buttonSave = wx.Button(self, wx.ID_SAVE)

		toggleEnabled.Bind(wx.EVT_TOGGLEBUTTON, self.event_toggle_enabled)
		buttonClose.Bind(wx.EVT_BUTTON, self.event_button_close)
		buttonSave.Bind(wx.EVT_BUTTON, self.event_button_save)

		box = wx.BoxSizer()
		sizer.Add(box, (6, 0), (1, 12), flag=wx.EXPAND)
		box.Add(buttonClose)
		box.AddStretchSpacer()
		box.Add(toggleEnabled)
		box.AddStretchSpacer()
		box.Add(buttonSave)

		# Restore Its State
		##############
		if not cfg_get("enabled"):
			for obj in self.controlList: obj.Disable()
		else:
			toggleEnabled.SetValue(True)

		# For keyboard controls
		self.Bind(wx.EVT_MENU, self.event_accelerator_key)
		aTable = wx.AcceleratorTable([(wx.ACCEL_NORMAL, ord('q'), 0),
									(wx.ACCEL_NORMAL, ord('w'), 1),
									(wx.ACCEL_NORMAL, ord('e'), 2),
									(wx.ACCEL_NORMAL, ord('r'), 3),
									(wx.ACCEL_NORMAL, ord('t'), 4),
									(wx.ACCEL_NORMAL, ord('y'), 5),
									(wx.ACCEL_NORMAL, ord('u'), 6),
									(wx.ACCEL_NORMAL, ord('i'), 7),
									(wx.ACCEL_NORMAL, ord('o'), 8),
									(wx.ACCEL_NORMAL, ord('p'), 9),
									(wx.ACCEL_NORMAL, ord('a'), 10),
									(wx.ACCEL_NORMAL, ord('s'), 11),
									(wx.ACCEL_NORMAL, ord('d'), 12),
									(wx.ACCEL_NORMAL, ord('f'), 13),
									(wx.ACCEL_NORMAL, ord('g'), 14),
									(wx.ACCEL_NORMAL, ord('h'), 15),
									(wx.ACCEL_NORMAL, ord('j'), 16),
									(wx.ACCEL_NORMAL, ord('k'), 17),
									(wx.ACCEL_NORMAL, ord('l'), 18),
									(wx.ACCEL_NORMAL, ord(';'), 19)])
		self.SetAcceleratorTable(aTable)

		# Finish up
		sizer.AddGrowableRow(1)
		box = wx.BoxSizer()
		box.Add(sizer, 1, wx.ALL|wx.EXPAND, 10)
		self.SetSizerAndFit(box)

		# Show it
		self.CenterOnScreen()
		self.Show()

	def event_accelerator_key(self, event):
		direction = 1 if event.Id < 10 else -1
		index = event.Id if event.Id < 10 else event.Id-10

		value = self.sliderList[index].GetValue()+(3*direction)

		if value < -240: value = -240
		elif value > 120: value = 120

		self.sliderList[index].SetValue(value)
		self.sliderList[index].label.SetLabel("%+.1f"%value)
		self.plugin.equalizer.equalizer.set_property("band%i"%index, int(value)*.1)

		event.Skip()

	def event_button_new_preset(self, event):
		dialog = wx.TextEntryDialog(self, "What shall the eq preset be named?", "Create an eq preset.")
		dialog.ShowModal()
		name = dialog.GetValue()
		if not name: return

		# Create it in the config file
		_config['presets'][_config['nextUid']] = [name, 0.0, [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]]
		_config['current'] = _config['nextUid']
		_config['nextUid'] += 1

		# Now locally
		self.currentValues = _config['presets'][_config['current']]
		self.choicesByValues = dict([(v[0], k) for k, v in _config['presets'].iteritems()])
		self.sortedChoices = sorted(self.choicesByValues.keys())

		self.plugin.equalizer.preamp.set_property('volume', self.currentValues[1])
		for slider in self.sliderList:
			slider.SetValue(0)
			slider.label.SetLabel("+0.0")
			self.plugin.equalizer.equalizer.set_property("band%i"%slider.index, 0)

		self.comboPresets.Clear()
		self.comboPresets.AppendItems(self.sortedChoices)
		self.comboPresets.SetValue(name)

		event.String = name
		self.event_combo_presets(event)

	def event_button_delete_preset(self, event):
		preset = self.comboPresets.GetValue()

		print 'Needs a flag like default to make the preset uneditable or undeleteable'
		dialog = wx.MessageDialog(self, "Are you sure you want to delete the eq preset: %s\nThis operation cannot be undone."%preset, "Delete Preset", wx.YES_NO | wx.ICON_QUESTION)
		if dialog.ShowModal() == wx.ID_NO: return

		if preset in self.sortedChoices:
			uid = self.choicesByValues[self.comboPresets.GetSelection()]

			del _config['presets'][uid]

			self.choicesByValues = dict([(v[0], k) for k, v in _config['presets'].iteritems()])
			self.sortedChoices = sorted(self.choicesByValues.keys())

			self.comboPresets.Clear()
			self.comboPresets.AppendItems(self.sortedChoices)

			if _config['presets']:
				_config['current'] = _config['presets'].keys()[0]
				self.currentValues = _config['presets'][_config['current']]
				self.comboPresets.SetValue(self.currentValues[0])

				self.plugin.equalizer.preamp.set_property('volume', self.currentValues[1])
				for slider, value in zip(self.sliderList, self.currentValues[2]):
					slider.SetValue(value)
					slider.label.SetLabel("%+.1f"%value)
					self.plugin.equalizer.equalizer.set_property("band%i"%slider.index, int(value)*.1)

			else:
				self.comboPresets.SetValue('NO PRESETS EXIST')

				self.plugin.equalizer.preamp.set_property('volume', 1.0)
				for slider in self.sliderList:
					slider.SetValue(0)
					slider.label.SetLabel("%+.1f"%0)
					self.plugin.equalizer.equalizer.set_property("band%i"%slider.index, 0)


	def event_toggle_enabled(self, event):
		value = event.GetInt()
		_config['enabled'] = value

		if value:
			self.plugin.equalizer.preamp.set_property('volume', self.currentValues[1]*.01)
			for obj in self.controlList: obj.Enable()
			for i, value in enumerate(self.currentValues[2]):
				self.plugin.equalizer.equalizer.set_property("band%i"%i, int(value)*.1)
		else:
			self.plugin.equalizer.preamp.set_property('volume', 1.0)
			for i in range(10): self.plugin.equalizer.equalizer.set_property("band%i"%i, 0)
			for obj in self.controlList: obj.Disable()

	def event_button_close(self, event):
		self.Destroy()

	def event_button_save(self, event):
		#dialog = wx.SingleChoiceDialog(self, 'Save Eq Setting For What?', 'Caption', ['Save For Preset', 'Save For Track', 'Save For Genre'])
		#result = dialog.ShowModal()

		self.currentValues[1] = self.sliderPreamp.GetValue()
		self.currentValues[2] = [slider.GetValue() for slider in self.sliderList]
		_config['presets'][_config['current']] = self.currentValues

		#if dialog.GetSelection() == 1:   # Preset
		#	_config['presets'][_config['current']] = self.currentValues
		#elif dialog.GetSelection() == 2: # Track
			#pass
		#elif dialog.GetSelection() == 3: # Genre
			#dialog.GetSelection()

		self.Destroy()



	def event_slider(self, event):
		slider = event.GetEventObject()

		if slider.index != -1:
			slider.label.SetLabel("%+.1f"%event.Int)
			self.plugin.equalizer.equalizer.set_property("band%i"%slider.index, event.Int*.1)
		else:
			slider.label.SetLabel("%.1f%%"%event.Int)
			self.plugin.equalizer.preamp.set_property('volume', event.Int*.01)

	def event_combo_presets(self, event):
		# Save the current
		self.currentValues[1] = self.sliderPreamp.GetValue()
		self.currentValues[2] = [slider.GetValue() for slider in self.sliderList]
		_config['presets'][_config['current']] = self.currentValues

		# Set the new
		_config['current'] = self.choicesByValues[event.String]
		self.currentValues = _config['presets'][_config['current']]

		# Preamp and then eq
		self.sliderPreamp.SetValue(self.currentValues[1])
		self.sliderPreamp.label.SetLabel("%.1f%%"%self.currentValues[1])
		self.plugin.equalizer.preamp.set_property('volume', self.currentValues[1]*.01)

		for slider, value in zip(self.sliderList, self.currentValues[2]):
			slider.SetValue(value)
			slider.label.SetLabel("%+.1f"%value)
			self.plugin.equalizer.equalizer.set_property("band%i"%slider.index, int(value)*.1)

class Plugin:
	def __plugin_start__(self):
		self.equalizer = EqualizerBin('EqualizerBin')
		pympe.ui.set_menu_hook('Equalizer', self.__plugin_settings__)

		if _config['enabled']:
			pympe.player.audioBin.add_element(self.equalizer)
			self.set_preset(_config['current'])

	def __plugin_stop__(self, shutdown):
		if not shutdown and _config['enabled']:
			pympe.ui.remove_menu_hook(self.__plugin_settings__)
			pympe.player.audioBin.remove_element(self.equalizer)
			#del self.equalizer

	def set_preset(self, name):
		if name in _config['presets']:
			self.equalizer.preamp.set_property('volume', _config['presets'][name][1]*.01)
			for i, value in enumerate(_config['presets'][name][2]):
				self.equalizer.equalizer.set_property("band%i"%i, int(value)*.1)

	def __plugin_settings__(self, parent=pympe.ui.uiMain):
		EqualizerDialog(self, parent)
