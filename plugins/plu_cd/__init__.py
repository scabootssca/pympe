import pympe, pm_mediafile, wx, CDDB, DiscID, pm_helpers, pm_language, dbus, gst, pm_devices, glib, os, pm_transcoder
import wx.lib.mixins.listctrl as listmix
from pm_dbus import HalHandler
#dbusSupport = pympe.plugins.get_plugin(1323703550.7)

class ListCtrl(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin, listmix.TextEditMixin):
	def __init__(self, parent, id, style):
		wx.ListCtrl.__init__(self, parent, id, style=style)
		listmix.ListCtrlAutoWidthMixin.__init__(self)
		self.parent = parent
		self.editPos = (-1, -1)

		self.InsertColumn(0, pm_language.libraryColumnHeaders["tracknumber"], width=25)
		self.InsertColumn(1, pm_language.libraryColumnHeaders["title"], width=150)
		self.InsertColumn(2, pm_language.libraryColumnHeaders["artist"], width=150)
		self.InsertColumn(3, pm_language.libraryColumnHeaders["genre"], width=150)
		self.InsertColumn(4, pm_language.libraryColumnHeaders["year"], width=45)
		self.InsertColumn(5, pm_language.libraryColumnHeaders["length"], width=45)
		self.columns = ["tracknumber", "title", "artist", "genre", "year", "length"]

		listmix.TextEditMixin.__init__(self)

		self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.event_list_item_activated)
		self.Bind(wx.EVT_LIST_BEGIN_LABEL_EDIT, self.event_list_begin_label_edit)
		self.Bind(wx.EVT_TEXT, self.event_text)

	def event_list_item_activated(self, event):
		pympe.playlists.set_active_playlist()
		pympe.playlists.main.replace(None, self.parent.tracks, [x.display("title") for x in self.parent.tracks])
		pympe.playlists.main.set_index(event.Index)
		pympe.player.play(uri=pympe.playlists.main.get_entry())

	def event_list_begin_label_edit(self, event):
		self.editPos = (event.Index, event.GetItem().GetColumn())

		if self.editPos[1] in [0, 5]:
			event.Veto()
			return

	def set_item_text(self, key, row, text):
		if not key in self.columns: return
		column = self.columns.index(key)
		self.SetStringItem(row, column, self.parent.tracks[row].display(key))

	def event_text(self, event):
		key = self.columns[self.editPos[1]]
		track = self.parent.tracks[self.editPos[0]]

		track[key] = event.String.split("\\\\")
		if key == "artist": track.artistModified = True
		if track == pympe.player.currentFile: pympe.player_current_updated.send({key:track[key]})
		if "\\\\" in event.String: event.EventObject.ChangeValue(track.display(key))

class CDDevice(pm_devices.Device):
	def __init__(self, device="/dev/cdrom", panel=None):
		pm_devices.Device.__init__(self, device)
		self.device = device
		self.autoconnects = True
		self.panel = panel

	def connect(self):
		self.connected = True
		self.panel.read_disc(self.device)

	def disconnect(self):
		self.connected = False
		self.panel.eject_disc(False)

class CdPanel(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)

		self.device = None
		self.discId = None
		self.importing = False
		self.tracks = []

		self.textAlbum       = wx.TextCtrl(self, 0)
		self.textAlbumArtist = wx.TextCtrl(self, 1)
		self.textGenre       = wx.TextCtrl(self, 2)
		self.textYear        = wx.TextCtrl(self, 3)
		self.textDiscNumber  = wx.TextCtrl(self, 4)
		self.textDiscTotal   = wx.TextCtrl(self, 5)
		self.listTracks      = ListCtrl(self, -1, style=wx.LC_REPORT|wx.LC_EDIT_LABELS)
		self.gaugeProgress   = wx.Gauge(self, -1, size=(-1, 5))

		self.Bind(wx.EVT_TEXT, self.event_text_ctrl)

		sizer = wx.GridBagSizer(5, 5)
		sizer.AddGrowableCol(1)
		sizer.AddGrowableRow(3)

		sizer.Add(wx.StaticText(self, -1, "Album:"), (0, 0), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer.Add(self.textAlbum, (0, 1), (1, 7), flag=wx.EXPAND)
		sizer.Add(wx.StaticText(self, -1, "Album Artist:"), (1, 0), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer.Add(self.textAlbumArtist, (1, 1), (1, 7), flag=wx.EXPAND)
		sizer.Add(wx.StaticText(self, -1, "Genre:"), (2, 0), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer.Add(self.textGenre, (2, 1), flag=wx.EXPAND)
		sizer.Add(wx.StaticText(self, -1, "Year:"), (2, 2), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer.Add(self.textYear, (2, 3))
		sizer.Add(wx.StaticText(self, -1, "Disc:"), (2, 4), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer.Add(self.textDiscNumber, (2, 5))
		sizer.Add(wx.StaticText(self, -1, "of"), (2, 6), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer.Add(self.textDiscTotal, (2, 7))
		sizer.Add(self.listTracks, (3, 0), (1, 8), flag=wx.EXPAND)
		sizer.Add(self.gaugeProgress, (4, 0), (1, 8), flag=wx.EXPAND)

		self.labelMessage = wx.StaticText(self, -1)
		buttonRead = wx.Button(self, -1, "Read")
		self.buttonImport = wx.Button(self, -1, "Import")
		buttonEject = wx.Button(self, -1, "Eject")

		self.labelMessage.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))

		buttonRead.Bind(wx.EVT_BUTTON, self.event_button_read)
		self.buttonImport.Bind(wx.EVT_BUTTON, self.event_button_import)
		buttonEject.Bind(wx.EVT_BUTTON, self.event_button_eject)

		s2 = wx.BoxSizer()
		s2.Add(self.labelMessage, 1, wx.ALIGN_CENTER_VERTICAL)
		s2.Add(buttonRead)
		s2.Add(self.buttonImport)
		s2.Add(buttonEject)
		sizer.Add(s2, (5, 0), (1, 8), flag=wx.EXPAND)

		box = wx.BoxSizer()
		box.Add(sizer, 1, wx.EXPAND|wx.ALL, 10)
		self.SetSizer(box)

	def event_text_ctrl(self, event):
		if event.Id < 0: return
		key = ["album", "albumartist", "genre", "year", "discnumber", "disctotal"][event.Id]
		value = event.String.split("\\\\")

		for index, track in enumerate(self.tracks):
			track[key] = value

			if key == "albumartist" and not track.artistModified:
				track["artist"] = value
				self.listTracks.set_item_text("artist", index, value)
				if track == pympe.player.currentFile: pympe.player_current_updated.send({"artist":track['artist'], "albumartist":track['albumartist']})
			else:
				self.listTracks.set_item_text(key, index, value)
				if track == pympe.player.currentFile: pympe.player_current_updated.send({key:track[key])

	def event_button_read(self, event):
		self.read_disc("/dev/sr0")
		self.fetch_album_information()

	def stop_import(self):
		if not self.importing: return
		self.importing = False
		self.transcoder.stop()
		self.transcoder.threadEvent.set()
		self.buttonImport.SetLabel("Import")

	def event_button_import(self, event):
		if self.importing: return self.stop_import()

		self.importing = True
		self.buttonImport.SetLabel("Stop")
		threadEvent = threading.Event()
		def end_cb(): threadEvent.set()
		transcoder = pm_transcoder.Transcoder(callbackEnd=end_cb)

		self.transcoder = transcoder
		self.transcoder.threadEvent = threadEvent

		def on_progress_update(progress=None):
			if progress is None: progress = (transcoder.get_position()/(transcoder.trackLength+0.0))*100
			self.gaugeProgress.SetValue(int(progress))
			self.labelMessage.SetLabel("Importing Track %i/%i %i%%"%(transcoder.trackIndex, len(self.tracks), int(round(progress))))
			return True


		@pm_helpers.idleadd
		def set_label(label=None):
			if label is None: self.labelMessage.SetLabel("Importing Track %i/%i %0.2f%%"%(transcoder.trackIndex, len(self.tracks), 0.0))
			else: self.labelMessage.SetLabel(label)

		@pm_helpers.threaded
		def import_tracks():
			p_id = glib.timeout_add_seconds(1, on_progress_update)
			for trackIndex, track in enumerate(self.tracks):
				threadEvent.clear()
				transcoder.trackLength = self.tracks[trackIndex]["length"]
				trackIndex += 1
				transcoder.trackIndex = trackIndex
				transcoder.set_raw_input('cdparanoiasrc track=%s device="%s"'%(trackIndex, self.device.get_name()))
				filename = pympe.config.get("library", "outputDirectory")
				filename = filename+os.sep+pm_helpers.get_mediafile_format_string(track, pympe.config.get("library", "renamingRules"))+"."+pm_transcoder.formats[pympe.config.get("import", "format")]["extension"]

				# Make the directory tree as needed
				directory = ""
				for d in filename.split(os.sep)[:-1]:
					directory += d+os.sep
					if not os.path.exists(directory): os.mkdir(directory)

				transcoder.set_output(filename)
				transcoder.start()
				threadEvent.wait()
				if not self.importing: break
				mediaFile = pm_mediafile.MediaFile("file://"+filename)
				mediaFile.read()
				mediaFile.write(dict([(key, ('\\\\'.join(track[key]) if key in mediaFile.multiTags else track[key])) for key in ["title", "artist", "albumartist", "album", "year", "genre", "tracknumber", "tracktotal", "discnumber", "disctotal"]]))
			glib.source_remove(p_id)
			on_progress_update(0)
			set_label("Finished Importing Tracks")

		import_tracks()

	def event_button_eject(self, event):
		self.eject_tray()

	def read_disc(self, deviceName):
		device = pympe.devices.get_device(deviceName)
		if device: self.device = device
		else:
			self.device = CDDevice(deviceName, self)
			pympe.devices.add_device(self.device)
			self.device.autoconnect()

		discId = DiscID.disc_id(DiscID.open(self.device.get_name()))
		(cddbChecksum, numTracks, frames, numSeconds) = (discId[0], discId[1], discId[2:-1], discId[-1])
		if self.discId and discId[0] == self.discId[0]: return
		else:
			self.discId = discId
			self.listTracks.DeleteAllItems()

		totalLength = 0
		self.tracks = []
		for i in range(numTracks):
			if i < numTracks-1: length = (frames[i+1]-frames[i])/75.0
			else: length = numSeconds-totalLength
			totalLength += length

			uri = "cdda://%s/#%s"%(i+1, self.device.get_name())
			mediaFile = pm_mediafile.MediaFile(uri)
			mediaFile["title"] = ["Track "+str(i+1)]
			mediaFile["artist"] = mediaFile["albumartist"] = ["Unknown Artist"]
			mediaFile["album"] = ["Unknown Album"]
			mediaFile["year"] = ""
			mediaFile["genre"] = [""]
			mediaFile["tracknumber"] = i+1
			mediaFile["tracktotal"] = numTracks
			mediaFile["discnumber"] = 1
			mediaFile["disctotal"] = 1
			mediaFile["length"] = int(length)
			mediaFile.artistModified = False
			self.tracks.append(mediaFile)

			# Update the display
			self.textAlbum.ChangeValue(mediaFile.display("album"))
			self.textAlbumArtist.ChangeValue(mediaFile.display("albumartist"))
			self.textGenre.ChangeValue(mediaFile.display("genre"))
			self.textYear.ChangeValue(mediaFile.display("year"))
			self.textDiscNumber.ChangeValue(mediaFile.display("discnumber"))
			self.textDiscTotal.ChangeValue(mediaFile.display("disctotal"))
			self.listTracks.InsertStringItem(i, mediaFile.display("tracknumber"))
			self.listTracks.SetStringItem(i, 1, mediaFile.display("title"))
			self.listTracks.SetStringItem(i, 2, mediaFile.display("artist"))
			self.listTracks.SetStringItem(i, 3, mediaFile.display("genre"))
			self.listTracks.SetStringItem(i, 4, mediaFile.display("year"))
			self.listTracks.SetStringItem(i, 5, pm_helpers.make_display_time(mediaFile.display("length"), showHours=False))

	def eject_disc(self, ejectTray=True):
		self.stop_import()
		self.listTracks.DeleteAllItems()
		self.textAlbum.ChangeValue("")
		self.textAlbumArtist.ChangeValue("")
		self.textGenre.ChangeValue("")
		self.textYear.ChangeValue("")
		self.textDiscNumber.ChangeValue("")
		self.textDiscTotal.ChangeValue("")
		self.discId = None

		if pympe.player.currentFile["uri"].endswith(self.device.get_name()): pympe.player.stop()

	def fetch_album_information(self):
		def display_info(readInfo):
			# Update the tracks
			(artist, album) = readInfo["DTITLE"].split("/")
			(artist, album) = (artist.strip(), album.strip())
			for i, mediaFile in enumerate(self.tracks):
				mediaFile["title"] = [readInfo["TTITLE"+str(i)]]
				mediaFile["artist"] = [artist]
				mediaFile["album"] = [album]
				if "DYEAR" in readInfo: mediaFile["year"] = [readInfo["DYEAR"]]
				if "DGENRE" in readInfo: mediaFile["genre"] = [readInfo["DGENRE"]]

				# If it's the now playing then update the info
				if pympe.player.currentFile and mediaFile["uri"] == pympe.player.currentFile["uri"]:
					pympe.events.player_current_updated(mediaFile.info)

			# Update the display
				self.listTracks.SetItemText(i, mediaFile.display("title"))
			self.textAlbum.ChangeValue(album)
			self.textAlbumArtist.ChangeValue(artist)
			self.textGenre.ChangeValue(mediaFile.display("genre"))
			self.textYear.ChangeValue(mediaFile.display("year"))

		def read_info(info):
			(readStatus, readInfo) = CDDB.read(info['category'], info['disc_id'])
			if readStatus != 210: return False
			display_info(readInfo)

		def display_results(info, multi=False):
			if multi:
				dialog = wx.SingleChoiceDialog(pympe.ui.uiMain, "Please select the correct album.", "Select an album.", ["%s: %s"%(x["category"].capitalize(), x["title"]) for x in info])
				if dialog.ShowModal() == wx.ID_OK:
					read_info(info[dialog.GetSelection()])
			else:
				read_info(info)

		try: (queryStatus, queryInfo) = CDDB.query(self.discId, client_name=pympe.config.get("main", "name"), client_version=pympe.config.get("main", "version"))
		except: return False

		if queryStatus == 200: # Success
			display_results(queryInfo)
		elif queryStatus == 210 or queryStatus == 211: # 210 Multiple exact or 211 Multiple inexact
			queryInfo = display_results(queryInfo, True)
		else: return False

#class CDHandler(dbusSupport.Handler):
    #name = "cd"
    #def __init__(self, panel):
		#self.panel = panel

    #def is_type(self, device, capabilities):
        #if "volume.disc" in capabilities:
            #return True
        #return False

    #def get_udis(self, hal):
        #udis = hal.hal.FindDeviceByCapability("volume.disc")
        #return udis

    #def device_from_udi(self, hal, udi):
		#cd_obj = hal.bus.get_object("org.freedesktop.Hal", udi)
		#cd = dbus.Interface(cd_obj, "org.freedesktop.Hal.Device")
		#if not cd.GetProperty("volume.disc.has_audio"): return

		#deviceName = str(cd.GetProperty("block.device"))

		#device = pympe.devices.get_device(deviceName)
		#if device: return device

		#cddev = CDDevice(deviceName, self.panel)

		#return cddev
class CDHandler(HalHandler):
    name = "cd"
    def __init__(self, panel):
		self.panel = panel

    def is_compatible(self, device, capabilities):
        if "volume.disc" in capabilities:
            return True
        return False

    def get_udis(self):
        return pympe.dbus.hal.FindDeviceByCapability("volume.disc")

    def device_from_udi(self, udi):
		cdObject = pympe.dbus.hal.bus.get_object("org.freedesktop.Hal", udi)
		cd = dbus.Interface(cdObject, "org.freedesktop.Hal.Device")
		if not cd.GetProperty("volume.disc.has_audio"): return

		deviceName = str(cd.GetProperty("block.device"))
		device = pympe.devices.get_device(deviceName)

		if device: return device
		return CDDevice(deviceName, self.panel)

class Plugin:
	def __plugin_start__(self):
		self.uiPanelMain = CdPanel(pympe.ui.uiMain.uiTabsLeft)
		pympe.dbus.hal.add_handler(CDHandler(self.uiPanelMain))
		pympe.ui.uiMain.uiTabsLeft.InsertPage(2, self.uiPanelMain, "CD")

	def __plugin_stop__(self, shutdown):
		self.uiPanelMain.stop_import()

		# Remove the panel
		for index in range(pympe.ui.uiMain.uiTabsLeft.GetPageCount()):
			if pympe.ui.uiMain.uiTabsLeft.GetPage(index) == self.uiPanelMain:
				pympe.ui.uiMain.uiTabsLeft.RemovePage(index)
				break

	def __plugin_settings__(self):
		pass

