import pympe, pm_mediafile, wx, pm_playlists, CDDB, DiscID, pm_helpers

class ChoiceDialog(wx.Dialog):
	def __init__(self, choices):
		dialog = wx.PreDialog()
		dialog.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
		dialog.Create(pympe.ui.uiMain, -1, "Select the correct result")
		self.PostCreate(dialog)
		
		sizer = wx.BoxSizer(wx.VERTICAL)
		listCtrl = wx.Choice(self, -1, choices=choices)
		button = wx.Button(self, wx.ID_OK)
		
		sizer.Add(listCtrl, 1, wx.EXPAND)
		sizer.Add(button)
		
		
		
		# Add the sizer into the dialog with a space
		box = wx.BoxSizer()
		box.Add(sizer, 0, wx.ALL, 10)
		self.SetSizerAndFit(box)
		
		self.Show()

class CdPlaylist(pm_playlists.Playlist):
	def __init__(self, name, plugin):
		pm_playlists.Playlist.__init__(self, name)
		self.plugin = plugin
	
	def build_menu(self, menu):
		menu.Append(6666, "Read Cd")
		menu.Append(6667, "Fetch Cd Information")
		menu.AppendSeparator()
		menu.Append(6668, "Eject Cd")
		
	def handle_menu(self, event):
		if event.Id == 6666:
			try: self.load_cd()
			except: wx.MessageDialog(None, "There was an error loading the cd: ", "Error loading cd.", wx.YES_NO).ShowModal()
		if event.Id == 6667: 
			self.plugin.fetch_info()

	def get_metadata(self, index):
		return self.displayQueue[index]["title"][0]

class Plugin:
	def __plugin_start__(self):
		self.playlist = pympe.playlists.insert_playlist(CdPlaylist("CD: ", self))
		self.device = "/dev/sr0"
		self.discId = None
		
		try: self.load_cd()
		except: pass

	def __plugin_stop__(self):
		if self.playlistName:
			pympe.playlists.delete_playlist(self.playlistName)
		
	def __plugin_settings__(self):
		pass

	def load_cd(self):
		self.discId = DiscID.disc_id(DiscID.open(self.device))
		(cddbChecksum, numTracks, frames, numSeconds) = (self.discId[0], self.discId[1], self.discId[2:-2], self.discId[-1])

		tracks = []
		for i in range(numTracks):
			uri = "cdda://%s/#%s"%(i+1, self.device)
			mediaFile = pm_mediafile.MediaFile(uri)
			mediaFile["title"] = ["Track "+str(i)]
			mediaFile["artist"] = ["Unknown Artist"]
			mediaFile["album"] = ["Unknown Album"]
			mediaFile["track"] = i
			mediaFile["year"] = ""
			mediaFile["genre"] = [""]
			tracks.append(mediaFile)

		#self.playlistName = "CD: Unknown Album"
		#playlist = pympe.playlists.insert_playlist(CdPlaylist(self.playlistName, self))
		pympe.playlists.rename_playlist(self.playlist.name, "CD: Unknown Album")
		self.playlist.replace(tracks)
		pympe.playlists.set_active_playlist(self.playlist.name)

	#@pm_helpers.mod_threaded
	def fetch_info(self):
		try: (queryStatus, queryInfo) = CDDB.query(self.discId, client_name=pympe.config.get("main", "name"), client_version=pympe.config.get("main", "version"))
		except: return False

		if queryStatus == 200: # Success
			self.display_results(queryInfo)
		elif queryStatus == 210 or queryStatus == 211: # 210 Multiple exact or 211 Multiple inexact
			queryInfo = self.display_results(queryInfo, True)
		else: return False
	
	#@pm_helpers.mod_idleadd
	def display_results(self, info, multi=False):
		if multi:
			dialog = wx.SingleChoiceDialog(pympe.ui.uiMain, "Please select the correct album.", "Select an album.", ["%s: %s"%(x["category"].capitalize(), x["title"]) for x in info])
			if dialog.ShowModal() == wx.ID_OK:
				self.read_info(info[dialog.GetSelection()])
		else:
			self.read_info(info)
		
	#@pm_helpers.mod_threaded
	def read_info(self, info):
		(readStatus, readInfo) = CDDB.read(info['category'], info['disc_id'])
		if readStatus != 210: return False
		self.display_info(readInfo)
	
	#@pm_helpers.mod_idleadd
	def display_info(self, readInfo):
		# Update the tracks
		(artist, album) = readInfo["DTITLE"].split("/")
		(artist, album) = (artist.strip(), album.strip())
		for i in range(self.playlist.get_length()):
			mediaFile = self.playlist.displayQueue[i]
			mediaFile["title"] = [readInfo["TTITLE"+str(i)]]
			mediaFile["artist"] = [artist]
			mediaFile["album"] = [album]
			if "DYEAR" in readInfo: mediaFile["year"] = [readInfo["DYEAR"]]
			if "DGENRE" in readInfo: mediaFile["genre"] = [readInfo["DGENRE"]]
			
			# If it's the now playing then update the info
			if pympe.player.currentFile and mediaFile["uri"] == pympe.player.currentFile["uri"]:
				pympe.event.send("currentfile-updated-tags", ["title", "artist", "album", "year", "genre"])

		# Update the playlist
		pympe.playlists.rename_playlist(self.playlist.name, "CD: "+album)
