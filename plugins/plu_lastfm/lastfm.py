# Compatible with last.fm scrobbling 2.0 (beta)
# http://users.last.fm/~tims/Scrobbling2_0_beta_docs.html
from hashlib import md5
import urllib, urllib2, httplib, webbrowser, time, pympe, json, pm_helpers
import pm_network, os
import cPickle as pickle
debug = True

class LastFM:
	def __init__(self):
		self.apiKey     = "bc7979aeb01b2063aa4a9412d2d71ed7"
		self.secret     = "5b0d449c41ef66483d4b4a1a797cd5f0"
		self.sessionKey = ""
		self.userName   = ""

		self.webServicesUrl = "http://ws.audioscrobbler.com/2.0/"
		self.authUrl        = "http://www.last.fm/api/auth"

		self.queryTimes = [0, 0, 0, 0, 0]

		if not os.path.exists('failedscrobbles.log'):
			with open('failedscrobbles.log', 'w') as f: f.write(pickle.dumps([]))
		with open('failedscrobbles.log', 'r') as f: self.backlog = pickle.load(f)

	def format_args(self, method, url, sign, **args):
		args["method"]  = method
		args["api_key"] = self.apiKey


		if sign:
			args["sk"]      = self.sessionKey
			args["api_sig"] = md5("".join(["%s%s"%(x, args[x]) for x in sorted(args.keys())])+self.secret).hexdigest()

		args["format"]  = "json"

		return args

	def send_get(self, method, sign, **args):
		if time.time()-self.queryTimes[0] < 1:
			time.sleep(time.time()-self.queryTimes[0])
		self.queryTimes = self.queryTimes[1:]+[time.time()]

		url = self.webServicesUrl+"?"+urllib.urlencode(sorted(self.format_args(method, self.webServicesUrl, sign, **args).items()))
		response = pm_network.get_url(url, refresh=True, returnData=True)
		if not response: return False
		return json.loads(response)

	@pm_helpers.threaded
	def send_get_threaded(self, method, sign, finishedCallback, errorCallback, **args):
		result = self.send_get(method, sign, **args)

		if result == False and errorCallback: return pm_helpers.unthread_function(errorCallback)
		elif finishedCallback: return pm_helpers.unthread_function(finishedCallback, result)

	def send_post(self, method, sign, **args):
		if time.time()-self.queryTimes[0] < 1:
			time.sleep(time.time()-self.queryTimes[0])
		self.queryTimes = self.queryTimes[1:]+[time.time()]

		pm_network.post_url_threaded(self.webServicesUrl, self.format_args(method, self.webServicesUrl, sign, **args), failureCallback=self.add_to_backlog, finishedCallback=self.scrobble_result, refresh=True, returnData=True, callbackArgs=[args])
		#if not response: return False
		#return json.loads(response)

	def session_auth(self):
		if debug: pympe.debug("Getting auth token")
		response = self.send_get("auth.gettoken", False)
		if not response: return False

		if debug: pympe.debug("Asking for user permission")
		webbrowser.open(self.authUrl+"?api_key="+self.apiKey+"&token="+response["token"])
		return response["token"]

	def session_get_key(self, token):
		if debug: pympe.debug("Getting session key")
		response = self.send_get("auth.getsession", True, token=token)
		if not response or not "session" in response: return (False, False)
		self.userName = response["session"]["name"]
		self.sessionKey = response["session"]["key"]
		if debug: pympe.debug("Session key: "+self.sessionKey)
		return (self.userName, self.sessionKey)

########################
	def update_now_playing(self, artist, track, album=None, albumArtist=None, duration=None, mbid=None):
		args = {"artist":artist, "track":track}
		if album:       args["album"]       = album
		if albumArtist: args["albumArtist"] = albumArtist
		if duration:    args["duration"]    = duration
		if mbid:        args["mbid"]        = mbid
		self.send_post("track.updatenowplaying", True, **args)

	def scrobble_result(self, result, *args):
		try:
			json.loads(result)
		except ValueError:
			print 'Value Error on scrobble_result json.loads'
			print result

	def scrobble_track(self, artist, track, album=None, albumArtist=None, duration=None, mbid=None, streamId=None, streamAuth=None):
		timestamp = int(time.time()-duration)
		args = {"timestamp":timestamp, "artist":artist, "track":track}
		if album:       args["album"]       = album
		if albumArtist: args["albumArtist"] = albumArtist
		if duration:    args["duration"]    = duration
		if mbid:        args["mbid"]        = mbid
		if streamId:    args["streamID"]    = streamId

		if self.backlog:
			self.update_backlog(self.backlog+[args])
			self.submit_backlog()
		else:
			self.send_post("track.scrobble", True, **args)
			#response = self.send_post("track.scrobble", True, **args)
			#if not response: self.backlog.append(args)

	def add_to_backlog(self, result, *args):
		#print 'Failed to submit scrobble, adding to backlog'
		self.backlog.append(args[0])

	def scrobble_batch(self, scrobbles):
		args = {}
		for index, scrobble in enumerate(scrobbles):
			args.update(dict(("%s[%s]"%(k, index), v) for (k, v) in scrobble.iteritems()))

		self.send_post("track.scrobble", True, **args)

	def love_track(self, artist, track):
		return self.send_post("track.love", True, track=track, artist=artist)

	def ban_track(self, artist, track):
		return self.send_post("track.ban", True, track=track, artist=artist)

	def get_info_artist(self, artist, finishedCallback, errorCallback):
		return self.send_get_threaded("artist.getInfo", False, finishedCallback, errorCallback, artist=artist, username=self.userName)

	def get_info_album(self, artist, album, finishedCallback, errorCallback):
		return self.send_get_threaded("album.getInfo", False, finishedCallback, errorCallback, artist=artist, album=album, username=self.userName)

	def get_info_track(self, artist, track, finishedCallback, errorCallback):
		return self.send_get_threaded("track.getInfo", False, finishedCallback, errorCallback, artist=artist, track=track, username=self.userName)

	def update_backlog(self, newBacklog):
		self.backlog = newBacklog
		with open('failedscrobbles.log', 'w') as f: f.write(pickle.dumps(self.backlog))

	def submit_backlog(self):
		self.scrobble_batch(self.backlog[:50])
		self.update_backlog(self.backlog[50:])
