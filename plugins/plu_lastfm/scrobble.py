import pympe, pm_helpers, time

class Scrobbler:
	def __init__(self, plugin):
		self.plugin = plugin
		self.submittedScrobble = None
		self.submittedNowPlaying = None

	def start(self):
		self.check_for_scrobble()
		self.scrobbleTimer = pympe.mainloop.add_timer(5, -1, self.check_for_scrobble)

	def stop(self):
		pympe.mainloop.remove_timer(self.scrobbleTimer)

	def check_for_scrobble(self):
		playTime = pympe.player.play_time()

		if self.plugin.cfg_get('current') and self.submittedNowPlaying != pympe.player.currentFile and playTime >= 15:
			if self.update_now_playing(pympe.player.currentFile):
				self.submittedNowPlaying = pympe.player.currentFile

		if self.plugin.cfg_get('scrobble') and self.submittedScrobble != pympe.player.currentFile and \
			pympe.player.length >= 30 and (playTime >= 240 or playTime >= pympe.player.length*.5):
				if self.scrobble_track(pympe.player.currentFile):
					self.submittedScrobble = pympe.player.currentFile

	def update_now_playing(self, t):
		#print 'Updating Last.fm Now Playing',t['title'],'by',t['artist']
		if not self.plugin.cfg_get('current'): return True
		if not t['title'] or not t['artist']: return False
		album = t["album"][0] if t["album"] else None
		albumartist = t["albumartist"][0] if t["albumartist"] else None
		self.plugin.fm.update_now_playing(t["artist"][0], t["title"][0], album, albumartist, int(t["length"]), None)
		return True

	def scrobble_track(self, t):
		#print 'Scrobbling Last.fm Track',t['title'],'by',t['artist']
		if not self.plugin.cfg_get('scrobble'): return True
		if not t['title'] or not t['artist']: return False
		album = t["album"][0] if t["album"] else None
		albumartist = t["albumartist"][0] if t["albumartist"] else None
		self.plugin.fm.scrobble_track(t["artist"][0], t["title"][0], album, albumartist, int(pympe.player.length))
		return True
