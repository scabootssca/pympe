# -*- coding: utf-8 -*-

# Import the lastfm module
from plugins.plu_lastfm import lastfm
from plugins.plu_lastfm import context
from plugins.plu_lastfm import scrobble
reload(context)
reload(lastfm)
reload(scrobble)

# Other modules
import pympe, wx, time, pm_helpers

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, "Last.fm Settings")

		# Save the plugin instance that spawned this
		self.plugin = plugin

		# Create the interface elements
		self.uiButtonAuthorize = wx.Button(self, -1, ("Authorize Pympe" if not cfg_get("user_name") else "Authorized by %s"%cfg_get("user_name")))
		uiCheckScrobble = wx.CheckBox(self, -1, "Scrobble Tracks")
		uiCheckCurrent = wx.CheckBox(self, -1, "Send Currently Playing Track To Last.fm")
		uiCheckLove = wx.CheckBox(self, -1, "Show The Love/Hate Buttons")
		uiCheckInfo = wx.CheckBox(self, -1, "Fetch Track Information")
		uiButtonOk = wx.Button(self, wx.ID_OK)
		uiButtonCancel = wx.Button(self, wx.ID_CANCEL)

		# Bind them
		self.uiButtonAuthorize.Bind(wx.EVT_BUTTON, self.event_button_authorize)

		# Fit them into the interface
		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.Add(self.uiButtonAuthorize, 0, wx.EXPAND)
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.ALL, 10)
		sizer.Add(uiCheckScrobble, 0, wx.EXPAND)
		sizer.Add(uiCheckCurrent, 0, wx.EXPAND)
		sizer.Add(uiCheckLove, 0, wx.EXPAND)
		sizer.Add(uiCheckInfo, 0, wx.EXPAND)
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.ALL, 10)

		s2 = wx.BoxSizer(wx.HORIZONTAL)
		s2.Add(uiButtonOk, 0, wx.EXPAND)
		s2.AddStretchSpacer()
		s2.Add(uiButtonCancel, 0, wx.EXPAND)
		sizer.Add(s2, 0, wx.EXPAND)

		# Add the sizer into the dialog with a space
		box = wx.BoxSizer()
		box.Add(sizer, 0, wx.ALL, 10)
		self.SetSizerAndFit(box)

		# Set the interface to match the settings
		if cfg_get("session_key"): self.uiButtonAuthorize.Disable()
		if cfg_get("scrobble"): uiCheckScrobble.SetValue(True)
		if cfg_get("current"): uiCheckCurrent.SetValue(True)
		if cfg_get("love_hate"): uiCheckLove.SetValue(True)
		if cfg_get("fetch_info"): uiCheckInfo.SetValue(True)

		# Show the dialog
		result = self.ShowModal()
		if result == wx.ID_CANCEL: return

		if uiCheckInfo.IsChecked() and not cfg_get('fetch_info'): self.plugin.context.start()
		elif not uiCheckInfo.IsChecked() and cfg_get('fetch_info'): self.plugin.context.stop()

		# Show or hide the buttons if needed
		if _config['love_hate'] != uiCheckLove.IsChecked(): plugin.show_love_hate(True if uiCheckLove.IsChecked() else False)

		# Save the new settings
		cfg_set("scrobble", uiCheckScrobble.IsChecked())
		cfg_set("current", uiCheckCurrent.IsChecked())
		cfg_set("love_hate", uiCheckLove.IsChecked())
		cfg_set('fetch_info', uiCheckInfo.IsChecked())
		pympe.config.save()

	def event_button_authorize(self, event):
		# Send for the token
		token = self.plugin.fm.session_auth()

		# To continue
		if token == False:
			dial = wx.MessageDialog(pympe.ui.uiMain, "There was an error fetching the auth token.", "Close")
			dial.ShowModal()
		else:
			dial = wx.MessageDialog(pympe.ui.uiMain, "Click ok when you have authorized the program in your web browser.", "Continue Authorization")
			dial.ShowModal()

			# Save the session key and disable the button
			(userName, sessionKey) = self.plugin.fm.session_get_key(token)
			if sessionKey == False: return False
			cfg_set("user_name", userName)
			cfg_set("session_key", sessionKey)
			self.uiButtonAuthorize.Disable()

class Plugin:
	def plu_start(self):
		# The last.fm object handle
		self.fm = lastfm.LastFM()
		self.fm.sessionKey = cfg_get("session_key")
		self.fm.userName = cfg_get("user_name")

		self.cfg_get = cfg_get
		self.cfg_set = cfg_set

		self.sizer = None
		self.waitingForTags = False

		self.context = context.ContextTab(self)
		if cfg_get('fetch_info'): self.context.start()

		self.scrobbler = scrobble.Scrobbler(self)
		self.scrobbler.start()


		self.userLoved = False
		if cfg_get('love_hate'): self.show_love_hate()

		self.running = True

		subscribe(pympe.events.player_current_changed, self.mpevent_player_current_changed)
		subscribe(pympe.events.player_current_updated, self.mpevent_player_current_updated)

	def plu_stop(self, shutdown):
		if not shutdown:
			self.context.stop()
			self.scrobbler.stop()
			self.show_love_hate(False)
		self.running = False

	def plu_settings(self, parent):
		SettingsDialog(self, parent)

	def mpevent_player_current_updated(self, event, tags):
		self.context.mpevent_player_current_updated(event, tags)

	def mpevent_player_current_changed(self, event, currentInfo, currentLibraryId, currentLibrary):
		self.context.mpevent_player_current_changed(event, currentInfo, currentLibraryId, currentLibrary)
		self.userLoved = False

	def set_loved(self, value):
		self.userLoved = value
		if self.sizer:
			if value: self.uiButtonLove.Disable()
			else: self.uiButtonLove.Enable()

	def show_love_hate(self, value=True):
		if value:
			# Love hate
			self.uiButtonLove = wx.Button(pympe.ui.uiMain, 0, "♥", size=(-1, 23), style=wx.BU_EXACTFIT)
			self.uiButtonHate = wx.Button(pympe.ui.uiMain, 1, "✖", size=(-1, 23), style=wx.BU_EXACTFIT)

			self.uiButtonLove.Bind(wx.EVT_BUTTON, self.event_button)
			self.uiButtonHate.Bind(wx.EVT_BUTTON, self.event_button)

			if self.userLoved: self.uiButtonLove.Disable()
			self.uiButtonHate.Disable()

			self.sizer = wx.BoxSizer(wx.VERTICAL)
			self.sizer.Add(self.uiButtonLove, 0, wx.EXPAND)
			self.sizer.Add(self.uiButtonHate, 0, wx.EXPAND)
			pympe.ui.uiMain.uiSizerExtras.Add(self.sizer)
			pympe.ui.uiMain.uiSizerMain.Layout()
		elif self.sizer:
			self.uiButtonLove.Destroy()
			self.uiButtonHate.Destroy()
			self.sizer.Clear()
			pympe.ui.uiMain.uiSizerExtras.Remove(self.sizer)
			self.sizer = self.uiButtonLove = self.uiButtonHate = None
			pympe.ui.uiMain.uiSizerMain.Layout()

	def event_button(self, event):
		if event.Id == 0:
			if self.fm.love_track(pympe.player.currentFile["artist"][0], pympe.player.currentFile["title"][0]) == False:
				wx.MessageDialog(pympe.ui.uiMain, 'There was a problem connecting to the last.fm servers please try again later.', style=wx.OK).ShowModal()
			self.uiButtonLove.Disable()
		else:
			self.fm.ban_track(pympe.player.currentFile["artist"][0], pympe.player.currentFile["title"][0])
			self.uiButtonHate.Disable()
