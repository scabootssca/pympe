import pympe, wx, pm_helpers, re, wx.animate, pm_network, os
import wx.lib.scrolledpanel as scrolled
from hashlib import md5

class PanelLoading(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)

		gauge = wx.Gauge(self, -1, size=(-1, 10))
		label = wx.StaticText(self, -1, 'Loading...')
		label.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))

		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.AddStretchSpacer()
		sizer.Add(gauge, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		sizer.Add(label, 0, wx.ALIGN_CENTER)
		sizer.AddStretchSpacer()

		self.SetSizer(sizer)

		self.Bind(wx.EVT_TIMER, lambda x: gauge.Pulse())
		self.timer = wx.Timer(self)

	def Show(self):
		self.timer.Start(70)
		wx.Panel.Show(self)

	def Hide(self):
		self.timer.Stop()
		wx.Panel.Hide(self)

class PanelError(wx.Panel):
	def __init__(self, parent, retryFunction=None):
		wx.Panel.__init__(self, parent)

		image = pm_helpers.PmImage(stock="gtk-dialog-error", pil=False)
		image.set_mask_color(0, 0, 0)
		bitmap = wx.StaticBitmap(self, bitmap=image.scaled(32, 32).convert_to_wx_bitmap())
		label = wx.StaticText(self, -1, 'There has been an error.')
		label.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		self.label2 = wx.StaticText(self, -1, '')
		self.label2.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		if retryFunction:
			button = wx.Button(self, -1, 'Retry')
			button.Bind(wx.EVT_BUTTON, lambda event: retryFunction())

		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.AddStretchSpacer()
		sizer.Add(bitmap, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		sizer.Add(label, 0, wx.ALIGN_CENTER)
		sizer.Add(self.label2, 0, wx.ALIGN_CENTER)
		if retryFunction:
			sizer.Add(button, 0, wx.ALIGN_CENTER|wx.TOP, 5)

		sizer.AddStretchSpacer()

		self.SetSizer(sizer)

	def set_message(self, message):
		if message != None:
			self.label2.Show()
			self.label2.SetLabel(message)
		else:
			self.label2.Hide()
		self.Layout()

class PanelIdle(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)

		image = pm_helpers.PmImage(stock="gtk-media-play", pil=False)
		image.set_mask_color(0, 0, 0)
		bitmap = wx.StaticBitmap(self, bitmap=image.scaled(32, 32).convert_to_wx_bitmap())
		label = wx.StaticText(self, -1, 'Play a track to fetch information.')
		label.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))

		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.AddStretchSpacer()
		sizer.Add(bitmap, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		sizer.Add(label, 0, wx.ALIGN_CENTER)

		sizer.AddStretchSpacer()

		self.SetSizer(sizer)


class LoadedPanel(scrolled.ScrolledPanel):
	def __init__(self, parent):
		scrolled.ScrolledPanel.__init__(self, parent)
		self.SetupScrolling()

class LoadablePanel(wx.Panel):
	def __init__(self, context, parent):
		wx.Panel.__init__(self, parent)
		self.context = context
		self.parent = parent

		self.uiPanelIdle    = PanelIdle(self)
		self.uiPanelLoading = PanelLoading(self)
		self.uiPanelLoaded  = self.layoutLoadedPanel()
		self.uiPanelError   = PanelError(self)

		self.uiPanelIdle.Show()
		self.uiPanelLoading.Hide()
		self.uiPanelLoaded.Hide()
		self.uiPanelError.Hide()


		sizer = wx.BoxSizer()
		sizer.Add(self.uiPanelIdle, 1, wx.EXPAND)
		sizer.Add(self.uiPanelLoading, 1, wx.EXPAND)
		sizer.Add(self.uiPanelLoaded, 1, wx.EXPAND)
		sizer.Add(self.uiPanelError, 1, wx.EXPAND)
		self.SetSizerAndFit(sizer)

	def showLoaded(self):
		self.uiPanelIdle.Hide()
		self.uiPanelLoading.Hide()
		self.uiPanelLoaded.Show()
		self.uiPanelError.Hide()
		self.Layout()

	def showLoading(self):
		self.uiPanelIdle.Hide()
		self.uiPanelLoading.Show()
		self.uiPanelLoaded.Hide()
		self.uiPanelError.Hide()
		self.Layout()

	def showError(self, *args, **kwargs):
		self.uiPanelIdle.Hide()
		self.uiPanelLoading.Hide()
		self.uiPanelLoaded.Hide()
		self.uiPanelError.Show()
		self.Layout()

class ArtistPanel(LoadablePanel):
	def layoutLoadedPanel(self):
		panel = LoadedPanel(self)

		self.sizer = wx.BoxSizer(wx.VERTICAL)

		self.uiLabelTitle = wx.StaticText(panel, -1)
		self.uiLabelTitle.SetFont(wx.Font(15, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		self.uiLabelInfo = wx.StaticText(panel, -1)
		self.uiLabelInfo.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		self.uiLabelTags = wx.StaticText(panel, -1)
		self.uiLabelTags.SetFont(wx.Font(7, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))

		self.sizer.Add(self.uiLabelTitle, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		self.sizer.Add(self.uiLabelInfo, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		self.sizer.Add(self.uiLabelTags, 0, wx.ALIGN_CENTER|wx.BOTTOM, 10)
		self.sizer.Add(wx.StaticLine(panel, style=wx.LI_HORIZONTAL), 0, wx.ALIGN_CENTER|wx.EXPAND|wx.BOTTOM, 15)

		sizerLower = wx.GridBagSizer(5, 5)
		self.sizer.Add(sizerLower, 1, wx.EXPAND|wx.ALIGN_CENTER)

		sizerLower.AddStretchSpacer((0, 0))
		sizerLower.AddStretchSpacer((0, 3))

		sizerSimilar = wx.GridBagSizer(5, 5)
		sizerLower.Add(sizerSimilar, (0, 2))

		self.similarArtists = []
		fontSimilar = wx.Font(9, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

		for x in range(5):
			label = wx.StaticText(panel, -1, '')
			label.SetFont(fontSimilar)
			pic = wx.StaticBitmap(panel, -1)
			sizerSimilar.Add(pic, (x, 0))
			sizerSimilar.Add(label, (x, 1), flag=wx.ALIGN_CENTER_VERTICAL)
			self.similarArtists.append((label, pic))

		sizerInfo = wx.BoxSizer(wx.VERTICAL)
		sizerLower.Add(sizerInfo, (1, 1), (1, 2), wx.EXPAND)

		self.uiBitmap = wx.StaticBitmap(panel, -1)
		sizerLower.Add(self.uiBitmap, (0, 1), (1, 1), wx.RIGHT, 15)

		self.uiLabelWikiHeader = wx.StaticText(panel, -1, 'Artist Wiki:')
		self.uiLabelWikiHeader.SetFont(wx.Font(9, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		sizerInfo.Add(self.uiLabelWikiHeader, 0, wx.ALIGN_CENTER|wx.TOP, 10)
		self.uiLabelWikiPublished = wx.StaticText(panel, -1, '')
		self.uiLabelWikiPublished.SetFont(wx.Font(6, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		sizerInfo.Add(self.uiLabelWikiPublished, 0, wx.ALIGN_CENTER|wx.TOP, 5)
		self.uiLineWikiHeader = wx.StaticLine(panel, style=wx.LI_HORIZONTAL)
		sizerInfo.Add(self.uiLineWikiHeader, 0, wx.EXPAND|wx.BOTTOM|wx.TOP, 10)

		self.uiLabelWikiContent = wx.StaticText(panel, -1, '', style=wx.TE_MULTILINE)
		sizerInfo.Add(self.uiLabelWikiContent, 0, wx.ALIGN_CENTER)

		sizerLower.AddGrowableCol(0)
		sizerLower.AddGrowableCol(3)

		box = wx.BoxSizer()
		box.Add(self.sizer, 1, wx.EXPAND|wx.ALL, 10)
		panel.SetSizerAndFit(box)

		return panel

	@pm_helpers.unthread
	def parseResponse(self, response):
		if not response or not 'artist' in response:
			return self.showError(debug='No Response Or No Artist In Response')

		response = response['artist']
		if not response:
			return self.showError(debug='No Response [Artist]')

		def update_pic(imagePath):
			self.uiBitmap.SetBitmap(pm_helpers.PmImage(imagePath).convert_to_wx_bitmap())
			self.Layout()

		self.uiBitmap.SetBitmap(pm_helpers.PmImage(pympe.cwd+os.sep+'images/1x1filler.png').scaled(252, 300).convert_to_wx_bitmap())
		if response['image'][3]['#text']:
			pm_network.get_url_threaded(response['image'][3]['#text'], finishedCallback=update_pic)

		self.uiLabelTitle.SetLabel(response['name'].replace("&", "&&"))

		labelInfo = '{:,} Listeners | {:,} Plays'.format(int(response['stats']['listeners']), int(response['stats']['playcount']))
		if 'userplaycount' in response['stats']: labelInfo += ' | %s User Plays'%response['stats']['userplaycount']
		self.uiLabelInfo.SetLabel(labelInfo.replace("&", "&&"))

		if 'tags' in response:
			if type(response['tags']) == type({}):
				if type(response['tags']['tag']) != type([]): response['tags']['tag'] = [response['tags']['tag']]
				self.uiLabelTags.SetLabel(' | '.join([' '.join([x.capitalize() for x in tag['name'].split()]) for tag in response['tags']['tag']]).replace("&", "&&"))

		if not response['bio']['published'].strip():
			self.uiLineWikiHeader.Show(False)
			self.uiLabelWikiHeader.Show(False)
		else:
			self.uiLineWikiHeader.Show(True)
			self.uiLabelWikiHeader.Show(True)
			self.uiLabelWikiPublished.SetLabel(response['bio']['published'])
			self.uiLabelWikiContent.SetLabel(pm_helpers.unencode(re.compile(r'<.*?>').sub("", response['bio']['content'])).replace("&", "&&"))
			self.uiLabelWikiContent.Wrap(450)

		qq = 'cache/'+md5(response['image'][3]['#text'].replace('\/', '/')).hexdigest()

		if hasattr(response['similar'], 'keys'):
			if type(response['similar']['artist']) != type([]): response['similar']['artist'] = [response['similar']['artist']]
			for i, x in enumerate(response['similar']['artist']):
				self.similarArtists[i][0].Show()
				self.similarArtists[i][0].SetLabel(x['name'].replace("&", "&&"))

				def update_pic_finished(imagePath, index):
					self.similarArtists[index][1].SetBitmap(pm_helpers.PmImage(imagePath).convert_to_wx_bitmap())
					self.Layout()
				def update_pic_failure(result, index):
					image = pm_helpers.PmImage(stock="gtk-dialog-error", pil=False)
					image.set_mask_color(0, 0, 0)
					self.similarArtists[index][1].SetBitmap(bitmap=image.scaled(34, 34).convert_to_wx_bitmap())
					self.Layout()
				self.similarArtists[i][1].Show()
				self.similarArtists[i][1].SetBitmap(pm_helpers.PmImage(pympe.cwd+os.sep+'images/1x1filler.png').scaled(34, 34).convert_to_wx_bitmap())
				if x['image'][0]['#text']:
					pm_network.get_url_threaded(x['image'][0]['#text'], finishedCallback=update_pic_finished, failureCallback=update_pic_failure, callbackArgs=[i])
				else:
					update_pic_failure('', i)
		else:
			self.similarArtists[0][0].Hide()
			self.similarArtists[1][0].Hide()
			self.similarArtists[2][0].Hide()
			self.similarArtists[3][0].Hide()
			self.similarArtists[4][0].Hide()
			self.similarArtists[0][1].Hide()
			self.similarArtists[1][1].Hide()
			self.similarArtists[2][1].Hide()
			self.similarArtists[3][1].Hide()
			self.similarArtists[4][1].Hide()

		self.Layout()
		self.showLoaded()


	def update(self):
		self.showLoading()
		self.context.parent.fm.get_info_artist(self.context.currentArtist, self.parseResponse, self.showError)

class AlbumPanel(LoadablePanel):
	def layoutLoadedPanel(self):
		panel = LoadedPanel(self)

		self.sizer = wx.BoxSizer(wx.VERTICAL)

		self.uiLabelTitle = wx.StaticText(panel, -1)
		self.uiLabelTitle.SetFont(wx.Font(15, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		self.uiLabelInfo = wx.StaticText(panel, -1)
		self.uiLabelInfo.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		self.uiLabelTags = wx.StaticText(panel, -1)
		self.uiLabelTags.SetFont(wx.Font(7, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))

		self.sizer.Add(self.uiLabelTitle, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		self.sizer.Add(self.uiLabelInfo, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		self.sizer.Add(self.uiLabelTags, 0, wx.ALIGN_CENTER|wx.BOTTOM, 10)
		self.sizer.Add(wx.StaticLine(panel, style=wx.LI_HORIZONTAL), 0, wx.ALIGN_CENTER|wx.EXPAND|wx.BOTTOM, 15)


		sizerInfo   = wx.BoxSizer(wx.VERTICAL)
		self.sizerTracks = wx.GridBagSizer(5, 5)
		sizerLower  = wx.BoxSizer(wx.HORIZONTAL)

		sizerLower.AddStretchSpacer()
		sizerLower.Add(sizerInfo)
		sizerLower.Add((10, -1))
		sizerLower.Add(self.sizerTracks)
		sizerLower.AddStretchSpacer()
		self.sizer.Add(sizerLower, 1, wx.EXPAND)

		self.uiBitmap = wx.StaticBitmap(panel, -1)
		sizerInfo.Add(self.uiBitmap)

		self.uiLabelWiki = wx.StaticText(panel, -1, 'Album Wiki:')
		self.uiLabelWiki.SetFont(wx.Font(9, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		sizerInfo.Add(self.uiLabelWiki, 0, wx.ALIGN_CENTER|wx.TOP, 10)
		self.uiLabelWikiPublished = wx.StaticText(panel, -1, '')
		self.uiLabelWikiPublished.SetFont(wx.Font(6, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		sizerInfo.Add(self.uiLabelWikiPublished, 0, wx.ALIGN_CENTER|wx.TOP, 5)
		self.uiLineWiki = wx.StaticLine(panel, style=wx.LI_HORIZONTAL)
		sizerInfo.Add(self.uiLineWiki, 0, wx.EXPAND|wx.BOTTOM|wx.TOP, 10)

		self.uiLabelWikiContent = wx.StaticText(panel, -1, '', style=wx.TE_MULTILINE)
		sizerInfo.Add(self.uiLabelWikiContent, 1, wx.EXPAND)

		# Top Tags
		self.bitmapPlay = pm_helpers.PmImage(pil=False, stock="gtk-media-play", stockSize=(24, 24)).convert_to_wx_bitmap()
		self.fontTracks = wx.Font(9, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)

		box = wx.BoxSizer()
		box.Add(self.sizer, 1, wx.EXPAND|wx.ALL, 10)
		panel.SetSizerAndFit(box)

		return panel

	@pm_helpers.unthread
	def parseResponse(self, response):
		if not response or not 'album' in response:
			return self.showError(debug='No Response Or No Album In Response')#retryFunction=self.reloadPane)

		response = response['album']
		if not response:
			return self.showError(debug='No Response [Album]')#retryFunction=self.reloadPane)

		def update_album_pic(imagePath):
			self.uiBitmap.SetBitmap(pm_helpers.PmImage(imagePath).scaled(300, 300).convert_to_wx_bitmap())
			self.Layout()
		self.uiBitmap.SetBitmap(pm_helpers.PmImage(pympe.cwd+os.sep+'images/1x1filler.png').scaled(300, 300).convert_to_wx_bitmap())
		albumImageUrl = response['image'][3]['#text']
		if albumImageUrl:
			pm_network.get_url_threaded(albumImageUrl, finishedCallback=update_album_pic)

		self.uiLabelTitle.SetLabel(response['name'].replace("&", "&&"))

		labelInfo = '{:,} Listeners | {:,} Plays'.format(int(response['listeners']), int(response['playcount']))
		if 'releasedate' in response and response['releasedate'].strip(): labelInfo += ' | Released In: '+response['releasedate'][-11:-7]
		self.uiLabelInfo.SetLabel(labelInfo.replace("&", "&&"))

		if 'toptags' in response and 'tag' in response['toptags']:
			if type(response['toptags']['tag']) != type([]): response['toptags']['tag'] = [response['toptags']['tag']]
			self.uiLabelTags.SetLabel(' | '.join([' '.join([x.capitalize() for x in tag['name'].split()]) for tag in response['toptags']['tag']]).replace("&", "&&"))

		if not 'wiki' in response:
			self.uiLabelWikiPublished.Hide()
			self.uiLabelWikiContent.Hide()
			self.uiLineWiki.Hide()
			self.uiLabelWiki.Hide()
		else:
			self.uiLabelWikiPublished.Show()
			self.uiLabelWikiContent.Show()
			self.uiLineWiki.Show()
			self.uiLabelWiki.Show()
			self.uiLabelWikiPublished.SetLabel(response['wiki']['published'])
			self.uiLabelWikiContent.SetLabel(pm_helpers.unencode(re.compile(r'<.*?>').sub("", response['wiki']['content'])).replace('&', '&&'))
			self.uiLabelWikiContent.Wrap(300)

		self.sizerTracks.Clear(True)
		if 'track' in response['tracks'] and type(response['tracks']) == dict:
			if type(response['tracks']['track']) != list: response['tracks']['track'] = [response['tracks']['track']]
			for i, track in enumerate(sorted(response['tracks']['track'], key=lambda x: int(x['@attr']['rank']))):
				label = wx.StaticText(self, -1, '%i. %s (%s)'%(i+1, track['name'], pm_helpers.make_display_time(track['duration'], False, False)))
				label.SetFont(self.fontTracks)

				uiButton = wx.BitmapButton(self, -1, self.bitmapPlay, size=(26, 26))
				if track['streamable']['#text'] != '1': uiButton.Disable()

				self.sizerTracks.Add(uiButton, (i, 0))
				self.sizerTracks.Add(label, (i, 1), flag=wx.ALIGN_CENTER_VERTICAL)


		self.Layout()
		self.showLoaded()


	def update(self):
		self.showLoading()
		self.context.parent.fm.get_info_album(self.context.currentArtist, self.context.currentAlbum, self.parseResponse, self.showError)


class TrackPanel(LoadablePanel):
	def layoutLoadedPanel(self):
		panel = LoadedPanel(self)

		self.sizer = wx.BoxSizer(wx.VERTICAL)

		self.uiLabelTitle = wx.StaticText(panel, -1)
		self.uiLabelTitle.SetFont(wx.Font(15, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		self.uiLabelInfo = wx.StaticText(panel, -1)
		self.uiLabelInfo.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		self.uiLabelTags = wx.StaticText(panel, -1)
		self.uiLabelTags.SetFont(wx.Font(7, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))

		self.sizer.Add(self.uiLabelTitle, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		self.sizer.Add(self.uiLabelInfo, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		self.sizer.Add(self.uiLabelTags, 0, wx.ALIGN_CENTER|wx.BOTTOM, 10)
		self.sizer.Add(wx.StaticLine(panel, style=wx.LI_HORIZONTAL), 0, wx.ALIGN_CENTER|wx.EXPAND)

		sizerInfo   = wx.BoxSizer(wx.VERTICAL)
		sizerLower  = wx.BoxSizer(wx.HORIZONTAL)

		sizerLower.AddStretchSpacer()
		sizerLower.Add(sizerInfo)
		sizerLower.AddStretchSpacer()
		self.sizer.Add(sizerLower, 1, wx.EXPAND)

		self.uiLabelWiki = wx.StaticText(panel, -1, 'Track Wiki:')
		self.uiLabelWiki.SetFont(wx.Font(9, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		sizerInfo.Add(self.uiLabelWiki, 0, wx.ALIGN_CENTER|wx.TOP, 10)
		self.uiLabelWikiPublished = wx.StaticText(panel, -1, '')
		self.uiLabelWikiPublished.SetFont(wx.Font(6, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		sizerInfo.Add(self.uiLabelWikiPublished, 0, wx.ALIGN_CENTER|wx.TOP, 5)
		self.uiLineWiki = wx.StaticLine(panel, style=wx.LI_HORIZONTAL)
		sizerInfo.Add(self.uiLineWiki, 0, wx.EXPAND|wx.BOTTOM|wx.TOP, 10)

		self.uiLabelWikiContent = wx.StaticText(panel, -1, '', style=wx.TE_MULTILINE)
		sizerInfo.Add(self.uiLabelWikiContent, 1, wx.EXPAND)

		box = wx.BoxSizer()
		box.Add(self.sizer, 1, wx.EXPAND|wx.ALL, 10)
		panel.SetSizerAndFit(box)

		return panel

	@pm_helpers.unthread
	def parseResponse(self, response):
		if not response or not 'track' in response:
			return self.showError(debug='No Response Or No Track In Response')#retryFunction=self.reloadPane)

		response = response['track']
		if not response:
			return self.showError(debug='No Response [Track]')#retryFunction=self.reloadPane)

		labelTitle = '%s (%s)'%(response['name'], pm_helpers.make_display_time(int(response['duration'])/1000, False, False))
		if 'album' in response: labelTitle = ('%s. '%response['album']['@attr']['position'])+labelTitle
		self.uiLabelTitle.SetLabel(labelTitle.replace("&", "&&"))

		labelInfo = '{:,} Listeners | {:,} Plays'.format(int(response['listeners']), int(response['playcount']))
		if 'userplaycount' in response: labelInfo += ' | {:,} User Plays'.format(int(response['userplaycount']))
		self.uiLabelInfo.SetLabel(labelInfo.replace("&", "&&"))

		if 'tag' in response['toptags']:
			if type(response['toptags']['tag']) != type([]): response['toptags']['tag'] = [response['toptags']['tag']]
			self.uiLabelTags.SetLabel(' | '.join([' '.join([x.capitalize() for x in tag['name'].split()]) for tag in response['toptags']['tag']]).replace("&", "&&"))

		if not 'wiki' in response:
			self.uiLabelWikiPublished.Hide()
			self.uiLabelWikiContent.Hide()
			self.uiLineWiki.Hide()
			self.uiLabelWiki.Hide()
		else:
			self.uiLabelWikiPublished.Show()
			self.uiLabelWikiContent.Show()
			self.uiLineWiki.Show()
			self.uiLabelWiki.Show()
			self.uiLabelWikiPublished.SetLabel(response['wiki']['published'])
			self.uiLabelWikiContent.SetLabel(pm_helpers.unencode(re.compile(r'<.*?>').sub("", response['wiki']['content'])).replace("&", "&&"))
			self.uiLabelWikiContent.Wrap(300)

		if 'userloved' in response:
			self.context.parent.set_loved(True if response['userloved'] == '1' else False)

		self.Layout()
		self.showLoaded()

	def update(self):
		self.showLoading()
		self.context.parent.fm.get_info_track(self.context.currentArtist, self.context.currentTitle, self.parseResponse, self.showError)


class ContextTab:
	def __init__(self, parent, left=True):
		if left: self.tabs = pympe.ui.uiMain.uiTabsLeft
		else: self.tabs = pympe.ui.uiMain.uiTabsRight
		self.parent = parent
		self.uiPanelMain = None
		self.running = False

	def selected_by_notebook(self):
		pass

	def start(self):
		self.running = True
		self.uiPanelMain = wx.Panel(self.tabs)

		self.currentArtist = self.currentAlbum = self.currentTrack = None

		box = wx.BoxSizer()
		notebook = wx.Notebook(self.uiPanelMain, style=wx.NB_TOP)
		box.Add(notebook, 1, wx.EXPAND|wx.ALL, 10)
		self.uiPanelMain.SetSizer(box)

		self.artistPanel = ArtistPanel(self, notebook)
		self.albumPanel = AlbumPanel(self, notebook)
		self.trackPanel = TrackPanel(self, notebook)

		notebook.AddPage(self.artistPanel, "Artist")
		notebook.AddPage(self.albumPanel, "Album")
		notebook.AddPage(self.trackPanel, "Track")

		self.tabs.InsertPage(1, self.uiPanelMain, "Information")
		self.tabs.SetSelection(1)

		if pympe.player.playing:
			self.mpevent_player_current_changed(None, pympe.player.currentInfo, pympe.player.currentId, pympe.player.currentLibrary)

	def stop(self):
		self.running = False
		if not self.uiPanelMain: return

		# Remove the panel
		for index in range(self.tabs.GetPageCount()):
			if self.tabs.GetPage(index) == self.uiPanelMain:
				self.tabs.RemovePage(index)
				break

	def mpevent_player_current_changed(self, event, currentInfo, currentLibraryId, currentLibrary):
		if not self.running: return
		if currentInfo['artist']: self.currentArtist = currentInfo['artist'][0]
		if currentInfo['album']:  self.currentAlbum  = currentInfo['album'][0]
		if currentInfo['title']:  self.currentTitle  = currentInfo['title'][0]

		if self.currentArtist:
			self.artistPanel.update()

			if self.currentAlbum: self.albumPanel.update()
			if self.currentTitle: self.trackPanel.update()

	def mpevent_player_current_updated(self, event, tags):
		if not self.running: return
		relevantUpdate = False
		if 'artist' in tags and 'artist' in pympe.player.currentInfo and pympe.player.currentInfo['artist'] and pympe.player.currentInfo['artist'][0] != self.currentArtist:
			self.currentArtist = pympe.player.currentInfo['artist'][0]
			relevantUpdate = True
			self.artistPanel.update()

		if 'album' in tags and 'album' in pympe.player.currentInfo and pympe.player.currentInfo['album'] and pympe.player.currentInfo['album'][0] != self.currentAlbum:
			relevantUpdate = True
			self.currentAlbum = pympe.player.currentInfo['album'][0]
			if self.currentArtist: self.albumPanel.update()

		if 'title' in tags and 'title' in pympe.player.currentInfo and pympe.player.currentInfo['title'] and pympe.player.currentInfo['title'][0] != self.currentTitle:
			relevantUpdate = True
			self.currentTitle = pympe.player.currentInfo['title'][0]
			if self.currentArtist: self.trackPanel.update()
