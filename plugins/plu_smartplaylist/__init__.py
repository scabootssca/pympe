# -*- coding: utf-8 -*-
import pympe, wx, pm_language, pm_helpers, pm_mediafile, os, time, random

def text_entry(parent, index):
	choice = wx.Choice(parent, -1, choices=[x[1] for x in parent.tagChoices])
	modifier = wx.Choice(parent, -1, choices=['contains', 'doesn\'t contain', 'is', 'is not', 'starts with', 'ends with'])
	value = wx.TextCtrl(parent, -1)
	plus  = wx.Button(parent, -1, "+", style=wx.BU_EXACTFIT|wx.NO_BORDER)
	minus = wx.Button(parent, -1, "-", style=wx.BU_EXACTFIT|wx.NO_BORDER)
	
	choice.Bind(wx.EVT_CHOICE, parent.event_choice_tag)
	plus.Bind(wx.EVT_BUTTON, parent.event_button_plus)
	minus.Bind(wx.EVT_BUTTON, parent.event_button_minus)
	
	choice.index = index
	plus.index = index
	minus.index = index
	
	sizer = wx.BoxSizer(wx.HORIZONTAL)
	sizer.Add(choice)
	sizer.Add(modifier)
	sizer.Add(value, 1, wx.EXPAND)
	sizer.Add(plus, 0, wx.EXPAND)
	sizer.Add(minus, 0, wx.EXPAND)
	return (sizer, choice, modifier, value, plus, minus)
	
def text_filter(value, entry):
	fType = entry[1]
	target = entry[2].lower()
	value = value.lower()

	#['contains', 'doesn\'t contain', 'is', 'is not', 'starts with', 'ends with']
	if (fType == 0 and target in value) or \
		(fType == 1 and not target in value) or \
		(fType == 2 and target == value) or \
		(fType == 3 and target != value) or \
		(fType == 4 and value.startswith(target)) or \
		(fType == 5 and value.endswith(target)): return True
	return False
	
def number_entry(parent, index):
	choice = wx.Choice(parent, -1, choices=[x[1] for x in parent.tagChoices])
	modifier = wx.Choice(parent, -1, choices=['is', 'is not', 'less than', 'more than', 'at least', 'at most'])
	value = wx.SpinCtrl(parent, -1, min=-99999, max=99999)
	plus  = wx.Button(parent, -1, "+", style=wx.BU_EXACTFIT)
	minus = wx.Button(parent, -1, "-", style=wx.BU_EXACTFIT)
	
	choice.Bind(wx.EVT_CHOICE, parent.event_choice_tag)
	plus.Bind(wx.EVT_BUTTON, parent.event_button_plus)
	minus.Bind(wx.EVT_BUTTON, parent.event_button_minus)
	
	choice.index = index
	plus.index = index
	minus.index = index
	
	sizer = wx.BoxSizer(wx.HORIZONTAL)
	sizer.Add(choice)
	sizer.Add(modifier)
	sizer.Add(value, 1, wx.EXPAND)
	sizer.Add(plus, 0, wx.EXPAND)
	sizer.Add(minus, 0, wx.EXPAND)
	return (sizer, choice, modifier, value, plus, minus)
	
def number_filter(value, entry):
	fType = entry[1]
	target = entry[2]
	
	try: value = int(value)
	except: return False

	#['is', 'is not', 'less than', 'more than', 'at least', 'at most']
	if (fType == 0 and value == target) or \
		(fType == 1 and value != target) or \
		(fType == 2 and value < target) or \
		(fType == 3 and value > target) or \
		(fType == 4 and value >= target) or \
		(fType == 5 and value <= target): return True
	return False
	
def rating_entry(parent, index):
	choice = wx.Choice(parent, -1, choices=[x[1] for x in parent.tagChoices])
	modifier = wx.Choice(parent, -1, choices=['is', 'is not', 'less than', 'more than', 'at least', 'at most'])
	value = wx.Choice(parent, -1, choices=["☆☆☆☆☆", "★☆☆☆☆", "★★☆☆☆", "★★★☆☆", "★★★★☆", "★★★★★"])
	plus  = wx.Button(parent, -1, "+", style=wx.BU_EXACTFIT)
	minus = wx.Button(parent, -1, "-", style=wx.BU_EXACTFIT)
	
	choice.Bind(wx.EVT_CHOICE, parent.event_choice_tag)
	plus.Bind(wx.EVT_BUTTON, parent.event_button_plus)
	minus.Bind(wx.EVT_BUTTON, parent.event_button_minus)
	
	choice.index = index
	plus.index = index
	minus.index = index
	
	sizer = wx.BoxSizer(wx.HORIZONTAL)
	sizer.Add(choice)
	sizer.Add(modifier)
	sizer.Add(value, 1, wx.EXPAND)
	sizer.Add(plus, 0, wx.EXPAND)
	sizer.Add(minus, 0, wx.EXPAND)
	return (sizer, choice, modifier, value, plus, minus)
	
def rating_filter(value, entry):
	fType = entry[1]
	target = entry[2]
	
	#['is', 'is not', 'less than', 'more than', 'at least', 'at most']
	if (fType == 0 and value == target) or \
		(fType == 1 and value != target) or \
		(fType == 2 and value < target) or \
		(fType == 3 and value > target) or \
		(fType == 4 and value >= target) or \
		(fType == 5 and value <= target): return True
	return False
	
def time_entry(parent, index, ago=True):
	choice = wx.Choice(parent, -1, choices=[x[1] for x in parent.tagChoices])
	choices = ['more than', 'less than', 'at least', 'at most', 'before', 'after']
	modifier = wx.Choice(parent, -1, choices=(choices if ago else choices[:-2]))
	value = wx.SpinCtrl(parent, -1)
	modifier2 = wx.Choice(parent, -1, choices=['seconds', 'minutes', 'hours', 'days', 'weeks', 'months', 'years'])
	plus  = wx.Button(parent, -1, "+", style=wx.BU_EXACTFIT)
	minus = wx.Button(parent, -1, "-", style=wx.BU_EXACTFIT)
	
	choice.Bind(wx.EVT_CHOICE, parent.event_choice_tag)
	plus.Bind(wx.EVT_BUTTON, parent.event_button_plus)
	minus.Bind(wx.EVT_BUTTON, parent.event_button_minus)
	
	choice.index = index
	plus.index = index
	minus.index = index
	
	sizer = wx.BoxSizer(wx.HORIZONTAL)
	sizer.Add(choice)
	sizer.Add(modifier)
	sizer.Add(value, 1, wx.EXPAND)
	sizer.Add(modifier2)
	if ago: sizer.Add(wx.StaticText(parent, -1, 'ago'), 0, wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 5)
	sizer.Add(plus, 0, wx.EXPAND)
	sizer.Add(minus, 0, wx.EXPAND)
	return (sizer, choice, modifier, value, modifier2, plus, minus)

def time_filter(value, entry):
	fType = entry[1]
	target = entry[2]*[1.0, 60.0, 3600.0, 86400.0, 604800.0, 2592000.0, 31536000.0][entry[3]]
	currentTime = time.time()

	if (fType == 4 and value < currentTime-target) or \
		(fType == 5 and value > currentTime-target): return True

	value = (currentTime-value)

	##['more than', 'less than', 'at least', 'at most', 'before', 'after']
	if (fType == 0 and value > target) or \
		(fType == 1 and value < target) or \
		(fType == 2 and value >= target) or \
		(fType == 3 and value <= target): return True
	return False
	
def time_filter_no_ago(value, entry):
	fType = entry[1]
	target = entry[2]
	
	##['more than', 'less than', 'at least', 'at most']
	if (fType == 0 and value > target) or \
		(fType == 1 and value < target) or \
		(fType == 2 and value >= target) or \
		(fType == 3 and value <= target): return True
	return False

def boolean_entry(parent, index):
	def event_button_toggle(event):
		event.GetEventObject().buttonAlt.SetValue(not event.Int)
	
	choice = wx.Choice(parent, -1, choices=[x[1] for x in parent.tagChoices])
	buttonTrue = wx.ToggleButton(parent, -1, "True")
	buttonFalse = wx.ToggleButton(parent, -1, "False")
	plus  = wx.Button(parent, -1, "+", style=wx.BU_EXACTFIT)
	minus = wx.Button(parent, -1, "-", style=wx.BU_EXACTFIT)
	
	choice.index = index
	plus.index = index
	minus.index = index
	
	buttonTrue.SetValue(1)
	buttonTrue.buttonAlt = buttonFalse
	buttonFalse.buttonAlt = buttonTrue
	
	choice.Bind(wx.EVT_CHOICE, parent.event_choice_tag)
	plus.Bind(wx.EVT_BUTTON, parent.event_button_plus)
	minus.Bind(wx.EVT_BUTTON, parent.event_button_minus)
	buttonTrue.Bind(wx.EVT_TOGGLEBUTTON, event_button_toggle)
	buttonFalse.Bind(wx.EVT_TOGGLEBUTTON, event_button_toggle)
	
	sizer = wx.BoxSizer(wx.HORIZONTAL)
	sizer.Add(choice)
	sizer.Add(wx.StaticText(parent, -1, 'is'), 0, wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 5)
	sizer.Add(buttonTrue, 1, wx.EXPAND)
	sizer.Add(buttonFalse, 1, wx.EXPAND)
	sizer.Add(plus, 0, wx.EXPAND)
	sizer.Add(minus, 0, wx.EXPAND)
	return (sizer, choice, buttonTrue, buttonFalse, plus, minus)
	
def boolean_filter(value, entry):
	return entry[1] == value

def generate_playlist(smartSeedData):
	trackIds = []
	tags = []
	filterFunctions = []
	match = smartSeedData[1]
	limit = smartSeedData[3]
	order = smartSeedData[4]
	limitCounter = 0

	# If its time make it seconds or data make it bytes
	if limit[2] == 1: limit[1] *= 60
	elif limit[2] == 2: limit[1] *= 3600
	elif limit[2] == 3: limit[1] *= 1048576
	elif limit[2] == 4: limit[1] *= 1073741824
	
	if match[0]:
		for entry in smartSeedData[2]:
			tag = entry[0]
			tags.append(tag)
			
			if tag in ["bitrate", "discnumber", "disctotal", "playcount", "skipcount", "tracknumber", "tracktotal", "year"]:
				filterFunctions.append(number_filter)
			elif tag in ["timeplayed", "timeadded", "timemodified"]:
				filterFunctions.append(time_filter)
			elif tag == "length":
				filterFunctions.append(time_filter_no_ago)
			elif tag == "compilation":
				filterFunctions.append(boolean_filter)
			elif tag == "rating":
				filterFunctions.append(rating_filter)
			else:
				filterFunctions.append(text_filter)

	keys = pympe.library.tracks.keys()
	# ['Random', 'Album', 'Artist', 'Name', 'Genre', 'Highest Rating', 'Lowest Rating', 'Most Often Played', 
	# 'Least Often Played', 'Most Recently Played', 'Least Recently Played', 'Most Recently Added', 'Least Recently Added'])
	if order == 0: random.shuffle(keys)
	elif order == 1: keys = sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'album'))
	elif order == 2: keys = sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'artist'))
	elif order == 3: keys = sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'title'))
	elif order == 4: keys = sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'genre'))
	elif order == 5: keys = reversed(sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'rating')))
	elif order == 6: keys = sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'rating'))
	elif order == 7: keys = reversed(sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'playcount')))
	elif order == 8: keys = sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'playcount'))
	elif order == 9: keys = reversed(sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'timeplayed')))
	elif order == 10: keys = sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'timeplayed'))
	elif order == 11: keys = reversed(sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'timeadded')))
	elif order == 12: keys = sorted(keys, key=lambda x: pympe.library.get_single_track_single_value(x, 'timeadded'))

	for trackId in keys:
		if match[0]:
			info = pympe.library.get_single_track_multi_values(trackId, tags, 'list')
			matches = False

			for tag, value, function, entry in zip(tags, info, filterFunctions, smartSeedData[2]):
				if tag in pympe.library.multiTagsShared+pympe.library.multiTagsLocal:
					matches = True in [True for x in value if function(x, entry)]
				else:
					matches = function(value, entry)

				if match[1] and matches: break
				elif not matches and not match[1]: break
			if not matches: continue

		if limit[0]:
			if limit[2] == 1 or limit[2] == 2: limitCounter += pympe.library.get_single_track_single_value(trackId, 'length')
			elif limit[2] == 3 or limit[2] == 4: limitCounter += pympe.library.get_single_track_single_value(trackId, 'filesize')
			if limit[2] == 0 and len(trackIds) == limit[1]: break
			elif limitCounter >= limit[1]: break
			
		trackIds.append(trackId)

	# Return it to origional
	if limit[2] == 1: limit[1] /= 60
	elif limit[2] == 2: limit[1] /= 3600
	elif limit[2] == 3: limit[1] /= 1048576
	elif limit[2] == 4: limit[1] /= 1073741824

	return trackIds

class PlaylistEditor(wx.Dialog):
	def __init__(self, playlist=None, parent=pympe.ui.uiMain):
		wx.Dialog.__init__(self, parent, -1, "Edit Smart Playlist")

		# The sizer
		sizer = wx.BoxSizer(wx.VERTICAL)
		
		# Playlist name
		s2 = wx.BoxSizer(wx.HORIZONTAL)
		self.playlistName = wx.TextCtrl(self, -1, "Favorites")
		s2.Add(wx.StaticText(self, -1, "Playlist Name: "), flag=wx.ALIGN_CENTER_VERTICAL)
		s2.Add(self.playlistName, 1, wx.EXPAND)
		sizer.Add(s2, 0, wx.EXPAND)
		
		# Match any/all criteria
		s2 = wx.BoxSizer(wx.HORIZONTAL)
		check = wx.CheckBox(self, -1, 'Match')
		choice = wx.Choice(self, -1, choices=['All', 'Any'])
		label = wx.StaticText(self, -1, 'of the following:')
		check.SetValue(1)
		check.Bind(wx.EVT_CHECKBOX, self.event_check_match)
		self.buttonsMatch = (check, choice, label)
		
		s2.Add(check, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5)
		s2.Add(choice)
		s2.Add(label, 0, wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 5)
		sizer.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.TOP|wx.BOTTOM|wx.EXPAND, 5)
		sizer.Add(s2, 0, wx.EXPAND)
		
		self.sizerEntries = wx.BoxSizer(wx.VERTICAL)
		sizer.Add(self.sizerEntries, 1, wx.EXPAND)
		
		self.entries = []
		self.tagChoices = sorted([(k, v) for k, v in pm_language.readableTags.iteritems() if k != "art"], key=lambda x: x[1])

		# Add the first entry
		entry = text_entry(self, 0)
		self.entries.append(entry)
		self.sizerEntries.Add(entry[0], flag=wx.EXPAND)
		entry[-1].Disable()

		# Limit
		# Check[Limit to] > NumberDial > [items, minutes, hours, MB, GB] > ordered by > [Random, Album, Artist, Name, Genre, -----, Highest Rating, Lowest Rating, ----, Most Often Played, Least Often Played, ----, Most Recently Played, Least Recently Played, ---- Most Recently Added, Least Recently Added]
		s2 = wx.BoxSizer(wx.HORIZONTAL)
		check = wx.CheckBox(self, -1, 'Limit To')
		spin = wx.SpinCtrl(self, -1, max=99999)
		choice = wx.Choice(self, -1, choices=['Items', 'Minutes', 'Hours', 'MB', 'GB'])
		check.Bind(wx.EVT_CHECKBOX, self.event_check_limit)
		self.buttonsLimit = (check, spin, choice)
		[x.Disable() for x in self.buttonsLimit[1:]]
		
		s2.Add(check, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5)
		s2.Add(spin)
		s2.Add(choice, 1)
		sizer.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.TOP|wx.BOTTOM|wx.EXPAND, 5)
		sizer.Add(s2, 0, wx.EXPAND)

		# Predefined Selection
		# ['Favorites', 'Recent Favorites', 'Recently Added', 'Recently Played', 'Unheard', 'Neglected Favorites', 'Least Favorite', '700 MB Of Favorites', '80 Minutes Of Favorites', 'Unrated']
		sizer.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.TOP|wx.BOTTOM|wx.EXPAND, 5)
		check = wx.CheckBox(self, -1, 'Select Preset')
		choice = wx.Choice(self, -1, choices=[x[0] for x in cfg_get("presets")])
		check.choice = choice
		
		check.Bind(wx.EVT_CHECKBOX, self.event_check_preset)
		choice.Bind(wx.EVT_CHOICE, self.event_choice_preset)
		choice.Disable()
		
		s2 = wx.BoxSizer(wx.HORIZONTAL)
		s2.Add(check, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT, 5)
		s2.Add(choice, 1, wx.EXPAND)
		sizer.Add(s2, 0, wx.EXPAND)
		
		# Cancel Save / Result Ordering
		label = wx.StaticText(self, -1, 'Order Results By')
		choice = wx.Choice(self, -1, choices=['Random', 'Album', 'Artist', 'Name', 'Genre', 'Highest Rating', 'Lowest Rating', 'Most Often Played', 'Least Often Played', 'Most Recently Played', 'Least Recently Played', 'Most Recently Added', 'Least Recently Added'])
		self.buttonsOrder = (choice, label)
		
		#def spre(event):
			#presets = cfg_get("presets")
			#presets.append(self.store_playlist())
			#cfg_set("presets", presets)
		
		#b = wx.Button(self, -1, "s")
		#b.Bind(wx.EVT_BUTTON, spre)
		
		
		s2 = wx.BoxSizer(wx.HORIZONTAL)
		s2.Add(label, 1, wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 5)
		s2.Add(choice)
		s2.AddStretchSpacer()
		#s2.Add(b)
		s2.Add(wx.Button(self, wx.ID_CANCEL), 0, wx.RIGHT, 5)
		self.buttonSave = wx.Button(self, wx.ID_SAVE)
		s2.Add(self.buttonSave)
		sizer.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.TOP|wx.BOTTOM|wx.EXPAND, 5)
		sizer.Add(s2, 0, wx.EXPAND)
		
		self.buttonSave.Bind(wx.EVT_BUTTON, self.event_button_save)
		
		# Fill the entire thing if possible
		if playlist:
			self.restore_playlist(playlist)
			self.playlist = playlist
		else: self.playlist = None
		
		# Add the sizer into the dialog with a space
		self.box = box = wx.BoxSizer()
		box.Add(sizer, 0, wx.ALL, 10)
		self.SetSizerAndFit(box)

		# Show the dialog
		self.ShowModal()
		
	def event_button_save(self, event):
		smartSeedData = self.store_playlist()

		if self.playlist:
			pympe.playlists.rename_playlist(self.playlist.uid, smartSeedData[0])
			self.playlist.update(smartSeedData)
			pympe.playlists.select_playlist(self.playlist.uid)
		else:
			playlist = pympe.playlists.create_playlist(smartSeedData[0])
			playlist.update(smartSeedData)
			playlist.refresh()
			pympe.playlists.select_playlist(playlist.uid)
			
		self.Destroy()
		
	def event_check_preset(self, event):
		if event.Int: event.EventObject.choice.Enable()
		else: event.EventObject.choice.Disable()
		
	def event_choice_preset(self, event):
		self.restore_playlist(cfg_get("presets")[event.Int])
		
	def event_check_limit(self, event):
		if event.Int:
			[x.Enable() for x in self.buttonsLimit[1:]]
			self.buttonSave.Enable()
		else:
			[x.Disable() for x in self.buttonsLimit[1:]]
			if not self.buttonsMatch[0].GetValue(): self.buttonSave.Disable()
		
	def event_check_match(self, event):
		if event.Int:
			[x.Enable() for x in self.buttonsMatch[1:]]
			for entry in self.entries: [x.Enable() for x in entry[1:]]
			self.buttonSave.Enable()
		else:
			[x.Disable() for x in self.buttonsMatch[1:]]
			for entry in self.entries: [x.Disable() for x in entry[1:]]
			if not self.buttonsLimit[0].GetValue(): self.buttonSave.Disable()

	def event_button_plus(self, event):
		index = event.GetEventObject().index+1
		
		entry = text_entry(self, index)
		self.entries.insert(index, entry)
	
		self.sizerEntries.Insert(index, entry[0], flag=wx.EXPAND)
	
		if len(self.entries) > 1: [entry[-1].Enable() for entry in self.entries]

		for i, entry in enumerate(self.entries[index:]):
			entry[1].index = entry[-2].index = entry[-1].index = index+i

		self.Layout()
		self.SetSizerAndFit(self.GetSizer())
		
	def event_button_minus(self, event):
		entry = self.entries[event.GetEventObject().index]
		self.entries.remove(entry)
		
		entry[0].Clear(True)
		self.sizerEntries.Detach(entry[0])
		entry[0].Destroy()
		
		if len(self.entries) == 1: self.entries[0][-1].Disable()
	
		for index, entry in enumerate(self.entries):
			entry[1].index = index
			entry[-2].index = index
			entry[-1].index = index
				
		self.Layout()
		self.SetSizerAndFit(self.GetSizer())
		
	def event_choice_tag(self, event):
		index = event.GetEventObject().index
		entry = self.entries[index]
		value = self.tagChoices[event.Int][0]

		if value in ["bitrate", "discnumber", "disctotal", "playcount", "skipcount", "tracknumber", "tracktotal", "year"]:
			newEntry = number_entry(self, index)
		elif value in ["timeplayed", "timeadded", "timemodified"]:
			newEntry = time_entry(self, index)
		elif value == "length":
			newEntry = time_entry(self, index, False)
		elif value == "compilation":
			newEntry = boolean_entry(self, index)
		elif value == "rating":
			newEntry = rating_entry(self, index)
		else:
			newEntry = text_entry(self, index)

		entry[0].Clear(True)
		self.sizerEntries.Detach(entry[0])
		entry[0].Destroy()
		
		self.sizerEntries.Insert(index, newEntry[0], flag=wx.EXPAND)
		self.entries[index] = newEntry
		
		newEntry[1].SetSelection(event.Int)
		if len(self.entries) == 1: newEntry[-1].Disable()
		
		self.Layout()
		
	def store_playlist(self):
		match = [True if self.buttonsMatch[0].GetValue() else False, self.buttonsMatch[1].GetSelection()]
		limit = [self.buttonsLimit[0].GetValue(), self.buttonsLimit[1].GetValue(), self.buttonsLimit[2].GetSelection()]
		entries = []
		
		for entry in self.entries: 
			x = [self.tagChoices[entry[1].GetSelection()][0]]
			tag = x[0]
			
			if tag in ["bitrate", "discnumber", "disctotal", "playcount", "skipcount", "tracknumber", "tracktotal", "year"]:
				x += [entry[2].GetSelection(), entry[3].GetValue()]
			elif tag in ["timeplayed", "timeadded", "timemodified"]:
				x += [entry[2].GetSelection(), entry[3].GetValue(), entry[4].GetSelection()]
			elif tag == "compilation":
				x += [entry[2].GetValue()]
			elif tag == "rating":
				x += [entry[2].GetSelection(), entry[3].GetSelection()]
			else:
				x += [entry[2].GetSelection(), entry[3].GetValue()]
				
			entries.append(x)
	
		smartSeedData = [self.playlistName.GetValue(), match, entries, limit, self.buttonsOrder[0].GetSelection()]
		return smartSeedData 
		
	def restore_playlist(self, playlist):
		store = playlist.smartSeedData
		
		#print store
		self.playlistName.ChangeValue(store[0])
		self.buttonsMatch[0].SetValue(store[1][0])
		self.buttonsMatch[1].SetSelection(store[1][1])
		
		self.sizerEntries.Clear(True)
		self.entries = []
		for index, entry in enumerate(store[2]):
			if entry[0] in ["bitrate", "discnumber", "disctotal", "playcount", "skipcount", "tracknumber", "tracktotal", "year"]: 
				newEntry = number_entry(self, index)
				newEntry[2].SetSelection(entry[1])
				newEntry[3].SetValue(entry[2])
			elif entry[0] in ["timeplayed", "timeadded", "timemodified", "length"]: 
				newEntry = time_entry(self, index, entry[0] != "length")
				newEntry[2].SetSelection(entry[1])
				newEntry[3].SetValue(entry[2])
				newEntry[4].SetSelection(entry[3])
			elif entry[0] == "compilation":
				newEntry = boolean_entry(self, index)
				newEntry[2].SetValue(entry[1])
				newEntry[2].SetValue(not entry[1])
			elif entry[0] == "rating":
				newEntry = rating_entry(self, index)
				newEntry[2].SetSelection(entry[1])
				newEntry[3].SetSelection(entry[2])
			else:
				newEntry = text_entry(self, index)
				newEntry[2].SetSelection(entry[1])
				newEntry[3].SetValue(entry[2])


			newEntry[1].SetSelection([i for i, x in enumerate(self.tagChoices) if x[0] == entry[0]][0])
			newEntry[1].index = index
			newEntry[-2].index = index
			newEntry[-1].index = index
			self.entries.append(newEntry)
			self.sizerEntries.Add(newEntry[0], flag=wx.EXPAND)
			
		self.buttonsLimit[0].SetValue(store[3][0])
		self.buttonsLimit[1].SetValue(store[3][1])
		self.buttonsLimit[2].SetSelection(store[3][2])
		self.buttonsOrder[0].SetSelection(store[4])

		# Enable/Disable if needed
		if len(self.entries) > 1: [entry[-1].Enable() for entry in self.entries]
		if store[1][0]:
			[x.Enable() for x in self.buttonsMatch[1:]]
			for entry in self.entries: [x.Enable() for x in entry[1:]]
			self.buttonSave.Enable()
		else:
			[x.Disable() for x in self.buttonsMatch[1:]]
			for entry in self.entries: [x.Disable() for x in entry[1:]]
			if not self.buttonsLimit[0].GetValue(): self.buttonSave.Disable()
		if store[3][0]: [x.Enable() for x in self.buttonsLimit[1:]]

		self.Layout()
		#self.SetSizerAndFit(self.GetSizer())

class Plugin:
	def plu_settings(self, parent):
		self.PlaylistEditor(parent=parent)

	def plu_start(self): 
		self.PlaylistEditor = PlaylistEditor
		self.generate_playlist = generate_playlist

	def plu_stop(self, shutdown):
		pass
