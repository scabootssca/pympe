import pympe, gst, wx
from pm_player import VideoBin

class VisualizerBin(VideoBin):
	#def set_window_handle(self, handle):
		#self.sink.set_property("force-aspect-ratio", True)
		#self.sink.set_xwindow_id(handle)

	#def setup(self):
		#tee = gst.element_factory_make('tee')
		#self.queueA = gst.element_factory_make("queue")
		#self.queueV = gst.element_factory_make("queue")

		#self.visualizer = gst.element_factory_make('goom2k1')
		#self.capsfilter = gst.element_factory_make("capsfilter")
		#color = gst.element_factory_make('ffmpegcolorspace')
		#self.sink  = gst.element_factory_make('xvimagesink')

		#self.elements = []

		#self.add(tee, self.queueA, self.queueV, self.visualizer, self.capsfilter, color, self.sink)
		#gst.element_link_many(tee, self.queueV, self.visualizer, self.capsfilter, color, self.sink)
		#gst.element_link_many(tee, self.queueA)

		#self.blockPad = self.queueV.get_static_pad('src')
		#self.srcPad = self.queueA.get_static_pad('src')

	#def set_visualizer(self, name): pass
	#def set_quality(self, width, height, framerate=None): pass

	def __init__(self, plugin='wavescope', quality=None):
		self.plugin = plugin
		self.quality = quality
		VideoBin.__init__(self, 'Visualizer')
		if quality: self.set_quality(*quality)

	def setup_pipeline(self):
		tee = gst.element_factory_make('tee')
		self.queueA = gst.element_factory_make("queue")
		self.queueV = gst.element_factory_make("queue")

		self.visualizer = gst.element_factory_make(self.plugin)
		self.capsfilter = gst.element_factory_make("capsfilter")
		color = gst.element_factory_make('ffmpegcolorspace')
		self.sink  = gst.element_factory_make('xvimagesink')

		self.blockPad = self.queueV.get_static_pad('src')

		self.add(tee, self.queueA, self.queueV, self.visualizer, self.capsfilter, color, self.sink)
		gst.element_link_many(tee, self.queueV, self.visualizer, self.capsfilter, color, self.sink)
		gst.element_link_many(tee, self.queueA)

		self.add_pad(gst.GhostPad("sink", tee.get_static_pad("sink")))
		self.add_pad(gst.GhostPad("src", self.queueA.get_static_pad("src")))

	#def set_window_handle(self, handle):
		#self.sink.set_property("force-aspect-ratio", True)
		#self.sink.set_xwindow_id(handle)

	def set_visualizer(self, name):
		try: newVisualizer = gst.element_factory_make(name)
		except gst.ElementNotFoundError:
			pympe.error('Unable to find the selected visualization.')
			self.settings.choiceVisualization.SetSelection(0)
			newVisualizer = gst.element_factory_make('goom')

		self.remove(self.visualizer)
		self.visualizer.set_state(gst.STATE_NULL)
		self.add(newVisualizer)
		gst.element_link_many(self.queueV, newVisualizer, self.capsfilter)
		self.visualizer = newVisualizer


	def set_quality(self, width, height, framerate=None):
		pad = self.visualizer.get_static_pad("src")
		templateCaps = pad.get_pad_template_caps()

		# Make sure the pad has predesignated caps
		if not templateCaps:
			print "Visualization element has no template caps?"
			return

		caps = pad.get_caps()
		if not caps.is_fixed():
			for x in range(caps.get_size()):
				struct = caps.get_structure(x)
				struct.fixate_field_nearest_int("width", width)
				struct.fixate_field_nearest_int("height", height)
				if framerate != None: struct.fixate_field_nearest_int("framerate", framerate)

		self.capsfilter.set_property("caps", caps)


#class VisualizerBin(VideoBin):
	#def __init__(self, plugin='goom2k1', quality=None):
		#self.plugin = plugin
		#self.quality = quality
		#VideoBin.__init__(self, 'Visualizer')
		#if quality: self.set_quality(*quality)

	#def setup_pipeline(self):
		#tee = gst.element_factory_make('tee')
		#self.queueA = gst.element_factory_make("queue")
		#self.queueV = gst.element_factory_make("queue")

		#self.visualizer = gst.element_factory_make(self.plugin)
		#self.capsfilter = gst.element_factory_make("capsfilter")
		#color = gst.element_factory_make('ffmpegcolorspace')
		#self.sink  = gst.element_factory_make('xvimagesink')

		#self.blockPad = self.queueV.get_static_pad('src')

		#self.add(tee, self.queueA, self.queueV, self.visualizer, self.capsfilter, color, self.sink)
		#gst.element_link_many(tee, self.queueV, self.visualizer, self.capsfilter, color, self.sink)
		#gst.element_link_many(tee, self.queueA)

		#self.add_pad(gst.GhostPad("sink", tee.get_static_pad("sink")))
		#self.add_pad(gst.GhostPad("src", self.queueA.get_static_pad("src")))

	##def set_window_handle(self, handle):
		##self.sink.set_property("force-aspect-ratio", True)
		##self.sink.set_xwindow_id(handle)

	#def set_visualizer(self, name):
		#try: newVisualizer = gst.element_factory_make(name)
		#except gst.ElementNotFoundError:
			#pympe.error('Unable to find the selected visualization.')
			#self.settings.choiceVisualization.SetSelection(0)
			#newVisualizer = gst.element_factory_make('goom')

		#self.remove(self.visualizer)
		#self.visualizer.set_state(gst.STATE_NULL)
		#self.add(newVisualizer)
		#gst.element_link_many(self.queueV, newVisualizer, self.capsfilter)
		#self.visualizer = newVisualizer

	#def set_quality(self, width, height, framerate=None):
		#pad = self.visualizer.get_static_pad("src")
		#templateCaps = pad.get_pad_template_caps()

		## Make sure the pad has predesignated caps
		#if not templateCaps:
			#print "Visualization element has no template caps?"
			#return

		#caps = pad.get_caps()
		#if not caps.is_fixed():
			#for x in range(caps.get_size()):
				#struct = caps.get_structure(x)
				#struct.fixate_field_nearest_int("width", width)
				#struct.fixate_field_nearest_int("height", height)
				#if framerate != None: struct.fixate_field_nearest_int("framerate", framerate)

		#self.capsfilter.set_property("caps", caps)

class VisualizationSettings(wx.Dialog):
	def __init__(self, plugin):
		wx.Dialog.__init__(self, parent, wx.ID_ANY, "Visualization Settings")

		self.plugin = plugin
		self.plugin.visualizerBin.settings = self

		sizer = wx.GridBagSizer(5, 5)

		toggle = wx.ToggleButton(self, -1, "Enable Visualizations")
		toggle.Bind(wx.EVT_TOGGLEBUTTON, self.event_toggle_enable_visualizer)
		sizer.Add(toggle, (0, 0), (1, 1), flag=wx.EXPAND)

		label = wx.StaticText(self, -1, "Select Visualization")
		self.choiceVisualization = wx.Choice(self, -1, choices=self.plugin.visNames)
		self.choiceVisualization.SetSelection(cfg_get("current"))
		self.choiceVisualization.Bind(wx.EVT_CHOICE, self.event_listbox_visualization)
		sizer.Add(label, (1, 0), (1, 1))
		sizer.Add(self.choiceVisualization, (2, 0), (1, 1), flag=wx.EXPAND)

		label = wx.StaticText(self, -1, "Set Quality")
		self.choiceQuality = wx.Choice(self, -1, choices=['Highest Quality', '3/4 Quality', '1/2 Quality', '1/4 Quality'])
		self.choiceQuality.SetSelection(cfg_get("quality"))
		self.choiceQuality.Bind(wx.EVT_CHOICE, self.event_listbox_quality)
		sizer.Add(label, (3, 0), (1, 1))
		sizer.Add(self.choiceQuality, (4, 0), (1, 1), flag=wx.EXPAND)

		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), (5, 0), (1, 1), flag=wx.EXPAND)

		buttonClose = wx.Button(self, -1, "Close")
		buttonClose.Bind(wx.EVT_BUTTON, self.event_button_close)
		sizer.Add(buttonClose, (6, 0), (1, 1), flag=wx.ALIGN_RIGHT)

		# Restore the state
		toggle.SetValue(cfg_get("enabled"))
		if not toggle.GetValue():
			self.choiceVisualization.Disable()
			self.choiceQuality.Disable()

		# Finish up
		box = wx.BoxSizer()
		box.Add(sizer, 1, wx.ALL|wx.EXPAND, 10)
		self.SetSizerAndFit(box)

		# Show it
		self.CenterOnScreen()
		self.Show()

	def event_listbox_visualization(self, event):
		def now_blocked(index):
			self.plugin.visualizerBin.set_visualizer(self.plugin.visPlugins[index])
			self.plugin.visualizerBin.unblock()
			cfg_set('current', index)

		self.plugin.visualizerBin.block(now_blocked, event.Int)

	def event_listbox_quality(self, event):
		def now_blocked(index):
			self.plugin.visualizerBin.set_quality(*self.plugin.qualities[index])
			self.plugin.visualizerBin.unblock()
			cfg_set("quality", index)

		self.plugin.visualizerBin.block(now_blocked, event.Int)

	def event_toggle_enable_visualizer(self, event):
		if event.Int:
			#self.plugin.visualizerBin.set_quality(*self.plugin.qualities[cfg_get('quality')])
			#self.plugin.visualizerBin.set_visualizer(self.plugin.visPlugins[cfg_get('current')])
			pympe.player.swap_video(self.plugin.visualizerBin)

			#print 'Swapped', pympe.player.playing
			#self.plugin.visualizerBin.set_state(pympe.player.playerBin.get_state()[1])
			def blocked_func():
				self.plugin.visualizerBin.queueA.set_state(pympe.player.audioBin.state)
				self.plugin.visualizerBin.sink.set_state(pympe.player.audioBin.state)
				self.plugin.visualizerBin.set_state(pympe.player.audioBin.state)
				print self.plugin.visualizerBin.get_state()

			pympe.player.audioBin.add_element(self.plugin.visualizerBin, blocked_func)
			#print 'Added', pympe.player.playing







			self.choiceVisualization.Enable()
			self.choiceQuality.Enable()
		else:
			pympe.player.swap_video()
			pympe.player.remove_audio_element(self.plugin.visualizerBin)
			self.choiceVisualization.Disable()
			self.choiceQuality.Disable()

		cfg_set("enabled", event.Int)

	def event_button_close(self, event):
		self.Destroy()

class Plugin:
	def plu_start(self):
		#print 'Needs to not change to art between songs when visualizer is on, also maybe find a way to disable the pop out window?'
		#print 'Adding an individual element works just not an entire bin with a tee so maybe it\' not setting the entire thing to playing or something?'

		self.visualizerBin = VisualizerBin('goom', (800, 600, 30))
		#self.visualizerBin.set_quality()#*self.qualities[cfg_get("quality")])

		self.visNames = ["Goom", "Goom 2K1", "Monoscope", "Bumpscope", "Corona", "Infinite", "Jakdaw", "Jess", "Level Analyzer", "Level Scope", "Oinksie", 'Space Scope', 'Spectrascope', 'Synaescope', 'Wavescope']
		self.visPlugins = ["goom", "goom2k1", "monoscope", "libvisual_bumpscope", "libvisual_corona", "libvisual_infinite", "libvisual_jakdaw", "libvisual_jess", "libvisual_lv_analyzer", "libvisual_lv_scope", "libvisual_oinksie", 'spacescope', 'spectrascope', 'synaescope', 'wavescope']
		self.qualities = [(800, 600, 30), (640, 480, 25), (320, 240, 20), (200, 150, 10)]

		self.visualizerMenu = wx.Menu()
		self.visualizerMenu.Bind(wx.EVT_MENU, self.event_menu)
		self.visualizerMenu.AppendRadioItem(0, 'None')
		for i, visualizerName in enumerate(self.visNames):
			self.visualizerMenu.AppendRadioItem(i+1, visualizerName)

		pympe.ui.set_menu_hook('Select Visualizer', self.visualizerMenu)

		pympe.player.audioBin.add_element(self.visualizerBin)
		#pympe.player.swap_video(self.visualizerBin)
		#self.visualizer = VisualizerBin(visualizer=self.visPlugins[cfg_get("current")])

		#self.visualizerBin.set_quality(800, 600, 30)#*self.qualities[cfg_get("quality")])

		#if cfg_get("enabled"):
			#pympe.player.add_audio_tee(self.visualizer)
			#pympe.player.set_video_sink(self.visualizer.elements[-1])
		#
		#self.visualizer.set_quality(1600, 1200, 30)
		#pympe.player.add_audio_element(self.visualizer)
		#pympe.player.set_video_sink(self.visualizer.elements[-1])

	def event_menu(self, event):
		print event.Id

	def plu_stop(self, shutdown):
		pympe.ui.remove_menu_hook(self.visualizerMenu)
		#if pympe.player.videoBin == self.visualizerBin:
		#	pympe.player.swap_video()
		#pympe.player.audioBin.remove_element(self.visualizerBin)

		#if shutdown: return
		#if cfg_get("enabled"):
			#pympe.player.remove_audio_tee(self.visualizer)
			#pympe.player.set_video_sink()



	def plu_settings(self, parent):
		VisualizationSettings(self)
