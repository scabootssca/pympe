import pympe
import wx, time
import os

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, "Screensaver Disabler")
		
		self.plugin = plugin
		
		sizer = wx.BoxSizer(wx.VERTICAL)

		self.checkDisableForAudio = wx.CheckBox(self, -1, 'Disable for Audio')
		self.checkDisableForVideo = wx.CheckBox(self, -1, 'Disable for Video')
		self.checkDisableForTheatre = wx.CheckBox(self, -1, 'Disable for Theatre mode')
		self.checkDisableForFullscreen = wx.CheckBox(self, -1, 'Disable for Fullscreen mode')
		self.checkEnableOnPause = wx.CheckBox(self, -1, 'Re-enable screensaver when paused')

		self.checkDisableForAudio.SetValue(cfg_get('disableForAudio'))
		self.checkDisableForVideo.SetValue(cfg_get('disableForVideo'))
		self.checkDisableForTheatre.SetValue(cfg_get('disableForTheatre'))
		self.checkDisableForFullscreen.SetValue(cfg_get('disableForFullscreen'))
		self.checkEnableOnPause.SetValue(cfg_get('enableOnPause'))
		
		sizer.Add(self.checkDisableForAudio, flag=wx.EXPAND)
		sizer.Add(self.checkDisableForVideo, flag=wx.EXPAND)
		sizer.Add((-1, 10))
		sizer.Add(self.checkDisableForTheatre, flag=wx.EXPAND)
		sizer.Add(self.checkDisableForFullscreen, flag=wx.EXPAND)
		sizer.Add((-1, 10))
		sizer.Add(self.checkEnableOnPause, flag=wx.EXPAND)

		self.textDisableCommands = wx.TextCtrl(self, -1,  cfg_get('disableCommands'))
		self.textEnableCommands = wx.TextCtrl(self, -1, cfg_get('enableCommands'))
		
		sizer.Add(wx.StaticText(self, -1, 'Extra screensaver disable commands'), 0, wx.TOP, 10)
		sizer.Add(self.textDisableCommands, flag=wx.EXPAND)
		
		sizer.Add(wx.StaticText(self, -1, 'Extra screensaver re-enable commands'), 0, wx.TOP, 10)
		sizer.Add(self.textEnableCommands, flag=wx.EXPAND)
		
		
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), flag=wx.EXPAND|wx.TOP|wx.BOTTOM, border=10)
		sizer.Add(wx.Button(self, id=wx.ID_CLOSE), flag=wx.ALIGN_RIGHT)
		
		self.Bind(wx.EVT_BUTTON, self.button)
		
		box = wx.BoxSizer()
		box.Add(sizer, 1, wx.EXPAND|wx.ALL, 5)
		self.SetSizerAndFit(box)
		self.Show()
		
	def button(self, event):
		if event.Id == 1:
			print 'Enable',self.plugin.enableScreensaver()
			
		elif event.Id == 2:
			print 'Disable',self.plugin.disableScreensaver()
		elif event.Id == wx.ID_CLOSE:
			cfg_set('disableForAudio', self.checkDisableForAudio.IsChecked())
			cfg_set('disableForVideo', self.checkDisableForVideo.IsChecked())
			cfg_set('disableForTheatre', self.checkDisableForTheatre.IsChecked())
			cfg_set('disableForFullscreen', self.checkDisableForFullscreen.IsChecked())
			cfg_set('enableOnPause', self.checkEnableOnPause.IsChecked())
			cfg_set('disableCommands', self.textDisableCommands.GetValue())
			cfg_set('enableCommands', self.textEnableCommands.GetValue())
			self.Destroy()
		else:
			event.Skip()
		
class Plugin:
	def plu_settings(self, parent):
		SettingsDialog(self, parent)

	def disableScreensaver(self):
		if self.screensaverDisabled: return False
		
		with os.popen('xset q') as query:
			queryResults = query.read()
			
		index = queryResults.find('timeout:')
		self.blanking = False if (queryResults[index:index+20]).split()[1] == '0' else True
		
		index = queryResults.find('DPMS is')
		self.dpms = queryResults[index:index+20].split()[2].strip().startswith('Enable') == True

		os.popen('xset s off && xset -dpms').close()
		
		commands = cfg_get('disableCommands')
		if commands: os.popen(commands).close()
		
		self.screensaverDisabled = True
		#print 'Screensaver Function Suspended'
		return True
		
	def enableScreensaver(self):
		if not self.screensaverDisabled: return False

		s = 'xset s %s && xset %sdpms'%('on' if self.blanking else 'off', '+' if self.dpms else '-')
		os.popen(s).close()
		
		commands = cfg_get('enableCommands')
		if commands: os.popen(commands).close()
		
		self.screensaverDisabled = False
		#print 'Screensaver Function Restored'
		return True

	def plu_start(self):
		self.screensaverDisabled = False
		self.blanking = None
		self.dpms     = None

		subscribe(pympe.events.player_current_changed, self.mpevent_player_current_changed)
		subscribe(pympe.events.player_playback_resumed, self.mpevent_player_playback_resumed)
		subscribe(pympe.events.player_playback_paused, self.mpevent_player_playback_paused)
		subscribe(pympe.events.player_playback_started, self.mpevent_player_playback_started)
		subscribe(pympe.events.player_playback_stopped, self.mpevent_player_playback_stopped)
		subscribe(pympe.events.ui_view_mode_changed, self.mpevent_ui_view_mode)

		if pympe.player.playing: self.mpevent_player_playback_started(None)

	def mpevent_player_playback_resumed(self, event):
		if cfg_get('enableOnPause'):
			if not cfg_get('disableForAudio') and not pympe.player.playingVideo: return
			if not cfg_get('disableForVideo') and pympe.player.playingVideo: return
			self.disableScreensaver()
			
	def mpevent_player_playback_paused(self, event):
		if cfg_get('enableOnPause'):
			self.enableScreensaver()

	def mpevent_player_current_changed(self, event, *args):
		self.mpevent_player_playback_started(event)
		
	def mpevent_player_playback_started(self, event):
		# If we are playing video and weren't before and suspend on video suspend it
		# If we are playing audio and weren't before and suspend on audio suspend it
		# else: resume it

		if pympe.player.playingVideo and cfg_get('disableForVideo'):
			self.disableScreensaver()
		
		elif not pympe.player.playingVideo and cfg_get('disableForAudio'):
			self.disableScreensaver()
			
		else:
			self.enableScreensaver()
		
	def mpevent_player_playback_stopped(self, event):
		self.enableScreensaver()
		
	def mpevent_ui_view_mode(self, event, viewMode):
		if not pympe.player.playing: return
		
		if pympe.player.paused and cfg_get('enableOnPause'):
			self.enableScreensaver()
			return
		
		if viewMode == 'theatre' and cfg_get('disableForTheatre'):
			self.disableScreensaver()
			
		elif viewMode == 'fullscreen' and cfg_get('disableForFullscreen'):
			self.disableScreensaver()
			
		elif viewMode == 'normal':
			if pympe.player.playingVideo and cfg_get('disableForVideo'):
				self.disableScreensaver()
			elif not pympe.player.playingVideo and cfg_get('disableForAudio'):
				self.disableScreensaver()
			else:
				self.enableScreensaver()
				

	def plu_stop(self, shutdown):
		self.enableScreensaver()
