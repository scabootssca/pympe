import pympe, pygtk, gtk, pm_helpers, time, wx

try:
	import pynotify
	pynotify.init("Pympe")
	hasPynotify = True
except:
	pympe.error("No module named pynotify.")
	hasPynotify = False

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, 'OSD Settings')

		self.plugin = plugin
		self.checks = {}

		sizer = wx.BoxSizer(wx.VERTICAL)

		uiLabelAppearance = wx.StaticText(self, -1, 'When To Appear')
		uiLabelIcon = wx.StaticText(self, -1, 'Icon Settings')

		uiFont = wx.Font(uiLabelAppearance.GetFont().GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.DEFAULT, wx.FONTWEIGHT_BOLD)
		uiLabelAppearance.SetFont(uiFont)
		uiLabelIcon.SetFont(uiFont)

		sizer.Add(uiLabelAppearance)

		uiCheck = wx.CheckBox(self, 10, 'On Track Change')
		uiCheck.SetValue(cfg_get('showOnTrackChange'))
		sizer.Add(uiCheck)

		uiCheck = wx.CheckBox(self, 11, 'On Playback Start, Pause or Stop')
		uiCheck.SetValue(cfg_get('showOnStateChange'))
		sizer.Add(uiCheck)

		uiCheck = wx.CheckBox(self, 12, 'On Tag Change')
		uiCheck.SetValue(cfg_get('showOnTagChange'))
		sizer.Add(uiCheck)

		uiCheck = wx.CheckBox(self, 13, 'When Main Window Is Focused')
		uiCheck.SetValue(cfg_get('showWhenFocused'))
		sizer.Add(uiCheck)

		sizer.Add((-1, 10))

		sizer.Add(uiLabelIcon)

		uiCheck = wx.CheckBox(self, 20, 'Use Album Cover As Icon')
		uiCheck.SetValue(cfg_get('useAlbumIcon'))
		sizer.Add(uiCheck)

		uiCheck = wx.CheckBox(self, 21, 'Use Stock Media Icons')
		uiCheck.SetValue(cfg_get('useStockIcons'))
		sizer.Add(uiCheck)

		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), flag=wx.EXPAND|wx.TOP|wx.BOTTOM, border=10)
		sizer.Add(wx.Button(self, wx.ID_CLOSE), flag=wx.ALIGN_RIGHT)

		self.Bind(wx.EVT_CHECKBOX, self.event_check)
		self.Bind(wx.EVT_BUTTON, self.event_button)

		# Add the sizer into the dialog with a space
		box = wx.BoxSizer(wx.VERTICAL)
		box.Add(sizer, 1, wx.ALL, 10)
		self.SetSizerAndFit(box)

		self.Show()

	def event_check(self, event):
		if event.Id == 10: cfg_set('showOnTrackChange', event.GetInt())
		elif event.Id == 11: cfg_set('showOnStateChange', event.GetInt())
		elif event.Id == 12: cfg_set('showOnTagChange', event.GetInt())
		elif event.Id == 13: cfg_set('showWhenFocused', event.GetInt())
		elif event.Id == 20: cfg_set('useAlbumIcon', event.GetInt())
		elif event.Id == 21: cfg_set('useStockIcons', event.GetInt())

	def event_button(self, event):
		self.Destroy()
		event.Skip()

class Plugin:
	def plu_start(self):
		self.notification = pynotify.Notification('Pympe', 'Pympe')
		self.notification.currentInfo = None

		subscribe(pympe.events.player_playback_started, self.mpevent_player_playback)
		subscribe(pympe.events.player_playback_paused, self.mpevent_player_playback)
		subscribe(pympe.events.player_playback_stopped, self.mpevent_player_playback)
		subscribe(pympe.events.player_current_changed, self.mpevent_player_playback)
		subscribe(pympe.events.player_current_updated, self.mpevent_player_playback)

	def plu_stop(self, shutdown):
		try: self.notification.close()
		except:
			# Hopefully it was Error: GDBus.Error:org.freedesktop.Notifications.InvalidId: Invalid notification identifier
			pass

	def plu_settings(self, parent): SettingsDialog(self, parent)

	@pm_helpers.threaded
	def update_notification(self, notificationType, currentInfo):
		(title, artist, album) = (pm_helpers.make_display_tag(currentInfo['title'], quote=False),
									pm_helpers.make_display_tag(currentInfo['artist'], quote=False),
									pm_helpers.make_display_tag(currentInfo['album'], quote=False))

		content = ''
		if artist: content += "By: "+artist+("\n" if album else "")
		if album: content += "From: "+album

		# Update the notification
		self.notification.clear_hints()
		self.notification.currentInfo = currentInfo
		self.notification.update(title, content)

		# Set the stock icon if needed
		if cfg_get('useStockIcons'):
			if notificationType == "stop": self.notification.props.icon_name = 'gtk-media-stop'
			elif notificationType == "pause": self.notification.props.icon_name = 'gtk-media-pause' #"notification-audio-pause"
			elif notificationType == 'play' or not cfg_get('useAlbumIcon'): self.notification.props.icon_name = 'gtk-media-play-ltr'

		# Set the art if there's not a stock icon
		if cfg_get('useAlbumIcon') and 'art' in currentInfo and currentInfo['art'] and (not self.notification.props.icon_name or (notificationType == 'play' and not pympe.playlists.current.previouslyPlayed)):
			pixbuf = pm_helpers.PmImage(string=currentInfo["art"]).scaled(64, 64).convert_to_pixbuf()

			if pixbuf:
				# This must jump back into the main thread to set it
				self.notification.set_icon_from_pixbuf(pixbuf)
			else:
				pixbufLoader = gtk.gdk.PixbufLoader()
				pixbufLoader.write(currentInfo["art"])
				pixbuf = pixbufLoader.get_pixbuf()
				pixbufLoader.close()

				if pixbuf:
					self.notification.set_icon_from_pixbuf(pixbuf)
				else:
					self.notification.props.icon_name = 'gtk-media-play-ltr'
		elif cfg_get('useStockIcons'):
			self.notification.props.icon_name = 'gtk-media-play-ltr'

		if currentInfo == pympe.player.currentInfo:
			self.notification.show()

	def mpevent_player_playback(self, event, *args):
		if not cfg_get('showWhenFocused') and pm_helpers.has_focus(): return

		if cfg_get('showOnStateChange'):
			if event == pympe.events.player_playback_started and self.notification.currentInfo == pympe.player.currentInfo:
				self.update_notification("play", pympe.player.currentInfo)
			elif event == pympe.events.player_playback_paused:
				self.update_notification("pause", pympe.player.currentInfo)
			elif event == pympe.events.player_playback_stopped:
				self.update_notification("stop", pympe.player.currentInfo)

		if cfg_get('showOnTrackChange') and event == pympe.events.player_current_changed:
			self.update_notification("changed", pympe.player.currentInfo)

		if cfg_get('showOnTagChange') and event == pympe.events.player_current_updated:
			self.update_notification("changed", pympe.player.currentInfo)
