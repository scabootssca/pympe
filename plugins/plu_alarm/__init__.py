import pympe
import wx, time, pm_helpers

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, "Alarm Settings")

		self.plugin = plugin

		sizer = wx.BoxSizer(wx.VERTICAL)
		controlSizer = wx.GridBagSizer(5, 5)
		controlSizer.AddGrowableCol(0, 1)

		checkEnable = wx.CheckBox(self, 0, 'Wake Up Timer')
		choiceType = wx.Choice(self, 0, choices=['After A Duration', 'At A Specific Time'])
		labelTime = wx.StaticText(self, 0, 'Time:')
		textTime = wx.TextCtrl(self, 0, '00:00', size=(150, -1))
		textTime.military = False
		choiceTime = wx.Choice(self, 0, choices=['AM', 'PM'])
		textTime.choice = choiceTime
		checkDate = wx.CheckBox(self, 6, 'On A Certain Date:')
		textDate = wx.DatePickerCtrl(self, 0, dt=wx.DateTimeFromTimeT(_config['onDate']))
		checkVolume = wx.CheckBox(self, 1, 'Set New Volume:')
		sliderVolume = wx.Slider(self, 0, 50, 0, 100)
		checkFade = wx.CheckBox(self, 4, 'Fade In (seconds):')
		spinFade = wx.SpinCtrl(self, 0, min=0, max=3600)
		checkPlaylist = wx.CheckBox(self, 2, 'Use Playlist:')
		choicePlaylist = wx.Choice(self, 0, choices=[pympe.playlists.main.name]+[playlist.name for playlist in pympe.playlists.playlists])
		choicePlaylist.playlistUids = [pympe.playlists.main.uid]+[playlist.uid for playlist in pympe.playlists.playlists]

		checkEnable.SetValue(_config['onEnabled'])
		choiceType.SetSelection(_config['onType'])
		checkDate.SetValue(_config['onDateEnabled'])
		checkVolume.SetValue(_config['newVolumeEnabled'])
		sliderVolume.SetValue(_config['newVolume'])
		checkFade.SetValue(_config['fadeInEnabled'])
		spinFade.SetValue(_config['fadeInDuration'])
		checkPlaylist.SetValue(_config['newPlaylistEnabled'])
		playlist = pympe.playlists.get_playlist(_config['newPlaylist'])

		if playlist != None:
			choicePlaylist.SetSelection(choicePlaylist.playlistUids.index(playlist.uid))

		choiceTime.SetSelection(_config['onAmPm'])

		if choiceType.GetSelection():
			hours, minutes = (_config['onTime']/3600, _config['onTime']%3600/60)
			if not _config['onMilitary']:
				if hours == 0 and choiceTime.GetSelection() == 0: hours += 12
				elif hours > 12: hours -= 12
			else:
				choiceTime.Disable()

			textTime.SetValue('%02i:%02i'%(hours, minutes))
		else:
			textTime.SetValue('%02i:%02i'%(_config['onDuration']/3600, _config['onDuration']%3600/60))

		if not checkDate.GetValue(): textDate.Disable()
		if not checkVolume.GetValue(): sliderVolume.Disable()
		if not checkPlaylist.GetValue(): choicePlaylist.Disable()
		if not checkFade.GetValue(): spinFade.Disable()

		if not choiceType.GetSelection():
			checkDate.Disable()
			textDate.Disable()
			labelTime.SetLabel('In How Long?:')
			choiceTime.Disable()
			choiceTime.SetItems([])

		choiceType.Bind(wx.EVT_CHOICE, self.event_choice_type)
		choiceTime.Bind(wx.EVT_CHOICE, self.event_choice_time)
		textTime.Bind(wx.EVT_CHAR, self.event_text_time)
		textTime.Bind(wx.EVT_KILL_FOCUS, self.event_text_time_kill_focus)

		self.controlsOn = (choiceType, textTime, choiceTime, textDate, checkVolume, sliderVolume, checkPlaylist, choicePlaylist, checkFade, spinFade, labelTime, checkDate)

		if not checkEnable.GetValue():
			for x in self.controlsOn: x.Disable()

		controlSizer.Add(checkEnable,    (0, 0),  (1, 2), flag=wx.EXPAND)
		controlSizer.Add(choiceType,     (1, 0),  (1, 2), flag=wx.EXPAND)
		controlSizer.Add(labelTime,      (2, 0),  (1, 2), flag=wx.ALIGN_BOTTOM|wx.TOP, border=10)
		controlSizer.Add(textTime,       (3, 0),  (1, 1), flag=wx.EXPAND)
		controlSizer.Add(choiceTime,     (3, 1),  (1, 1), flag=wx.ALIGN_RIGHT)
		#controlSizer.Add(wx.StaticText(self, -1, 'Repeat Count:'), (4, 0), (1, 2), flag=wx.ALIGN_BOTTOM)
		#controlSizer.Add(wx.SpinCtrl(self, -1, min=0, max=100), (5, 0), (1, 2), flag=wx.EXPAND)
		controlSizer.Add(checkDate,      (4, 0),  (1, 2), flag=wx.EXPAND|wx.TOP, border=10)
		controlSizer.Add(textDate,       (5, 0),  (1, 2), flag=wx.EXPAND)
		controlSizer.Add(checkFade,      (6, 0),  (1, 2), flag=wx.EXPAND|wx.TOP, border=10)
		controlSizer.Add(spinFade,       (7, 0), (1, 2), flag=wx.EXPAND)
		controlSizer.Add(checkVolume,    (8, 0),  (1, 2), flag=wx.EXPAND|wx.TOP, border=10)
		controlSizer.Add(sliderVolume,   (9, 0),  (1, 2), flag=wx.EXPAND)
		controlSizer.Add(checkPlaylist,  (10, 0), (1, 2), flag=wx.EXPAND|wx.TOP, border=10)
		controlSizer.Add(choicePlaylist, (11, 0), (1, 2), flag=wx.EXPAND)

	# Line
		controlSizer.Add(wx.StaticLine(self, style=wx.LI_VERTICAL), (0, 2), (11, 1), wx.EXPAND|wx.LEFT|wx.RIGHT, 5)


	# Off Items
		checkEnable = wx.CheckBox(self, 3, 'Sleep Timer')
		choiceType = wx.Choice(self, 1, choices=['After A Duration', 'At A Specific Time'])
		labelTime = wx.StaticText(self, 1, 'Time:')
		textTime = wx.TextCtrl(self, 1, '00:00', size=(150, -1))
		textTime.military = False
		choiceTime = wx.Choice(self, 1, choices=['AM', 'PM'])
		textTime.choice = choiceTime
		checkDate = wx.CheckBox(self, 7, 'On A Certain Date:')
		textDate = wx.DatePickerCtrl(self, 1, dt=wx.DateTimeFromTimeT(_config['offDate']))
		checkFade = wx.CheckBox(self, 5, 'Fade Out (seconds):')
		spinFade = wx.SpinCtrl(self, 1, min=0, max=3600)

		choiceType.Bind(wx.EVT_CHOICE, self.event_choice_type)
		choiceTime.Bind(wx.EVT_CHOICE, self.event_choice_time)
		textTime.Bind(wx.EVT_CHAR, self.event_text_time)
		textTime.Bind(wx.EVT_KILL_FOCUS, self.event_text_time_kill_focus)

		checkEnable.SetValue(_config['offEnabled'])
		choiceType.SetSelection(_config['offType'])
		checkDate.SetValue(_config['offDateEnabled'])
		checkFade.SetValue(_config['fadeOutEnabled'])
		spinFade.SetValue(_config['fadeOutDuration'])
		choiceTime.SetSelection(_config['offAmPm'])

		if choiceType.GetSelection():
			hours, minutes = (_config['offTime']/3600, _config['offTime']%3600/60)
			if not _config['offMilitary']:
				if hours == 0 and choiceTime.GetSelection() == 0: hours += 12
				elif hours > 12: hours -= 12
			else:
				choiceTime.Disable()

			textTime.SetValue('%02i:%02i'%(hours, minutes))
		else:
			textTime.SetValue('%02i:%02i'%(_config['offDuration']/3600, _config['offDuration']%3600/60))

		self.controlsOff = (choiceType, textTime, choiceTime, textDate, labelTime, checkDate, checkFade, spinFade)

		if not checkEnable.GetValue():
			for x in self.controlsOff: x.Disable()

		if not checkDate.GetValue(): textDate.Disable()
		if not checkFade.GetValue(): spinFade.Disable()
		checkDate.Enable(choiceType.GetSelection())

		if not choiceType.GetSelection():
			checkDate.Disable()
			textDate.Disable()
			labelTime.SetLabel('In How Long?:')
			choiceTime.Disable()
			choiceTime.SetItems([])

		controlSizer.Add(checkEnable,    (0, 3),  (1, 2), flag=wx.EXPAND)
		controlSizer.Add(choiceType,     (1, 3),  (1, 2), flag=wx.EXPAND)
		controlSizer.Add(labelTime,      (2, 3),  (1, 2), flag=wx.ALIGN_BOTTOM)
		controlSizer.Add(textTime,       (3, 3),  (1, 1), flag=wx.EXPAND)
		controlSizer.Add(choiceTime,     (3, 4),  (1, 1), flag=wx.ALIGN_RIGHT)
		controlSizer.Add(checkDate,      (4, 3),  (1, 2), flag=wx.EXPAND|wx.TOP, border=10)
		controlSizer.Add(textDate,       (5, 3),  (1, 2), flag=wx.EXPAND)
		controlSizer.Add(checkFade,      (6, 3),  (1, 2), flag=wx.EXPAND|wx.TOP, border=10)
		controlSizer.Add(spinFade,       (7, 3), (1, 2), flag=wx.EXPAND)

		buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
		buttonSizer.Add(wx.Button(self, wx.ID_OK))
		buttonSizer.AddStretchSpacer()
		buttonSizer.Add(wx.Button(self, wx.ID_CANCEL))


		sizer.Add(controlSizer, 1, wx.EXPAND|wx.ALL, 10)
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND)
		sizer.Add(buttonSizer, 0, wx.EXPAND|wx.ALL, 10)

		self.Bind(wx.EVT_CHECKBOX, self.event_check_enable)
		self.Bind(wx.EVT_BUTTON, self.event_button)



		self.SetSizerAndFit(sizer)
		self.Show()

	def event_choice_time(self, event):
		if event.Id == 0: _config['onAmPm'] = event.Int
		else: _config['offAmPm'] = event.Int

	def event_choice_type(self, event):
		if event.Id == 0:
		# Save Time
			hours, minutes = (int(x) for x in self.controlsOn[1].GetValue().split(':'))

			if not self.controlsOn[1].military:
				if self.controlsOn[2].GetSelection() == 1: hours += 12
				elif hours == 12: hours = 0

			seconds = (hours*3600)+(minutes*60)

			if not self.controlsOn[0].GetSelection():
				_config['onTime'] = seconds
			else:
				_config['onDuration'] = seconds

		# Setup
			self.controlsOn[11].Enable(event.Int)
			self.controlsOn[3].Enable(event.Int and self.controlsOn[11].GetValue())

			self.controlsOn[10].SetLabel('Time:' if event.Int else 'In How Long?:')
			self.controlsOn[1].choice.Enable(event.Int)
			self.controlsOn[2].SetItems(['AM', 'PM'] if event.Int else [])
			if event.Int: self.controlsOn[2].SetSelection(_config['onAmPm'])

			# Duration, Specific
			if event.Int:
				hours, minutes = (_config['onTime']/3600, _config['onTime']%3600/60)

				if not _config['onMilitary']:
					if hours == 0 and self.controlsOn[2].GetSelection() == 0: hours += 12
					elif hours > 12: hours -= 12
				else:
					self.controlsOn[2].Disable()
				self.controlsOn[1].SetValue('%02i:%02i'%(hours, minutes))
			else:
				self.controlsOn[1].SetValue('%02i:%02i'%(_config['onDuration']/3600, _config['onDuration']%3600/60))

		else:
		# Save Time
			hours, minutes = (int(x) for x in self.controlsOff[1].GetValue().split(':'))

			if not self.controlsOff[1].military:
				if self.controlsOff[2].GetSelection() == 1: hours += 12
				elif hours == 12: hours = 0

			seconds = (hours*3600)+(minutes*60)

			if not self.controlsOff[0].GetSelection():
				_config['offTime'] = seconds
			else:
				_config['offDuration'] = seconds

		# Setup

			self.controlsOff[5].Enable(event.Int)
			self.controlsOff[3].Enable(event.Int and self.controlsOff[5].GetValue())

			self.controlsOff[4].SetLabel('Time:' if event.Int else 'In How Long?:')
			self.controlsOff[1].choice.Enable(event.Int)
			self.controlsOff[2].SetItems(['AM', 'PM'] if event.Int else [])
			if event.Int: self.controlsOff[2].SetSelection(_config['offAmPm'])

			# Duration, Specific
			if event.Int:
				hours, minutes = (_config['offTime']/3600, _config['offTime']%3600/60)

				if not _config['offMilitary']:
					if hours == 0 and self.controlsOff[2].GetSelection() == 0: hours += 12
					elif hours > 12: hours -= 12
				else:
					self.controlsOff[2].Disable()
				self.controlsOff[1].SetValue('%02i:%02i'%(hours, minutes))
			else:
				self.controlsOff[1].SetValue('%02i:%02i'%(_config['offDuration']/3600, _config['offDuration']%3600/60))


	def event_button(self, event):
		event.Skip()

		if event.Id == wx.ID_OK:
			_config['onType'] = self.controlsOn[0].GetSelection()
			_config['offType'] = self.controlsOff[0].GetSelection()
			_config['newVolume'] = self.controlsOn[5].GetValue()
			playlistSelection = self.controlsOn[7].GetSelection()
			if playlistSelection >= 0:
				_config['newPlaylist'] = self.controlsOn[7].playlistUids[self.controlsOn[7].GetSelection()]
			_config['fadeInDuration'] = self.controlsOn[9].GetValue()
			_config['fadeOutDuration'] = self.controlsOff[7].GetValue()
			_config['onDate'] = self.controlsOn[3].GetValue().GetTicks()
			_config['offDate'] = self.controlsOff[3].GetValue().GetTicks()

		# For Wake Timer
		# 1 = time, 2 = am/pm, 3 = date
			hours, minutes = (int(x) for x in self.controlsOn[1].GetValue().split(':'))

			if not self.controlsOn[1].military:
				if self.controlsOn[2].GetSelection() == 1: hours += 12
				elif hours == 12: hours = 0

			seconds = (hours*3600)+(minutes*60)

			if self.controlsOn[0].GetSelection():
				_config['onTime'] = seconds
			else:
				_config['onDuration'] = seconds

			_config['onMilitary'] = self.controlsOn[1].military

			self.plugin.enable_alarm(_config['onEnabled'])
			#print 'Line 504 Needs To Enable Alarm Clock If Needed'

		# For Sleep Timer
		# 1 = time, 2 = am/pm, 3 = date
			hours, minutes = (int(x) for x in self.controlsOff[1].GetValue().split(':'))

			if not self.controlsOff[1].military:
				if self.controlsOff[2].GetSelection() == 1: hours += 12
				elif hours == 12: hours = 0

			seconds = (hours*3600)+(minutes*60)

			if self.controlsOff[0].GetSelection():
				_config['offTime'] = seconds
			else:
				_config['offDuration'] = seconds

			_config['offMilitary'] = self.controlsOff[1].military

			self.plugin.enable_sleep_timer(_config['offEnabled'])
			#print 'Line 532 Needs To Enable Sleep Timer If Needed'

			self.Destroy()

	def event_text_time_kill_focus(self, event):
		value = event.EventObject.GetValue()

		if len(value) == 1:
			event.EventObject.SetValue('0%s:00'%value)
			event.EventObject.military = False
			event.EventObject.choice.Enable()
		elif len(value) == 2:
			event.EventObject.SetValue('%s:00'%value)
		elif len(value) == 3:
			event.EventObject.SetValue('%s00'%value)
		elif len(value) == 4:
			event.EventObject.SetValue('%s0'%value)

	def event_check_enable(self, event):
		if event.Id == 0:
			_config['onEnabled'] = bool(event.Int)
			for x in self.controlsOn:
				x.Enable(event.Int)

			if not self.controlsOn[4].GetValue(): self.controlsOn[5].Disable()
			if not self.controlsOn[6].GetValue(): self.controlsOn[7].Disable()
			if not self.controlsOn[8].GetValue(): self.controlsOn[9].Disable()
			if not self.controlsOn[11].GetValue(): self.controlsOn[3].Disable()

			if not self.controlsOn[0].GetSelection():
				self.controlsOn[3].Disable()
				self.controlsOn[11].Disable()
				self.controlsOn[2].Disable()


		elif event.Id == 1:
			_config['newVolumeEnabled'] = bool(event.Int)
			self.controlsOn[5].Enable(event.Int)

		elif event.Id == 2:
			_config['newPlaylistEnabled'] = bool(event.Int)
			self.controlsOn[7].Enable(event.Int)

		elif event.Id == 3:
			_config['offEnabled'] = bool(event.Int)
			for x in self.controlsOff:
				x.Enable(event.Int)

			if not self.controlsOff[5].GetValue(): self.controlsOff[3].Disable()
			if not self.controlsOff[6].GetValue(): self.controlsOff[7].Disable()

			if not self.controlsOff[0].GetSelection():
				self.controlsOff[3].Disable()
				self.controlsOff[5].Disable()


		elif event.Id == 4:
			_config['fadeInEnabled'] = bool(event.Int)
			self.controlsOn[9].Enable(event.Int)

		elif event.Id == 5:
			_config['fadeOutEnabled'] = bool(event.Int)
			self.controlsOff[7].Enable(event.Int)

		elif event.Id == 6:
			_config['onDateEnabled'] = bool(event.Int)
			self.controlsOn[3].Enable(event.Int)

		elif event.Id == 7:
			_config['offDateEnabled'] = bool(event.Int)
			self.controlsOff[3].Enable(event.Int)

	def event_text_time(self, event):
		uniChar = event.GetUniChar()
		char = event.GetKeyCode()
		textCtrl = event.EventObject
		insertionPoint = textCtrl.GetInsertionPoint()
		value = textCtrl.GetValue()
		timerType = self.controlsOff[0].GetSelection() if event.Id else self.controlsOn[0].GetSelection()

		if char in [wx.WXK_BACK, wx.WXK_DELETE]:
			return event.Skip()

		elif char == wx.WXK_TAB or char == wx.WXK_SPACE or char == wx.WXK_RETURN:
			if insertionPoint == 1:
				textCtrl.SetValue('0%s:'%value)
				textCtrl.SetInsertionPointEnd()
				textCtrl.military = False
			elif insertionPoint == 3:
				textCtrl.SetValue('%s00'%value)
				textCtrl.SetInsertionPointEnd()
			elif char != wx.WXK_SPACE:
				return event.Skip()

		elif unichr(uniChar) == 'A':
			textCtrl.choice.SetSelection(0)
			_config['offAmPm' if event.Id else 'onAmPm'] = 0
			return

		elif unichr(uniChar) == 'P':
			textCtrl.choice.SetSelection(1)
			_config['offAmPm' if event.Id else 'onAmPm'] = 1
			return


		elif char == 58:
			if textCtrl.GetSelection() == (0, len(value)):
				textCtrl.SetValue('00:')
				textCtrl.SetInsertionPointEnd()
				textCtrl.military = True
				textCtrl.choice.Enable(not textCtrl.military and timerType)

			elif len(value) == 1:
				textCtrl.SetValue('0%s:'%value)
				textCtrl.SetInsertionPointEnd()
				textCtrl.military = False
				textCtrl.choice.Enable(not textCtrl.military and timerType)

		textCtrl.choice.Enable(not textCtrl.military and timerType)

		# Translate the number
		if uniChar >= 48 and uniChar <= 57: number = uniChar-48
		elif uniChar >= 324 and uniChar <= 333: number = uniChar-324
		else: return

		if insertionPoint == 0:
			if number == 2:
				textCtrl.military = True
				textCtrl.choice.Enable(not textCtrl.military and timerType)

			elif number == 1:
				textCtrl.military = False
				textCtrl.choice.Enable(not textCtrl.military and timerType)

			elif number > 2:
				textCtrl.SetValue('0%i:'%number)
				textCtrl.SetInsertionPointEnd()
				textCtrl.military = False
				textCtrl.choice.Enable(not textCtrl.military and timerType)
				return

		elif insertionPoint == 1:
			if number > 3: return
			elif number > 2:
				textCtrl.military = True
				textCtrl.choice.Enable(not textCtrl.military and timerType)

			textCtrl.SetValue(value+'%i:'%number)
			textCtrl.SetInsertionPointEnd()
			return

		elif insertionPoint == 3:
			if number > 5: return

		elif insertionPoint > 4:
			return

		event.Skip()

class Plugin:
	def plu_settings(self, parent):
		SettingsDialog(self, parent)

	def plu_start(self):
		self.timerUpdate = wx.Timer()
		self.timerUpdate.Bind(wx.EVT_TIMER, self.event_timer_update)

		self.onTime  = None
		self.offTime = None

		self.uiButtonOn = wx.StaticText(pympe.ui.uiMain, -1, '12:16:32')
		self.uiButtonOff = wx.StaticText(pympe.ui.uiMain, -1, '1:00:00')

		self.uiLabels = (wx.StaticText(pympe.ui.uiMain, -1, 'Play in:'), wx.StaticText(pympe.ui.uiMain, -1, 'Stop in:'))

		self.box = wx.GridBagSizer(5, 5)
		self.box.Add(self.uiLabels[0], (0, 0), flag=wx.ALIGN_RIGHT)
		self.box.Add(self.uiLabels[1], (1, 0), flag=wx.ALIGN_RIGHT)
		self.box.Add(self.uiButtonOn, (0, 1), flag=wx.ALIGN_RIGHT)
		self.box.Add(self.uiButtonOff, (1, 1), flag=wx.ALIGN_RIGHT)
		pympe.ui.uiMain.uiSizerExtras.Add(self.box, 0)

		self.uiLabels[0].Hide()
		self.uiLabels[1].Hide()
		self.uiButtonOn.Hide()
		self.uiButtonOff.Hide()

	def enable_alarm(self, enabled):
		if not enabled:
			self.onTime = None
			self.uiButtonOn.Hide()
			self.uiLabels[0].Hide()
			pympe.ui.uiMain.uiSizerMain.Layout()
			return

		timeNow = time.time()

		# Duration
		if _config['onType'] == 0:
			self.onTime = timeNow+_config['onDuration']
		# Specific Time
		else:
			self.onTime = _config['onTime']
			dateTime = wx.DateTimeFromTimeT(timeNow)
			dayBegin = timeNow-(dateTime.Hour*3600)-(dateTime.Minute*60)-dateTime.Second

			# For a certain date
			if _config['onDateEnabled']:
				self.onTime += _config['onDate']

				# If it already happened then pass
				if self.onTime < timeNow: return pympe.output("Time in past, not setting")
			# Elif Already Happened, Do It Tomorrow
			elif dayBegin+self.onTime < timeNow:
				self.onTime += dayBegin+86400
			else:
				self.onTime += dayBegin

		# Display it
		self.uiButtonOn.SetLabel(pm_helpers.make_display_time(self.onTime-time.time()))
		(self.uiButtonOn.Show(), self.uiLabels[0].Show()) if enabled else (self.uiButtonOn.Hide(), self.uiLabels[0].Hide())
		pympe.ui.uiMain.uiSizerMain.Layout()
		self.timerUpdate.Start(1000)

	def enable_sleep_timer(self, enabled):
		if not enabled:
			self.offTime = None
			self.uiButtonOff.Hide()
			self.uiLabels[1].Hide()
			pympe.ui.uiMain.uiSizerMain.Layout()
			return

		timeNow = time.time()

		# Duration
		if _config['offType'] == 0:
			self.offTime = timeNow+_config['offDuration']
		# Specific Time
		else:
			self.offTime = _config['offTime']
			dateTime = wx.DateTimeFromTimeT(timeNow)
			dayBegin = timeNow-(dateTime.Hour*3600)-(dateTime.Minute*60)-dateTime.Second

			# For a certain date
			if _config['offDateEnabled']:
				self.offTime += _config['offDate']

				# If it already happened then pass
				if self.offTime < timeNow: return pympe.output("Time in past, not setting")
			# Elif Already Happened, Do It Tomorrow
			elif dayBegin+self.offTime < timeNow:
				self.offTime += dayBegin+86400
			else:
				self.offTime += dayBegin

		# Display it
		self.uiButtonOff.SetLabel(pm_helpers.make_display_time(self.offTime-time.time()))
		(self.uiButtonOff.Show(), self.uiLabels[1].Show()) if enabled else (self.uiButtonOff.Hide(), self.uiLabels[1].Hide())
		pympe.ui.uiMain.uiSizerMain.Layout()
		self.timerUpdate.Start(1000)

	#def enable_alarm_off(self, enabled, alarmType, hour, minute, date):
		#if alarmType: self.offTime = date+(hour*3600)+(minute*60)
		#else: self.offTime = time.time()+(hour*3600)+(minute*60)

		#self.uiButtonOff.SetLabel(pm_helpers.make_display_time(self.offTime-time.time()))
		#(self.uiButtonOff.Show(), self.uiLabels[1].Show()) if enabled else (self.uiButtonOff.Hide(), self.uiLabels[1].Hide())
		#pympe.ui.uiMain.uiSizerMain.Layout()

		#self.timerUpdate.Start(1000)

	#def enable_alarm_on(self, enabled, alarmType, hour, minute, date, volumeEnabled, volume, playlist):
		#if alarmType: self.onTime = date+(hour*3600)+(minute*60)
		#else: self.onTime = time.time()+(hour*3600)+(minute*60)

		#self.uiButtonOn.SetLabel(pm_helpers.make_display_time(self.onTime-time.time()))
		#(self.uiButtonOn.Show(), self.uiLabels[0].Show()) if enabled else (self.uiButtonOn.Hide(), self.uiLabels[0].Hide())
		#pympe.ui.uiMain.uiSizerMain.Layout()

		#self.timerUpdate.Start(1000)

	def plu_stop(self, shutdown):
		self.timerUpdate.Stop()
		self.timerUpdate.Destroy()

		self.box.Clear(True)
		pympe.ui.uiMain.uiSizerExtras.Remove(self.box)
		pympe.ui.uiMain.uiSizerMain.Layout()

	@pm_helpers.unthread
	def event_timer_update(self, event):
		timeNow = time.time()

		if self.onTime:
			timeLeft = self.onTime-timeNow

			if timeLeft <= 0:
				self.onTime = None
				self.uiButtonOn.Hide()
				self.uiLabels[0].Hide()

				if not self.offTime: self.timerUpdate.Stop()

				if _config['newVolumeEnabled']:
					volume = _config['newVolume']
					pympe.events.player_set_volume.send(volume*.01)
					pympe.ui.uiMain.uiControls["volume"].SetValue(volume)

				if _config['newPlaylistEnabled']:
					pympe.events.playlist_select.send(_config['newPlaylist'])
					pympe.events.player_play.send(pympe.playlists.get_playlist(_config['newPlaylist']).get_current())

				else:
					pympe.events.player_play.send()
			else:
				self.uiButtonOn.SetLabel(pm_helpers.make_display_time(self.onTime-timeNow))

		if self.offTime:
			timeLeft = self.offTime-timeNow

			if timeLeft <= 0:
				self.offTime = None
				self.uiButtonOff.Hide()
				self.uiLabels[1].Hide()

				if not self.onTime: self.timerUpdate.Stop()

				pympe.events.player_stop.send()
			else:
				self.uiButtonOff.SetLabel(pm_helpers.make_display_time(self.offTime-timeNow))
