import pympe, gst, wx

__plugin_name__ = "EffectTv"
__plugin_version__ = 0.1
__plugin_description__ = "Realtime Video Effects"
__plugin_has_settings__ = True

class Plugin:
	def __plugin_start__(self):
		#http://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-good-plugins/html/gst-plugins-good-plugins-plugin-effectv.html
		pass

	def __plugin_stop__(self, shutdown):
		pass
		
	def __plugin_settings__(self):
		pass
		
	def __plugin_firstrun__(self): pass
