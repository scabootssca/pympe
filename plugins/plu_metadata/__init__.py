# -*- coding: utf-8 -*-
import pympe, wx, pm_language, pm_helpers, pm_mediafile, os
import re
import wx.lib.mixins.listctrl as listmix

try:
	import musicbrainzngs as musicbrainz
except ImportError:
	musicbrainz = None


def tag_entry(parent, tag, width=400):
	sizer = wx.GridBagSizer()

	multiline = True if tag == "comment" else False

	uiLabel = wx.StaticText(parent, -1, pm_language.readableTags[tag]+":")
	uiText = wx.TextCtrl(parent, -1, "", size=(width, (80 if multiline else -1)), style=(wx.TE_MULTILINE if multiline else 0))
	uiButton = wx.Button(parent, -1, "     ", style=wx.BU_EXACTFIT)

	parent.entries[tag] = (uiText, uiButton)
	uiText.modified = False
	uiButton.applyAll = None
	uiText.tag = uiButton.tag = tag

	uiText.Bind(wx.EVT_TEXT, parent.event_text_modified)
	uiButton.Bind(wx.EVT_BUTTON, parent.event_button_apply_all)
	uiText.Bind(wx.EVT_KEY_DOWN, parent.event_key_down)

	sizer.Add(uiLabel, (0, 0), (1, 2))
	sizer.Add(uiText, (1, 0), flag=wx.EXPAND)
	sizer.Add(uiButton, (1, 1), flag=wx.ALIGN_CENTER)

	return sizer

def number_entry(parent, tag, width=400):
	sizer = wx.GridBagSizer()

	uiLabel = wx.StaticText(parent, -1, pm_language.readableTags[tag]+":")
	uiText = wx.SpinCtrl(parent, -1, size=(width, 0))
	uiButton = wx.Button(parent, -1, "     ", style=wx.BU_EXACTFIT)

	parent.entries[tag] = (uiText, uiButton)
	uiText.modified = False
	uiButton.applyAll = None
	uiText.tag = uiButton.tag = tag

	uiText.Bind(wx.EVT_TEXT, parent.event_text_modified)
	uiButton.Bind(wx.EVT_BUTTON, parent.event_button_apply_all)
	uiText.Bind(wx.EVT_KEY_DOWN, parent.event_key_down)

	sizer.Add(uiLabel, (0, 0), (1, 2))
	sizer.Add(uiText, (1, 0), flag=wx.EXPAND)
	sizer.Add(uiButton, (1, 1), flag=wx.ALIGN_CENTER)

	return sizer


def boolean_entry(parent, tag, width=400):
	sizer = wx.GridBagSizer()

	def event_toggle_modified(event):
		obj = event.GetEventObject()
		#other = obj.other

		#if obj.GetValue() or other.GetValue():
		#	obj.SetBackgroundColour((255, 250, 250))
		#	other.SetBackgroundColour((255, 250, 250))


		#other.SetValue(False)
		obj.SetValue(not obj.current)
		parent.event_text_modified(event)

	uiLabel = wx.StaticText(parent, -1, pm_language.readableTags[tag]+":")
	uiText = wx.Button(parent, -1, "True", size=(width, 0))
	#uiText2 = wx.Button(parent, -1, "False", size=(width/2, -1))
	uiButton = wx.Button(parent, -1, "     ", style=wx.BU_EXACTFIT)

	def SetBackgroundColour(*args, **kwargs):
		pass

	def SetValue(value):
		uiText.current = value
		if value: uiText.SetLabel('Is True')
		else: uiText.SetLabel('Is False')

	def GetValue():
		return uiText.current

	#uiText.SetBackgroundColour = SetBackgroundColour
	uiText.SetValue = SetValue
	uiText.GetValue = GetValue
	uiText.current = False
	#uiText.other = uiText2
	#uiText2.other = uiText

	parent.entries[tag] = (uiText, uiButton)
	uiText.modified = False
	uiButton.applyAll = None
	#uiText.tag = uiText2.tag = uiButton.tag = tag
	uiText.tag = uiButton.tag = tag

	uiText.Bind(wx.EVT_BUTTON, event_toggle_modified)
	#uiText2.Bind(wx.EVT_BUTTON, event_toggle_modified)
	uiButton.Bind(wx.EVT_BUTTON, parent.event_button_apply_all)

	sizer.Add(uiLabel, (0, 0), (1, 2))
	sizer.Add(uiText, (1, 0), flag=wx.EXPAND)
	#sizer.Add(uiText2, (1, 1), flag=wx.EXPAND)
	sizer.Add(uiButton, (1, 1), flag=wx.ALIGN_CENTER)

	return sizer

def art_entry(parent):
	uiBitmap = wx.StaticBitmap(parent, -1, size=(250, 250))
	uiLabel = wx.StaticText(parent, -1, 'N/A x N/A')
	uiButton = wx.Button(parent, -1, "     ", style=wx.BU_EXACTFIT)

	parent.entries["art"] = (uiBitmap, uiButton, uiLabel)
	uiBitmap.modified = False
	uiBitmap.hasImage = False
	uiButton.applyAll = None
	uiButton.tag = "art"

	uiBitmap.Bind(wx.EVT_RIGHT_UP, parent.event_bitmap_right)
	uiButton.Bind(wx.EVT_BUTTON, parent.event_button_apply_all)

	sizerH = wx.BoxSizer()
	sizerV = wx.BoxSizer(wx.VERTICAL)
	sizerV.Add(uiBitmap, flag=wx.ALIGN_CENTER_VERTICAL)
	sizerV.Add(uiLabel, flag=wx.ALIGN_CENTER)
	sizerH.Add(sizerV, flag=wx.ALIGN_CENTER_VERTICAL)
	sizerH.Add(uiButton, flag=wx.ALIGN_CENTER_VERTICAL)

	return sizerH

def lyric_entry(parent):
	sizer = wx.GridBagSizer()
	sizer.AddGrowableRow(1)

	uiLabel = wx.StaticText(parent, -1, "Lyrics:")
	uiText = wx.TextCtrl(parent, -1, "", size=(250, 0), style=wx.TE_MULTILINE)
	uiButton = wx.Button(parent, -1, "     ", style=wx.BU_EXACTFIT)

	parent.entries["lyrics"] = (uiText, uiButton)
	uiText.modified = False
	uiButton.applyAll = None
	uiText.tag = uiButton.tag = "lyrics"

	uiText.Bind(wx.EVT_TEXT, parent.event_text_modified)
	uiButton.Bind(wx.EVT_BUTTON, parent.event_button_apply_all)
	uiText.Bind(wx.EVT_KEY_DOWN, parent.event_key_down)

	sizer.Add(uiLabel, (0, 0), (1, 2))
	sizer.Add(uiText, (1, 0), flag=wx.EXPAND)
	sizer.Add(uiButton, (1, 1), flag=wx.ALIGN_CENTER_VERTICAL)

	return sizer

class AdvancedSettings(wx.Panel):
	def __init__(self, parent):
		#wx.Dialog.__init__(self, main, -1, "Advanced Settings")
		wx.Panel.__init__(self, parent)
		self.main = parent
		self.hasLibrary = False
		parent.entries["advancedSettings"] = self

		uiSizer = wx.GridBagSizer(5, 5)

		self.uiChoiceEqPreset = wx.Choice(self, -1, choices=['None'])
		self.uiCheckEqPreset = wx.CheckBox(self, 0, 'Use Equalizer Preset')
		self.uiCheckStartTime = wx.CheckBox(self, 1, 'Start Time:')
		self.uiCheckStopTime = wx.CheckBox(self, 2, 'Stop Time:')
		self.uiTextStartTime = wx.TextCtrl(self, -1, '0:00')
		self.uiTextStopTime = wx.TextCtrl(self, -1, '0:00')
		self.uiCheckRemember = wx.CheckBox(self, 3, 'Remember Playback Position')
		self.uiCheckShuffleSkip = wx.CheckBox(self, 4, 'Skip When Shuffling')
		#uiButtonOk = wx.Button(self, wx.ID_OK)
		#uiButtonClose = wx.Button(self, wx.ID_CLOSE)

		uiSizer.Add(self.uiCheckEqPreset, (0, 1), (1, 2), flag=wx.ALIGN_CENTER_VERTICAL)
		uiSizer.Add(self.uiChoiceEqPreset, (1, 1), (1, 2))

		uiSizer.Add(self.uiCheckStartTime, (2, 1), flag=wx.ALIGN_CENTER_VERTICAL)
		uiSizer.Add(self.uiTextStartTime, (2, 2))

		uiSizer.Add(self.uiCheckStopTime, (3, 1), flag=wx.ALIGN_CENTER_VERTICAL)
		uiSizer.Add(self.uiTextStopTime, (3, 2))

		uiSizer.Add(self.uiCheckRemember, (4, 1), (1, 3))
		uiSizer.Add(self.uiCheckShuffleSkip, (5, 1), (1, 3))

		#uiSizer.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), (6, 0), (1, 4), wx.EXPAND)
		#uiSizer.Add(uiButtonOk, (7, 0), (1, 1))
		#uiSizer.Add(uiButtonClose, (7, 3), (1, 1), wx.ALIGN_RIGHT)

		self.populate()

		#uiButtonOk.Bind(wx.EVT_BUTTON, self.event_button_ok)
		#uiButtonClose.Bind(wx.EVT_BUTTON, self.event_button_close)
		self.Bind(wx.EVT_CHECKBOX, self.event_check_enable)

		self.uiTextStartTime.Bind(wx.EVT_TEXT, self.event_text_validate)
		self.uiTextStopTime.Bind(wx.EVT_TEXT, self.event_text_validate)

		box = wx.BoxSizer()
		box.Add(uiSizer, 0, wx.ALL, 10)
		#self.SetSizerAndFit(box)
		self.SetSizer(box)
		self.Show()

	def event_text_validate(self, event):
		string = event.GetString()

		if string:
			if string[-1] == ':':
				if len(string) == 1 or ':' in string[:-1]:
					event.GetEventObject().ChangeValue(string[:-1])
			elif not string[-1] in '0123456789': event.GetEventObject().ChangeValue(string[:-1])
			elif ':' in string and len(string[string.index(':'):]) > 3:
				event.GetEventObject().ChangeValue(string[:-1])

		event.Skip()

	def event_check_enable(self, event):
		enabled = event.Int

		if event.Id == 0:
			self.uiChoiceEqPreset.Enable(enabled)
		elif event.Id == 1:
			self.uiTextStartTime.Enable(enabled)
		elif event.Id == 2:
			self.uiTextStopTime.Enable(enabled)

	def populate(self):
		self.hasLibrary = self.main.library != -1

		self.uiChoiceEqPreset.Enable(self.hasLibrary)
		self.uiCheckEqPreset.Enable(self.hasLibrary)
		self.uiCheckStartTime.Enable(self.hasLibrary)
		self.uiCheckStopTime.Enable(self.hasLibrary)
		self.uiTextStartTime.Enable(self.hasLibrary)
		self.uiTextStopTime.Enable(self.hasLibrary)
		self.uiCheckRemember.Enable(self.hasLibrary)
		self.uiCheckShuffleSkip.Enable(self.hasLibrary)

		if not self.hasLibrary: return


		self.uri = self.main.uris[self.main.index]
		self.trackId = self.main.trackIds[self.main.index]

		changes = self.main.changes[self.main.index]

		self.varUsePlaybackPosition = changes['usePlaybackPosition'] if 'usePlaybackPosition' in changes else self.main.library.get_single_track_single_value(self.trackId, "usePlaybackPosition")
		self.varUseStartTime = changes['useStartTime'] if 'useStartTime' in changes else self.main.library.get_single_track_single_value(self.trackId, "useStartTime")
		self.varStartTime = changes['startTime'] if 'startTime' in changes else self.main.library.get_single_track_single_value(self.trackId, "startTime")
		self.varUseStopTime = changes['useStopTime'] if 'useStopTime' in changes else self.main.library.get_single_track_single_value(self.trackId, "useStopTime")
		self.varStopTime = changes['stopTime'] if 'stopTime' in changes else self.main.library.get_single_track_single_value(self.trackId, "stopTime")
		self.varSkipOnShuffle = changes['skipOnShuffle'] if 'skipOnShuffle' in changes else self.main.library.get_single_track_single_value(self.trackId, "skipOnShuffle")
		self.varUseEqualizerPreset = changes['useEqualizerPreset'] if 'useEqualizerPreset' in changes else self.main.library.get_single_track_single_value(self.trackId, "useEqualizerPreset")
		self.varEqualizerPreset = changes['equalizerPreset'] if 'equalizerPreset' in changes else self.main.library.get_single_track_single_value(self.trackId, "equalizerPreset")

		self.varStartTime = '%i:%i'%(self.varStartTime/60, self.varStartTime%60)
		self.varStopTime = '%i:%i'%(self.varStopTime/60, self.varStopTime%60)

		eqPlugin = pympe.plugins.get_plugin(1323703538, returnDict=True)
		if eqPlugin != False:
			presets = pympe.config[eqPlugin["path"]]['presets']
			self.uiChoiceEqPreset.presetIds = sorted(presets.keys(), key=lambda x: presets[x][0])
			self.uiChoiceEqPreset.SetItems([presets[x][0] for x in self.uiChoiceEqPreset.presetIds])
			try: self.uiChoiceEqPreset.SetSelection(self.uiChoiceEqPreset.presetIds.index(self.varEqualizerPreset))
			except: self.uiChoiceEqPreset.SetSelection(0)

		self.uiChoiceEqPreset.Enable((True if eqPlugin else False) and self.varUseEqualizerPreset)
		self.uiCheckEqPreset.Enable(True if eqPlugin else False)
		self.uiCheckEqPreset.SetValue(self.varUseEqualizerPreset)

		self.uiCheckRemember.SetValue(self.varUsePlaybackPosition)

		self.uiCheckStartTime.SetValue(self.varUseStartTime)
		self.uiTextStartTime.Enable(self.varUseStartTime)
		self.uiTextStartTime.ChangeValue(self.varStartTime)

		self.uiCheckStopTime.SetValue(self.varUseStopTime)
		self.uiTextStopTime.Enable(self.varUseStopTime)
		self.uiTextStopTime.ChangeValue(self.varStopTime)

		self.uiCheckShuffleSkip.SetValue(self.varSkipOnShuffle)

	def store_changes(self):
		if not self.hasLibrary: return

		changes = {}

		val = self.uiCheckEqPreset.GetValue()
		if self.varUseEqualizerPreset != val:
			changes['useEqualizerPreset'] = val
			changes['equalizerPreset'] = self.uiChoiceEqPreset.presetIds[self.uiChoiceEqPreset.GetSelection()]

		val = self.uiCheckRemember.GetValue()
		if self.varUsePlaybackPosition != val:
			changes['usePlaybackPosition'] = val

		val = self.uiCheckStartTime.GetValue()


		if self.varUseStartTime != val:
			changes['useStartTime'] = val

		val = self.uiTextStartTime.GetValue()
		if self.varStartTime != val:
			seconds = val.split(':')
			if len(seconds) == 1: seconds = [0, seconds[0]]
			seconds = int(seconds[0])*60+int(seconds[1])
			changes['startTime'] = seconds

		val = self.uiCheckStopTime.GetValue()

		if self.varUseStopTime != val:
			changes['useStopTime'] = val

		val = self.uiTextStopTime.GetValue()
		if self.varStopTime != val:
			seconds = val.split(':')
			if len(seconds) == 1: seconds = [0, seconds[0]]
			seconds = int(seconds[0])*60+int(seconds[1])
			changes['stopTime'] = seconds

		val = self.uiCheckShuffleSkip.GetValue()
		if self.varSkipOnShuffle != val:
			changes['skipOnShuffle'] = val

		if changes and not self.main.changes[self.main.index]:
			self.main.changes[self.main.index] = changes
		else:
			for key in changes:
				self.main.changes[self.main.index][key] = changes[key]

		#self.Destroy()

	#def event_button_close(self, event):
	#	self.Destroy()

class AutoWidthList(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
	def __init__(self, parent, *args, **kwargs):
		wx.ListCtrl.__init__(self, parent, *args, **kwargs)
		listmix.ListCtrlAutoWidthMixin.__init__(self)
		self.parent = parent

	def update(self):
		self.SetItemCount(len(self.parent.results[0]))

	def OnGetItemText(self, row, column):
		#print 'Get %i:%i %s'%(row, column, self.parent.results[column][row])
		return self.parent.results[column][row]

class AutoNumberDialog(wx.Dialog):
	def __init__(self, parent, results=[[],[],[],[]]):
		wx.Dialog.__init__(self, parent, -1, "Autonumber", size=(500, 600), style=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER)
		self.parent = parent

		# Variables
		self.results = [[], [], [], [], []]

		sizerMain = wx.BoxSizer(wx.VERTICAL)

		# Track Display
		self.listInfo = AutoWidthList(self, -1, style=wx.LC_REPORT|wx.LC_VIRTUAL)
		self.listInfo.InsertColumn(0, '#', width=30)
		self.listInfo.InsertColumn(1, 'Total', width=40)
		self.listInfo.InsertColumn(2, 'Filename', width=450)
		self.listInfo.InsertColumn(3, 'Path', width=600, format=wx.LIST_FORMAT_RIGHT)

		self.checkResetPerDirectory = wx.CheckBox(self, -1, 'Reset for each directory.')
		self.checkResetPerDirectory.SetValue(cfg_get('resetPerDirectory'))
		self.checkResetPerDirectory.Bind(wx.EVT_CHECKBOX, self.event_checkbox)

		sizerMain.Add(self.listInfo, 1, wx.EXPAND)
		sizerMain.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)
		sizerMain.Add(self.checkResetPerDirectory, 0, wx.EXPAND)
		sizerMain.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

		# Buttons
		sizerButtons = wx.BoxSizer()
		sizerButtons.Add(wx.Button(self, wx.ID_SAVE), flag=wx.ALIGN_LEFT)
		sizerButtons.AddStretchSpacer()
		sizerButtons.Add(wx.Button(self, wx.ID_CANCEL), flag=wx.ALIGN_RIGHT)
		sizerMain.Add(sizerButtons, 0, wx.EXPAND)

		self.Bind(wx.EVT_BUTTON, self.event_button)

		box = wx.BoxSizer()
		box.Add(sizerMain, 1, wx.EXPAND|wx.ALL, 5)
		self.SetSizer(box)

		self.update()

	def event_extraction_enter(self, event):
		self.extractionRegex = event.GetString()
		self.update()

	def event_checkbox(self, event):
		cfg_set('resetPerDirectory', self.checkResetPerDirectory.IsChecked())
		self.update()

	def event_button(self, event):
		if event.Id == wx.ID_SAVE:
			self.EndModal(wx.ID_SAVE)
		else:
			event.Skip()

	def update(self):
		# Sort them by uri keeping in order with track numbers if they exists before the title
		def sort_key(entry):
			match = re.match('^(.*?)([0-9]+)(.*)$', entry[1])

			if match and match.group(2):
				return '%10i%s'%(int(match.group(2)), entry[1])

			return entry[1]

		groups = {}
		self.results = [[], [], [], [], []]
		resetPerDirectory = self.checkResetPerDirectory.IsChecked()
		total = len(self.parent.uris)
		runningIndex = 0

		for index, uri in enumerate(self.parent.uris):
			(path, filename) = uri.rsplit(os.sep, 1)

			try: groups[path].append((index, filename))
			except KeyError: groups[path] = [(index, filename)]

		for group in groups.keys():
			if resetPerDirectory:
				total = len(groups[group])

			for index, track in enumerate(sorted(groups[group], key=sort_key)):
				self.results[0].append(index+1+runningIndex)  # Tracknumber
				self.results[1].append(total)    # Tracktotal
				self.results[2].append(track[1]) # Title
				self.results[3].append(os.sep.join(reversed(group.split(os.sep)[2:])))    # Path
				self.results[4].append(track[0]) # Id

			if not resetPerDirectory:
				runningIndex += index

		# Cleanup?
		del groups
		del resetPerDirectory
		del total
		del runningIndex

		self.listInfo.update()

class GuessTagsDialog(wx.Dialog):
	def __init__(self, parent, results=[[],[],[],[]]):
		wx.Dialog.__init__(self, parent, -1, "Guess Tags", size=(500, 600), style=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER)
		self.parent = parent

		# Variables
		self.results = [self.parent.uris]
		self.columnKeys = []

		sizerMain = wx.BoxSizer(wx.VERTICAL)

		# Track Display
		self.listInfo = AutoWidthList(self, -1, style=wx.LC_REPORT|wx.LC_VIRTUAL)
		#self.listInfo.InsertColumn(0, 'Uri', width=450)
		#self.listInfo.InsertColumn(1, 'Path', width=600, format=wx.LIST_FORMAT_RIGHT)

		self.textFormat = wx.TextCtrl(self, -1, '%tracknumber% %title%')
		self.textFormat.Bind(wx.EVT_TEXT, self.event_text)
		self.textUri = wx.StaticText(self, -1, self.parent.uris[0])

		self.labelFormat = wx.StaticText(self, -1, '')

		sizerMain.Add(self.listInfo, 1, wx.EXPAND)
		sizerMain.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)
		sizerMain.Add(self.labelFormat, 0, wx.EXPAND)
		sizerMain.Add(self.textFormat, 0, wx.EXPAND)
		sizerMain.Add(self.textUri, 0, wx.EXPAND|wx.ALIGN_RIGHT)
		sizerMain.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

		# Buttons
		sizerButtons = wx.BoxSizer()
		sizerButtons.Add(wx.Button(self, wx.ID_SAVE), flag=wx.ALIGN_LEFT)
		sizerButtons.AddStretchSpacer()
		sizerButtons.Add(wx.Button(self, wx.ID_CANCEL), flag=wx.ALIGN_RIGHT)
		sizerMain.Add(sizerButtons, 0, wx.EXPAND)

		self.Bind(wx.EVT_BUTTON, self.event_button)

		box = wx.BoxSizer()
		box.Add(sizerMain, 1, wx.EXPAND|wx.ALL, 5)
		self.SetSizer(box)

		self.update(self.textFormat.GetValue())

	def event_button(self, event):
		if event.Id == wx.ID_SAVE:
			self.EndModal(wx.ID_SAVE)
		else:
			event.Skip()

	def event_text(self, event):
		self.update(event.String)
		event.Skip()

	def extract_info(self, uri, matchString):
		try:
			uri = '/'.join(uri.split('/')[-matchString.count('/')-1:]).rsplit('.', 1)[0]
			regex = '%s$'%re.sub(r'\\\%(.*?)\\\%', r'(?P<\1>.+?)', re.escape(matchString))
			regex = regex.replace('\\*', '.*?')

			match = re.match(regex, uri)
		except:
			match = None

		if match: return dict((k, v) for k, v in match.groupdict().iteritems() if k in pympe.library.dbKeys)
		return {}

	def update(self, matchString=None):
		matches = self.extract_info(self.parent.uris[0], matchString)
		#print '\n	Update: %s\n	Matches: %s\n	String:%s\n'%(self.parent.uris[0], matches, matchString)
		if matches:
			columnKeys = sorted(matches.keys())

			self.labelFormat.SetLabel('\n'.join(['%s: %s'%(pm_language.readableTags[key], matches[key]) for key in columnKeys]))

			if columnKeys != self.columnKeys:
				self.columnKeys = columnKeys

				self.listInfo.ClearAll()

				index = -1
				for index, key in enumerate(columnKeys):
					self.listInfo.InsertColumn(index, pm_language.libraryColumnHeaders[key])

				self.listInfo.InsertColumn(index+1, 'Uri')

			self.results = []
			for x in range(len(columnKeys)+1): self.results.append([])

			# Fill them
			for uri in self.parent.uris:
				matches = self.extract_info(uri, matchString)

				for index, key in enumerate(columnKeys):
					if key in matches: self.results[index].append(matches[key])
					else: self.results[index].append('')

				self.results[index+1].append(uri)
		else:
			self.labelFormat.SetLabel('Invalid')

			self.columnKeys = []
			self.results = [self.parent.uris]

			#for x in reversed(range(self.listInfo.GetColumnCount()-1)):
				#self.listInfo.DeleteColumn(x)
			self.listInfo.ClearAll()
			self.listInfo.InsertColumn(0, 'Uri')

		self.Layout()
		self.listInfo.update()

class AddBookmarkDialog(wx.Dialog):
	def __init__(self, parent):
		wx.Dialog.__init__(self, None, -1, "Add Bookmark")


		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.Add(wx.TextCtrl(self, -1, 'Description', style=wx.TE_MULTILINE), 0, wx.EXPAND|wx.LEFT|wx.RIGHT, 5)

		sizer2 = wx.BoxSizer(wx.HORIZONTAL)
		sizer2.Add(wx.StaticText(self, -1, 'Minutes:'), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer2.Add(wx.SpinCtrl(self, -1))
		sizer2.Add(wx.StaticText(self, -1, 'Seconds:'), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer2.Add(wx.SpinCtrl(self, -1))
		sizer.Add(sizer2, 0, wx.ALL^wx.BOTTOM, 5)

		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.ALL^wx.BOTTOM, 5)

		sizer2 = wx.BoxSizer(wx.HORIZONTAL)
		sizer2.Add(wx.Button(self, wx.ID_ADD))
		sizer2.AddStretchSpacer()
		sizer2.Add(wx.Button(self, wx.ID_CANCEL))
		sizer.Add(sizer2, 1, wx.ALL|wx.EXPAND, 5)

		self.SetSizerAndFit(sizer)

		self.ShowModal()

class BookmarkEditor(wx.Dialog):
	def __init__(self, parent, results=[[],[],[],[]]):
		wx.Dialog.__init__(self, parent, -1, "Edit Bookmarks", size=(500, 600), style=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER)
		self.parent = parent

		# Variables
		self.results = [[], []]

		sizerMain = wx.BoxSizer(wx.VERTICAL)

		# Track Display
		self.listInfo = AutoWidthList(self, -1, style=wx.LC_REPORT|wx.LC_VIRTUAL)
		self.listInfo.InsertColumn(0, 'Time', width=50)
		self.listInfo.InsertColumn(1, 'Description', format=wx.LIST_FORMAT_RIGHT)

		#self.checkResetPerDirectory = wx.CheckBox(self, -1, 'Reset for each directory.')
		#self.checkResetPerDirectory.SetValue(cfg_get('resetPerDirectory'))
		#self.checkResetPerDirectory.Bind(wx.EVT_CHECKBOX, self.event_checkbox)


		sizerButtons = wx.BoxSizer()
		sizerButtons.Add(wx.Button(self, wx.ID_ADD))
		sizerButtons.Add(wx.Button(self, wx.ID_DELETE))
		sizerMain.Add(sizerButtons, 0, wx.EXPAND)

		sizerMain.Add(self.listInfo, 1, wx.EXPAND)
		#sizerMain.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)
		#sizerMain.Add(self.checkResetPerDirectory, 0, wx.EXPAND)
		sizerMain.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

		# Buttons
		sizerButtons = wx.BoxSizer()
		sizerButtons.Add(wx.Button(self, wx.ID_SAVE), flag=wx.ALIGN_LEFT)
		sizerButtons.AddStretchSpacer()
		sizerButtons.Add(wx.Button(self, wx.ID_CANCEL), flag=wx.ALIGN_RIGHT)
		sizerMain.Add(sizerButtons, 0, wx.EXPAND)

		self.Bind(wx.EVT_BUTTON, self.event_button)

		box = wx.BoxSizer()
		box.Add(sizerMain, 1, wx.EXPAND|wx.ALL, 5)
		self.SetSizer(box)

	def event_button(self, event):
		if event.Id == wx.ID_ADD:
			AddBookmarkDialog(self)

		event.Skip()

class MetadataEditor(wx.Dialog):
	def __init__(self, uris=None, trackIds=None, libraries=None, library=None):
		wx.Dialog.__init__(self, pympe.ui.uiMain, -1, 'Metadata Editor')

		self.index = 0
	# Figure up the libraries, ids, uris
		if library:
			self.library = library
			self.libraries = []
		elif libraries:
			if libraries[self.index] == -1:
				self.library = -1
			else:
				self.library = pympe.libraries[libraries[self.index]]
			self.libraries = libraries
		else:
			self.library = pympe.library
			self.libraries = []

		if uris == None:
			if libraries:
				self.uris = [self.libraries[index].get_single_track_single_value(trackId, 'uri') for index, trackId in enumerate(trackIds if trackIds != None else uris)]
			else:
				self.uris = self.library.get_multi_track_single_value(trackIds if trackIds != None else uris, 'uri')
		else: self.uris = uris
		if trackIds == None: self.trackIds = [None]*len(uris)
		else: self.trackIds = trackIds

	# Variables
		self.entries = {}  # {'artist':[textctrl, togglebutton]}, 'album':[textctrl, togglebutton]}
		self.extraTags = ["year"]
		self.changes =  [{} for x in range(len(self.uris))]# [{'title':'new title'}, {'artist':'new artist', 'title':'newtitle', 'tracknumber':5}]

		self.currentInfo = {}
		self.mediaFile = None
		self.currentMediaType = None

		self.defaultTagsAudio = {'title':'', 'album':'', 'artist':'', 'tracknumber':0, 'tracktotal':0, 'albumartist':'',
							'discnumber':0, 'disctotal':0, 'genre':'', 'lyrics':'', 'art':'', 'year':'', 'comment':'',
							'compilation':False, 'composer':''}

		self.defaultTagsVideo = {'title':'', 'discnumber':0, 'disctotal':0, 'genre':'', 'year':'', 'comment':'',
								'series':'', 'season':'', 'episodenumber':0, 'episodetotal':0}

		self.tagChoices = self.defaultTagsAudio.keys()

		self.tagOrder = ['title', 'series', 'albumartist', 'artist', 'composer', 'season', 'album', 'compilation',
						'episodenumber', 'episodetotal', 'tracknumber', 'tracktotal', 'discnumber', 'disctotal',
						'genre', 'year', 'comment', 'lyrics', 'art']

	# Layout
		# The Sizers
		sizerMain = wx.BoxSizer(wx.VERTICAL)
		sizerControls = wx.BoxSizer(wx.HORIZONTAL)
		sizerSaveCancel = wx.BoxSizer(wx.HORIZONTAL)
		self.sizerTagEntries = wx.GridBagSizer(5, 5)

		# Info Display
		self.uiLabelCurrent = wx.StaticText(self, -1)
		self.uiLabelCurrentUri = wx.StaticText(self, -1)
		self.uiGaugeCurrent = wx.Gauge(self, -1, size=(-1, 10))

		# Backward | Preferences | Forward
		self.uiButtonBackward = wx.Button(self, wx.ID_BACKWARD)
		uiButtonPreferences = wx.Button(self, wx.ID_PREFERENCES)
		self.uiButtonForward = wx.Button(self, wx.ID_FORWARD)

		# Fill The Control Sizer
		sizerControls.Add(self.uiButtonBackward)
		sizerControls.AddStretchSpacer()
		sizerControls.Add(uiButtonPreferences)
		sizerControls.AddStretchSpacer()
		sizerControls.Add(self.uiButtonForward)

		# Fill The Save/Cancel Sizer
		sizerSaveCancel.Add(wx.Button(self, wx.ID_SAVE))
		sizerSaveCancel.AddStretchSpacer()
		sizerSaveCancel.Add(wx.Button(self, wx.ID_CANCEL))

		# Fill The Main Sizer
		sizerMain.Add(self.uiLabelCurrent, 0, wx.EXPAND|wx.BOTTOM, 5)
		sizerMain.Add(self.uiLabelCurrentUri, 0, wx.EXPAND|wx.BOTTOM, 5)
		sizerMain.Add(self.uiGaugeCurrent, 0, wx.EXPAND)
		sizerMain.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)
		sizerMain.Add(self.sizerTagEntries, 1, wx.EXPAND)
		sizerMain.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 10)
		sizerMain.Add(sizerControls, 0, flag=wx.EXPAND)
		sizerMain.Add(wx.StaticLine(self, wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)
		sizerMain.Add(sizerSaveCancel, 0, flag=wx.EXPAND)

		# Setup And Bind Everything
		self.uiButtonBackward.Enable(not self.index == 0)
		self.uiButtonForward.Enable(self.index != len(self.uris)-1)
		self.uiButtonBackward.Bind(wx.EVT_BUTTON, self.event_button_directional)
		uiButtonPreferences.Bind(wx.EVT_BUTTON, self.event_button_preferences)
		self.uiButtonForward.Bind(wx.EVT_BUTTON, self.event_button_directional)
		self.Bind(wx.EVT_BUTTON, self.event_button)
		self.uiGaugeCurrent.Hide()

		# Populate It
		self.populate()

		# Add It To The Box
		self.box = wx.BoxSizer()
		self.box.Add(sizerMain, 1, wx.ALL|wx.EXPAND, 10)
		self.SetSizerAndFit(self.box)

		#self.Fit()

		# Show It
		self.Show()


	def set_media_type(self, mediaType='audio', forceRefresh=False):
		# Check that it's not the same
		if mediaType == self.currentMediaType and not forceRefresh: return

		# Clear any old entries
		if self.currentMediaType != None:
			self.sizerTagEntries.Clear(True)
			self.entries = {}

		# Store the new type
		self.currentMediaType = mediaType

		# The tags
		if mediaType == 'audio':
			self.tags = cfg_get("tagsAudio")
			self.tagChoices = self.defaultTagsAudio.keys()
		elif mediaType == 'video':
			self.tags = cfg_get("tagsVideo")
			self.tagChoices = self.defaultTagsVideo.keys()
		else:
			self.tags = []

		index = 0
		for tag in self.tagOrder:
			if not tag in self.tags: continue

			if tag == 'tracknumber':
				if 'tracktotal' in self.tags:
					self.sizerTagEntries.Add(number_entry(self, "tracknumber", 180),   (index, 0), (1, 1), wx.ALIGN_RIGHT)
					self.sizerTagEntries.Add(number_entry(self, "tracktotal", 180),  (index, 1), (1, 1), wx.ALIGN_RIGHT)
				else:
					self.sizerTagEntries.Add(number_entry(self, "tracknumber"),   (index, 0), (1, 1), wx.ALIGN_RIGHT)
				index += 1
			elif tag == "discnumber":
				if 'disctotal' in self.tags:
					self.sizerTagEntries.Add(number_entry(self, "discnumber", 180),   (index, 0), (1, 1), wx.ALIGN_RIGHT)
					self.sizerTagEntries.Add(number_entry(self, "disctotal", 180),  (index, 1), (1, 1), wx.ALIGN_RIGHT)
				else:
					self.sizerTagEntries.Add(number_entry(self, "discnumber"),   (index, 0), (1, 1), wx.ALIGN_RIGHT)
				index += 1
			elif tag == "episodenumber":
				if 'episodetotal' in self.tags:
					self.sizerTagEntries.Add(number_entry(self, "episodenumber", 180),   (index, 0), (1, 1), wx.ALIGN_RIGHT)
					self.sizerTagEntries.Add(number_entry(self, "episodetotal", 180),  (index, 1), (1, 1), wx.ALIGN_RIGHT)
				else:
					self.sizerTagEntries.Add(number_entry(self, "episodenumber"),   (index, 0), (1, 1), wx.ALIGN_RIGHT)
				index += 1

			elif tag == 'tracktotal':
				if 'tracknumber' in self.tags: continue
				self.sizerTagEntries.Add(number_entry(self, "tracktotal"),  (index, 0), (1, 1), wx.ALIGN_RIGHT)
				index += 1

			elif tag == 'disctotal':
				if 'discnumber' in self.tags: continue
				self.sizerTagEntries.Add(number_entry(self, "disctotal"),  (index, 0), (1, 1), wx.ALIGN_RIGHT)
				index += 1

			elif tag == 'episodetotal':
				if 'episodenumber' in self.tags: continue
				self.sizerTagEntries.Add(number_entry(self, "episodetotal"),  (index, 0), (1, 1), wx.ALIGN_RIGHT)
				index += 1

			elif tag == "art" or tag == "lyrics": continue
			elif tag == "compilation":
				self.sizerTagEntries.Add(boolean_entry(self, tag),  (index, 0), (1, 2), wx.ALIGN_RIGHT)
				index += 1
			elif tag == 'season':
				self.sizerTagEntries.Add(number_entry(self, 'season'),  (index, 0), (1, 2), wx.ALIGN_RIGHT|wx.EXPAND)
				index += 1
			else:
				self.sizerTagEntries.Add(tag_entry(self, tag),  (index, 0), (1, 2), wx.ALIGN_RIGHT|wx.EXPAND)
				index += 1

		extraIndex = 3
		if "lyrics" in self.tags:
			self.sizerTagEntries.Add(lyric_entry(self), (0, extraIndex), (index, 1), wx.ALIGN_CENTER|wx.EXPAND)
			extraIndex += 1

		if "art" in self.tags:
			self.sizerTagEntries.Add(art_entry(self), (0, extraIndex), (index, 1), wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL)
			extraIndex += 1

		if _config['showAdvancedSettings']:
			self.sizerTagEntries.Add(AdvancedSettings(self), (0, extraIndex), (index, 1), wx.ALIGN_CENTER|wx.EXPAND)

		# Refresh the layout
		self.Layout()
		self.Fit()

	def event_key_down(self, event):
		#print event.GetKeyCode(), event.ControlDown()
		if event.GetKeyCode() == wx.WXK_RETURN:
			event.Id = 1
			self.event_button_directional(event)
			event.EventObject.SetSelection(0, -1)
		else: event.Skip()

	def event_button(self, event):
		if event.Id == wx.ID_CANCEL:
			self.Destroy()
			return
		elif event.Id != wx.ID_SAVE: return

		self.store_changes()

		self.uiLabelCurrent.SetLabel("Saving Edit 1 of %s"%(len(self.changes)))
		self.uiGaugeCurrent.SetRange(len(self.changes))
		self.uiGaugeCurrent.Show()
		self.Layout()

		self.write_changes()


	@pm_helpers.unthread
	def callback(self, finished=False):
		if finished: self.Destroy()
		self.uiLabelCurrent.SetLabel("Saving Edit %s of %s"%(self.saveIndex+1, self.saveTotal))
		self.uiGaugeCurrent.SetValue(self.saveIndex)

	@pm_helpers.threaded
	def write_changes(self):
		self.saveIndex = 0
		self.saveTotal = len(self.changes)
		for index, uri in enumerate(self.uris):
			changes = self.changes[index]
			if not changes: continue

			self.saveIndex += 1
			self.callback()

			# Update the file here
			mediaFile = pm_mediafile.MediaFile(uri)
			mediaFile.read()
			mediaFile.write(changes)

			# If it's in a library then update
			if self.trackIds[index] != -1 and self.library:
				for key, value in changes.iteritems():
					if key in pympe.library.multiTagsLocal+pympe.library.multiTagsShared:
						if type(value) == list:
							changes[key] = value
						else: changes[key] = value.split('\\\\')

				self.library.set_single_track_multi_values(self.trackIds[index], changes.iterkeys(), changes.itervalues())
			# Otherwise at least update the displayed information
			else:
				if pympe.player.currentInfo and uri == pympe.player.currentInfo['uri']:
					for key, value in changes.iteritems():
						if key in pympe.library.multiTagsLocal+pympe.library.multiTagsShared:
							changes[key] = value.split('\\\\')

						pympe.player.currentInfo[key] = changes[key]

					pympe.events.player_current_updated.send(changes)
				elif 'title' in changes:
					playlist = pympe.playlists.current
					pympe.events.playlist_update_entries.send(playlist.uid, [playlist.uids[playlist.uris.index(uri)]], titles=[changes['title']])

		self.callback(True)

	def store_changes(self): # New
		''' Stores any changes into self.changes when the displayed track is changed '''
		# Save the previous
		uri = self.uris[self.index]

		for tag, value in self.entries.iteritems():
			if tag == 'advancedSettings':
				value.store_changes()
				continue

			if value[0].modified:
				value[0].modified = False
				#if not uri in self.changes: self.changes[uri] = {tag:0}

				if tag == "art":
					self.changes[self.index][tag] = self.currentInfo["art"]
				elif tag in ["tracknumber", "tracktotal", "discnumber", "disctotal", 'episodenumber', 'episodetotal']:
					try: self.changes[self.index][tag] = int(value[0].GetValue())
					except: self.changes[self.index][tag] = value[0].GetValue()
				else:
					self.changes[self.index][tag] = value[0].GetValue()

	def populate(self, forceRefresh=False):
		# The current file information
		self.mediaFile = pm_mediafile.MediaFile(self.uris[self.index]).read()

		if self.mediaFile == None:
			pympe.error('Unable to find file: %s'%self.uris[self.index])
			return False

		# Setup the library info
		if self.libraries:
			if self.libraries[self.index] == -1: self.library = -1
			else: self.library = pympe.libraries[self.libraries[self.index]]

		# Pull the info from the library if it exists and gaps from the media file
		if self.library != -1:
			self.currentInfo = dict(zip(self.tagChoices, self.library.get_single_track_multi_values(self.trackIds[self.index], self.tagChoices, 'unmodified')))

			# Read it from the file if it exists else try the artwork cache
			if 'art' in self.mediaFile and self.mediaFile['art']: self.currentInfo['art'] = self.mediaFile['art']
			else:
				artworkPath = self.library.get_artwork_path(self.trackIds[self.index])
				if artworkPath:
					try:
						with open(artworkPath, 'rb') as artwork:
							self.currentInfo['art'] = artwork.read()
					except:
						pass
				# Else no artwork
				else:
					pass
		else:
			if self.mediaFile:
				self.currentInfo = dict([(key, self.mediaFile[key]) for key in self.tagChoices if key in self.mediaFile])
			else:
				self.currentInfo = {}

		# Set the media type
		if self.mediaFile:
			self.set_media_type(self.mediaFile['mimetype'].split('/', 1)[0], forceRefresh)

		# The edit index and uri display
		self.uiLabelCurrent.SetLabel('Editing %i of %i'%(self.index+1, len(self.uris)))
		maxLength = self.GetVirtualSize()[0]/self.uiLabelCurrentUri.GetCharWidth()-5
		label = self.uris[self.index].split('://', 1)[1]
		if self.library != -1:
			label = label.replace(pympe.config.get(self.library.configKey, 'defaultPath'), '.')
		if len(label) > maxLength: label = '...'+label[-maxLength:]
		self.uiLabelCurrentUri.SetLabel(label)

		# Split the multi tags for editing
		if self.library != -1:
			multiTags = self.library.multiTagsLocal+self.library.multiTagsShared
		else:
			multiTags = pympe.library.multiTagsLocal+pympe.library.multiTagsShared

		for key in set(multiTags).intersection(self.currentInfo.keys()):
			self.currentInfo[key] = '\\\\'.join(self.currentInfo[key])

		# Fill current info from file and from changes
		for key in self.tagChoices:
			# If it was changed then set that as it
			if self.uris[self.index] in self.changes and key in self.changes[self.uris[self.index]]:
				self.currentInfo[key] = self.changes[self.uris[self.index]][key]
			# If it's not in currentinfo at all then set it from default
			elif not key in self.currentInfo:
				if self.currentMediaType == 'audio':
					self.currentInfo[key] = self.defaultTagsAudio[key]
				elif self.currentMediaType == 'video':
					self.currentInfo[key] = self.defaultTagsVideo[key]
				else:
					self.currentInfo[key] = ''

		# Load all other info
		for tag, entry in self.entries.iteritems():
			if tag == 'advancedSettings':
				entry.populate()
				continue

			info = self.changes[self.index][tag] if tag in self.changes[self.index] else self.currentInfo[tag]

			if tag == 'art':
				if info:
					try:
						image = pm_helpers.PmImage(string=info)
						entry[2].SetLabel("%s x %s"%(image.get_width(), image.get_height()))

					except:
						image = pm_helpers.PmImage(stock=pm_helpers.get_stock_media_icon())
						entry[2].SetLabel("No Artwork")

				else:
					image = pm_helpers.PmImage(stock=pm_helpers.get_stock_media_icon())
					entry[2].SetLabel("No Artwork")

				entry[0].SetBitmap(image.scaled(250, 250).convert_to_wx_bitmap())

				#check if self.entries[tag][1].applyValue is the same as the changes value if it is turn blue else turn red
				if entry[1].applyAll == info:
					entry[0].SetBackgroundColour((200, 255, 200))
					entry[2].SetBackgroundColour((200, 255, 200))
				elif 'art' in self.changes[self.index]:
					entry[0].SetBackgroundColour((255, 200, 200))
					entry[2].SetBackgroundColour((255, 200, 200))
				else:
					entry[0].SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))
					entry[2].SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))

			else:
				if entry[1].applyAll == info:
					entry[0].SetBackgroundColour((200, 255, 200))
				elif tag in self.changes[self.index] and self.changes[self.index][tag] != self.currentInfo[tag]:
					entry[0].SetBackgroundColour((255, 200, 200))
				else:
					entry[0].SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))

				if tag == "compilation":
					entry[0].SetValue(info)
					#entry[2].SetValue(not info)
				elif tag == 'lyrics':
					entry[0].ChangeValue(str(info))
				elif tag in ["tracknumber", "tracktotal", "discnumber", "disctotal", 'episodenumber', 'episodetotal', 'season']:
					try: entry[0].SetValue(int(info))
					except: entry[0].SetValue(0)
				else:
					entry[0].ChangeValue(str(info))


			entry[0].modified = False

	def event_button_directional(self, event): # New
		self.store_changes()

		if event.Id == wx.ID_BACKWARD:
			if self.index > 0: self.index -= 1
		else:
			if self.index < len(self.uris)-1: self.index += 1

		self.uiButtonBackward.Enable(not self.index == 0)
		self.uiButtonForward.Enable(self.index != len(self.uris)-1)

		self.uiLabelCurrent.SetLabel("Editing %s of %s"%(self.index+1, len(self.uris)))
		self.populate()

	def event_text_modified(self, event): # New
		self.entries[event.GetEventObject().tag][0].modified = True
		self.entries[event.GetEventObject().tag][0].SetBackgroundColour((255, 200, 200))
		self.entries[event.GetEventObject().tag][1].applyAll = None

	def event_button_apply_all(self, event): # New
		tag = event.GetEventObject().tag

		for index in range(len(self.changes)):
			if tag == 'art':
				self.changes[index][tag] = self.currentInfo['art']
			else:
				self.changes[index][tag] = self.entries[tag][0].GetValue()

		self.entries[tag][0].SetBackgroundColour((200, 255, 200))
		self.entries[tag][1].applyAll = self.changes[index][tag]

	def event_preferences_menu(self, event): # New
		if event.Id == 11: # Fields
			dialog = wx.MultiChoiceDialog(None, "Select the tags you would like to show", "Please make your selection", [pm_language.readableTags[x] for x in self.tagChoices])
			dialog.SetSelections([self.tagChoices.index(x) for x in self.tags])
			if dialog.ShowModal() == wx.ID_CANCEL: return
			cfg_set('tagsAudio' if self.currentMediaType == 'audio' else 'tagsVideo', [self.tagChoices[x] for x in dialog.GetSelections()])
			self.populate(forceRefresh=True)
		elif event.Id == 21: # Auto number
			dialog = AutoNumberDialog(self)
			if dialog.ShowModal() == wx.ID_SAVE:
				for number, total, trackId in zip(dialog.results[0], dialog.results[1], dialog.results[4]):
					self.changes[trackId]['tracknumber'] = number
					self.changes[trackId]['tracktotal'] = total


				if 'tracknumber' in self.entries:
					self.entries['tracknumber'][0].SetBackgroundColour((255, 200, 200))
					self.entries['tracknumber'][0].SetValue(self.changes[self.index]['tracknumber'])
				if 'tracktotal' in self.entries:
					self.entries['tracktotal'][0].SetBackgroundColour((255, 200, 200))
					self.entries['tracktotal'][0].SetValue(self.changes[self.index]['tracktotal'])
		elif event.Id == 22: # Guess Tags
			dialog = GuessTagsDialog(self)
			if dialog.ShowModal() == wx.ID_SAVE:
				for index, uri in enumerate(self.uris):
					for keyIndex, key in enumerate(dialog.columnKeys):
						self.changes[index][key] = dialog.results[keyIndex][index]

				self.populate()

			dialog.Destroy()
		elif event.Id == 31: # Fetch Track Information

			pass
		elif event.Id == 32: # Fetch Cover
			pass
		elif event.Id == 41: # Advanced Settings
			_config['showAdvancedSettings'] = event.Int
			self.populate(forceRefresh=True)
			#AdvancedSettings(self)
		elif event.Id == 42: # Edit Bookmarks
			dialog = BookmarkEditor(self)
			if dialog.ShowModal() == wx.ID_SAVE:
				pass

		event.Skip()

	def event_button_preferences(self, event): # New
		menu = wx.Menu()
		menu.Append(11, "Choose Fields")
		menu.AppendSeparator()
		menu.Append(21, "Auto Number Tracks")
		menu.Append(22, "Guess Tags From Filename")
		menu.AppendSeparator()
		menu.Append(31, "Fetch Track Information")
		menu.Append(32, "Fetch Cover")
		menu.AppendSeparator()
		menu.Append(42, 'Edit Bookmarks')
		menu.AppendCheckItem(41, 'Show Advanced Settings').Check(_config['showAdvancedSettings'])

		menu.Bind(wx.EVT_MENU, self.event_preferences_menu)

		if not musicbrainz:
			menu.Enable(31, False)
		menu.Enable(32, False)

		event.GetEventObject().PopupMenu(menu)

	def event_bitmap_right(self, event): # New
		menu = wx.Menu()
		menu.Append(11, "Browse For Cover")
		menu.Append(15, "Set Cover From Uri")
		menu.AppendSeparator()
		menu.Append(12, "Remove Cover")
		menu.Append(13, "Save Cover To Disk")
		menu.AppendSeparator()
		menu.Append(21, "Set Cover Type (submenu)")
		menu.Append(22, "Set Cover Description")
		menu.AppendSeparator()
		menu.Append(31, "Cut Cover")
		menu.Append(32, "Copy Cover")
		menu.Append(33, "Paste Cover")
		menu.AppendSeparator()
		menu.Append(41, "Correct Aspect Ratio (checkbox)")
		menu.Bind(wx.EVT_MENU, self.event_bitmap_menu)

		menu.Enable(15, False)
		menu.Enable(21, False)
		menu.Enable(22, False)
		menu.Enable(31, False)
		menu.Enable(32, False)
		menu.Enable(33, False)
		menu.Enable(41, False)

		event.GetEventObject().PopupMenu(menu)

	def event_bitmap_menu(self, event): # New
		if event.Id == 11:  # Browse For Cover
			dialog = wx.FileDialog(self, message="Choose an image please.", wildcard="Jpg Image (*.jpg)|*.jpg|Jpeg Image (*.jpeg)|*.jpeg|Png Image (*.png)|*.png", style=wx.FD_PREVIEW)

			if dialog.ShowModal() == wx.ID_OK:
				paths = dialog.GetPaths()
				if paths:
					with open(paths[0]) as f: data = f.read()
					image = pm_helpers.PmImage(string=data)

					self.currentInfo['art'] = data
					self.changes[self.index]["art"] = data
					self.entries["art"][0].SetBitmap(image.scaled(250, 250).convert_to_wx_bitmap())
					self.entries["art"][0].modified = True
					self.entries["art"][2].SetLabel("%s x %s"%(image.get_width(), image.get_height()))
		elif event.Id == 13: # Save Cover To Disk
			dialog = wx.FileDialog(self, wildcard="Png And Jpg images (*.png;*.jpg)|*.png;*.jpg;*.jpeg", style=wx.FD_SAVE|wx.FD_OVERWRITE_PROMPT)

			if dialog.ShowModal() == wx.ID_OK:
				path = dialog.GetPath()

				if not path.lower().endswith('.jpg') and not path.lower().endswith('.png'):
					path += '.jpg'

				with open(path, 'wb') as artworkFile:
					if 'art' in self.changes[self.index]:
						data = self.changes[self.index]['art']
					else:
						data = self.currentInfo['art']

					artworkFile.write(data)

		elif event.Id == 15: # Set Cover From URI
			pass
		elif event.Id == 12: # Remove Cover
			self.changes[self.index]["art"] = None
			self.entries["art"][0].SetBitmap(pm_helpers.PmImage(stock=pm_helpers.get_stock_media_icon()).scaled(250, 250).convert_to_wx_bitmap())
			self.entries["art"][0].modified = True
			self.entries["art"][2].SetLabel("")
		elif event.Id == 32: # Copy Cover
			data = self.changes[self.index]["art"] if 'art' in self.changes[self.index] else self.currentInfo["art"]
			pm_helpers.clipboard_write(data)
		elif event.Id == 33: # Paste Cover
			pass
			#clipdata = wx.TextDataObject()
			#wx.TheClipboard.Open()
			#print '-'*70
			#for x in ['DF_BITMAP', 'DF_DIB', 'DF_DIF', 'DF_ENHMETAFILE', 'DF_FILENAME', 'DF_HTML', 'DF_INVALID', 'DF_LOCALE', 'DF_MAX', 'DF_METAFILE', 'DF_OEMTEXT', 'DF_PALETTE', 'DF_PENDATA', 'DF_PRIVATE', 'DF_RIFF', 'DF_SYLK', 'DF_TEXT', 'DF_TIFF', 'DF_UNICODETEXT', 'DF_WAVE']:
				#dataFormat = wx.DataFormat(getattr(wx, x))
				#dataObject = wx.DataObjectSimple(dataFormat)
				#available = wx.TheClipboard.IsSupported(dataFormat)
				#print 'Supports %s:'%(x), available
				#if available:
					#print dataObject.GetDataSize()
			#result = wx.TheClipboard.GetData(clipdata)
			#wx.TheClipboard.Close()
			#print result

			#print dir(clipdata)
			#print clipdata.GetFormat()
			#print clipdata.GetDataSize()
			#print clipdata.TextLength
			#print clipdata.GetText()
			#if result: return clipdata.GetText()
			#return None
			#data = pm_helpers.clipboard_read()

			#try:
				#pm_helpers.PmImage(string=data)
				#self.changes[self.index]["art"] = data
			#except:
				#pympe.error('Problem Pasting Cover', traceback=True)

class Plugin:
	def plu_settings(self, parent):
		libraryIds = [pympe.library.tracks.keys()[0]]
		libraryIds = [8823, 8824, 8825, 8826, 8827, 8828, 8829, 8830, 8831]
		self.MetadataEditor(pympe.library.get_multi_track_single_value(libraryIds, 'uri'), libraryIds)

	def plu_start(self):
		self.MetadataEditor = MetadataEditor

	def plu_stop(self, shutdown):
		pass
