# -*- coding: utf-8 -*-
import pympe, pickle, wx, pm_mediafile, pm_network, pm_playlists, pm_helpers, os, webbrowser, pm_language, time
import pithos as pandora

class PandoraPlaylist(pm_playlists.Playlist):
	def __init__(self, name, uid):
		pm_playlists.Playlist.__init__(self, name, uid)
		self.multiSelect = False
		self.allowDragDrop = False
		self.userModifiable = False
		self.generateTime = 0

		self.singleItemMenu = (('Love Track', 10), ('Go To Artist Page', 15), None, ('Ban Track', 20), ('I\'m Tired Of This Track', 25))
		self.multiItemMenu  = ()

	def handle_custom_menu(self, eventId, selected):
		# We only really want to be able to love or ban one track at a time
		if not selected: return
		selected = selected[0]

		#This needs to use by selected instead if of plugin.song.rate
		if eventId == 10:
			self.plugin.love_song(self.songs[selected], selected)

		elif eventId == 15:
			webbrowser.open(self.songs[selected].songDetailURL)
			pm_helpers.clipboard_write(self.songs[selected].songDetailURL)

		elif eventId == 20:
			self.plugin.ban_song(self.songs[selected])

		elif eventId == 25:
			self.plugin.bored_song(self.songs[selected])

		return True


	def clear(self, sendEvent=True):
		pm_playlists.Playlist.clear(self, sendEvent)
		self.mediaFiles = []
		self.songs = []

	def replace(self, library, uris, titles=[], libraryIds=[]): pass
	def enqueue(self, library, uris, titles=[], libraryIds=[], index=None): pass
	def dequeue(self, uids): pass
	def set_item_info(self, uid=None, uri=None, index=None, info={}): pass
	def previous(self): pass
	def set_index(self, index): pass
	def mpevent_player_shuffle_changed(self, event, shuffle): pass
	def mpevent_player_repeat_changed(self, event, repeat): pass
	def mpevent_player_current_updated(self, event, tags): pass
	def update(self, stored): pass
	def refresh(self):  pass

	def callback_art(self, result, mediaFile):
		mediaFile['art'] = result
		if pympe.player.currentInfo and mediaFile['uri'] == pympe.player.currentInfo['uri']:
			pympe.events.player_current_updated.send({'art':result})


	def update_playlist(self, playlist):
		if not playlist: return
		self.generateTime = time.time()
		self.uids += range(self.nextUid, self.nextUid+len(playlist))
		self.nextUid = self.uids[-1]+1

		#if self.plugin.debug: self.uris += [x.audioUrlMap[pandoraData.default_audio_quality]['audioUrl'] for x in playlist]
		#else: self.uris += [x.audioUrl for x in playlist]
		self.uris += [x.audioUrl for x in playlist]

		self.titles += [('[♥] ' if x.rating == pandora.RATE_LOVE else '')+x.title for x in playlist]
		self.libraryIds += [0, 1, 2, 3]
		self.libraries += [None]*len(playlist)

		for song in playlist:
			mediaFile = pm_mediafile.MediaFile(song.audioUrl)
			mediaFile['artist'] = [song.artist]
			mediaFile['album'] = [song.album]
			mediaFile['title'] = [song.title]
			if song.artRadio:
				pm_network.get_url_threaded(song.artRadio, self.callback_art, returnData=True, callbackArgs=(mediaFile,))
			self.mediaFiles.append(mediaFile)

		self.songs += playlist[:]

		self.previouslyPlayed = []
		self.shuffleOrder = range(len(self.uids))

		pympe.events.playlist_entries_added.send(self.uid, self.uids[-len(playlist):])

	def get_next(self): return self.index+1
	def get_previous(self): return None

	def next(self):
		# Get a new playlist and go from there
		if not self.is_valid():
			self.plugin.pandoraPlaylist.clear()
			self.plugin.get_playlist()

		# If we're at the end
		if self.index == len(self.uids)-1:
			self.plugin.get_playlist()

		self.index += 1
		return self.index

	def is_valid(self):
		if time.time()-self.generateTime > 1800: # The playlists last 30 MINUTES[XXthree hours] from generation otherwise they are invalid
			return False
		return True

	def get_current(self, key=None):
		item = self.get_item(self.index)
		return item

	def get_item(self, index, key=None):
		# This is where it's messed up
		if index >= len(self.uids): return None

		if key == 'uri': return self.uris[index]
		elif key == 'title': return self.titles[index]
		elif key == 'libraryId': return None
		elif key == 'library': return None
		elif key == None and index != None:
			#song = self.plugin.playlist[self.libraryIds[index]]
			#self.plugin.song = song

			#mediaFile = pm_mediafile.MediaFile(self.uris[index])
			#mediaFile['artist'] = [song.artist]
			#mediaFile['album'] = [song.album]
			#mediaFile['title'] = [song.title]

			#def callback_art(result, mediaFile):
				#mediaFile['art'] = result
				#if mediaFile is pympe.player.currentFile:
					#pympe.events.player-current-tags-updated.send({'art':result})

			#if song.artRadio:
				#pm_network.get_url_threaded(song.artRadio, callback_art, returnData=True, callbackArgs=(mediaFile,))

			self.plugin.song = self.songs[index]
			self.plugin.uiPanel.hilightStation = self.plugin.station
			self.plugin.uiPanel.update_hilight(self.libraryIds[index])
			return (self.uris[index], self.titles[index], None, None, self.mediaFiles[index])


class Panel(wx.Panel):
	def __init__(self, parent, plugin):
		wx.Panel.__init__(self, parent)
		self.plugin = plugin
		self.labels = []
		self.hilight = 0
		self.hilightStation = None

		self.uiFontTitle = wx.Font(10, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD)
		self.uiFontArtist = wx.Font(8, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_NORMAL)
		self.uiFontAlbum = wx.Font(7, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_NORMAL)

		self.sizer = sizer = wx.BoxSizer(wx.VERTICAL)

	# Station selector
		sizer2 = wx.BoxSizer(wx.HORIZONTAL)
		self.sizer.Add(sizer2, 0, wx.EXPAND)

		self.uiChoice = wx.Choice(self, -1, choices=[x.name for x in plugin.stations])
		self.uiChoice.Bind(wx.EVT_CHOICE, self.event_choice)

		# Play this
		self.uiPlayButton = button = wx.Button(self, -1, pm_language.uiPlay, size=(self.uiChoice.GetSize()[1], -1))
		button.Bind(wx.EVT_BUTTON, self.event_button_play_this)

		sizer2.Add(button, 0)
		sizer2.Add(self.uiChoice, 1, wx.EXPAND|wx.BOTTOM, 5)



	# First
		labelTitle = wx.StaticText(self, -1, '')
		labelArtist = wx.StaticText(self, -1, '')
		labelAlbum = wx.StaticText(self, -1, '')
		self.labels.append((labelTitle, labelArtist, labelAlbum))
		labelTitle.SetFont(self.uiFontTitle)
		labelArtist.SetFont(self.uiFontArtist)
		labelAlbum.SetFont(self.uiFontAlbum)
		sizer.Add(labelTitle)
		sizer.Add(labelArtist)
		sizer.Add(labelAlbum)
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

	# Second
		labelTitle = wx.StaticText(self, -1, '')
		labelArtist = wx.StaticText(self, -1, '')
		labelAlbum = wx.StaticText(self, -1, '')
		self.labels.append((labelTitle, labelArtist, labelAlbum))
		labelTitle.SetFont(self.uiFontTitle)
		labelArtist.SetFont(self.uiFontArtist)
		labelAlbum.SetFont(self.uiFontAlbum)
		sizer.Add(labelTitle)
		sizer.Add(labelArtist)
		sizer.Add(labelAlbum)
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

	# Third
		labelTitle = wx.StaticText(self, -1, '')
		labelArtist = wx.StaticText(self, -1, '')
		labelAlbum = wx.StaticText(self, -1, '')
		self.labels.append((labelTitle, labelArtist, labelAlbum))
		labelTitle.SetFont(self.uiFontTitle)
		labelArtist.SetFont(self.uiFontArtist)
		labelAlbum.SetFont(self.uiFontAlbum)
		sizer.Add(labelTitle)
		sizer.Add(labelArtist)
		sizer.Add(labelAlbum)
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

	# Fourth
		labelTitle = wx.StaticText(self, -1, '')
		labelArtist = wx.StaticText(self, -1, '')
		labelAlbum = wx.StaticText(self, -1, '')
		self.labels.append((labelTitle, labelArtist, labelAlbum))
		labelTitle.SetFont(self.uiFontTitle)
		labelArtist.SetFont(self.uiFontArtist)
		labelAlbum.SetFont(self.uiFontAlbum)
		sizer.Add(labelTitle)
		sizer.Add(labelArtist)
		sizer.Add(labelAlbum)
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 5)

	# Controls
		buttonLove = wx.Button(self, 0, 'Love')
		buttonBan = wx.Button(self, 1, 'Ban')
		buttonTired = wx.Button(self, 2, 'Tired Of Track')

		self.controlButtons = (buttonLove, buttonBan, buttonTired)

		sizerButtons = wx.BoxSizer(wx.HORIZONTAL)
		sizerButtons.Add(buttonLove)
		sizerButtons.Add(buttonBan)
		sizerButtons.Add(buttonTired)

		buttonLove.Disable()
		buttonBan.Disable()
		buttonTired.Disable()

		buttonLove.Bind(wx.EVT_BUTTON, self.event_button_rating)
		buttonBan.Bind(wx.EVT_BUTTON, self.event_button_rating)
		buttonTired.Bind(wx.EVT_BUTTON, self.event_button_rating)

		sizer.Add(sizerButtons, 0, wx.EXPAND)

	# Search Results
		self.uiListSearch = wx.ListBox(self, -1)
		self.uiListSearch.Bind(wx.EVT_LISTBOX_DCLICK, self.event_listbox_activated)

		self.uiSearch = wx.SearchCtrl(self, -1, style=wx.TE_PROCESS_ENTER)
		self.uiSearch.ShowSearchButton(False)
		self.uiSearch.ShowCancelButton(True)
		self.uiSearch.SetDescriptiveText('Create new station')
		self.uiSearch.Bind(wx.EVT_TEXT_ENTER, self.event_search_enter)
		self.uiSearch.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, self.event_search_cancel)

		sizer.Add(self.uiListSearch, 1, wx.EXPAND)
		sizer.Add(self.uiSearch, 0, wx.EXPAND)
		sizer.AddStretchSpacer()
		sizer.Show(18, False)
		sizer.Show(19, False)

	# Options
		button = wx.Button(self, -1, 'Options')
		button.Bind(wx.EVT_BUTTON, self.event_button_options)
		sizer.Add(button, 0, wx.EXPAND)

	# Connect button
		sizerConnect = wx.BoxSizer(wx.VERTICAL)
		sizerConnect.AddStretchSpacer()
		self.buttonConnect = wx.Button(self, 50, 'Connect to Pandora')
		self.buttonConnect.Bind(wx.EVT_BUTTON, self.event_button_connect)
		self.labelConnect = wx.StaticText(self, -1, 'You are not connected')
		sizerConnect.Add(self.buttonConnect, 0, wx.ALIGN_CENTER)
		sizerConnect.Add(self.labelConnect, 0, wx.ALIGN_CENTER|wx.TOP, 5)
		sizerConnect.AddStretchSpacer()

		self.box = wx.BoxSizer()
		self.box.Add(sizer, 1, wx.EXPAND)
		self.box.Add(sizerConnect, 1, wx.EXPAND)
		self.box.Show(0, False) # Hide the connected sizer and only show the one to connect
		self.SetSizer(self.box)

	def event_button_rating(self, event):
		if event.Id == 0:
			self.plugin.love_song(self.plugin.song)
		elif event.Id == 1:
			self.plugin.ban_song(self.plugin.song)
		if event.Id == 2:
			self.plugin.bored_song(self.plugin.song)

		event.EventObject.Disable()
		self.update_playlist()

	def event_button_connect(self, event):
		self.plugin.connect()

	def event_search_cancel(self, event):
		self.uiListSearch.Clear()
		self.uiSearch.SetValue('')
		self.sizer.Show(18, False)
		self.sizer.Show(19, False)
		self.sizer.Show(20, True)

		self.sizer.Layout()

	def event_search_enter(self, event):
		string = event.EventObject.GetValue()
		if not string: return event.Skip()

		results = sorted(self.plugin.connection.search(string), key=lambda y: y.score)

		self.uiListSearch.Clear()
		self.uiListSearch.AppendItems([('(artist) %s'%x.name if x.resultType=='artist' else '(track) %s by %s'%(x.title, x.artist)) for x in results])
		self.uiListSearch.musicIds = [x.musicId for x in results]

	def event_button_options(self, event):
		index = self.uiChoice.GetSelection()

		def handle_menu(event):
			if event.Id == 10:
				dialog = wx.TextEntryDialog(self, "What shall the station be renamed to?", "Rename a station.", self.plugin.station.name)
				if dialog.ShowModal() == wx.ID_CANCEL: return
				name = dialog.GetValue()
				if not name: return
				self.plugin.station.rename(name)
				self.uiChoice.SetString(index, name)

			elif event.Id == 30:
				result = wx.MessageDialog(self, "Delete station: %s?"%self.plugin.station.name, style=wx.YES_NO|wx.NO_DEFAULT).ShowModal()
				if result == wx.ID_YES:
					self.plugin.station.delete()
					self.uiChoice.Delete(index)
					self.uiChoice.SetSelection(0)
					self.plugin.set_station(0)

			elif event.Id == 15:
				self.plugin.pandoraPlaylist.clear()
				self.plugin.get_playlist()

			elif event.Id == 20:
				self.sizer.Show(18, True)
				self.sizer.Show(19, True)
				self.sizer.Show(20, False)
				self.Layout()
				#dialog = wx.TextEntryDialog(self, "What to base the station from?", "Create a station.", '')
				#if dialog.ShowModal() == wx.ID_CANCEL: return
				#search = dialog.GetValue()
				#if not search: return
				#results = self.plugin.connection.search(search)

				#with open('searchresults.pkle', 'wb') as srp: pickle.dump(results, srp)

				#self.uiListSearch.Clear()
				#self.uiListSearch.AppendItems([('(artist) %s'%x.name if x.resultType=='artist' else '(track) %s by %s'%(x.title, x.artist)) for x in sorted(results, key=lambda y: y.score)])

			#elif event.Id == 100: # Love Track
			#elif event.Id == 101: # Shelve Track
			#elif event.Id == 102: # Hate Track

			elif event.Id == 666: # Disconnect
				self.plugin.disconnect()


		menu = wx.Menu()
		#menu.Append(100, 'I Love This Track!')
		#menu.AppendSeparator()
		#menu.Append(101, 'Don\'t Play For A While')
		#menu.Append(102, 'I Hate This Track.')
		#menu.AppendSeparator()
		menu.Append(20, 'Create New Station')
		if index != 0:
			menu.Append(10, 'Rename Station')
			menu.Append(15, 'Refresh Station')
			menu.AppendSeparator()
			menu.Append(30, 'Delete Station')
			menu.AppendSeparator()
			menu.Append(666, 'Disconnect')
		menu.Bind(wx.EVT_MENU, handle_menu)
		event.EventObject.PopupMenu(menu)

	def event_listbox_activated(self, event):
		musicId = event.EventObject.musicIds[event.Int]

		newStation = self.plugin.connection.add_station_by_music_id(musicId)

		#quickMix = self.plugin.connection.stations.pop(0)
		#self.plugin.connection.stations.sort()
		#self.plugin.connection.stations.insert(0, quickMix)
		self.plugin.stations = self.plugin.connection.stations

		self.uiChoice.Clear()
		self.uiChoice.AppendItems([x.name for x in self.plugin.stations])

		for index, station in enumerate(self.plugin.stations):
			if station == newStation:
				self.uiChoice.SetSelection(index)
				self.plugin.set_station(index)
				break

		# Redo the layout
		self.sizer.Show(18, False)
		self.sizer.Show(19, False)
		self.sizer.Show(20, True)
		self.Layout()

	def event_button_play_this(self, event):
		''' Generates a dynamic playlist for pympe to follow '''
		pympe.playlists.select_playlist(self.plugin.pandoraPlaylist.uid)
		self.plugin.pandoraPlaylist.index = 0
		pympe.player.play(entry=self.plugin.pandoraPlaylist.get_current())

	# Apparently this is required or it crashes; meaning we have some severe threading leaks
	# Man fuck threading If you need to ask how to use it you dont need to use it
	@pm_helpers.unthread
	def update_hilight(self, index=None):
		self.labels[self.hilight][0].SetFont(self.uiFontTitle)
		self.labels[self.hilight][1].SetFont(self.uiFontArtist)
		self.labels[self.hilight][2].SetFont(self.uiFontAlbum)

		if index != None: self.hilight = index

		if self.plugin.station == self.hilightStation:
			self.labels[self.hilight][0].SetFont(wx.Font(12, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
			self.labels[self.hilight][1].SetFont(wx.Font(9, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
			self.labels[self.hilight][2].SetFont(wx.Font(7, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.wx.FONTWEIGHT_BOLD))

		if self.plugin.song:
			if not self.plugin.song.tired:
				self.controlButtons[2].Enable()

			if self.plugin.song.rating != pandora.RATE_LOVE:
				self.controlButtons[0].Enable()

			if self.plugin.song.rating != pandora.RATE_BAN:
				self.controlButtons[1].Enable()

		#self.controlButtons

		self.Refresh()

	@pm_helpers.unthread
	def update_playlist(self):
		''' Updates the labels to match the current playlist in plugin.playlist '''
		if not self.plugin.playlist:
			# Might have something to blank the labels here?
			return

		loved = '[♥] ' if self.plugin.playlist[0].rating == pandora.RATE_LOVE else ('[X]' if self.plugin.playlist[0].rating == pandora.RATE_BAN else '')
		self.labels[0][0].SetLabel(loved+self.plugin.playlist[0].title.replace("&", "&&"))
		self.labels[0][1].SetLabel(self.plugin.playlist[0].artist.replace("&", "&&"))
		self.labels[0][2].SetLabel(self.plugin.playlist[0].album.replace("&", "&&"))
		loved = '[♥] ' if self.plugin.playlist[1].rating == pandora.RATE_LOVE else ('[X]' if self.plugin.playlist[0].rating == pandora.RATE_BAN else '')
		self.labels[1][0].SetLabel(loved+self.plugin.playlist[1].title.replace("&", "&&"))
		self.labels[1][1].SetLabel(self.plugin.playlist[1].artist.replace("&", "&&"))
		self.labels[1][2].SetLabel(self.plugin.playlist[1].album.replace("&", "&&"))
		loved = '[♥] ' if self.plugin.playlist[2].rating == pandora.RATE_LOVE else ('[X]' if self.plugin.playlist[0].rating == pandora.RATE_BAN else '')
		self.labels[2][0].SetLabel(loved+self.plugin.playlist[2].title.replace("&", "&&"))
		self.labels[2][1].SetLabel(self.plugin.playlist[2].artist.replace("&", "&&"))
		self.labels[2][2].SetLabel(self.plugin.playlist[2].album.replace("&", "&&"))
		loved = '[♥] ' if self.plugin.playlist[3].rating == pandora.RATE_LOVE else ('[X]' if self.plugin.playlist[0].rating == pandora.RATE_BAN else '')
		self.labels[3][0].SetLabel(loved+self.plugin.playlist[3].title.replace("&", "&&"))
		self.labels[3][1].SetLabel(self.plugin.playlist[3].artist.replace("&", "&&"))
		self.labels[3][2].SetLabel(self.plugin.playlist[3].album.replace("&", "&&"))

	def event_choice(self, event):
		''' Station selector '''
		self.plugin.set_station(event.Int)
		self.plugin.uiPanel.update_hilight()

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent, connectOnExit=False):
		wx.Dialog.__init__(self, parent, -1, 'Pandora Settings')

		self.plugin = plugin
		self.connectOnExit = connectOnExit

		sizer = wx.BoxSizer(wx.VERTICAL)

		self.uiTextUsername = wx.TextCtrl(self, -1, cfg_get('username'))
		self.uiTextPassword = wx.TextCtrl(self, -1, _config['password'], style=wx.TE_PASSWORD)

		sizer.Add(wx.StaticText(self, -1, 'Username/Email'))
		sizer.Add(self.uiTextUsername, 0, wx.EXPAND)
		sizer.Add(wx.StaticText(self, -1, 'Password'), 0, wx.TOP, 5)
		sizer.Add(self.uiTextPassword, 0, wx.EXPAND)

		sizer.Add(wx.StaticLine(self, -1, size=(300, -1)), 0, wx.EXPAND|wx.TOP|wx.BOTTOM, 10)

		s2 = wx.BoxSizer(wx.HORIZONTAL)
		s2.Add(wx.Button(self, wx.ID_OK), 0, wx.EXPAND)
		s2.AddStretchSpacer()
		s2.Add(wx.Button(self, wx.ID_CANCEL), 0, wx.EXPAND)
		sizer.Add(s2, 0, wx.EXPAND)

		self.Bind(wx.EVT_BUTTON, self.event_button)
		self.Bind(wx.EVT_CLOSE, self.event_close)

		# Add the sizer into the dialog with a space
		box = wx.BoxSizer(wx.VERTICAL)
		box.Add(sizer, 1, wx.ALL, 10)
		self.SetSizerAndFit(box)

		self.Show()

	def event_close(self, event):
		self.plugin.settingsDialog = None
		event.Skip()

	def event_button(self, event):
		if event.Id == wx.ID_OK:
			cfg_set('username', self.uiTextUsername.GetValue())
			cfg_set('password', self.uiTextPassword.GetValue())
			self.Destroy()

			if self.connectOnExit:
				self.plugin.connect()

			return
		if event.Id == wx.ID_CANCEL:
			self.Destroy()
			return
		if event.Id == 50:
			self.plugin.uiPanel.event_button_connect(event)

class Plugin:
	connection = None
	station = None
	song = None
	playlist = []
	stations = []
	pandoraPlaylist = None
	connected = False
	debug = False
	uiPanel = None
	settingsDialog = None

	def love_song(self, song, playlistIndex=None):
		if song.rating == pandora.RATE_LOVE: return

		song.rate(pandora.RATE_LOVE)
		if playlistIndex == None: playlistIndex = self.pandoraPlaylist.index
		self.pandoraPlaylist.titles[playlistIndex] = '[♥] '+self.pandoraPlaylist.titles[playlistIndex]
		pympe.events.playlist_entries_updated.send([self.pandoraPlaylist.uids[playlistIndex]])
		pympe.output('Loved %s by %s'%(song.title, song.artist))

	def ban_song(self, song):
		if song.rating == pandora.RATE_BAN: return

		song.rate(pandora.RATE_BAN)
		pympe.output('Banned %s by %s'%(song.title, song.artist))

		if pympe.player.currentInfo and pympe.player.currentInfo['uri'] == song.audioUrl and pympe.player.playing and not pympe.player.paused:
			pympe.events.player_next()

	def bored_song(self, song):
		song.set_tired()
		pympe.output('Grew bored of %s by %s'%(song.title, song.artist))

		if pympe.player.currentInfo and pympe.player.currentInfo['uri'] == song.audioUrl and pympe.player.playing and not pympe.player.paused:
			pympe.events.player_next()

	def set_station(self, index=None, id=None):
		if id != None:
			station = [station for station in self.stations if station.id == id]
			if not station: return
			station = station[0]
		else:
			if index >= len(self.stations): return
			station = self.stations[index]

		if self.station == station: return

		self.station = station
		self.pandoraPlaylist.clear()

		#print 'Tuning into %s radio'%self.station.name
		return self.get_playlist(fetchNew=True)

	def get_playlist(self, fetchNew=True):
		''' Fetches a new playlist from the current station and updates the interface accordingly
		fetchNew = To force a playlist refresh from pandoras servers
		'''
		# If we don't want a new one and already have one
		if not fetchNew and hasattr(self.station, 'playlist'):
			self.playlist = self.station.playlist

		# If we don't already have one or we want a new one for this station
		else:
			try:
				self.station.playlist = self.playlist = self.station.get_playlist()
			# Reconnect and try once more
			except pandora.PandoraAuthTokenInvalid:
				def stage_two_get_playlist():
					try:
						self.station.playlist = self.playlist = self.station.get_playlist()
					except:
						self.disconnect()
						return False

				self.disconnect('Reconnecting', False)
				self.station.playlist = self.playlist = []
				self.pandoraPlaylist.update_playlist(self.playlist)

				pympe.debug('Pandora timed out: Reconnecting')
				self.connect(runOnConnect=stage_two_get_playlist)

			# Disconnect on all other errors
			except:
				pympe.debug('Pandora had an error fetching station.')
				self.disconnect()
				return False

		# Now we've gotten our new playlist (I think?) update the interface
		#print 'Pandora New Playlist:\n	%s\n'%'\n	'.join(['%s: %s'%(x.artist, x.title) for x in self.playlist])
		self.pandoraPlaylist.update_playlist(self.playlist)
		self.uiPanel.update_playlist()

		return True

	def connect(self, runOnConnect=None, runOnError=None):
		@pm_helpers.unthread
		def handle_connection_error(error=None):
			if error:
				pympe.error('\n%s: %s\n%s'%(error.message, error.status, error.submsg), False, False)
				self.uiPanel.labelConnect.SetLabel('%s:\n%s'%(error.message, error.submsg))

			self.uiPanel.buttonConnect.Enable()
			self.uiPanel.labelConnect.SetLabel('Problem connecting')
			self.uiPanel.Layout()

			if runOnError:
				pm_helpers.unthread_function(runOnError, e.status, e.message, e.submsg)

		# Do the unthreaded part
		@pm_helpers.unthread
		def finish_connecting():
			if not self.connection:
				self.disconnect()
				return False

			self.uiPanel.buttonConnect.Enable()

			# Select a station
			self.stations = self.connection.stations
			self.uiPanel.uiChoice.SetItems([x.name for x in self.stations])

			# If we had a problem fetching the station
			result = self.set_station(id=cfg_get('defaultStation'))

			if result == False:
				handle_connection_error()
				return False

			# Update the gui
			self.uiPanel.box.Show(0, True)
			self.uiPanel.box.Show(1, False)
			self.uiPanel.sizer.Show(18, False)
			self.uiPanel.sizer.Show(19, False)
			self.uiPanel.sizer.Show(20, True)

			if self.station in self.stations:
				self.uiPanel.uiChoice.SetSelection(self.stations.index(self.station))
			else:
				self.uiPanel.uiChoice.SetSelection(0)

			self.uiPanel.Layout()

			# Successfully connected
			pympe.debug('Pandora Sucessfully Connected')
			self.connected = True

			if runOnConnect:
				runOnConnect()

		@pm_helpers.threaded
		def start_connecting():
			# Try and connect
			try:
				self.connection.connect(cfg_get('username'), cfg_get('password'))
				finish_connecting()  # Finish connecting in the main thread
			except pandora.PandoraError, e:
				handle_connection_error(e)

				if e.status == 1002: # Bad user/password
					pm_helpers.unthread_function(self.plu_settings, None, connectOnExit=True)

		###############################################
		# Show it
		pympe.debug('Pandora Initiating Connection')

		self.uiPanel.labelConnect.SetLabel('Connecting')
		self.uiPanel.buttonConnect.Enable(False)
		self.uiPanel.Layout()

		# Make the connection
		self.connection = pandora.make_pandora()
		self.connection.stations = [] # Put this here cause it only gets added later
		start_connecting()


	def disconnect(self, label='You are not connected', showButton=True):
		self.connected = False
		self.stations = []
		self.connection = None

		# Set the label and show/hide the connect button
		self.uiPanel.labelConnect.SetLabel(label)
		self.uiPanel.buttonConnect.Show(showButton)
		self.uiPanel.buttonConnect.Enable(showButton)

		self.uiPanel.box.Show(0, False, True) # Hide the connected one
		self.uiPanel.box.Show(1, True, True) # Show the connect sizer

		self.uiPanel.Layout()

	def mpevent_player_gst_error(self, event, error, debug):
		if self.pandoraPlaylist and pympe.player.currentPlaylist == self.pandoraPlaylist.uid:
			self.disconnect('Reconnecting', False)
			self.connect()

	def plu_start(self):
		# Show the panel
		self.uiPanel = Panel(pympe.ui.uiMain.uiTabsLeft, self)
		pympe.ui.uiMain.uiTabsLeft.InsertPage(1, self.uiPanel, "Pandora Radio")
		pympe.ui.uiMain.uiTabsLeft.SetSelection(1)

		## Menu Hooks
		#self.menuHook = wx.Menu()
		#self.menuHook.Append(0, 'Thumbs Up')
		#self.menuHook.AppendSeparator()
		#self.menuHook.Append(1, 'Thumbs Down')
		#self.menuHook.Append(2, 'Tired Of This')
		#self.menuHook.Append(3, 'Ban')
		#self.menuHook.Bind(wx.EVT_MENU, self.event_menu_hook)

		#pympe.ui.set_menu_hook('Pandora', self.menuHook)

		# Gst error catch
		pympe.events.player_gst_error.subscribe(self.mpevent_player_gst_error)

		# Create the playlist
		self.pandoraPlaylist = pympe.playlists.create_playlist('Pandora Radio', PandoraPlaylist)
		self.pandoraPlaylist.plugin = self

		if self.debug:
			with open('stationout.pickle') as pd: self.stations = pickle.load(pd)
		#else:
			## Connect and get the stations
			#self.connect(self.finish_starting)

			#if not os.path.exists('stationout.pickle'):
				#with open('stationout.pickle', 'w') as pd: pickle.dump(self.stations, pd)


	def plu_stop(self, shutdown):
		#print 'Stopping Pandora'
		if self.uiPanel is None: return
		if self.connected: cfg_set('defaultStation', self.stations[self.uiPanel.uiChoice.GetSelection()].id)
		pympe.playlists.delete_playlist(self.pandoraPlaylist.uid)

		#pympe.ui.remove_menu_hook(self.menuHook)
		pympe.events.player_gst_error.unsubscribe(self.mpevent_player_gst_error)

		# Remove the panel
		for index in range(pympe.ui.uiMain.uiTabsLeft.GetPageCount()):
			if pympe.ui.uiMain.uiTabsLeft.GetPage(index) == self.uiPanel:
				pympe.ui.uiMain.uiTabsLeft.RemovePage(index)
				break


	def plu_settings(self, parent, connectOnExit=False):
		self.settingsDialog = SettingsDialog(self, parent, connectOnExit)
