import pympe, pyinotify
import wx, time, pm_helpers, os

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, "Library Watch Settings")

		self.plugin = plugin
		sizer = wx.GridBagSizer(5, 5)

	# Update frequency
		uiLabelUpdate = wx.StaticText(self, -1, 'Scan Every')
		uiChoiceUpdate = wx.Choice(self, -1, choices=['10 minutes', '20 minutes', '30 minutes', '1 hour', '2 hours', '4 hours', '8 hours', '12 hours', '24 hours', 'Never'], size=(200, -1))

		uiFont = uiLabelUpdate.GetFont()
		uiFont.SetWeight(wx.FONTWEIGHT_BOLD)
		uiLabelUpdate.SetFont(uiFont)

		uiChoiceUpdate.choiceMap = [600, 1200, 1800, 3600, 7200, 14400, 28800, 43200, 86400, None]

		uiChoiceUpdate.SetSelection(uiChoiceUpdate.choiceMap.index(cfg_get('frequency')))

		uiChoiceUpdate.Bind(wx.EVT_CHOICE, self.event_choice_update)

		sizer.Add(uiLabelUpdate, (0, 0), (1, 3), wx.ALIGN_CENTER|wx.TOP, 10)
		sizer.Add(uiChoiceUpdate, (1, 0), (1, 3), wx.EXPAND)

		sizer.Add(wx.StaticText(self, -1, 'Next scan in %s'%pm_helpers.make_display_time(cfg_get('frequency')-(time.time()-cfg_get('lastRan')))), (2, 0))

	# Controls
		sizer.Add(wx.StaticLine(self, -1), (3, 0), (1, 3), wx.EXPAND|wx.TOP|wx.BOTTOM, 10)
		sizer2 = wx.BoxSizer(wx.HORIZONTAL)
		sizer.Add(sizer2, (4, 0), (1, 3), wx.EXPAND|wx.BOTTOM, 10)
		sizer2.Add(wx.Button(self, wx.ID_OK))
		sizer2.AddStretchSpacer()
		sizer2.Add(wx.Button(self, wx.ID_CANCEL))

		# Add the sizer into the dialog with a space
		box = wx.BoxSizer(wx.VERTICAL)
		box.Add(sizer, 1, wx.EXPAND|wx.LEFT|wx.RIGHT, 20)
		self.SetSizerAndFit(box)

		result = self.ShowModal()
		if result == wx.ID_CANCEL: return

	def event_choice_update(self, event):
		cfg_set('frequency', event.EventObject.choiceMap[event.Int])
		self.plugin.timer.Stop()
		if event.EventObject.choiceMap[event.Int] is not None:
			self.plugin.timer.Start(cfg_get('frequency')*1000)

def pp(*args, **kwargs):
	print 'PP'

class EventHandler(pyinotify.ProcessEvent):
	lastCookie = None

	def process_default(self, event):
		if event.dir: return

		#print event
		#if event.mask&pyinotify.IN_MOVED_FROM:
		#	print event

		#if event.mask&(pyinotify.IN_MOVED_TO):
		#	print event.maskname, event.cookie, event.src_pathname, event.pathname
		#print event.cookie
		if event.mask&(pyinotify.IN_CLOSE_WRITE|pyinotify.IN_MOVED_TO) and not event.dir:
			print event.maskname, 'Add/Update File %s'%event.pathname
			#pympe.library.insert_track_from_uri('file://%s'%event.pathname)
		elif event.mask&(pyinotify.IN_DELETE|pyinotify.IN_MOVED_FROM) and not event.dir:
			print event.maskname, 'Remove File %s'%event.pathname
			#trackId = pympe.library.get_trackid_from_uri('file://%s'%event.pathname)
			#if trackId:
				#pympe.library.remove_single_track(trackId)
			#pympe.library.insert_track_from_uri('file://%s'%event.pathname)

		#if 'nitrogen' in event.name and not self.saw:
			#self.saw = True
		#if self.saw:
			#print time.strftime('%H:%M:%S'),event.maskname, event.pathname

class Plugin:
	def plu_settings(self, parent):
		pass
		#SettingsDialog(self, parent)

	def plu_start(self):
		self.watchManager = pyinotify.WatchManager()

		events = pyinotify.IN_CLOSE_WRITE|pyinotify.IN_MOVED_FROM|pyinotify.IN_MOVED_TO|pyinotify.IN_DELETE

		print 'Starting watch'
		for library in pympe.libraries.values():
			if pympe.config.get(library.configKey, 'watchForChanges'):
				for path in pympe.config.get(library.configKey, 'searchPaths'):
					print 'Watching %s'%path
					watch = self.watchManager.add_watch(os.path.realpath(path), events, rec=True, auto_add=True)


		self.notifier = pyinotify.ThreadedNotifier(self.watchManager, EventHandler())
		self.notifier.coalesce_events()
		self.notifier.start()

		#self.timer = wx.Timer(None)
		#self.timer.Bind(wx.EVT_TIMER, self.event_timer)
		#if cfg_get('frequency') is not None:
			#self.timer.Start(cfg_get('frequency')*1000)

		#if time.time()-cfg_get('lastRan') > cfg_get('frequency'):
			#self.event_timer(None)


	def  plu_stop(self, shutdown):
		self.notifier.stop()
		#self.watchManager.close()
		#self.timer.Stop()

	#def event_timer(self, event):
		#cfg_set('lastRan', time.time())
		#pympe.library.update_library(pympe.config.get("database", "locations"))

		##print 'Update Library', time.time()
