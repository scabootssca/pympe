import pympe, dbus, pm_helpers, pm_devices
from dbus.mainloop.glib import DBusGMainLoop
from dbus.exceptions import DBusException

class Plugin:
	def plu_start(self):
		self.obj = pympe.dbus.systemBus.get_object('org.freedesktop.UDisks', '/org/freedesktop/UDisks')
		self.manager = dbus.Interface(self.obj, 'org.freedesktop.UDisks')

		self.manager.connect_to_signal('DeviceAdded', self.udisk_device_added)
		self.manager.connect_to_signal('DeviceRemoved', self.udisk_device_removed)
		self.manager.connect_to_signal('DeviceChanged', self.udisk_device_changed)

		# For signals for device disconnected to actually disconnect it
		pympe.events.device_disconnected.subscribe(self.mpevent_device_disconnected)

		# Scan for new devices
		for devicePath in self.manager.EnumerateDevices():
			self.udisk_device_added(devicePath)

	def plu_stop(self, shutdown):
		pympe.events.device_disconnected.unsubscribe(self.mpevent_device_disconnected)

	def plu_settings(self, parent):
		pass

	def mpevent_device_disconnected(self, event, device):
		obj = pympe.dbus.systemBus.get_object('org.freedesktop.UDisks', device.devPath)
		iface = dbus.Interface(obj, 'org.freedesktop.UDisks.Device')

		## Unmount and detach it
		#try:
			#iface.FilesystemUnmount([])
			#iface.DriveDetach([])
		#except DBusException, e:
			#pympe.output('Problem Disconnecting Physical Drive: %s'%e)

	def udisk_device_added(self, path):
		# Check that it already exists and if it does then return
		if pympe.devices.get_device_by('devPath', path): return

		# Dbus interface for the device
		obj = pympe.dbus.systemBus.get_object('org.freedesktop.UDisks', path)
		iface = dbus.Interface(obj, 'org.freedesktop.DBus.Properties')

		# If it's a partition table then skip it or not a usb device (Might need to work with other interfaces?)
		if bool(iface.Get(path, 'DeviceIsPartitionTable')): return
		if str(iface.Get(path, 'DriveConnectionInterface')) != 'usb': return

		# Extract the device name and capacity
		deviceName = '%s %s'%(iface.Get(path, 'DriveVendor'), iface.Get(path, 'DriveModel'))
		deviceCapacity = int(iface.Get(path, 'DeviceSize'))
		deviceUuid = str(iface.Get(path, 'IdUuid'))

		if bool(iface.Get(path, 'DeviceIsMounted')): mountPoint = list(iface.Get(path, 'DeviceMountPaths'))[0]
		else: mountPoint = None

		# Create the (generic) device
		device = pympe.devices.new_device(pympe.devices.DeviceClass, devPath=path, mountPoint=mountPoint, uuid=deviceUuid, name=deviceName, capacity=deviceCapacity)

		print 'Created new device [%s][%s] with capacity [%s]'%(deviceName, deviceUuid, pm_helpers.make_display_filesize(deviceCapacity))
		if device.mountPoint:
			print 'It is mounted at [%s]'%device.mountPoint
			try:
				device.ready()
			except:
				pympe.error('Unable to ready device via Udisks', True, False)

		'''
		with meizu DevicePresentationIconName: multimedia-player
		idUsage = filesystem

		with ipod
		DevicePresentationIconName: multimedia-player-apple-ipod-classic-black

		not DeviceIsPartitionTable?
		'''

	def udisk_device_removed(self, path):
		# The existing device
		device = pympe.devices.get_device_by('devPath', path)
		if not device: return

		# Remove it
		pympe.devices.remove_device(device.uid)
		print 'Removed Device [%s]'%device.name

		# Delete it
		del device

	def udisk_device_changed(self, path):
		# Dbus interface for the device
		obj = pympe.dbus.systemBus.get_object('org.freedesktop.UDisks', path)
		iface = dbus.Interface(obj, 'org.freedesktop.DBus.Properties')

		# The existing device
		device = pympe.devices.get_device_by('devPath', path)
		if not device: return

		# Mount it if it's not already
		if not device.mountPoint and bool(iface.Get(path, 'DeviceIsMounted')):
			device.mountPoint = list(iface.Get(path, 'DeviceMountPaths'))[0]
			print 'Mounted [%s] at [%s]'%(device.name, device.mountPoint)
			device.ready()

		capacity = int(iface.Get(path, 'DeviceSize'))
		if not device.capacity and capacity:
			device.capacity = int(iface.Get(path, 'DeviceSize'))
			print 'Set Capacity of [%s] to [%s]'%(device.name, pm_helpers.make_display_filesize(device.capacity))
