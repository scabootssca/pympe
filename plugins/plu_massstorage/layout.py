class SettingsDialog:
	def __init__(self):
		Display mount interface
		
		#[ Music Folder On Mount Point ]
		#  [Read]               [Sync]
		#  [Ok]                [Close]
		
	def read(self):
		read the folder on the device
		
	def sync(self):
		sync
		

# -*- coding: iso-8859-15 -*-
import pympe, wx, pm_helpers, os, shutil, dbus
import pm_ui.pm_ui_library_tab as pm_ui_library_tab
from pm_devices import Device
from pm_dbus import HalHandler

def menu_misc(topMenu):
	topMenu.Append(7443687, "Copy To Local Library")
	topMenu.Append(4323564, "Remove From Device")

def menu_device(topMenu): pass

class DeviceMassStorage(Device):
	def __init__(self, name, mountpoint, plugin):
		Device.__init__(self, name)
		self.mountpoint = mountpoint
		self.plugin = plugin
		self.plugin.balls = self

	def connect(self):
		self.uiPanelMain = pm_ui_library_tab.TabLibrary(pympe.ui.uiMain.uiTabsRight, self.library)
		self.uiPanelMain.uiPanelTracks.menu_misc = menu_misc
		self.uiPanelMain.device = self
		pympe.ui.uiMain.uiTabsRight.InsertPage(1, self.uiPanelMain, self.name)
		print self.mountpoint

		self.connected = True
		
	def disconnect(self):
		if not self.connected: return

		# Remove the panel
		for index in range(pympe.ui.uiMain.uiTabsRight.GetPageCount()):
			if pympe.ui.uiMain.uiTabsRight.GetPage(index) == self.uiPanelMain:
				pympe.ui.uiMain.uiTabsRight.RemovePage(index)
				break
				
		self.connected = False

class Mountpoint:
	def __init__(self, udi):
		self.udi = udi
		
	def __str__(self):
		udis = pympe.dbus.hal.FindDeviceStringMatch("info.parent", self.udi)
		for udi in udis:
			uObject = pympe.dbus.hal.bus.get_object("org.freedesktop.Hal", udi)
			uDevice = dbus.Interface(uObject, "org.freedesktop.Hal.Device")
			if uDevice.GetProperty("volume.is_mounted"): return str(uDevice.GetProperty("volume.mount_point"))
		return ""

class HandlerMassStorage(HalHandler):
    name = "massstorage"
    def __init__(self, plugin):
		HalHandler.__init__(self)
		self.plugin = plugin
    
    def is_compatible(self, device, capabilities):
		if "portable_audio_player" in capabilities:
			try: return "storage" in device.GetProperty("portable_audio_player.access_method.protocols")
			except: pass
		return False

    def get_udis(self):
		udis = []
		for udi in pympe.dbus.hal.FindDeviceByCapability("portable_audio_player"):
			deviceObject = pympe.dbus.hal.bus.get_object("org.freedesktop.Hal", udi)
			device = dbus.Interface(deviceObject, "org.freedesktop.Hal.Device")
			if device.PropertyExists("portable_audio_player.access_method.protocols") and \
				"storage" in device.GetProperty("portable_audio_player.access_method.protocols"):
				udis.append(udi)
		return udis

    def get_device(self, udi):
		massObject = pympe.dbus.hal.bus.get_object("org.freedesktop.Hal", udi)
		mass = dbus.Interface(massObject, "org.freedesktop.Hal.Device")
		if not "storage" in mass.GetProperty("portable_audio_player.access_method.protocols"): return

		mountpoint = Mountpoint(mass.GetProperty("portable_audio_player.storage_device"))
		name = mass.GetProperty("info.vendor")+" "+mass.GetProperty("info.product")
		return DeviceMassStorage(name, mountpoint, self.plugin)

class Plugin:
	def __plugin_start__(self):
		self.handler = HandlerMassStorage(self)

	def __plugin_stop__(self, shutdown):
		self.handler.destroy()
	
	def __plugin_settings__(self):
		self.balls.connect()

	
