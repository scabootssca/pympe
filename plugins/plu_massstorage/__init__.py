import pympe, pm_library, wx, cPickle, os, pm_mediafile, pm_helpers
import pm_ui.pm_ui_library_tab as pm_ui_library_tab
from pm_devices import Device

# Maybe org/freedesktop/UDisks2

class DeviceMSD(pympe.devices.DeviceClass):
	def _setup(self):
		self.musicDir = None

	def _initalize(self): pass

	def _connect(self, indicator):
		# Find the database
		if not os.path.exists(self.mountPoint):
			print 'Unable to mount device, mount point does not exist'
			return False

		return True

	def _read(self):
		if not self.musicDir: return
		self.library.update_library([self.musicDir])
		return True

	def _disconnect(self):
		self.library.stop_scanning()
		return True

	def _write(self, trackIds=[], library=None, data=None):
		result = library.get_multi_track_multi_values(trackIds, library.dbKeys, 'dict')
		print 'Needs a safeguard to prevent writing multiple tracks'

		def inserted_callback(trackIds):
			self.library.organize_threaded(trackIds, None, self.musicDir, keepOriginal=True, winSafe=True)

		trackIds = self.library.insert_multi_tracks(result, callback=inserted_callback)


	def _delete(self, trackIds):
		print 'indevice',trackIds
		self.library.remove_multi_tracks(trackIds)
		return True

	def _syncronize(self, library):
		pass

	def cleanup(self):
		pass


class MSDTabLibrary(pm_ui_library_tab.TabLibrary):
	def __init__(self, parent, database, plugin, device):
		pm_ui_library_tab.TabLibrary.__init__(self, parent, database)
		self.plugin = plugin
		self.device = device


		menu = wx.Menu()
		menu.Append(0, 'Unmount')
		menu.Append(1, 'Sync')
		self.enable_menu(menu, self.event_menu, device.name)

	def event_menu(self, event):
		if event.Id == 0:
			pympe.events.device_disconnect(self.device.uid)
		elif event.Id == 1:
			self.device.syncronize(pympe.library)

class Plugin:
	def plu_start(self):
		self.devices = []

		pympe.events.device_ready.subscribe(self.mpevent_device_ready)
		pympe.events.device_connected.subscribe(self.mpevent_device_connected)
		pympe.events.device_disconnected.subscribe(self.mpevent_device_disconnected)

		for device in pympe.devices.get_ready():
			self.mpevent_device_ready(None, device)

	def plu_stop(self, shutdown):
		# Disconnect all
		for deviceUid in self.devices:
			self.remove_device_library_tab(pympe.devices[deviceUid])
			pympe.devices.remove_device(deviceUid)

		pympe.events.device_ready.unsubscribe(self.mpevent_device_ready)
		pympe.events.device_connected.unsubscribe(self.mpevent_device_connected)
		pympe.events.device_disconnected.unsubscribe(self.mpevent_device_disconnected)

	def plu_settings(self, parent): pass

	def mpevent_device_ready(self, event, device):
		print 'IM READY'
		if device.uid in self.devices: return

		# Need a reliable automatic way to do this
		message = '%s detected would you like to mount it as a mass storage device?'%device.name
		result = wx.MessageDialog(None, message, style=wx.YES_NO|wx.NO_DEFAULT).ShowModal()
		if result != wx.ID_YES: return

		self.devices.append(device.uid)

		# Set the device type
		device = pympe.devices.change_type(device, DeviceMSD)

		# Read
		for directory in os.listdir(device.mountPoint):
			if directory.lower() == 'music':
				device.musicDir = device.mountPoint+os.sep+directory

		# If none was found then browse
		if device.musicDir == None:
			dialog = wx.DirDialog(pympe.ui.uiMain, 'Choice the music directory.', device.mountPoint, style=wx.DD_DEFAULT_STYLE|wx.DD_DIR_MUST_EXIST)
			if dialog.ShowModal() == wx.ID_OK:
				device.musicDir = dialog.GetPath()

		# Try and connect [Maybe check if we have automount?]
		device.connect()

	def mpevent_device_connected(self, event, device):
		if not device.uid in self.devices: return
		self.devices.append(device.uid)
		print 'Device is now connected MSD: %s'%device.name

		# We connected then add the tab
		device.uiTabLibrary = MSDTabLibrary(pympe.ui.uiMain.uiTabsLeft, device.library, self, device)
		pympe.ui.uiMain.uiTabsLeft.InsertPage(2, device.uiTabLibrary, device.name)

		# Read from the device
		device.read()

	def mpevent_device_disconnected(self, event, device):
		if not device.uid in self.devices: return
		print 'Device Disconnected MSD: %s'%device.name

		self.devices.remove(device.uid)
		self.remove_device_library_tab(device)

	def remove_device_library_tab(self, device):
		# Remove the library tab
		if hasattr(device, 'uiTabLibrary'):
			if device.uiTabLibrary:
				for index in range(pympe.ui.uiMain.uiTabsLeft.GetPageCount()):
					if pympe.ui.uiMain.uiTabsLeft.GetPage(index) == device.uiTabLibrary:
						pympe.ui.uiMain.uiTabsLeft.RemovePage(index)
						break

			# Delete the tab
			del device.uiTabLibrary
















	#def device_added(self, device):
		#if not device.uuid in _config['savedDevices']:
			#message = 'Device [%s] detected would you like to mount it as a mass storage device?'%device.name
			#result = wx.MessageDialog(None, message, style=wx.YES_NO|wx.NO_DEFAULT).ShowModal()
			#if result != wx.ID_YES: return
			#else:
				#print 'Needs a browser dialog to select the music directory'

		#if device in self.devices: return

		## Print maybe check if it's already been claimed
		#device.set_type(DeviceMSD)
		#self.devices.append(device)
		#_config['savedDevices'][device.uuid] = [device.name, device.capacity]
		#if self.settingsDialog: self.settingsDialog.update_device_list()
		#self.mount(device)

	#def create_device(self):
		#self.device = pympe.devices.add_device(DeviceMSD, _config['mountPoint'], _config['deviceName'])

	#def mount(self, device):
		#print 'Connecting Device'
		#if not device.connect():
			#print 'Failure'
			#return False
		#print 'Sucessfully Connected'

		## Read
		#for directory in os.listdir(device.mountPoint):
			#if directory.lower() == 'music':
				#device.read(device.mountPoint+os.sep+directory)

		## If none was found then browse
		#if device.typeClass.musicDir == None:
			#dialog = wx.DirDialog(pympe.ui.uiMain, 'Choice the music directory.', device.mountPoint, style=wx.DD_DEFAULT_STYLE|wx.DD_DIR_MUST_EXIST)
			#if dialog.ShowModal() == wx.ID_OK:
				#device.read(dialog.GetPath())

		## Create the library tab
		#device.uiTabLibrary = MsdTabLibrary(pympe.ui.uiMain.uiTabsLeft, device.library, self)
		#pympe.ui.uiMain.uiTabsLeft.InsertPage(2, device.uiTabLibrary, device.name)

		#return True

	#def unmount(self, device):
		#if not device in self.devices: return

		## Unmount
		#if not device.disconnect(): return False

		## Remove the library tab
		#if hasattr(device, 'uiTabLibrary'):
			#if device.uiTabLibrary:
				#for index in range(pympe.ui.uiMain.uiTabsLeft.GetPageCount()):
					#if pympe.ui.uiMain.uiTabsLeft.GetPage(index) == device.uiTabLibrary:
						#pympe.ui.uiMain.uiTabsLeft.RemovePage(index)
						#break

			## Delete the tab
			#del device.uiTabLibrary

		## Remove the device
		#self.devices.remove(device)



