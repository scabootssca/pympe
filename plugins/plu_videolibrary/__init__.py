import pympe, wx, pm_library, pm_language
import pm_ui.pm_ui_library_tab as pm_ui_library_tab



class Plugin:
	def plu_start(self):
		reload(pm_library)

		config = {'ui_filters':[[False, 'genre'], [False, 'year'], [False, 'series'], [False, 'season']],
				'ui_columns':[[1, 'episodenumber', 31], [1, 'title', 270], [1, 'rating', 81], [1, 'series', 200], \
							[1, 'season', 200], [1, 'year', 45], [1, 'genre', 200], [0, 'length', 45], [0, 'bitrate', 30], \
							[0, 'playcount', 35], [0, 'compilation', 30], [0, 'timemodified', 130], [0, 'timeplayed', 130], \
							[0, 'timeadded', 130], [0, 'uri', 300]],
				'sortOrder':{'series':["series", "season", "episodenumber"],
							'episodenumber':["series", "season", "episodenumber"],
							'season':["season", "episodenumber"],
							None:["series", "season", "episodenumber"]},
				'defaultColumn':'title'}

		self.videoLibrary = pm_library.Library('videos', config)

		self.uiTabLibrary = pm_ui_library_tab.TabLibrary(pympe.ui.uiMain.uiTabsLeft, self.videoLibrary)
		pympe.ui.uiMain.uiTabsLeft.InsertPage(2, self.uiTabLibrary, "Videos")
		#
	def plu_stop(self, shutdown):
		self.videoLibrary.shutdown()

		for index in range(pympe.ui.uiMain.uiTabsLeft.GetPageCount()):
			if pympe.ui.uiMain.uiTabsLeft.GetPage(index) == self.uiTabLibrary:
				pympe.ui.uiMain.uiTabsLeft.RemovePage(index)
				break

		del self.videoLibrary
		del self.uiTabLibrary
		#
	#def plu_settings(self, parent):
		#print 'Scanning'
		#self.videoLibrary.scan_single_directory_threaded('/mnt/StorageA/Videos/')
