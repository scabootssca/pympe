import pympe, wx, pm_helpers, gtk, cairo, time

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, "Desktop Cover Settings")

		self.plugin = plugin
		self.lastUpdateTime = 0
		self.plugin.window.update(forceDraw=True)
		sizer = wx.GridBagSizer(5, 5)

		uiLabelPosition = wx.StaticText(self, -1, 'Positioning')
		uiLabelSize = wx.StaticText(self, -1, 'Sizing')
		uiLabelSpacing = wx.StaticText(self, -1, 'Spacing From Edge')
		uiLabelOther = wx.StaticText(self, -1, 'Other')
		uiFont = wx.Font(uiLabelPosition.GetFont().GetPointSize()+1, wx.FONTFAMILY_DEFAULT, wx.DEFAULT, wx.FONTWEIGHT_BOLD)
		uiLabelPosition.SetFont(uiFont)
		uiLabelSize.SetFont(uiFont)
		uiLabelSpacing.SetFont(uiFont)
		uiLabelOther.SetFont(uiFont)

		self.uiChoiceCorner = wx.Choice(self, -1, choices=['Top Left','Top Right', 'Bottom Left', 'Bottom Right', 'Center'])
		self.uiSpinMonitor = wx.SpinCtrl(self, -1, min=0, max=gtk.gdk.screen_get_default().get_n_monitors()-1, initial=cfg_get('monitor'))
		self.uiCheckSize = wx.CheckBox(self, -1, 'Force To Size')
		self.uiSpinSize = wx.SpinCtrl(self, -1, min=50, max=10000, initial=cfg_get('size'))
		self.uiSpinXSpacing = wx.SpinCtrl(self, -1, min=-10000, max=10000, initial=cfg_get('spacingX'))
		self.uiSpinYSpacing = wx.SpinCtrl(self, -1, min=-10000, max=10000, initial=cfg_get('spacingY'))
		self.uiCheckOnlyWhenMinimized = wx.CheckBox(self, -1, 'Only Show When Minimized')
		self.uiSliderOpacity = wx.Slider(self, -1, minValue=0, maxValue=100)
		self.uiSliderOpacityHover = wx.Slider(self, -1, minValue=0, maxValue=100)

		self.uiChoiceCorner.SetSelection(cfg_get('corner'))
		self.uiCheckSize.SetValue(cfg_get('useCustomSize'))
		self.uiSpinSize.Enable(self.uiCheckSize.GetValue())
		self.uiCheckOnlyWhenMinimized.SetValue(_config['onlyShowWhenMinimized'])
		self.uiSliderOpacity.SetValue(_config['opacity']*100)
		self.uiSliderOpacityHover.SetValue(_config['opacityHover']*100)


		sizer.Add(uiLabelPosition, (0, 0), (1, 2), flag=wx.ALIGN_CENTER|wx.TOP|wx.BOTTOM, border=5)
		sizer.Add(wx.StaticText(self, -1, 'Position:'), (1, 0), flag=wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT)
		sizer.Add(self.uiChoiceCorner, (1, 1), flag=wx.EXPAND)

		sizer.Add(wx.StaticText(self, -1, 'Monitor Number:'), (2, 0), flag=wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT)
		sizer.Add(self.uiSpinMonitor, (2, 1), flag=wx.EXPAND)

		sizer.Add(uiLabelSize, (3, 0), (1, 2), flag=wx.ALIGN_CENTER|wx.TOP|wx.BOTTOM, border=5)
		sizer.Add(self.uiCheckSize, (4, 0), flag=wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT)
		sizer.Add(self.uiSpinSize, (4, 1), flag=wx.EXPAND)

		sizer.Add(uiLabelSpacing, (5, 0), (1, 2), flag=wx.ALIGN_CENTER|wx.TOP|wx.BOTTOM, border=5)
		sizer.Add(wx.StaticText(self, -1, 'X Spacing:'), (6, 0), flag=wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT)
		sizer.Add(self.uiSpinXSpacing, (6, 1), flag=wx.EXPAND)

		sizer.Add(wx.StaticText(self, -1, 'Y Spacing:'), (7, 0), flag=wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT)
		sizer.Add(self.uiSpinYSpacing, (7, 1), flag=wx.EXPAND)

		sizer.Add(uiLabelOther, (8, 0), (1, 2), flag=wx.ALIGN_CENTER|wx.TOP|wx.BOTTOM, border=5)
		sizer.Add(self.uiCheckOnlyWhenMinimized, (9, 0), (1, 2), flag=wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT)

		sizer.Add(wx.StaticText(self, -1, 'Opacity:'), (10, 0), flag=wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT)
		sizer.Add(self.uiSliderOpacity, (10, 1), flag=wx.EXPAND)

		sizer.Add(wx.StaticText(self, -1, 'Opacity On Hover:'), (11, 0), flag=wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT)
		sizer.Add(self.uiSliderOpacityHover, (11, 1), flag=wx.EXPAND)

		sizer.Add(wx.StaticLine(self, -1), (12, 0), (1, 2), flag=wx.EXPAND|wx.TOP|wx.BOTTOM, border=5)
		sizer.Add(wx.Button(self, wx.ID_OK), (13, 1), flag=wx.ALIGN_RIGHT)

		self.uiChoiceCorner.Bind(wx.EVT_CHOICE, self.save_and_update)
		self.uiCheckSize.Bind(wx.EVT_CHECKBOX, self.event_check)
		self.Bind(wx.EVT_BUTTON, self.event_button)
		self.Bind(wx.EVT_TEXT, self.save_and_update)

		box = wx.BoxSizer()
		box.Add(sizer, 1, wx.EXPAND|wx.ALL, 10)
		self.SetSizerAndFit(box)
		self.Show()

	def event_check(self, event):
		self.uiSpinSize.Enable(self.uiCheckSize.GetValue())
		self.save_and_update()

	def event_button(self, event):
		if event.Id == wx.ID_OK:
			self.save_and_update(forceDraw=False)
			self.Destroy()

		event.Skip()

	def Destroy(self):
		self.plugin.settings = None
		wx.Dialog.Destroy(self)

	def save_and_update(self, event=None, forceDraw=True):
		if forceDraw and time.time()-self.lastUpdateTime < .2:
			return
		self.lastUpdateTime = time.time()

		_config['onlyShowWhenMinimized'] = self.uiCheckOnlyWhenMinimized.GetValue()
		cfg_set('useCustomSize', self.uiCheckSize.GetValue())
		cfg_set('size', self.uiSpinSize.GetValue())
		cfg_set('corner', self.uiChoiceCorner.GetSelection())
		cfg_set('monitor', self.uiSpinMonitor.GetValue())
		cfg_set('spacingX', self.uiSpinXSpacing.GetValue())
		cfg_set('spacingY', self.uiSpinYSpacing.GetValue())
		cfg_set('opacity', self.uiSliderOpacity.GetValue()*.01)
		cfg_set('opacityHover', self.uiSliderOpacityHover.GetValue()*.01)
		self.plugin.window.update(forceDraw)

class ArtworkWindow(gtk.Window):
	def __init__(self, plugin):
		gtk.Window.__init__(self)

		self.plugin = plugin
		self.image = gtk.Image()

		# Play/Pause Button
		buttonPlayPause = gtk.Button()
		buttonPlayPause.connect('clicked', self.event_button_clicked, 0)
		imagePlayPause = gtk.Image()
		imagePlayPause.set_from_stock(gtk.STOCK_MEDIA_PLAY if not pympe.player.playing or pympe.player.paused else gtk.STOCK_MEDIA_PAUSE, gtk.ICON_SIZE_BUTTON)
		buttonPlayPause.set_image(imagePlayPause)

		# Next Button
		buttonNext = gtk.Button()
		buttonNext.connect('clicked', self.event_button_clicked, 1)
		imageNext = gtk.Image()
		imageNext.set_from_stock(gtk.STOCK_MEDIA_NEXT, gtk.ICON_SIZE_BUTTON)
		buttonNext.set_image(imageNext)

		# Next Button
		buttonPlayer = gtk.Button()
		buttonPlayer.connect('clicked', self.event_button_clicked, 2)
		imagePlayer = gtk.Image()
		logo = gtk.gdk.pixbuf_new_from_file(pympe.cwd+"images/logoSmall.png")
		imagePlayer.set_from_pixbuf(logo)
		buttonPlayer.set_image(imagePlayer)

		buttonPlayPause.show()
		buttonNext.show()
		buttonPlayer.show()

		self.defaultSize = buttonPlayPause.size_request()[1]
		buttonPlayPause.set_size_request(self.defaultSize, self.defaultSize)
		buttonNext.set_size_request(self.defaultSize, self.defaultSize)
		buttonPlayer.set_size_request(self.defaultSize, self.defaultSize)

		# Button Box
		alignment = gtk.Alignment(0.5)
		fixed = gtk.Fixed()
		self.buttonBox = gtk.HBox()

		alignment.add(fixed)
		fixed.put(self.buttonBox, 0, -self.defaultSize)
		self.buttonBox.pack_start(buttonPlayer)
		self.buttonBox.pack_start(buttonPlayPause)
		self.buttonBox.pack_start(buttonNext)
		self.buttonBox.show()

		self.box = gtk.VBox()
		self.box.pack_start(self.image)
		self.box.pack_start(alignment, False)
		self.add(self.box)

		self.minSize = self.buttonBox.size_request()[0]

		self.set_accept_focus(False)
		self.set_app_paintable(True)
		self.set_decorated(False)
		self.set_keep_below(True)
		self.set_resizable(False)
		self.set_role("desktopcover")
		self.set_skip_pager_hint(True)
		self.set_skip_taskbar_hint(True)
		self.set_title("Pympe desktop cover")
		self.stick()

		self.connect('enter-notify-event', self.event_enter_notify, True)
		self.connect('leave-notify-event', self.event_enter_notify, False)
		self.show_all()

		self.buttonBox.hide()

		self.update()

	def event_button_clicked(self, button, buttonId):
		if buttonId == 0:
			if not pympe.player.playing or pympe.player.paused:
				pympe.events.player_play()
				imagePlayPause = gtk.Image()
				imagePlayPause.set_from_stock(gtk.STOCK_MEDIA_PAUSE, gtk.ICON_SIZE_BUTTON)
				button.set_image(imagePlayPause)
			else:
				pympe.events.player_pause()
				imagePlayPause = gtk.Image()
				imagePlayPause.set_from_stock(gtk.STOCK_MEDIA_PLAY, gtk.ICON_SIZE_BUTTON)
				button.set_image(imagePlayPause)
		elif buttonId == 1:
			pympe.events.player_next()
		elif buttonId == 2:
			pympe.events.pympe_raise()

	def event_enter_notify(self, widget, event, entered):
		if entered:
			self.set_opacity(_config['opacityHover'])
			self.buttonBox.show()

		elif event.detail != gtk.gdk.NOTIFY_INFERIOR:
			self.set_opacity(_config['opacity'])
			self.buttonBox.hide()

	@pm_helpers.threaded
	def update(self, forceDraw=False):
		if pympe.player.currentInfo and 'art' in pympe.player.currentInfo:
			image = pm_helpers.PmImage(string=pympe.player.currentInfo["art"])
		else:
			image = pm_helpers.PmImage(stock=pm_helpers.get_stock_media_icon())

		if cfg_get('useCustomSize'):
			size = cfg_get('size')
			image.scale(size, size)

		pixbuf = image.convert_to_pixbuf()

		@pm_helpers.unthread
		def draw(self, pixbuf):
			if pixbuf: self.image.set_from_pixbuf(pixbuf)

			spacing = (cfg_get('spacingX'), cfg_get('spacingY'))
			corner = cfg_get('corner')
			size = image.get_size()
			#size = cfg_get('size')

			# Get all the screen size variables
			offset = gtk.gdk.get_default_root_window().property_get(gtk.gdk.atom_intern('_NET_WORKAREA'))[2]
			dimensions = list(gtk.gdk.screen_get_default().get_monitor_geometry(cfg_get('monitor')))
			#allocation = (0, 0, size[0], size[1])

			# Calculate the position
			if corner == 0: (x, y) = (dimensions[0]+spacing[0], dimensions[1]+spacing[1])
			elif corner == 1: (x, y) = (dimensions[0]+dimensions[2]-spacing[0]-size[0], dimensions[1]+spacing[1])
			elif corner == 2: (x, y) = (dimensions[0]+spacing[0], dimensions[1]+dimensions[3]-size[1]-spacing[1])
			elif corner == 3: (x, y) = (dimensions[0]+dimensions[2]-spacing[0]-size[0], dimensions[1]+dimensions[3]-size[1]-spacing[1])
			elif corner == 4: (x, y) = (dimensions[0]+spacing[0]+(dimensions[2]*.5)-(size[0]*.5), dimensions[1]+spacing[1]+(dimensions[3]*.5)-(size[1]*.5))

			if x < offset[0]+spacing[0]: x = offset[0]+spacing[0]
			if y < offset[1]+spacing[1]: y = offset[1]+spacing[1]

			# For removing the buttons that don't fit
			if size[0] >= self.defaultSize*3: self.buttonBox.get_children()[0].show()
			else: self.buttonBox.get_children()[0].hide()

			if size[0] >= self.defaultSize*2: self.buttonBox.get_children()[2].show()
			else: self.buttonBox.get_children()[2].hide()

			if size[0] >= self.defaultSize: self.buttonBox.get_children()[1].show()
			else: self.buttonBox.get_children()[1].hide()

			imagePlayPause = gtk.Image()
			imagePlayPause.set_from_stock(gtk.STOCK_MEDIA_PLAY if not pympe.player.playing or pympe.player.paused else gtk.STOCK_MEDIA_PAUSE, gtk.ICON_SIZE_BUTTON)
			self.buttonBox.get_children()[1].set_image(imagePlayPause)

			# Move the box and center the buttons
			self.move(int(x), int(y))
			#self.box.move(self.buttonBox, int((size[0]-self.buttonBox.size_request()[0])/2.0), size[1]-self.buttonBox.size_request()[1])

			#fixed = self.box.get_children()[1]
			#fixed.move(fixed.get_children()[0], (self.image.size_request()[0]-fixed.size_request()[0])/2, -self.buttonBox.size_request()[1])

			self.box.resize_children()

			self.set_opacity(_config['opacity'])


			# Don't show if we are not minimized and only show on minimize
			if _config['onlyShowWhenMinimized'] and not pympe.ui.uiMain.minimized() and not forceDraw and not self.plugin.settings:
				self.hide()
			else:
				self.show()

		draw(self, pixbuf)

	def destroy(self):
		gtk.Window.destroy(self)

class Plugin:
	def plu_settings(self, parent):
		self.settings = SettingsDialog(self, parent)

	def plu_start(self):
		self.settings = None
		self.window = ArtworkWindow(self)

		subscribe(pympe.events.player_playback_paused, self.mpevent_update)
		subscribe(pympe.events.player_playback_resumed, self.mpevent_update)
		subscribe(pympe.events.player_playback_stopped, self.mpevent_update)
		subscribe(pympe.events.player_playback_started, self.mpevent_update)
		subscribe(pympe.events.player_current_changed, self.mpevent_update)
		subscribe(pympe.events.player_current_updated, self.mpevent_update)
		subscribe(pympe.events.ui_iconized, self.mpevent_iconize)

	def plu_stop(self, shutdown):
		self.window.destroy()

	def mpevent_update(self, event, *args):
		self.window.update()

	def mpevent_iconize(self, event, iconized):
		self.window.stick()

		if _config['onlyShowWhenMinimized'] and not iconized and not self.settings:

			self.window.hide()
		else:
			self.window.show()

