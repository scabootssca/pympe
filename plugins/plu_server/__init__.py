import pympe
import wx, time, pm_helpers, shutil

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, "Network Settings")


		self.plugin = plugin
		
	# Controls
		# Create them
		self.uiSpinPort = wx.SpinCtrl(self, -1, min=0, max=9999, initial=cfg_get('port'))
		self.uiCheckShareMusic = wx.CheckBox(self, -1, 'Share Music')
		self.uiCheckShareVideos = wx.CheckBox(self, -1, 'Share Videos')
		self.uiCheckShowPlayer = wx.CheckBox(self, -1, 'Show Flash Player')
		self.uiCheckRequirePassword = wx.CheckBox(self, -1, 'Require Password')
		self.uiTextPassword = wx.TextCtrl(self, -1, cfg_get('password'))
		self.uiLabelRunning = wx.StaticText(self, -1, 'Server is: %s'%('Running' if plugin.serverRunning else 'Stopped'))
		self.uiButtonRunning = wx.Button(self, 2001, '%s Server'%('Stop' if plugin.serverRunning else 'Start'))
		
		
		# Set them
		self.uiCheckShareMusic.SetValue(cfg_get('shareMusic'))
		self.uiCheckShareVideos.SetValue(cfg_get('shareVideos'))
		self.uiCheckShowPlayer.SetValue(cfg_get('showPlayer'))
		self.uiCheckRequirePassword.SetValue(cfg_get('requirePassword'))
		self.uiTextPassword.Enable(self.uiCheckRequirePassword.GetValue())
		
		# Add them to the sizer
		sizerControls = wx.BoxSizer(wx.VERTICAL)
		sizerControls.Add(wx.StaticText(self, -1, 'Port:'))
		sizerControls.Add(self.uiSpinPort, flag=wx.ALIGN_LEFT|wx.EXPAND)
		sizerControls.Add(wx.StaticLine(self), 0, wx.EXPAND|wx.ALL, 10)
		sizerControls.Add(self.uiCheckShareMusic, flag=wx.EXPAND)
		sizerControls.Add(self.uiCheckShareVideos, flag=wx.EXPAND)
		sizerControls.Add(self.uiCheckShowPlayer, flag=wx.EXPAND)
		sizerControls.Add(wx.StaticLine(self), 0, wx.EXPAND|wx.ALL, 10)
		sizerControls.Add(self.uiCheckRequirePassword, flag=wx.EXPAND)
		sizerControls.Add(self.uiTextPassword, flag=wx.EXPAND)
		sizerControls.Add(wx.StaticLine(self), 0, wx.EXPAND|wx.ALL, 10)
		sizerControls.Add(self.uiLabelRunning)
		sizerControls.Add(self.uiButtonRunning, 0, wx.EXPAND)
		
		sizerControls.Add(wx.Button(self, 10004, 'Reload ServFunc'), 0, wx.EXPAND)
		
		# Confirm buttons
		sizerConfirm = wx.BoxSizer()
		sizerConfirm.Add(wx.Button(self, wx.ID_OK))
		sizerConfirm.AddStretchSpacer()
		sizerConfirm.Add(wx.Button(self, wx.ID_CANCEL))

		# Add the sizer into the dialog with a space
		box = wx.BoxSizer(wx.VERTICAL)
		box.Add(sizerControls, 0, wx.EXPAND|wx.ALL, 10)
		box.Add(wx.StaticLine(self), 1, wx.EXPAND|wx.LEFT|wx.RIGHT, 10)
		box.Add(sizerConfirm, 0, wx.ALL|wx.EXPAND, 10)
		self.SetSizerAndFit(box)
		
		self.Bind(wx.EVT_BUTTON, self.event_button)
		self.uiCheckRequirePassword.Bind(wx.EVT_CHECKBOX, self.event_check)
		
		self.Show()
		
			
	def event_button(self, event):
		if event.Id == wx.ID_OK:
			cfg_set('shareMusic', self.uiCheckShareMusic.GetValue())
			cfg_set('shareVideos', self.uiCheckShareVideos.GetValue())
			cfg_set('showPlayer', self.uiCheckShowPlayer.GetValue())
			cfg_set('requirePassword', self.uiCheckRequirePassword.GetValue())
			cfg_set('password', self.uiTextPassword.GetValue())
			cfg_set('port', self.uiSpinPort.GetValue())
			self.Destroy()
		elif event.Id == 2001:
			cfg_set('shareMusic', self.uiCheckShareMusic.GetValue())
			cfg_set('shareVideos', self.uiCheckShareVideos.GetValue())
			cfg_set('showPlayer', self.uiCheckShowPlayer.GetValue())
			cfg_set('requirePassword', self.uiCheckRequirePassword.GetValue())
			cfg_set('password', self.uiTextPassword.GetValue())
			cfg_set('port', self.uiSpinPort.GetValue())
			
			if not self.plugin.serverRunning: self.plugin.start_server()
			else: self.plugin.stop_server()
			
			self.uiLabelRunning.SetLabel('Server is: %s'%('Running' if self.plugin.serverRunning else 'Stopped'))
			self.uiButtonRunning.SetLabel('%s Server'%('Stop' if self.plugin.serverRunning else 'Start'))
		elif event.Id == 10004:
			import simpleServer, servfunc
			reload(simpleServer)
			reload(servfunc)
			simpleServer.Client.send_response = servfunc.respond
		else:
			event.Skip()
			
	def event_check(self, event):
		self.uiTextPassword.Enable(event.Int)

class Plugin:
	def plu_settings(self, parent):
		SettingsDialog(self, parent)

	def start_server(self):
		import simpleServer, servfunc
		reload(simpleServer)
		reload(servfunc)
		simpleServer.Client.send_response = servfunc.respond
		self.server = simpleServer.Server(self, cfg_get('port'))
		self.server.plugin = self
		self.server.playlistIds = {}
		pm_helpers.run_thread(self.server.serve_forever)
		self.serverRunning = True
		
	def stop_server(self):
		if not self.serverRunning: return
		self.server.shutdown()
		self.serverRunning = False
	
	def plu_start(self):
		self.serverRunning = False
		
		self.cfg_get = cfg_get


	def  plu_stop(self, shutdown):
		self.stop_server()
		#self.timerUpdate.Stop()
		#self.timerUpdate.Destroy()
		
		#self.box.Clear(True)
		#pympe.ui.uiMain.uiSizerExtras.Remove(self.box)
		#pympe.ui.uiMain.uiSizerMain.Layout()
