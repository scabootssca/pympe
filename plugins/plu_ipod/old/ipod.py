import pympe, pm_library, gpod, os, time, socket, pm_helpers



class IPod:
	def init_ipod(self):
		"""
		itdb_info_get_ipod_generation_string(*args)
		itdb_info_get_ipod_generation_string(Itdb_IpodGeneration generation) -> gchar
		itdb_info_get_ipod_info_table()
		itdb_info_get_ipod_info_table() -> Itdb_IpodInfo
		itdb_info_get_ipod_model_name_string(*args)
		itdb_info_get_ipod_model_name_string(Itdb_IpodModel model) -> gchar
		itdb_init_ipod(*args)
		itdb_init_ipod(gchar mountpoint, gchar model_number, gchar ipod_name, 
			GError error) -> gboolean
		"""
		model      = gpod.ITDB_IPOD_MODEL_CLASSIC_BLACK
		modelName  = gpod.gpod.itdb_info_get_ipod_model_name_string(model)
		print gpod.gpod.itdb_init_ipod(str(self.mountPoint), modelName, "iPood", None)

	def __init__(self, plugin):
		self.plugin = plugin
		self.mountPoint = None
		
		self.ipodDb  = None
		self.localDb = pm_library.Library()
		self.localDb.blank_database()
		
		self.mounted = False
		self.syncing = False
		
		self.keyLookup = {"uri":"ipod_path",
					"title":"title",
					"artist":"artist",
					"album":"album",
					"albumartist":"albumartist",
					"genre":"genre",
					"playcount":"playcount",
					"samplerate":"samplerate",
					"bitrate":"bitrate",
					"comment":"comment",
					"compilation":"compilation",
					"filesize":"size",
					"tracktotal":"tracks",
					"tracknumber":"track_nr",
					"length":"tracklen", #(in miliseconds not seconds)
					"year":"year",
					"disctotal":"cds",
					"discnumber":"cd_nr",
					"rating":"rating",
					"mimetype":"filetype",
					"timeadded":"time_added",
					"timeplayed":"time_played",
					"timemodified":"time_modified"}
					
	def get_name(self):
		return "iPod: "+self.mountPoint

	def mount(self, mountPoint=None):
		if self.mounted and (not mountPoint or mountPoint == self.mountPoint): return
		if mountPoint: self.mountPoint = mountPoint
		
		pympe.output("Mounting iPod: "+self.mountPoint)
		
		# Check for the mountpoint
		if not os.path.isdir(self.mountPoint):
			return pympe.debug("Invalid iPod mount path: "+self.mountPoint)

		# If it's already mounted then unmount it
		if self.mounted: self.unmount()

		# Try and load the ipod
		#try:
		self.ipodDb = gpod.Database(str(self.mountPoint))
		#except gpod.ipod.DatabaseException as e:
		#	return pympe.error(e)#pympe.error("Error opening iPod at: "+self.mountPoint)

		# Setup the local database
		self.localDb.module_start(self.mountPoint.replace(os.sep, "_"))
		self.plugin.uiPanelMain.update_all()
		
		pympe.output("Sucessfully mounted iPod: "+self.mountPoint)
		
		# Success
		self.mounted = True
		return True
		
	def unmount(self):
		if not self.mounted: return
		
		def printoff(database, track, iterator, total):
			print "%s/%s"%(iterator, total)

		self.ipodDb.copy_delayed_files(printoff)
		self.ipodDb.close()
		self.mounted = False
		#self.localDb.module_stop()
		self.localDb.blank_database()
		pympe.output("Unmounted iPod: "+self.mountPoint)
		return True



	def read(self, updateDelta=237):
		if not self.mounted: return pympe.error("Cannot read ipod without first mounting it.")

		# For each track in the ipod masterDb transfer it to the local
		index, total = 0, len(self.ipodDb.get_master())
		trackBuffer = []

		for track in self.ipodDb.get_master():
			existing = False
			index += 1
			#if not index%10: print "Reading track %s/%s"%(index, total)
			localData = {}
			
			# Get the hash
			localData['hash'] = track['userdata']['sha1_hash']
			
			#Needs a check for existing uri and not reading it if it's already in the local db
			for localKey, ipodKey in self.keyLookup.iteritems():
				if ipodKey in track.keys():
					# Some have specific use cases
					if localKey == "uri":
						localData[localKey] = "file://"+self.mountPoint+track[ipodKey].replace(":", os.sep)
						if localData[localKey] in self.localDb.trackDict:
							existing = True
							break
					elif localKey == "length": localData[localKey] = track[ipodKey]/1000
					elif localKey == 'rating': localData[localKey] = int(track[ipodKey])
					elif localKey in ("timeadded", "timeplayed", "timemodified"):
						try:
							localData[localKey] = time.mktime(track[ipodKey].timetuple())
						except:
							# This has an internal error on some timestamps
							#  File "/media/Storage/Projects/Pympe v3/plugins/plu_ipod/ipod.py", line 73, in read
							#	print track[ipodKey]
							#  File "/usr/lib/pymodules/python2.7/gpod/ipod.py", line 504, in __getitem__
							#	return datetime.datetime.fromtimestamp(getattr(self._track, item))
							#ValueError: timestamp out of range for platform time_t
							pass

							
					else:
						if localKey in self.localDb.multiTagsLocal+self.localDb.multiTagsShared:
							localData[localKey] = [track[ipodKey]]
						else:
							localData[localKey] = track[ipodKey]
					
			if not existing: trackBuffer.append(localData)
			
			## Every updateDelta directories update the library
			if index%updateDelta == 0:
				self.localDb.insert_multi_tracks(trackBuffer)
				trackBuffer = []
				
		## Every updateDelta directories update the library
		if trackBuffer:
			self.localDb.insert_multi_tracks(trackBuffer)

		#self.localDb.send_update_event()
		pympe.output("Sucessfully read iPod: "+self.mountPoint)
		return True
		
	def write_track(self, data=None, mediaFile=None, sendUpdateEvent=False):
		if mediaFile: data = mediaFile.info
		track = gpod.Track(ownerdb=self.ipodDb)

		# Gpod specific info
		track['userdata'] = {'transferred': 0, 'hostname': socket.gethostname(), 'charset': gpod.defaultencoding, 'pc_mtime': data['timemodified']}
		track._set_userdata_utf8('filename', data['uri'][7:])

		integerType = type(0)
		stringType  = type('')

		# Populate the track information
		for key, value in data.iteritems():
			if key in self.keyLookup:
				if key in self.localDb.multiTagsShared+self.localDb.multiTagsLocal: value = value[0] if value else self.localDb.defaultValues[self.localDb.dbKeys.index(key)]
				if key == "length": value *= 1000
				elif key == "year": value = int(value[0])
				elif key == "uri": continue
				
				if key in ["discnumber", "disctotal", "tracknumber", "tracktotal"]:
					if type(value) != integerType: 
						try: value = int(value)
						except: value = 0

				try: track[self.keyLookup[key]] = value
				except: print 'Could not set %s the value is %s'%(key, value)

		

		# Add it to the ipod database and the master playlist
		self.ipodDb.add(track)
		self.ipodDb.get_master().add(track)
		
		# Now copy it and after it's done put it in the local database
		track.copy_to_ipod()
		
		# And the cover art
		#if data['art']: track.set_coverart(pm_helpers.PmImage(string=data['art']).image.GetData())

		#print track.ipod_filename()
		#data["uri"] = "file://"+track.ipod_filename()
		self.localDb.insert_single_track(data, sendUpdateEvent)

	def write_tracks(self, data=None, mediaFiles=None):
		if not data: data = [mf.info for mf in mediaFiles]

		total = len(data)
		for i, x in enumerate(data):
			print "Writing Track %s/%s"%(i+1, total)
			self.write_track(x)
			
		print 'Finished writing tracks to ipod database.'
		#self.localDb.send_update_event()
		
	def remove_track(self, trackId, update=True):
		uri = self.localDb.get_single_track_single_value(trackId, 'uri')

		success = False
		for track in self.ipodDb.get_master():
			if track.ipod_filename() == uri[7:]:
				success = True
				break

		if not success: return


		self.ipodDb.get_master().remove(track)
		self.ipodDb.remove(track)
		self.localDb.remove_single_track(trackId)
		if update: pympe.events.library_entries_removed.send(self.localDb.uid, [trackId])
		
	def remove_multi_tracks(self, trackIds):
		# Maybe make this use the for track in self.ipodDb.get_master():
		# loop in itself where it doesn't have to go through for each uri
		# Maybe go through and make sure of all the uris in one pass through
		for trackId in trackIds: self.remove_track(trackId, False)
		pympe.events.library_entries_removed.send(self.localDb.uid, trackIds)
		#self, trackId)
		
	def sync(self):
		#for track in pympe.library.tracks
		#onIpod = set([(key, self.localDb.tracks[key]['filesize']) for key in self.localDb.tracks.keys()])
		print pympe.library.tracks.values()[0]['hash']
		
