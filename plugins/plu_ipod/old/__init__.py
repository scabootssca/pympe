# -*- coding: iso-8859-15 -*-
import pympe, wx, pm_helpers, os, shutil, cPickle, socket
import pm_ui.pm_ui_library_tab as pm_ui_library_tab
import gpod
import gpod.gpod as cpod
import gpod.gtkpod as gtkpod

import os, stat, time, pm_mediafile, gtk, pm_library
from pm_devices import Device



	#def __init__(self, name):
		#self.name = name
		#self.library = pm_library.Library(name.replace(os.sep, "_"))
		#self.connected = False
		#self.autoconnects = False
		#self.playlists = []
		
	#def get_name(self): return self.name
	
	#def autoconnect(self):
		#if self.autoconnects: return self.connect()
			
	#def connect(self): return False
	#def disconnect(self): return False

	#def get_library(self): return self.library
	#def get_playlists(self): return self.playlists
	
	#def add_tracks(self, trackIds=[], library=None, data=None):
		#if not data:
			#keys =  library.dbKeys
			#data = [dict([(k, v) for k, v in zip(keys, library.get_single_track_multi_values(trackId, keys, 'split'))]) for trackId in trackIds]
		#self.__add_tracks(data)
		
	#def __add_tracks(self, data):
		#pass
		
	#def start_transfer(self):
		#pass


#keyLookup = {"uri":"ipod_path",
					#"title":"title",
					#"artist":"artist",
					#"album":"album",
					#"albumartist":"albumartist",
					#"genre":"genre",
					#"playcount":"playcount",
					#"samplerate":"samplerate",
					#"bitrate":"bitrate",
					#"comment":"comment",
					#"compilation":"compilation",
					#"filesize":"size",
					#"tracktotal":"tracks",
					#"tracknumber":"track_nr",
					#"length":"tracklen", #(in miliseconds not seconds)
					#"year":"year",
					#"disctotal":"cds",
					#"discnumber":"cd_nr",
					#"rating":"rating",
					#"mimetype":"filetype",
					#"timeadded":"time_added",
					#"timeplayed":"time_played",
					#"timemodified":"time_modified"}
					
keyLookup = {'artist':'artist',
			'title':'title',
			'genre':'genre',
			'album':'album',
			'disctotal':'cds',
			'discnumber':'cd_nr',
			'tracknumber':'track_nr',
			'tracktotal':'tracks',
			'length':'tracklen',
			'timeadded':'time_added',
			'timeplayed':'time_played',
			'timemodified':'time_modified'}

#class PympeTrack(gpod.Track):
	#def __init__(self, keys, values, writeArt, trackId):
		#gpod.Track.__init__(self, values[-1][7:])
		##if not os.path.exists(values[-1][7:]):
			##print 'Track file doesn\'t exist %s'%values[-1]
			##self._track = None
			##return
		
		##self._track = gpod.itdb_track_new()
		##self['userdata'] = {'transferred': 0, 'hostname': socket.gethostname(), 'charset':gpod.defaultencoding}
		##self._set_userdata_utf8('filename',values[-1][7:])


		##for key, value in zip(keys, values[:-1]):
			##if value == None: continue
				
			##ipodKey = keyLookup[key]
			
			##if key == 'albumartist': self['artist'] = value
			##elif key == 'title': self['title'] = value
			##elif key == 'album': self['genre'] = value
			##elif key == 'genre': self['genre'] = value
			##elif key == 'tracknumber': self['track_nr'] = int(value)
			##elif key == 'tracktotal': self['tracks'] = int(value)
			##elif key == 'discnumber': self['cd_nr'] = int(value)
			##elif key == 'disctotal': self['cds'] = int(value)
			###elif key == 'timeadded': self['time_added'] = int(value)
			###elif key == 'timeplayed': self['time_played'] = int(value) if value != 0 else int(time.time())
			###elif key == 'timemodified': self['time_modified'] = int(value)

			
			

                                ###('TBPM','BPM'),
                                ###('TCON','genre'),

                                ###('TPOS',('cd_nr','cds')),
                                ###('TRCK',('track_nr','tracks'))):
                ###try:
				
			###if key == 'length': value = int(value*1000)
			###elif key == 'timemodified':
				###self['userdata']['pc_mtime'] = int(value)

			
			###elif key in ['disctotal', 'discnumber', 'tracknumber', 'tracktotal', 'timeadded', 'timeplayed', 'timemodified']:
				###try: value = int(value)
				###except:
					###print 'Problem With %s %s'%(key, value)
					###continue
			###else:
				###value = value.encode('UTF-8', 'replace')

			###self[ipodKey] = value
			
		##for k in self.keys():
			##print k
			##print self[k]

		##if writeArt:
			### Try and write artwork
			##filename = pympe.library.get_artwork_path(trackId)
			##if filename:
				##with open(filename, 'r') as artwork:
					##loader = gtk.gdk.PixbufLoader()
					##loader.write(artwork.read())
					##loader.close()
					##pixbuf = loader.get_pixbuf()
					##self.set_coverart(pixbuf)
		
				
	#def _userdata_into_default_locale(self, key):
		#''' This is needed cause it was sending unicode when it shouldn't have '''
		#return str(gpod.Track._userdata_into_default_locale(self, key))
					
	##def copy_to_ipod(self):
		##self['userdata']['sha1_hash'] = gtkpod.sha1_hash(self._userdata_into_default_locale('filename'))
		##mp = gpod.itdb_get_mountpoint(self._track.itdb)
		##if not mp:
			##return False
		##if gpod.itdb_cp_track_to_ipod(self._track,str(), None) != 1:
			##raise gpod.TrackException('Unable to copy %s to iPod as %s' % (self._userdata_into_default_locale('filename'),self))
		##fname = self.ipod_filename().replace(mp, '').replace(os.path.sep, ':')
		##self['userdata']['filename_ipod'] = fname
		##self['userdata']['transferred'] = 1
		##return True
	
					
					
					
					
		
            ##for tag, attrib in (('TPE1','artist'),
                                ##('TIT2','title'),
                                ##('TBPM','BPM'),
                                ##('TCON','genre'),
                                ##('TALB','album'),
                                ##('TPOS',('cd_nr','cds')),
                                ##('TRCK',('track_nr','tracks'))):
                ##try:
                    ##value = audiofile[tag]
                    ##if isinstance(value,mutagen.id3.NumericPartTextFrame):
                        ##parts = map(int,value.text[0].split("/"))
                        ##if len(parts) == 2:
                            ##self[attrib[0]], self[attrib[1]] = parts
                        ##elif len(parts) == 1:
                            ##self[attrib[0]] = parts[0]
                    ##elif isinstance(value,mutagen.id3.TextFrame):
                        ##self[attrib] = value.text[0]
                ##except KeyError:
                    ##pass
            ##if self['title'] is None:
                ##self['title'] = os.path.splitext(
                    ##os.path.split(filename)[1])[0].decode(
                    ##defaultencoding).encode('UTF-8')
           ## self['tracklen'] = int(audiofile.info.length * 1000)
            ##self.set_podcast(podcast)

##def add_track(itdb, device):
	##filename = '/home/scabootssca/Music/Bob Seger/1994 Greatest Hits/02 Night Moves.mp3'
	##track = gpod.itdb_track_new() # Create track, set metadata
	##track.visible = 1
	##track.filetype = "mp3"
	##track.ipod_path = filename 
	##track.size = os.stat(filename)[stat.ST_SIZE]

	##mediaFile = pm_mediafile.MediaFile('file://'+track.ipod_path)
	##mediaFile.read()

	##bitrate = mediaFile['bitrate']
	##if bitrate > 7: track.bitrate = bitrate # br is (cbr, vbr)
	##else: track.bitrate = bitrate # br is (cbr, vbr)

	##track.tracklen = mediaFile['length'] * 1000
	##track.album = str(mediaFile.display('album'))
	##track.artist = str(mediaFile.display('artist'))
	##track.title = str(mediaFile.display('title'))
	##track.genre = str(mediaFile.display('genre'))
	##now = int(time.time()) + 2082844800
	##track.time_added = now
	##track.rating = 100#int(options.rating) * 20

	### Add it and print result
	##mpl = gpod.itdb_playlist_mpl(itdb)
	##gpod.itdb_track_add(itdb, track, -1)
	##gpod.itdb_playlist_add_track(mpl, track, -1)

	##track.transferred = False
	##track.ipod_path = None

	##gpod.itdb_cp_track_to_ipod(track, filename, None)
	###thumb = thumbfile(file)
	###if thumb:
	###  gpod.itdb_track_set_thumbnails(track, thumb)

	###tracksAdded.append(track)
	##print( "INFO: Added file: %s" % filename, 1)
	##print( "          Artist: %s" % track.artist, 2)
	##print( "           Album: %s" % track.album, 2)
	##print( "           Title: %s" % track.title, 2)
	###print( "          Rating: %s" % stars(track), 2)
	
#def add_track(db, device):
	#pass

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, "iPod Settings")
		
		# Save the plugin instance that spawned this
		self.plugin = plugin

		# Create the interface elements
		self.uiTextMountPoint     = wx.TextCtrl(self, 0, cfg_get("mountPoint"), size=(350, -1))
		uiButtonMountPoint   = wx.Button(self, 0, "Browse")
		uiButtonOk           = wx.Button(self, wx.ID_OK)
		uiButtonCancel       = wx.Button(self, wx.ID_CANCEL)
		
		# Bind them
		uiButtonMountPoint.Bind(wx.EVT_BUTTON, self.event_button_browse)
		self.Bind(wx.EVT_BUTTON, self.event_button)
		
		# Fit them into the interface
		sizer = wx.GridBagSizer(5, 5)
		sizer.AddGrowableCol(0)
		sizer.Add(wx.StaticText(self, -1, "iPod Mount Point:"), (0, 0))
		sizer.Add(self.uiTextMountPoint, (1, 0))
		sizer.Add(uiButtonMountPoint, (1, 1))
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), (2, 0), flag=wx.EXPAND)
		
		s2 = wx.BoxSizer(wx.HORIZONTAL)
		s2.AddStretchSpacer()
		s2.Add(wx.Button(self, 4, "Mount"), 0, wx.EXPAND)
		s2.Add(wx.Button(self, 5, "Read"), 0, wx.EXPAND)
		s2.Add(wx.Button(self, 6, "Unount"), 10, wx.EXPAND|wx.LEFT)
		s2.Add(wx.Button(self, 7, "Init"), 10, wx.EXPAND|wx.LEFT)
		s2.Add(wx.Button(self, 8, "Sync"), 10, wx.EXPAND|wx.LEFT)
		s2.AddStretchSpacer()
		sizer.Add(s2, (3, 0), (1, 2), wx.EXPAND)
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), (4, 0), flag=wx.EXPAND)
		
		s2 = wx.BoxSizer(wx.HORIZONTAL)
		s2.Add(uiButtonOk, 0, wx.EXPAND)
		s2.AddStretchSpacer()
		s2.Add(uiButtonCancel, 0, wx.EXPAND)
		sizer.Add(s2, (5, 0), (1, 2), wx.EXPAND)
		
		# Add the sizer into the dialog with a space
		box = wx.BoxSizer()
		box.Add(sizer, 0, wx.ALL, 10)
		self.SetSizerAndFit(box)

		# Show the dialog
		result = self.ShowModal()
		if result == wx.ID_CANCEL: return
		
		# Save it
		value = self.uiTextMountPoint.GetValue()
		cfg_set("mountPoint",   value+(os.sep if not value.endswith(os.sep) else ""))

	def event_button_browse(self, event):
		label = "Choose The iPod Mount Point" if event.Id == 0 else "Choose A Directory To Copy Tracks To (Your Music Folder Works Well)"
		dialog = wx.DirDialog(pympe.ui.uiMain, label)

		if dialog.ShowModal() == wx.ID_OK:
			path = str(dialog.GetPath().rstrip(os.sep)+os.sep)
			
			if not os.path.exists(path):
				pympe.error('Mount point does not exist')
			
			if event.Id == 0:
				self.uiTextMountPoint.ChangeValue(path)
				cfg_set('mountPoint', path)

	def event_button(self, event):
		if event.Id == 4:
			self.plugin.mount()
		elif event.Id == 5:
			self.plugin.read()
		elif event.Id == 6:
			self.plugin.unmount()
		elif event.Id == 7:
			self.plugin.initalize()
		elif event.Id == 8:
			self.plugin.sync()
		else:
			event.Skip()
			
			
class IPodTabLibrary(pm_ui_library_tab.TabLibrary):
	def __init__(self, parent, database, plugin):
		pm_ui_library_tab.TabLibrary.__init__(self, parent, database)
		self.plugin = plugin
		#self.SetDropTarget(IPodDropTarget(self))
		
		self.uiPanelTracks.menu_misc = self.menu_misc
		self.uiPanelTracks.menu_sendto = self.menu_sendto
		self.event_menu_track_list_old = self.uiPanelTracks.event_menu_track_list
		self.uiPanelTracks.event_menu_track_list = self.event_menu_track_list
		
	def menu_sendto(self, menu): pass
		
	def menu_misc(self, topMenu):
		menu = wx.Menu()
		menu.Append(9946784, "Remove From iPod")
		menu.AppendSeparator()
		menu.Append(9946785, "Import Into Library")
		menu.Bind(wx.EVT_MENU, self.event_menu_track_list)
		
		topMenu.AppendSubMenu(menu, "Misc")
		
	def event_menu_track_list(self, event):
		libraryIds = [self.uiPanelTracks.indices[i] for i in self.uiPanelTracks.selected]
		
		if event.Id == 9946784: # Remove From iPod
			print 'Remove Track'
			
			filenames = [x[7:] for x in self.plugin.localDb.get_multi_track_single_value(libraryIds, 'uri')]
			
			for track in reversed(self.plugin.ipodDb.Master):
				if track.ipod_filename() in filenames:
					
					print 'Remove %s by %s'%(track['title'], track['artist'])
					
					trackId = libraryIds[filenames.index(track.ipod_filename())]
					self.plugin.localDb.remove_single_track(trackId)
					#pympe.events.library-\\\track-removed.send(trackId)
					
					self.plugin.ipodDb.Master.remove(track)
					self.plugin.ipodDb.remove(track)
			
		elif event.Id == 9946785: # Import into library
			print 'Import Track'
		else:
			self.event_menu_track_list_old(event)
			
class DeviceIpod(Device):
	def _start(self):
		self.iPodDb = None
		
	def _initalize(self):
		device = cpod.itdb_device_new()
		cpod.itdb_device_set_mountpoint(device, str(self.mountPoint))
		info = cpod.itdb_device_get_ipod_info(device)
		print info.model_number
		
		modelNumber = None#gpod.ITDB_IPOD_MODEL_CLASSIC_BLACK
		modelName   = None#cpod.itdb_info_get_ipod_model_name_string(modelNumber)
		ipodName    = 'Jason Richards'

		successfullyInitalized = cpod.itdb_init_ipod(str(self.mountPoint), modelName, ipodName, None)
		
		if successfullyInitalized:
			return pympe.ui.show_message('Successfully initalized iPod.', 'Success')
		else:
			return pympe.ui.show_error('There was a problem initializing the iPod.')

	def _connect(self):
		# Find the database
		if not os.path.exists(self.mountPoint):
			pympe.error('Unable to mount iPod, mount point does not exist')
			return False
		
		# Try and read the database, if we fail ask to initalize the database
		try:
			self.ipodDb = gpod.Database(str(self.mountPoint))
		except:# gpod.DatabaseException:
			result = wx.MessageDialog(None, 'Cannot read iPod database would you like to create a fresh one?' , 'Initalize iPod', style=wx.YES_NO|wx.NO_DEFAULT|wx.ICON_ERROR).ShowModal()
			if result == wx.ID_YES:
				if self._initalize(): self.ipodDb = gpod.Database(self.mountPoint)
				else: return False
			else: return False
			
		pympe.output('Successfully Mounted iPod')
		return True
		
	def _disconnect(self):
		def print_progress(db, track, i, total):
			print 'Copying: (%s/%s) %s'%(i, total, str(track))
		
		try:
			self.ipodDb.copy_delayed_files(print_progress)
			self.ipodDb.close()
		except:
			return pympe.error('Unable to save iPod database.', traceback=True)

		self.library.blank_database()
		pympe.output('Successfully Unmounted')
		return True
	
	def write_tracks(self, trackIds=[], library=None, data=None):
		print 'Write %s tracks'%len(trackIds)
		
		totalWrites = len(trackIds)
		indicator = pympe.progress.new('Writing To Ipod', 0, len(trackIds), updateDelta=3)
		writeArt = True
		
		for index, trackId in enumerate(trackIds):
			keys = keyLookup.keys()
			values = library.get_single_track_multi_values(trackId, keys+['uri'])
			
			##track = PympeTrack(keys, values, writeArt, trackId)
			# Create the track in ipod db
			track = gpod.Track(filename=str(values[-1][7:]))
			self.ipodDb.add(track)
			self.ipodDb.Master.add(track)
			track.__database = self.ipodDb
			track.copy_to_ipod()
			
			print 'Copying: (%s/%s) %s'%(index, totalWrites, str(track))

			# Create it in the local
			localInfoDict = dict(zip(keys, values[:-1]))
			localInfoDict['uri'] = 'file://%s/%s'%(self.mountPoint.rstrip(os.sep), track['userdata']['filename_ipod'].replace(":", os.sep).lstrip(os.sep))
			
			for key in localInfoDict.keys():
				if key in self.library.multiTagsShared+self.library.multiTagsLocal:
					localInfoDict[key] = [localInfoDict[key]]

			self.library.insert_single_track(localInfoDict)
			indicator.update(index, 'Preparing: %s by %s'%(track['title'], track['artist']))
			
		def print_progress(db, track, i, total):
			print 'Copying: (%s/%s) %s'%(i, total, str(track))
			indicator.update(i, 'Copying: %s by %s'%(track['title'], track['artist']))

		self.ipodDb.copy_delayed_files(callback=print_progress)
		indicator.finish()

class Plugin:
	def plu_start(self):
		self.uiTabLibrary = None

		self.device = pympe.devices.add_device(DeviceIpod, cfg_get('mountPoint'))
		self.device.plugin = self

	def plu_stop(self, shutdown):
		self.device.disconnect()
	
	def plu_settings(self, parent):
		SettingsDialog(self, parent)
		
	def mount(self):
		if not self.device.connect(cfg_get('mountPoint')): return False
		
		self.uiTabLibrary = IPodTabLibrary(pympe.ui.uiMain.uiTabsLeft, self.device.library, self)
		pympe.ui.uiMain.uiTabsLeft.InsertPage(2, self.uiTabLibrary, "iPod")
		
		return True
		#self.destination = pm_helpers.Destination('iPod', self.write_tracks)
	
		#return self.ipodDb
		
		#try:
			#self.ipodDb = gpod.Database(str(cfg_get('mountPoint')))
		#except gpod.DatabaseException:
			#result = wx.MessageDialog(None, 'Cannot read iPod database would you like to create a fresh one?' , 'Initalize iPod', style=wx.YES_NO|wx.NO_DEFAULT|wx.ICON_ERROR).ShowModal()
			#if result == wx.ID_YES:
				#if self.initalize():
					#self.ipodDb = gpod.Database(str(cfg_get('mountPoint')))
				#else:
					#return None
			#else:
				#return None
			
		##self.localDb = pm_library.Library()
		##self.localDb.blank_database()
		##self.mounted = True
		
		
		#self.device = DeviceIpod(cfg_get('mountPoint'))
		#pympe.devices.add_device(self.device)

		#if self.ipodDb == None:
			#pympe.ui.show_error('There was a problem mounting the ipod, unable to read database.')
		#else:
			#pympe.output('Successfully Mounted iPod')
	
		#self.uiTabLibrary = IPodTabLibrary(pympe.ui.uiMain.uiTabsLeft, self.localDb, self)
		#pympe.ui.uiMain.uiTabsLeft.InsertPage(2, self.uiTabLibrary, "iPod")
		#self.destination = pm_helpers.Destination('iPod', self.write_tracks)
	
		#return self.ipodDb
		
		
		
		
	def read(self):
		for track in self.device.ipodDb.Master:
			if track['ipod_path']: values = {'uri':'file://%s/%s'%(cfg_get('mountPoint').rstrip(os.sep), track['ipod_path'].replace(":", os.sep).lstrip(os.sep))}
			else: values = {'uri':''}
			for localName, ipodName in keyLookup.iteritems():
				if localName in ("timeadded", "timeplayed", "timemodified"):
					try: values[localName] = time.mktime(track[ipodName].timetuple())
					except:
						# This has an internal error on some timestamps
						#  File "/media/Storage/Projects/Pympe v3/plugins/plu_ipod/ipod.py", line 73, in read
						#	print track[ipodKey]
						#  File "/usr/lib/pymodules/python2.7/gpod/ipod.py", line 504, in __getitem__
						#	return datetime.datetime.fromtimestamp(getattr(self._track, item))
						#ValueError: timestamp out of range for platform time_t
						values[localName] = int(time.time())
				elif localName in self.device.library.multiTagsShared+self.device.library.multiTagsLocal:
					if track[ipodName] is None:
						values[localName] = ['Unknown']
					else:
						values[localName] = [track[ipodName]]
				else:
					values[localName] = track[ipodName]

			self.device.library.insert_single_track(values, True)
			
		print 'Read %i tracks from iPod'%len(self.device.ipodDb.Master)
	
	def unmount(self):
		if not self.device.disconnect(): return False
		
		for index in range(pympe.ui.uiMain.uiTabsLeft.GetPageCount()):
			if pympe.ui.uiMain.uiTabsLeft.GetPage(index) == self.uiTabLibrary:
				pympe.ui.uiMain.uiTabsLeft.RemovePage(index)
				break
		
		#if not self.mounted: return False
		
		
				
		##if self.destination: self.destination.destroy()
		
		#def print_progress(db, track, i, total):
			#print 'Copying: (%s/%s) %s'%(i, total, str(track))
		
		#try:
			#self.ipodDb.copy_delayed_files(print_progress)
			#self.ipodDb.close()
		#except gpod.DatabaseException, e:
			#pympe.error('Unable to save iPod database.', traceback=True)

		##finally:
		##	pympe.error('Unknown error saving iPod database')
			
		#self.localDb.blank_database()
		#self.mounted = False
		#pympe.output('Successfully Unmounted')
		#return True

	def initalize(self):
		pass
		#device = cpod.itdb_device_new()
		#cpod.itdb_device_set_mountpoint(device, str(cfg_get('mountPoint')))
		#info = cpod.itdb_device_get_ipod_info(device)
		#print info.model_number
		
		#modelNumber = None#gpod.ITDB_IPOD_MODEL_CLASSIC_BLACK
		#modelName   = None#cpod.itdb_info_get_ipod_model_name_string(modelNumber)
		#ipodName    = 'Jason Richards'

		#successfullyInitalized = cpod.itdb_init_ipod(str(cfg_get('mountPoint')),
													 #modelName,
													 #ipodName,
													 #None)
		
		#if successfullyInitalized:
			#return pympe.ui.show_message('Successfully initalized iPod.', 'Success')
		#else:
			#return pympe.ui.show_error('There was a problem initializing the iPod.')
		
	@pm_helpers.threaded
	def write_tracks(self, trackIds):
		pass
		#print 'Write %s tracks'%len(trackIds)
		
		#totalWrites = len(trackIds)
		#indicator = pympe.progress.new('Writing To Ipod', 0, len(trackIds), updateDelta=3)
		#writeArt = True
		
		#for index, trackId in enumerate(trackIds):
			#keys = keyLookup.keys()
			#values = pympe.library.get_single_track_multi_values(trackId, keys+['uri'])
			
			###track = PympeTrack(keys, values, writeArt, trackId)
			## Create the track in ipod db
			#track = gpod.Track(filename=str(values[-1][7:]))
			#self.device.ipodDb.add(track)
			#self.device.ipodDb.Master.add(track)
			#track.__database = self.device.ipodDb
			#track.copy_to_ipod()

			## Create it in the local
			#localInfoDict = dict(zip(keys, values[:-1]))
			#localInfoDict['uri'] = 'file://%s/%s'%(cfg_get('mountPoint').rstrip(os.sep), track['userdata']['filename_ipod'].replace(":", os.sep).lstrip(os.sep))
			
			#for key in localInfoDict.keys():
				#if key in self.device.library.multiTagsShared+self.device.library.multiTagsLocal:
					#localInfoDict[key] = [localInfoDict[key]]

			#self.device.library.insert_single_track(localInfoDict)
			#indicator.update(index, 'Preparing: %s by %s'%(track['title'], track['artist']))
			
		#def print_progress(db, track, i, total):
			##print 'Copying: (%s/%s) %s'%(i, total, str(track))
			#indicator.update(i, 'Copying: %s by %s'%(track['title'], track['artist']))

		##self.device.ipodDb.copy_delayed_files(callback=print_progress)
		#indicator.finish()
		
	@pm_helpers.threaded
	def sync(self):
		print 'sync'
		#totalWrites = len(pympe.library.tracks.keys())
		#indicator = pympe.progress.new('Syncing Ipod', 0, len(pympe.library.tracks.keys()[:totalWrites]), updateDelta=3)
		#writeArt = True
		
		

		
		#for index, trackId in enumerate(pympe.library.tracks.keys()[:totalWrites]):
			#keys = keyLookup.keys()
			#values = pympe.library.get_single_track_multi_values(trackId, keys+['uri'])
			
			#track = PympeTrack(keys, values, writeArt, trackId)
			#self.ipodDb.add(track)
			#self.ipodDb.Master.add(track)
			#track.__database = self.ipodDb
			
			#localInfoDict = dict(zip(keys, values[:-1]))
			#localInfoDict['uri'] = 'file://%s/%s'%(cfg_get('mountPoint').rstrip(os.sep), values[-1].replace(":", os.sep).lstrip(os.sep))
			
			#for key in localInfoDict.keys():
				#if key in self.localDb.multiTagsShared+self.localDb.multiTagsLocal:
					#localInfoDict[key] = [localInfoDict[key]]
					
			#self.localDb.insert_single_track(localInfoDict)
			
			##for localName, ipodName in keyLookup.iteritems():
				##if localName in ("timeadded", "timeplayed", "timemodified"):
					##try: values[localName] = time.mktime(track[ipodName].timetuple())
					##except:
						### This has an internal error on some timestamps
						###  File "/media/Storage/Projects/Pympe v3/plugins/plu_ipod/ipod.py", line 73, in read
						###	print track[ipodKey]
						###  File "/usr/lib/pymodules/python2.7/gpod/ipod.py", line 504, in __getitem__
						###	return datetime.datetime.fromtimestamp(getattr(self._track, item))
						###ValueError: timestamp out of range for platform time_t
						##values[localName] = int(time.time())
				##elif localName in self.localDb.multiTagsShared+self.localDb.multiTagsLocal:
					##values[localName] = [track[ipodName]]
				##else:
					##values[localName] = track[ipodName]
					

			#print 'Adding: %s'%str(track)
			#indicator.update(index, 'Reading: %s by %s'%(track['title'], track['artist']))
			
		#def print_progress(db, track, i, total):
			#print 'Copying: (%s/%s) %s'%(i, total, str(track))
			#indicator.update(i, 'Copying: %s by %s'%(track['title'], track['artist']))

		#self.ipodDb.copy_delayed_files(callback=print_progress)
		
		
		#indicator.finish()
		

	
