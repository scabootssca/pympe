# -*- coding: iso-8859-15 -*-
import pympe, wx, pm_helpers, os, shutil, gobject, cPickle
import pm_ui.pm_ui_library_tab as pm_ui_library_tab
import plugins.plu_ipod
from plugins.plu_ipod import ipod
reload(ipod)

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin):
		dialog = wx.PreDialog()
		dialog.SetExtraStyle(wx.DIALOG_EX_CONTEXTHELP)
		dialog.Create(pympe.ui.uiMain, -1, "iPod Settings")
		self.PostCreate(dialog)
		
		# Save the plugin instance that spawned this
		self.plugin = plugin

		# Create the interface elements
		#self.uiButtonAuthorize = wx.Button(self, -1, ("Authorize Pympe" if not cfg_get("user_name") else "Authorized by %s"%cfg_get("user_name")))
		self.uiTextMountPoint     = wx.TextCtrl(self, 0, cfg_get("mountPoint"), size=(350, -1))
		#self.uiTextCopyLocation   = wx.TextCtrl(self, 1, cfg_get("copyLocation"), size=(350, -1))
		uiButtonMountPoint   = wx.Button(self, 0, "Browse")
		#uiButtonCopyLocation = wx.Button(self, 1, "Browse")
		uiButtonOk           = wx.Button(self, wx.ID_OK)
		uiButtonCancel       = wx.Button(self, wx.ID_CANCEL)
		
		# Bind them
		uiButtonMountPoint.Bind(wx.EVT_BUTTON, self.event_button_browse)
		uiButtonCopyLocation.Bind(wx.EVT_BUTTON, self.event_button_browse)
		self.Bind(wx.EVT_BUTTON, self.event_button)
		
		# Fit them into the interface
		sizer = wx.GridBagSizer(5, 5)
		sizer.AddGrowableCol(0)
		sizer.Add(wx.StaticText(self, -1, "iPod Mount Point:"), (0, 0))
		sizer.Add(self.uiTextMountPoint, (1, 0))
		sizer.Add(uiButtonMountPoint, (1, 1))
		#sizer.Add(wx.StaticText(self, -1, "Copy Directory:"), (2, 0))
		#sizer.Add(self.uiTextCopyLocation, (3, 0))
		#sizer.Add(uiButtonCopyLocation, (3, 1))
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), (2, 0), flag=wx.EXPAND)
		
		s2 = wx.BoxSizer(wx.HORIZONTAL)
		s2.AddStretchSpacer()
		s2.Add(wx.Button(self, 4, "Mount"), 0, wx.EXPAND)
		s2.Add(wx.Button(self, 5, "Read"), 0, wx.EXPAND)
		s2.Add(wx.Button(self, 6, "Unount"), 10, wx.EXPAND|wx.LEFT)
		s2.Add(wx.Button(self, 7, "Init"), 10, wx.EXPAND|wx.LEFT)
		s2.Add(wx.Button(self, 8, "Sync"), 10, wx.EXPAND|wx.LEFT)
		s2.AddStretchSpacer()
		sizer.Add(s2, (3, 0), (1, 2), wx.EXPAND)
		sizer.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), (4, 0), flag=wx.EXPAND)
		
		s2 = wx.BoxSizer(wx.HORIZONTAL)
		s2.Add(uiButtonOk, 0, wx.EXPAND)
		s2.AddStretchSpacer()
		s2.Add(uiButtonCancel, 0, wx.EXPAND)
		sizer.Add(s2, (5, 0), (1, 2), wx.EXPAND)
		
		# Add the sizer into the dialog with a space
		box = wx.BoxSizer()
		box.Add(sizer, 0, wx.ALL, 10)
		self.SetSizerAndFit(box)

		# Show the dialog
		result = self.ShowModal()
		if result == wx.ID_CANCEL: return
		
		# Save it
		value = self.uiTextMountPoint.GetValue()
		cfg_set("mountPoint",   value+(os.sep if not value.endswith(os.sep) else ""))
		value = self.uiTextCopyLocation.GetValue()
		cfg_set("copyLocation", value+(os.sep if not value.endswith(os.sep) else ""))

	def event_button_browse(self, event):
		label = "Choose The iPod Mount Point" if event.Id == 0 else "Choose A Directory To Copy Tracks To (Your Music Folder Works Well)"
		dialog = wx.DirDialog(pympe.ui.uiMain, label)

		if dialog.ShowModal() == wx.ID_OK:
			path = dialog.GetPath()
			
			if event.Id == 0:
				self.uiTextMountPoint.ChangeValue(path)
			else:
				self.uiTextCopyLocation.ChangeValue(path)
				
	def event_button(self, event):
		if event.Id == 4:
			self.plugin.iPod.mount(self.uiTextMountPoint.GetValue())
		elif event.Id == 5:
			self.plugin.iPod.read()
		elif event.Id == 6:
			self.plugin.iPod.unmount()
		elif event.Id == 7:
			self.plugin.iPod.init_ipod()
		elif event.Id == 8:
			self.plugin.iPod.sync()
		else:
			event.Skip()
	
class IPodDropTarget(wx.DropTarget):
	def __init__(self, libraryTab):
		"""['DataObject', 'DefaultAction', 'GetData', 'GetDataObject', 'GetDefaultAction', 'OnData', 
		'OnDragOver', 'OnDrop', 'OnEnter', 'OnLeave', 'SetDataObject', 'SetDefaultAction', '__class__',d
		 '__del__', '__delattr__', '__dict__', '__doc__', '__format__', '__getattribute__', '__hash__', 
		 '__init__', '__module__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__',
		  '__sizeof__', '__str__', '__subclasshook__', '__swig_destroy__', '__weakref__', '_setCallbackInfo',
		   'base_OnDragOver', 'base_OnDrop', 'base_OnEnter', 'base_OnLeave', 'do', 'doFile', 'doMedia', 'doText', 'playlist', 'this', 'thisown']
		"""
		wx.PyDropTarget.__init__(self)
		self.libraryTab = libraryTab
		
		#self.dropIndicatorIndex = None
		
		self.do = wx.DataObjectComposite() # The dataobject that gets filled with the appropriate data
		
		self.doMedia = wx.CustomDataObject("MediaFile")
		self.doText  = wx.TextDataObject()
		self.doFile  = wx.FileDataObject()
		
		self.do.Add(self.doMedia)
		self.do.Add(self.doText)
		self.do.Add(self.doFile)
		
		self.SetDataObject(self.do)

	#def OnDragOver(self, x, y, d):
		#dropIndex = self.playlist.HitTest((x, y))[0]
		#if dropIndex == -1: dropIndex = pympe.playlists.current.get_length()
		#if self.dropIndicatorIndex == None: self.dropIndicatorIndex = dropIndex
		##pympe.playlists.current.displayQueue.insert(dropIndex, pympe.playlists.current.displayQueue.pop(self.dropIndicatorIndex))
		#self.dropIndicatorIndex = dropIndex
		#self.playlist.drop(self.dropIndicatorIndex)
		##self.playlist.refresh()
		#return d
		
	#def OnEnter(self, x, y, d):
		##pympe.playlists.current.enqueue([">----------------<"], index=0)
		#self.dropIndicatorIndex = 0
		#self.playlist.drop(self.dropIndicatorIndex)
		#return d

	#def OnLeave(self):
		#self.playlist.drop(False)
		##pympe.playlists.current.dequeue([self.dropIndicatorIndex])

	def OnData(self, x, y, d):
		if self.GetData():
			dropFormat = self.do.GetReceivedFormat().GetType()
			##['DF_BITMAP', 'DF_DIB', 'DF_DIF', 'DF_ENHMETAFILE', 'DF_FILENAME', 'DF_HTML', 'DF_INVALID', 'DF_LOCALE', 'DF_MAX', 'DF_METAFILE', 'DF_OEMTEXT', 'DF_PALETTE', 'DF_PENDATA', 'DF_PRIVATE', 'DF_RIFF', 'DF_SYLK', 'DF_TEXT', 'DF_TIFF', 'DF_UNICODETEXT', 'DF_WAVE']

			#for x in ['DF_BITMAP', 'DF_DIB', 'DF_DIF', 'DF_ENHMETAFILE', 'DF_FILENAME', 'DF_HTML', 'DF_INVALID', 'DF_LOCALE', 'DF_MAX', 'DF_METAFILE', 'DF_OEMTEXT', 'DF_PALETTE', 'DF_PENDATA', 'DF_PRIVATE', 'DF_RIFF', 'DF_SYLK', 'DF_TEXT', 'DF_TIFF', 'DF_UNICODETEXT', 'DF_WAVE']:
			#	print(x, getattr(wx, x))

			# Dropped from the library
			if dropFormat == wx.DF_PRIVATE:
				indices = cPickle.loads(self.doMedia.GetData())
				#print pympe.dragData.dbKeys
				self.libraryTab.plugin.iPod.write_tracks([dict(zip(pympe.dragData.dbKeys, pympe.dragData.get_single_track_multi_values(indice, pympe.dragData.dbKeys, 'unmodified'))) for indice in indices])
				#print 
				#pympe.playlists.current.enqueue(pympe.dragData.get_multi_track_single_value(indices, 'uri'),
				#						pympe.dragData.get_multi_track_single_value(indices, 'title', 'joined'), indices, index=self.dropIndicatorIndex)

				#pympe.playlists.current.enqueue(pympe.dragData.get_multi_track_info(indices, "uri"), 
												#pympe.dragData.get_multi_track_info(indices, "title"), 
												#indices, self.dropIndicatorIndex)
			##	pympe.playlist.enqueue(pympe.library.get_multi_track_data(cPickle.loads(self.doMedia.GetData()), ["uri"]))
			##	pympe.ui.update_playlist()
			##print(x, y, d)
			##print('\n').join(sorted(dir(self.playlist)))
			
			## Dropped a file from the browser
			#elif dropFormat == wx.DF_FILENAME:
				#uris =  ["file://"+filename for filename in self.doFile.GetFilenames()]
				#pympe.playlists.current.enqueue(uris, index=self.dropIndicatorIndex)

			#elif dropFormat == wx.DF_UNICODETEXT or dropFormat == wx.DF_TEXT:
				#text = self.doText.GetText()
				#if len(text) > 12:
					#if text[:7] in ['http://', 'file://']:
						#pympe.playlists.current.enqueue([text], index=self.dropIndicatorIndex)
						
			#self.dropIndicatorIndex = None
			
				
		return d

class IPodPanelTracks(pm_ui_library_tab.PanelTracks):
	def event_right_up(self, event):
		self.update_selected_list()

		def event_menu_playlist_selection(event):
			if event.Id == -20:
				# Get the name
				dialog = wx.TextEntryDialog(self, "What shall the playlist be named?", "Create a playlist.")
				dialog.ShowModal()
				name = dialog.GetValue()
				if not name: return
				
				# Create the playlist
				playlist = pympe.playlists.create_playlist(name)
				pympe.playlists.set_active_playlist(name)

				self.update_selected_list()
				indices = [self.indices[x] for x in self.selected]
				
				playlist.enqueue(self.uiTabLibrary.library, self.uiTabLibrary.library.get_multi_track_info(indices, "uri"),
								self.uiTabLibrary.library.get_multi_track_info(indices, "title"),
								indices)
			else:
				name = sorted(pympe.playlists.playlists.keys())[event.Id]
				pympe.playlists.set_active_playlist(name)
				
				self.update_selected_list()
				indices = [self.indices[x] for x in self.selected]

				pympe.playlists.current.enqueue(self.uiTabLibrary.library, self.uiTabLibrary.library.get_multi_track_info(indices, "uri"), 
												self.uiTabLibrary.library.get_multi_track_info(indices, "title"), indices)
		
		def playlist_menu():
			menu = wx.Menu()
			for index, playlist in enumerate(sorted(pympe.playlists.playlists.keys())):
				menu.Append(index, playlist)
			menu.AppendSeparator()
			menu.Append(-20, "New Playlist")
			menu.Bind(wx.EVT_MENU, event_menu_playlist_selection)
			return menu

		def search_menu(single):
			def event_menu_search(event):
				if event.Id == 0: # Artist
					values = set()
					[values.update(self.uiTabLibrary.library.get_single_track_info(self.indices[indice], "artist", True)) for indice in self.selected]
					search = " | ".join(["artist=="+x for x in values])
				else: # Album
					values = set()
					[values.update(self.uiTabLibrary.library.get_single_track_info(self.indices[indice], "album", True)) for indice in self.selected]
					search = " | ".join(["album=="+x for x in values])
				pympe.event.library_search.send(self.uiTabLibrary.library.uid, search)
			
			menu = wx.Menu()
			menu.Append(0, "Artist"+("" if single else "s"))
			menu.Append(1, "Album"+("" if single else "s"))
			menu.Bind(wx.EVT_MENU, event_menu_search)
			return menu
			
		def rating_menu():
			def event_menu_set_rating(event):
				self.uiTabLibrary.library.change_multi_track_local_value([self.indices[x] for x in self.selected], "rating", event.Id)
			
			menu = wx.Menu()
			
			menu.Append(0, "0: ")
			menu.Append(1, "1: ☆")
			menu.Append(2, "2: ☆☆")
			menu.Append(3, "3: ☆☆☆")
			menu.Append(4, "4: ☆☆☆☆")
			menu.Append(5, "5: ☆☆☆☆☆")#★
			menu.Bind(wx.EVT_MENU, event_menu_set_rating)
			return menu

		menu = wx.Menu()
		menu.Append(11, "Play Now")
		menu.Append(12, "Enqueue")
		menu.AppendSeparator()
		#menu.AppendSubMenu(playlist_menu(), "Add To Playlist")
		if len(self.selected) == 1:
			rating = self.uiTabLibrary.library.get_single_track_single_value(self.indices[self.selected[0]], "rating")
			menu.AppendSubMenu(rating_menu(), "Rating: %s%s"%("★"*rating, "☆"*(5-rating)))
			menu.AppendSeparator()
			menu.Append(21, "Delete From iPod")
			menu.AppendSeparator()
			menu.Append(31, "Copy File Location")
			menu.Append(32, "Open Containing Folder")
			menu.Append(67356342, "Copy To Local Library")
			menu.AppendSeparator()
			menu.AppendSubMenu(search_menu(True), "Search By")
		else:
			menu.AppendSubMenu(rating_menu(), "Rating")
			menu.AppendSeparator()
			menu.Append(21, "Delete From iPod")
			menu.AppendSeparator()
			menu.Append(31, "Copy File Locations")
			menu.Append(67356342, "Copy To Local Library")
			menu.AppendSeparator()
			menu.AppendSubMenu(search_menu(False), "Search By")
		menu.AppendSeparator()
		menu.Append(41, "Edit Metadata")
		menu.Bind(wx.EVT_MENU, self.event_menu_track_list_ipod)
		self.PopupMenu(menu)
		
	def event_menu_track_list_ipod(self, event):
		if event.Id == 67356342:
			self.copy_selected_to_local()
		elif event.Id == 21:
			self.uiTabLibrary.plugin.iPod.remove_multi_tracks([self.indices[x] for x in self.selected])
		else:
			pm_ui_library_tab.PanelTracks.event_menu_track_list(self, event)
		

	@pm_helpers.run_in_thread
	def copy_selected_to_local(self):
		total = len(self.selected)
		for index, indice in enumerate(self.selected):
			print "Copying %s of %s"%(index, total)
			indice = self.indices[indice]
			
			# Extract the info into a portable format
			keys = self.uiTabLibrary.library.dbKeys
			info = dict([(k, v) for k, v in zip(keys, self.uiTabLibrary.library.get_single_track_multi_info(indice, keys, splitValue=True))])
			uri  = info["uri"] 

			# Create the path name
			pathInfo = dict([(k, info[k] if not k in self.uiTabLibrary.library.multiTagsShared+self.uiTabLibrary.library.multiTagsLocal else info[k][0]) for k in pympe.config.get("library", "renamingTags")])
			path = cfg_get("copyLocation")+pm_helpers.format_by_string(pympe.config.get("library", "renamingRules"), pathInfo, multiTagSource=False)+"."+uri.rsplit(".")[-1]
			info["uri"] = "file://"+path
		
			# Make the directory tree as needed
			directory = ""
			for d in path.split(os.sep)[:-1]:
				directory += d+os.sep
				if not os.path.exists(directory): os.mkdir(directory)
				
			# Copy the file
			shutil.copyfile(uri[7:], path)

			# Add it to the local database
			gobject.idle_add(pympe.library.insert_track, info)
			if not index%20: pympe.event.send("library-updated", (), pympe.library)
			
		# Update again if needed
		if index%20 != 0:pympe.event.send("library-updated", (), pympe.library)	

class IPodTabLibrary(pm_ui_library_tab.TabLibrary):
	def __init__(self, parent, database, plugin):
		pm_ui_library_tab.TabLibrary.__init__(self, parent, database)
		self.plugin = plugin
		self.SetDropTarget(IPodDropTarget(self))

class Plugin:
	def __plugin_start__(self):
		self.iPod = ipod.IPod(self)
		
		#if not self.iPod.mount(cfg_get("mountPoint")): return
		#if not self.iPod.read(): return

		pm_ui_library_tab.PanelTracks = IPodPanelTracks
		self.uiPanelMain = IPodTabLibrary(pympe.ui.uiMain.uiTabsRight, self.iPod.localDb, self)
		pympe.ui.uiMain.uiTabsRight.InsertPage(1, self.uiPanelMain, "iPod")
		#pympe.copyDestinations.append(self.iPod)

	def __plugin_stop__(self, shutdown):
		# Remove the panel
		for index in range(pympe.ui.uiMain.uiTabsRight.GetPageCount()):
			if pympe.ui.uiMain.uiTabsRight.GetPage(index) == self.uiPanelMain:
				pympe.ui.uiMain.uiTabsRight.RemovePage(index)
				break
			
		#pympe.copyDestinations.remove(self.iPod)	
		self.iPod.unmount()
	
	def __plugin_settings__(self):
		SettingsDialog(self)

	
