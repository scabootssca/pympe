#!/usr/bin/python

##  Copyright (C) 2006 Nick Piper <nick-gtkpod at nickpiper co uk>
##  Part of the gtkpod project.
 
##  URL: http://www.gtkpod.org/
##  URL: http://gtkpod.sourceforge.net/

##  The code contained in this file is free software; you can redistribute
##  it and/or modify it under the terms of the GNU Lesser General Public
##  License as published by the Free Software Foundation; either version
##  2.1 of the License, or (at your option) any later version.

##  This file is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Lesser General Public License for more details.

##  You should have received a copy of the GNU Lesser General Public
##  License along with this code; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# this file is just a little example to see how you could add music

import os, os.path
import gpod
import sys
from optparse import OptionParser
import urlparse, urllib2
import tempfile
import shutil



db = gpod.Database('/media/iPod')

for path in ["/mnt/StorageA/Music/3Oh!3/2007 3Oh!3/1 Holler 'Til You Pass Out.mp3"]:
    try:
        track = db.new_Track(filename=path)
    except gpod.TrackException, e:
        print "Exception handling %s: %s" % (path, e)
        continue # skip this track

    print "Added %s to database" % track

def print_progress(database, track, i, total):
    sys.stdout.write("Copying to iPod %04d/%d: %s\r" % (i,total,track))
    sys.stdout.flush()
        
print "Copying to iPod"
db.copy_delayed_files(callback=print_progress)

print "Saving database"
db.close()
print "Saved db"


