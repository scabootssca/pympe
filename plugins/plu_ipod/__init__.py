import pympe, gpod, wx, threading, os, pm_helpers, time, socket, shutil
import pm_ui.pm_ui_library_tab as pm_ui_library_tab
import gpod.gpod as cpod
import gpod.gtkpod as gtkpod
reload(gpod)

class SyncDialog(wx.Dialog):
	def __init__(self, parent, device, hashes, toAdd=[], toRemove=[]):
		wx.Dialog.__init__(self, parent, -1, "Syncronize", style=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER)
		self.parent = parent

		sizerMain = wx.GridBagSizer(5,5)

		self.hashes = hashes
		self.toAdd = toAdd
		self.toRemove = toRemove

		# Track Display
		self.listAdd    = wx.CheckListBox(self, -1, choices=[' - '.join(x) for x in pympe.library.get_multi_track_multi_values([self.hashes[1][x] for x in toAdd], ['artist', 'title'])], size=(250, 500))#, style=wx.LC_REPORT|wx.LC_VIRTUAL)
		self.listRemove = wx.CheckListBox(self, -1, choices=[' - '.join(x) for x in device.library.get_multi_track_multi_values([self.hashes[0][x] for x in toRemove], ['artist', 'title'])], size=(250, 500))#, style=wx.LC_REPORT|wx.LC_VIRTUAL)

		self.listAdd.SetChecked(range(len(toAdd)))
		self.listRemove.SetChecked(range(len(toRemove)))

		self.listAdd.Bind(wx.EVT_CHECKLISTBOX, self.event_checklistbox)
		self.listRemove.Bind(wx.EVT_CHECKLISTBOX, self.event_checklistbox)

		self.labelAdd = wx.StaticText(self, -1, 'Media To Add (%s)'%len(self.toAdd))
		self.labelRemove = wx.StaticText(self, -1, 'Media To Remove (%s)'%len(self.toRemove))

		sizerMain.Add(self.labelAdd, (0, 0), flag=wx.ALIGN_CENTER)
		sizerMain.Add(self.labelRemove, (0, 1), flag=wx.ALIGN_CENTER)

		button = wx.Button(self, 0, 'Uncheck All')
		button.toggle = False
		button.checkList = self.listAdd
		sizerMain.Add(button, (2, 0), flag=wx.EXPAND)

		button = wx.Button(self, 1, 'Uncheck All')
		button.toggle = False
		button.checkList = self.listRemove
		sizerMain.Add(button, (2, 1), flag=wx.EXPAND)

		sizerMain.Add(self.listAdd, (1, 0), flag=wx.EXPAND)
		sizerMain.Add(self.listRemove, (1, 1), flag=wx.EXPAND)

		sizerMain.Add(wx.StaticLine(self, -1, style=wx.LI_HORIZONTAL), (3, 0), (1, 2), border=10, flag=wx.EXPAND|wx.TOP|wx.BOTTOM)

		sizerMain.Add(wx.Button(self, 100, 'Sync'), (4, 0), flag=wx.ALIGN_LEFT)
		sizerMain.Add(wx.Button(self, wx.ID_CANCEL), (4, 1), flag=wx.ALIGN_RIGHT)

		self.Bind(wx.EVT_BUTTON, self.event_button)

		sizerMain.AddGrowableRow(1)
		sizerMain.AddGrowableCol(0)
		sizerMain.AddGrowableCol(1)

		box = wx.BoxSizer()
		box.Add(sizerMain, 1, wx.EXPAND|wx.ALL, 5)
		self.SetSizerAndFit(box)

	def event_checklistbox(self, event):
		self.labelAdd.SetLabel('Media To Add (%s)'%len(self.listAdd.GetChecked()))
		self.labelRemove.SetLabel('Media To Remove (%s)'%len(self.listRemove.GetChecked()))

	def event_button(self, event):
		if event.Id <= 1:
			obj = event.EventObject

			for index in range(obj.checkList.GetCount()):
				obj.checkList.Check(index, check=obj.toggle)

			obj.toggle = not obj.toggle
			obj.SetLabel('Check All' if obj.toggle else 'Uncheck All')
			self.event_checklistbox(None)

		elif event.Id == wx.ID_CANCEL:
			self.Destroy()
			return
		elif event.Id == 100: # Sync
			self.syncFunction([self.hashes[1][self.toAdd[x]] for x in self.listAdd.GetChecked()],
								[self.hashes[0][self.toRemove[x]] for x in self.listRemove.GetChecked()])


			self.Destroy()
			return


# localKeyName:[ipodKeyName, localKeyType]
keyLookup = {'artist':['artist', list],
			'title':['title', list],
			'genre':['genre', list],
			'album':['album', list],
			'disctotal':['cds', int],
			'discnumber':['cd_nr', int],
			'tracknumber':['track_nr', int],
			'tracktotal':['tracks', int],
			'length':['tracklen', int],
			'timeadded':['time_added', float],
			'year':['year', str],
			#'timeplayed':['time_played', float],
			'timemodified':['time_modified', float],
			'albumartist':['albumartist', list],
			'compilation':['compilation', bool],
			'comment':['comment', list],
			'playcount':['playcount', int],
			'filesize':['size', int],
			'composer':['composer', list]
			# starttime
			# stoptime?
			}

def gpod_track_from_library(trackId, library):
	localKeys = keyLookup.keys()
	trackInfo = library.get_single_track_multi_values(trackId, localKeys+['uri'], 'first')
	filename = trackInfo.pop()[7:].encode('UTF-8', 'replace')


	track = gpod.Track()

	track['userdata'] = {'transferred': 0,
		'hostname': socket.gethostname(),
		'charset':gpod.defaultencoding}
	track['userdata']['pc_mtime'] = os.stat(filename).st_mtime
	track._set_userdata_utf8('filename', filename)

	# Artwork
	possible_image = os.path.join(os.path.split(filename)[0],'folder.jpg')
	if os.path.exists(possible_image):
		track.set_coverart_from_file(possible_image.encode(gpod.defaultencoding))
	elif 'art' in trackInfo:
		# Else set it from 128x128 embedd image
		track.set_coverart(pm_helpers.PmImage(string=trackInfo["art"]).scaled(128, 128).convert_to_pixbuf())

	# Tags
	for key, value in zip(localKeys, trackInfo):
		if type(value) == unicode:
			value = value.encode('UTF-8', 'replace')

		if key == 'length': value *= 1000
		if key == 'compilation': value = int(value)
		if key == 'year':
			try: value = int(value)
			except ValueError: value = 0

		track[keyLookup[key][0]] = value

	# Set them to ipod specifications
	if track['title'] is None:
		track['title'] = os.path.splitext(os.path.split(filename)[1])[0].decode(gpod.defaultencoding).encode('UTF-8')

	return track

def gpod_get_dest_filename(track, mp):
	filename = cpod.itdb_cp_get_dest_filename(track._track, mp,
									track['userdata']['filename_utf8'], None)

	return filename

def gpod_copy_track_to_ipod(track):
	track['userdata']['sha1_hash'] = gtkpod.sha1_hash(track._userdata_into_default_locale('filename'))
	mp = cpod.itdb_get_mountpoint(track._track.itdb)
	if not mp:
		return False
	else:
		filename = track._userdata_into_default_locale('filename')

		if not track._track or \
			not track._track.itdb or \
			not cpod.itdb_get_mountpoint(track._track.itdb) or \
			not filename:
			return False

		if track._track.transferred: return True

		destinationFilename = gpod_get_dest_filename(track, mp)
		shutil.copy(filename, destinationFilename)

		track['userdata']['filename_ipod'] = destinationFilename.replace(mp, '').replace(os.path.sep, ':')
		track['userdata']['transferred'] = 1

	cpod.itdb_cp_finalize(track._track, None, destinationFilename, None)

def gpod_copy_delayed_files(ipodDb, indicator=None):
	if not gpod.itdb_get_mountpoint(ipodDb._itdb):
		# we're not working with a real ipod.
		return

	to_copy=[]
	for track in ipodDb:
		try:
			transferred = int(track['userdata']['transferred'])
		except (KeyError, TypeError):
			transferred = 1
		if not transferred:
			to_copy.append(track)

		total = len(to_copy)
		for index, track in enumerate(to_copy):
			if indicator: indicator.update(index, 'Transferring: %s by %s'%(track['title'], track['artist']), total)
			gpod_copy_track_to_ipod(track)

class DeviceIPod(pympe.devices.DeviceClass):
	def _setup(self):
		self.ipodDb = None

	def _initalize(self):
		device = cpod.itdb_device_new()
		cpod.itdb_device_set_mountpoint(device, str(self.mountPoint))
		info = cpod.itdb_device_get_ipod_info(device)
		print info.model_number

		modelNumber = gpod.ITDB_IPOD_MODEL_CLASSIC_BLACK
		modelName   = cpod.itdb_info_get_ipod_model_name_string(modelNumber)

		# Selection for what to name it
		ipodInfoDialog = wx.TextEntryDialog(None, 'What should the iPod be named?', 'iPod Name')
		ipodInfoDialog.ShowModal()
		ipodName = ipodInfoDialog.GetValue()
		if not ipodName: ipodName = 'iPod'

		successfullyInitalized = cpod.itdb_init_ipod(str(self.mountPoint), modelName, ipodName, None)

		if successfullyInitalized:
			return pympe.ui.show_message('Successfully initalized iPod.', 'Success')
		else:
			return pympe.ui.show_error('There was a problem initializing the iPod.')

	def _connect(self, indicator):
		# Find the database
		if not os.path.exists(self.mountPoint):
			print 'Unable to mount iPod, mount point does not exist'
			return self.finished_connecting(indicator, False)

		if not self.ipodDb:
			# Try and read the database, if we fail ask to initalize the database
			try:
				self.ipodDb = gpod.Database(str(self.mountPoint))
			except gpod.DatabaseException:
				@pm_helpers.unthread
				def create_fresh_database(self):
					result = wx.MessageDialog(None, 'Cannot read iPod database would you like to create a fresh one?' , 'Initalize iPod', style=wx.YES_NO|wx.NO_DEFAULT|wx.ICON_ERROR).ShowModal()
					if result == wx.ID_YES:
						if self.initalize():
							self.ipodDb = gpod.Database(str(self.mountPoint))
							self.connect()
					return self.finished_connecting(indicator, False)

				create_fresh_database(self)
				return self.finished_connecting(indicator, False)

		print 'Successfully Mounted iPod'
		return self.finished_connecting(indicator, True)

	def _read(self):
		# Now read the tracks
		indicator = pympe.progress.new("Reading iPod Tracks" , 0, len(self.ipodDb.Master), updateDelta=17)

		@pm_helpers.threaded
		def do_threaded():
			trackBuffer = []
			toRemoveBuffer = []
			for index, track in enumerate(self.ipodDb.Master):
				if track['ipod_path']: values = {'uri':'file://%s/%s'%(self.mountPoint.rstrip(os.sep), track['ipod_path'].replace(":", os.sep).lstrip(os.sep))}
				else: values = {'uri':''}

				if not os.path.exists(values['uri'].split('://', 1)[1]):
					print 'Missing file \'%s\' by \'%s\': %s'%(track['title'], track['artist'], values['uri'])
					self.ipodDb.remove(track)
					# This should fail gracefully and continue if the uri does not exist
					toRemoveBuffer.append(self.library.get_trackid_from_uri(values['uri']))

					if len(toRemoveBuffer) > 37:
						indicator.update(total=indicator.total-1)
						self.library.remove_multi_tracks(toRemoveBuffer)
						toRemoveBuffer = []

					continue

				# Convert the values into the type for our database
				for localName, (ipodName, localType) in keyLookup.iteritems():
					if localName.startswith('time'):
						values[localName] = localType(getattr(track._track, ipodName))
					elif localName == 'length':
						values[localName] = localType(track[ipodName]*.001)
					elif localType == list:
						if track[ipodName] is None:
							values[localName] = []
						else:
							values[localName] = [unicode(track[ipodName])]
					else:
						values[localName] = localType(track[ipodName])

				trackBuffer.append(values)

				if not self.connected:
					trackBuffer = []
					indicator.finish()
					break

				if not index%400:
					self.library.insert_multi_tracks(trackBuffer, indicator)
					trackBuffer = []

			if trackBuffer:
				self.library.insert_multi_tracks(trackBuffer, indicator)

			if toRemoveBuffer:
				self.library.remove_multi_tracks(toRemoveBuffer)

			print 'Done'
			self.library.remove_dead_links()
			indicator.finish()

		do_threaded()
		return True


	@pm_helpers.unthread
	def _disconnect(self):
		indicator = pympe.progress.new("Saving iPod Database" , 0, 1)

		with self.lock:
			print 'Saving iPod Database'

			def print_progress(db, track, i, total):
				indicator.update(i, 'Finshing Writing: %s by %s'%(track['title'], track['artist']), total)

			try:
				#self.ipodDb.copy_delayed_files(print_progress)
				gpod_copy_delayed_files(self.ipodDb, indicator)
				self.ipodDb.close()
				self.ipodDb = None
			except:
				indicator.finish()
				return pympe.error('Unable to save iPod database, possibly already disconnected?', False, False)

			pympe.alert('Successfully Unmounted')

		indicator.finish()
		return True

	def _write(self, trackIds=[], sourceLibrary=None, data=None):
		with self.lock:
			updateDelta = int(len(trackIds)**.25)
			indicator = pympe.progress.new("Writing Tracks To iPod" , 0, len(trackIds), updateDelta=updateDelta)
			trackBuffer = []
			writeArt = True

			for index, trackId in enumerate(trackIds):
				keys = keyLookup.keys()
				values = sourceLibrary.get_single_track_multi_values(trackId, keys+['uri'])

				indicator.update(index, 'Writing %s by %s'%(values[keys.index('title')], values[keys.index('artist')]))

				###track = PympeTrack(keys, values, writeArt, trackId)
				## Create the track in ipod db
				#print values[-1]
				#track = gpod.Track(filename=str(values[-1][7:]))
				track = gpod_track_from_library(trackId, sourceLibrary)
				self.ipodDb.add(track)
				self.ipodDb.Master.add(track)
				track.__database = self.ipodDb
				gpod_copy_track_to_ipod(track)

				# Create it in the local
				localInfoDict = dict(zip(keys, values[:-1]))
				localInfoDict['uri'] = 'file://%s/%s'%(self.mountPoint.rstrip(os.sep), track['userdata']['filename_ipod'].replace(":", os.sep).lstrip(os.sep))

				for key in localInfoDict.keys():
					if key in self.library.multiTagsShared+self.library.multiTagsLocal:
						localInfoDict[key] = [localInfoDict[key]]

				trackBuffer.append(localInfoDict)
				if not index%updateDelta:
					self.library.insert_multi_tracks(trackBuffer)
					trackBuffer = []

				if not self.connected:
					break


			if trackBuffer:
				self.library.insert_multi_tracks(trackBuffer)
				trackBuffer = []

			#def print_progress(db, track, i, total):
			#	indicator.update(i, 'Transferring: %s by %s'%(track['title'], track['artist']))

			#self.ipodDb.copy_delayed_files(callback=print_progress)
			#gpod_copy_delayed_files(self.ipodDb, indicator)
			indicator.finish()

	def _delete(self, trackIds, newThread=True):
		indicator = pympe.progress.new("Deleting Tracks" , 0, len(trackIds), updateDelta=int(len(trackIds)**.25))

		def do_delete(trackIds):
			with self.lock:
				mountPointLength = len(self.mountPoint)
				trackLookup = dict(zip([uri[7:][mountPointLength:].replace(os.sep, ':') for uri in self.library.get_multi_track_single_value(trackIds, 'uri')], trackIds))

				## Loop through the tracks and if the path matches our path then remove it
				## There has to be a better way to do this? Hopefully at least..
				index = 1
				removed = []
				for track in reversed(self.ipodDb):
					try:
						trackId = trackLookup[track['ipod_path']]
						del trackLookup[track['ipod_path']]
					except KeyError:
						continue

					self.ipodDb.remove(track)
					removed.append(trackId)

					indicator.update(index)
					index += 1

					if not self.connected:
						break

					if not index%17:
						self.library.remove_multi_tracks(removed)
						removed = []

				if removed:
					self.library.remove_multi_tracks(removed)

				# If there were files to be removed that weren't in the ipod db just on the device do it now
				if trackLookup:
					self.library.remove_multi_tracks(trackLookup.values())

			indicator.finish()

		if newThread: pm_helpers.run_thread(do_delete, trackIds)
		else: do_delete(trackIds)
		return True

	@pm_helpers.threaded
	def _syncronize(self, library):
		with self.lock:
			localHashes = {}
			deviceHashes = {}

			for trackId in library.tracks.keys():
				values = library.get_single_track_multi_values(trackId, ('artist', 'album', 'discnumber', 'tracknumber', 'title', 'genre', 'length'), 'first')
				# Round the length down to the nearest 5 seconds and all others to lowercase if applicable
				values[-1] -= values[-1]%5
				localHashes[('%s%s%s%s%s%s%s'%tuple(values)).lower()] = trackId


			for trackId in self.library.tracks.keys():
				values = self.library.get_single_track_multi_values(trackId, ('artist', 'album', 'discnumber', 'tracknumber', 'title', 'genre', 'length'), 'first')
				# Round the length down to the nearest 5 seconds and all others to lowercase if applicable
				##trackHash = '%s%s%s%s%s%s'%(values[0].lower().replace(' ', ''), values[1].lower().replace(' ', ''), values[2].lower().replace(' ', ''), values[3]-(values[3]%5), values[4], values[5])
				#deviceHashes['%i%s'%(values[3]%5, ''.join(values[1-1]), values[-1])] = trackId
				values[-1] -= values[-1]%5
				deviceHashes[('%s%s%s%s%s%s%s'%tuple(values)).lower()] = trackId

		# Not in local so delete them
		needsDeleted = sorted(set(deviceHashes.keys()).difference(localHashes.keys()))
		needsAdded = sorted(set(localHashes.keys()).difference(deviceHashes.keys()))

		#print needsDeleted
		#print needsAdded

		@pm_helpers.threaded
		def sync_threaded(needsAdded, needsDeleted):
			# Do the dirty
			self.delete(needsDeleted, False)#[deviceHashes[x] for x in needsDeleted])
			#print 'Write'
			self.write(needsAdded, library)#[localHashes[x] for x in needsAdded], library)

		@pm_helpers.unthread
		def show_sync_dialog(self,deviceHashes,localHashes,needsAdded,needsDeleted):
			if not needsDeleted and not needsAdded:
				pympe.alert('iPod is already syncronized')
			else:
				syncDialog = SyncDialog(None, self, (deviceHashes, localHashes), needsAdded, needsDeleted)
				syncDialog.syncFunction = sync_threaded
				syncDialog.Show()

		show_sync_dialog(self,deviceHashes,localHashes,needsAdded,needsDeleted)

		return True

	@pm_helpers.threaded
	def cleanup(self):
		with self.lock:
			missingTracks = []

			updateDelta = int(len(self.library.tracks)**.25)
			indicator = pympe.progress.new("Cleaning Database" , 0, len(self.library.tracks), updateDelta=updateDelta)

			badCount = 0
			trackBuffer = []

			# Remove them from the local database
			for index, (trackId, track) in enumerate(self.library.tracks.iteritems()):
				if not index%updateDelta:
					indicator.update(index)

				if not os.path.exists(track['uri'][7:]):
					trackBuffer.append(trackId)

					if not badCount%updateDelta:
						self.library.remove_multi_tracks(trackBuffer)
						trackBuffer = []

					badCount += 1

			# Remove them from the ipod database
			indicator.update(0, total=len(self.ipodDb))
			for index, track in enumerate(reversed(self.ipodDb)):
				if not os.path.exists(track.ipod_filename()):
					self.ipodDb.remove(track)

				if not index%updateDelta:
					indicator.update(index)

			# Add any files with no entries
			for root, dirs, files in os.walk(str(cpod.itdb_get_music_dir(str(self.mountPoint)))):
				for testFile in files:
					try:
						# If this returns a key error we've found a file that isnt in the database
						self.library.tracksByUri['file://%s%s'%(root, testFile)]
					except KeyError:
						print 'Add it!'
						pass


			indicator.finish()


class IpodTabLibrary(pm_ui_library_tab.TabLibrary):
	def __init__(self, parent, database, plugin, device):
		pm_ui_library_tab.TabLibrary.__init__(self, parent, database)
		self.plugin = plugin
		self.device = device


		menu = wx.Menu()
		menu.Append(1, 'Sync')
		menu.Append(3, 'Clean Database')
		#menu.Append(2, 'Save Database')
		menu.AppendSeparator()
		menu.Append(0, 'Unmount')
		self.enable_menu(menu, self.event_menu, device.name)

	def event_menu(self, event):
		if event.Id == 0:
			pympe.events.device_disconnect(self.device.uid)
		elif event.Id == 1:
			self.device.syncronize(pympe.library)
		elif event.Id == 2:
			print 'SAVE! DOES NOT WORK '
		elif event.Id == 3: # Clean Database
			self.device.cleanup()
			# Add orphaned files to database and remove orphaned database entries


class Plugin:
	def plu_settings(self, parent):
		pass

	def plu_start(self):
		self.devices = []

		pympe.events.device_ready.subscribe(self.mpevent_device_ready)
		pympe.events.device_connected.subscribe(self.mpevent_device_connected)
		pympe.events.device_disconnected.subscribe(self.mpevent_device_disconnected)

		for device in pympe.devices.get_ready():
			self.mpevent_device_ready(None, device)

	def plu_stop(self, shutdown):
		# Disconnect all
		for deviceUid in self.devices:
			if not deviceUid in pympe.devices: continue
			self.remove_device_library_tab(pympe.devices[deviceUid])
			pympe.devices[deviceUid].disconnect()

		pympe.events.device_ready.unsubscribe(self.mpevent_device_ready)
		pympe.events.device_connected.unsubscribe(self.mpevent_device_connected)
		pympe.events.device_disconnected.unsubscribe(self.mpevent_device_disconnected)

	def mpevent_device_ready(self, event, device):
		if device.uid in self.devices: return

		# Only ipods should? end with the word ipod
		if not device.name.lower().endswith('ipod'): return
		self.devices.append(device.uid)

		# Set the device type
		device = pympe.devices.change_type(device, DeviceIPod)

		# Try and connect [Maybe check if we have automount?]
		device.connect()

	def mpevent_device_connected(self, event, device):
		if not device.uid in self.devices: return
		print 'Device is now connected: %s'%device.name

		# We connected then add the tab
		device.uiTabLibrary = IpodTabLibrary(pympe.ui.uiMain.uiTabsLeft, device.library, self, device)
		pympe.ui.uiMain.uiTabsLeft.InsertPage(2, device.uiTabLibrary, device.name)

		# Read from the device
		device.read()

		## Delete any missing tracks
		#device.cleanup()


	def mpevent_device_disconnected(self, event, device):
		print event, device
		if not device.uid in self.devices: return
		print 'Device Disconnected: %s'%device.name

		self.devices.remove(device.uid)
		self.remove_device_library_tab(device)

		print dir(device)


	def remove_device_library_tab(self, device):
		# Remove the library tab
		if hasattr(device, 'uiTabLibrary'):
			if device.uiTabLibrary:
				for index in range(pympe.ui.uiMain.uiTabsLeft.GetPageCount()):
					if pympe.ui.uiMain.uiTabsLeft.GetPage(index) == device.uiTabLibrary:
						pympe.ui.uiMain.uiTabsLeft.RemovePage(index)
						break

			# Delete the tab
			del device.uiTabLibrary


