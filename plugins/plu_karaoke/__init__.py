import pympe, gst

class Plugin:
	def __plugin_settings__(self, checked):
		# Now enabled, wasn't previously
		if checked and not _config['enabled']:
			pympe.player.audioBin.add_element(self.element)
		# Now disabled, enabled previously
		elif not checked and _config['enabled']:
			pympe.player.audioBin.remove_element(self.element)

		_config['enabled'] = checked

	def __plugin_start__(self):
		self.element = gst.element_factory_make('audiokaraoke')
		pympe.ui.set_menu_hook('Kareoke', self.__plugin_settings__, True, _config['enabled'])

		if _config['enabled']:
			pympe.player.audioBin.add_element(self.element)

	def  __plugin_stop__(self, shutdown):
		if _config['enabled']:
			pympe.player.audioBin.remove_element(self.element)

		pympe.ui.remove_menu_hook(self.__plugin_settings__)


