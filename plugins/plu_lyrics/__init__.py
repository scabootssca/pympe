import wx, pympe, pm_helpers, urllib, pm_network, re, time
""" Options to auto embed lyrics
Able to submit corrections
Also choice between almosts """

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, "Lyric Settings")

		self.plugin = plugin

		sizer = wx.BoxSizer(wx.VERTICAL)

		self.uiChoiceProvider = wx.Choice(self, -1, choices=['LyricWiki'])
		self.uiCheckEmbed = wx.CheckBox(self, -1, 'Automatically embed lyrics into files')

		self.uiCheckEmbed.SetValue(cfg_get('embedLyrics'))

		sizer.Add(wx.StaticText(self, -1, 'Fetch Lyrics From'))
		sizer.Add(self.uiChoiceProvider, 0, wx.EXPAND)
		sizer.Add(self.uiCheckEmbed, 0, wx.TOP, 10)
		sizer.Add(wx.Button(self, wx.ID_OK), 0, wx.TOP|wx.ALIGN_RIGHT, 10)

		self.Bind(wx.EVT_BUTTON, self.event_button)

		# Add the sizer into the dialog with a space
		box = wx.BoxSizer(wx.VERTICAL)
		box.Add(sizer, 1, wx.ALL, 10)
		self.SetSizerAndFit(box)

		self.Show()

	def event_button(self, event):
		if event.Id == wx.ID_OK:
			cfg_set('embedLyrics', self.uiCheckEmbed.GetValue())
			self.Destroy()


class Panel(wx.Panel):
	def __init__(self, parent, plugin):
		wx.Panel.__init__(self, parent)
		self.updateTime = 0
		self.plugin = plugin

		self.panelType = 'idle'
		image = pm_helpers.PmImage(stock="gtk-media-play", pil=False)
		image.set_mask_color(0, 0, 0)

		self.bitmap = wx.StaticBitmap(self, bitmap=image.scaled(32, 32).convert_to_wx_bitmap())

		self.gauge = wx.Gauge(self, -1, size=(-1, 10))
		self.labelType = wx.StaticText(self, -1, 'Play a track?')
		self.labelMessage = wx.StaticText(self, -1, '')

		self.labelType.SetFont(wx.Font(8, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))
		self.labelMessage.SetFont(wx.Font(7, wx.FONTFAMILY_DEFAULT, style=wx.FONTSTYLE_NORMAL, weight=wx.FONTWEIGHT_BOLD))

		self.lyrics = wx.TextCtrl(self, -1, '', style=wx.TE_MULTILINE|wx.TE_READONLY)
		self.lyrics.Hide()

		self.buttonOptions = wx.Button(self, -1, 'Embedded Lyrics')
		self.buttonOptions.Bind(wx.EVT_BUTTON, self.event_button_options)
		self.buttonOptions.Hide()

		self.sizer = wx.BoxSizer(wx.VERTICAL)
		self.sizer.AddStretchSpacer()
		self.sizer.Add(self.bitmap, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		self.sizer.Add(self.gauge, 0, wx.ALIGN_CENTER|wx.BOTTOM, 5)
		self.sizer.Add(self.labelType, 0, wx.ALIGN_CENTER)
		self.sizer.Add(self.labelMessage, 0, wx.ALIGN_CENTER)
		self.sizer.Add(self.lyrics, 1, wx.EXPAND)
		self.sizer.AddStretchSpacer()
		self.sizer.Add(self.buttonOptions, 0, wx.EXPAND)

		self.labelMessage.Hide()
		self.gauge.Hide()

		self.SetSizer(self.sizer)

		self.Bind(wx.EVT_TIMER, lambda x: self.gauge.Pulse())
		self.timer = wx.Timer(self)
		self.timer.Start(70)

	def event_button_options(self, event):
		def event_menu(event):
			if event.Id == 10:
				self.plugin.uiPanel.set_loading(callTime=time.time())
				self.plugin.show_lyrics(self.plugin.currentInfo, self.plugin.libraryId, forceFetch=True)

		menu = wx.Menu()
		menu.Append(10, "Fetch Lyrics From Server")
		menu.Bind(wx.EVT_MENU, event_menu)
		event.GetEventObject().PopupMenu(menu)

	@pm_helpers.unthread
	def set_error(self, message='', callTime=None):
		if callTime < self.updateTime: return
		self.updateTime = callTime

		if message:
			self.labelMessage.SetLabel(message)
			self.labelMessage.Show()
		else:
			self.labelMessage.Hide()

		if self.panelType == 'error': return
		self.panelType = 'error'

		self.labelType.SetLabel('An error has occured.')
		self.labelType.Show()


		image = pm_helpers.PmImage(stock="gtk-dialog-error", pil=False)
		image.set_mask_color(0, 0, 0)
		self.bitmap.SetBitmap(image.scaled(32, 32).convert_to_wx_bitmap())
		self.bitmap.Show()

		self.gauge.Hide()
		self.timer.Stop()
		self.lyrics.Hide()
		self.buttonOptions.Hide()
		self.sizer.Show(0)
		self.sizer.Show(6)
		self.Layout()


	def set_loading(self, message='', callTime=None):
		if callTime < self.updateTime: return
		self.updateTime = callTime

		if message:
			self.labelMessage.SetLabel(message)
			self.labelMessage.Show()
		else:
			self.labelMessage.Hide()

		if self.panelType == 'loading': return
		self.panelType = 'loading'

		self.labelType.SetLabel('Loading...')
		self.labelType.Show()
		self.bitmap.Hide()
		self.gauge.Show()
		self.timer.Start(70)
		self.lyrics.Hide()
		self.buttonOptions.Hide()
		self.sizer.Show(0)
		self.sizer.Show(6)
		self.Layout()

	@pm_helpers.unthread
	def set_idle(self, callTime=None):
		if callTime < self.updateTime: return
		self.updateTime = callTime

		if self.panelType == 'idle': return
		self.panelType = 'idle'

		image = pm_helpers.PmImage(stock="gtk-media-play", pil=False)
		image.set_mask_color(0, 0, 0)
		self.bitmap.SetBitmap(image.scaled(32, 32).convert_to_wx_bitmap())
		self.bitmap.Show()

		self.labelType.SetLabel('Play a track?')
		self.labelType.Show()
		self.labelMessage.Hide()

		self.gauge.Hide()
		self.timer.Stop()
		self.lyrics.Hide()
		self.buttonOptions.Hide()
		self.sizer.Show(0)
		self.sizer.Show(6)
		self.Layout()

	@pm_helpers.unthread
	def set_lyrics(self, lyrics, callTime=None, embed=False):
		if callTime < self.updateTime: return
		self.updateTime = callTime

		if self.panelType == 'lyrics': return
		self.panelType = 'lyrics'

		self.labelMessage.Hide()
		self.labelType.Hide()
		self.gauge.Hide()
		self.timer.Stop()
		self.bitmap.Hide()

		self.lyrics.SetValue(lyrics)
		self.lyrics.Show()
		if embed: self.buttonOptions.Show()
		else: self.buttonOptions.Hide()
		self.sizer.Show(0, False)
		self.sizer.Show(6, False)
		self.Layout()

class Plugin:
	def plu_start(self):
		self.uiPanel = Panel(pympe.ui.uiMain.uiTabsLeft, self)
		pympe.ui.uiMain.uiTabsLeft.InsertPage(1, self.uiPanel, "Lyrics")
		pympe.ui.uiMain.uiTabsLeft.SetSelection(1)

		subscribe(pympe.events.player_current_changed, self.mpevent_player_current_changed)

		if pympe.player.playing:
			self.uiPanel.set_loading(callTime=time.time())
			self.show_lyrics(pympe.player.currentInfo, pympe.player.currentId, pympe.player.currentLibrary)

	def plu_stop(self, shutdown=False):
		self.uiPanel.timer.Stop()

		# Remove the panel
		for index in range(pympe.ui.uiMain.uiTabsLeft.GetPageCount()):
			if pympe.ui.uiMain.uiTabsLeft.GetPage(index) == self.uiPanel:
				pympe.ui.uiMain.uiTabsLeft.RemovePage(index)
				break

	def plu_settings(self, parent):
		SettingsDialog(self, parent)

	@pm_helpers.threaded
	def show_lyrics(self, currentInfo, currentId=None, currentLibrary=None, forceFetch=False):
		(self.currentInfo, self.currentId, self.currentLibrary) = (currentInfo, currentId, currentLibrary)

		if 'lyrics' in currentInfo and currentInfo['lyrics'] and not forceFetch:
			return self.uiPanel.set_lyrics(currentInfo['lyrics'], callTime=time.time(), embed=True)

		if not 'title' in currentInfo: return self.uiPanel.set_error('Track not found.', callTime=time.time())

		title = urllib.quote_plus(currentInfo['title'][0].encode('UTF-8'))

		if 'artist' in currentInfo and currentInfo['artist']:
			artist = urllib.quote_plus(currentInfo['artist'][0].encode('UTF-8'))
		else:
			artist = ''

		result = pm_network.get_url('http://lyrics.wikia.com/Special:Search?search=%s:%s' % (artist, title), returnData=True)
		if not self.currentInfo or self.currentInfo != currentInfo: return

		#with file('/home/scabootssca/Desktop/current.htm', 'w') as f:
		#	f.write(result)


		# The error/not found pages
		if not result: return self.uiPanel.set_error('Connection error.', callTime=time.time())

		## For random redirect pages
		#if '/Special:UserLogin?returnto=' in result:
			#result = re.search("\/Special\:UserLogin\?returnto\=(.+?)\&amp\;type\=login", result)
			#if not result: return self.uiPanel.set_error('Track not found.', callTime=time.time())
			#result = pm_network.get_url('http://lyrics.wikia.com/'+result.group(1), returnData=True)
			#if not self.currentInfo or self.currentInfo != currentInfo: return
			#if not result: return self.uiPanel.set_error('Connection error.', callTime=time.time())


		# For lyric links
		elif not "class='lyricbox'" in result and 'result-link' in result:
			self.approxMatch = True
			result = re.search("<a href=\"(.+?)\".+?result\-link", result)
			if not result: return self.uiPanel.set_error('Track not found.', callTime=time.time())
			result = pm_network.get_url(result.group(1), returnData=True)
			if not self.currentInfo or self.currentInfo != currentInfo: return
			if not result: return self.uiPanel.set_error('Connection error.', callTime=time.time())
		else:
			self.approxMatch = False

		# Else
		if not "class='lyricbox'" in result: return self.uiPanel.set_error('Lyrics not found.', callTime=time.time())

		# Parse the lyrics
		result = result[result.index("class='lyricbox'"):]
		result = result[result.index("</script>")+9:result.index("<!--")]
		result = pm_helpers.unencode(re.sub("<.*?>", "", result.replace("<br />", "\n")))
		self.uiPanel.set_lyrics(result, callTime=time.time())

		# Embed the lyrics if needed
		if cfg_get('embedLyrics') and libraryId != None:
			if currentInfo['lyrics'] != result:
				currentLibrary.set_single_track_single_value(currentId, 'lyrics', result, writeMediaFile=True)

	def mpevent_player_current_changed(self, event, currentInfo, currentId, currentLibrary):
		self.uiPanel.set_loading(callTime=time.time())
		self.show_lyrics(currentInfo, currentId, currentLibrary)
