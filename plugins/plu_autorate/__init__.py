import pympe, wx
import time

#lastAnalysisDate = ''
#wholeStarRatings = False
#rateUnratedTracksOnly = False
#cacheResults = True
#cacheTime = 3
#ratingBias = 0.5
#ratingMemory = 0.0
#useHalfStarForItemsWithMoreSkipsThanPlays = True

#binLimitFrequencies = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
#binLimitCounts = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
"""
Make it so it has an auto rating based on play count, how many times it was skipped, 
how many times it was finished and what percentage was listened to, and how many times it was played directly
"""

def analyseTracks(library):
	# Vars
	skipCountFactor = 3.0
	#/END

	numAnalysed = 0
	countList = []
	frequencyList = []
	
	# Analize Each Track
	for track in library.tracks.values():
		playCount = track['playcount']
		skipCount = track['skipcount']*skipCountFactor

		if playCount > skipCount:
			numAnalysed += 1
			combinedCount = (playCount-skipCount)*track['length']

			if combinedCount <= 0:
				combinedCount = 0
				combinedFrequency = 0.0
			else:
				combinedFrequency = combinedCount/(time.time()-track['timeadded'])
				
			countList.append(combinedCount)
			frequencyList.append(combinedFrequency)
			
	# If the library is empty
	if not countList: return [], []
			
	# Get the section caps
	countList = sorted(countList)
	frequencyList = sorted(frequencyList)
	binLimits = [0.0, 0.01, 0.04, 0.11, 0.23, 0.4, 0.6, 0.77, 0.89, 0.96]
	binLimitFrequencies = []
	binLimitCounts = []
	
	for binLimit in binLimits:
		binLimitIndex = int(numAnalysed*binLimit)
		
		if binLimitIndex < 0: binLimitIndex = 0
		elif binLimitIndex >= numAnalysed: binLimitIndex = numAnalysed-1
		
		binLimitFrequencies.append(frequencyList[binLimitIndex])
		binLimitCounts.append(countList[binLimitIndex])
		
	return binLimitCounts, binLimitFrequencies
	
def rateTracks(library, binLimitCounts, binLimitFrequencies):
	# Vars
	minRating = 1.0
	maxRating = 5.0
	skipCountFactor = 3.0
	ratingBias = cfg_get('ratingBias')#0.5
	#/END
	
	#if cfg_get('useWholeStarRatings'):
	#minRatingPercent = int(minRating*20)
	#else:
	minRatingPercent = int(minRating*10)
	maxRatingPercent = int(maxRating*20)
	
	ratingScale = maxRatingPercent-minRatingPercent
	
	minBin = minRatingPercent/10
	maxBin = maxRatingPercent/10

	#if cfg_get('useWholeStarRatings'):
	#	binIncrement = 2
	#else:
	binIncrement = 1
	
	for trackId in library.tracks.keys():
		track = library.tracks[trackId]
		if track['hasCustomRating']: continue
		
		playCount = track['playcount']
		skipCount = track['skipcount']*skipCountFactor
		
		if playCount == 0 and skipCount == 0:
			trackRating = 0
		else:
			combinedCount = (playCount-skipCount)*track['length']
			if combinedCount <= 0:
				combinedCount = 0
				combinedFrequency = 0.0
			else:
				combinedFrequency = (combinedCount/(time.time()-track['timeadded']))

			# Find which bin this belongs in by frequency
			currentBin = maxBin
			while combinedFrequency < binLimitFrequencies[currentBin-1] and currentBin > minBin: currentBin -= binIncrement
			frequencyMethodRating = currentBin
			
			# Find which bin this belongs in by count
			currentBin = maxBin
			while combinedCount < binLimitCounts[currentBin-1] and currentBin > minBin: currentBin -= binIncrement
			countMethodRating = currentBin

			# Combine them
			trackRating = (frequencyMethodRating*(1-ratingBias))+(countMethodRating*ratingBias)

			# Round to whole stars
			if cfg_get('useWholeStarRatings'):
				trackRating = round(trackRating/2)*2
			else:
				trackRating = round(trackRating)
		
		if trackRating > 0:
			#track['rating'] = trackRating
			library.set_single_track_single_value(trackId, 'rating', trackRating)
			
	cfg_set('lastRan', time.time())

def rate_libraries():
	#print 'Rating...'
	for library in pympe.libraries.values():
		#print 'Rating library:%s'%library.dbFilename
		binLimitCounts, binLimitFrequencies = analyseTracks(library)
		rateTracks(library, binLimitCounts, binLimitFrequencies)
	#print 'Finished Rating'

class SettingsDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, "Autorate Settings")

		self.plugin = plugin
		sizer = wx.GridBagSizer(5, 5)
		
	# Bias slider
		uiLabelBias = wx.StaticText(self, -1, 'Rating Bias')
		uiLabelBiasLeft = wx.StaticText(self, -1, 'Play\nFrequency', style=wx.ALIGN_CENTRE)
		uiLabelBiasRight = wx.StaticText(self, -1, 'Play\nCount', style=wx.ALIGN_CENTRE)
		self.uiSliderBias = wx.Slider(self, -1, 5, 0, 10, size=(200, -1))
		
		# Set the font
		uiFont = uiLabelBias.GetFont()
		uiLabelBias.SetFont(wx.Font(uiFont.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.DEFAULT, wx.FONTWEIGHT_BOLD))
		uiFont.SetPointSize(uiFont.GetPointSize()-2)
		uiLabelBiasLeft.SetFont(uiFont)
		uiLabelBiasRight.SetFont(uiFont)
		
		self.uiSliderBias.SetValue(int(cfg_get('ratingBias')*10))
		
		# Add it to the sizer
		sizer.Add(uiLabelBias, (0, 1), (1, 1), wx.ALIGN_CENTER|wx.TOP, 10)
		sizer.Add(uiLabelBiasLeft, (1, 0))
		sizer.Add(self.uiSliderBias, (1, 1))
		sizer.Add(uiLabelBiasRight, (1, 2))

	# Update frequency
		uiLabelUpdate = wx.StaticText(self, -1, 'Automatic Update Every')
		uiChoiceUpdate = wx.Choice(self, -1, choices=['10 minutes', '20 minutes', '30 minutes', '1 hour', '2 hours', '4 hours', '8 hours', '12 hours', '24 hours', 'Never'])

		uiFont = uiLabelUpdate.GetFont()
		uiFont.SetWeight(wx.FONTWEIGHT_BOLD)
		uiLabelUpdate.SetFont(uiFont)
		
		uiChoiceUpdate.choiceMap = [600, 1200, 1800, 3600, 7200, 14400, 28800, 43200, 86400, None]
		
		uiChoiceUpdate.SetSelection(uiChoiceUpdate.choiceMap.index(cfg_get('updateFrequency')))
		
		uiChoiceUpdate.Bind(wx.EVT_CHOICE, self.event_choice_update)
		
		sizer.Add(uiLabelUpdate, (2, 1), (1, 1), wx.ALIGN_CENTER|wx.TOP, 10)
		sizer.Add(uiChoiceUpdate, (3, 1), (1, 1), wx.EXPAND)

	# More settings
		uiLabelMisc = wx.StaticText(self, -1, 'Misc')
		uiCheckWholeStars = wx.CheckBox(self, -1, 'Only use whole star ratings')
		uiButtonUpdate = wx.Button(self, -1, 'Rate Library Now')
		
		uiLabelMisc.SetFont(uiFont)
		
		uiCheckWholeStars.Bind(wx.EVT_CHECKBOX, self.event_check_whole_stars)
		uiButtonUpdate.Bind(wx.EVT_BUTTON, self.event_button_update)
		
		uiCheckWholeStars.SetValue(cfg_get('useWholeStarRatings'))

		sizer.Add(uiLabelMisc, (4, 1), (1, 1), wx.ALIGN_CENTER|wx.TOP, 10)
		sizer.Add(uiCheckWholeStars, (5, 1), (1, 1))
		sizer.Add(uiButtonUpdate, (6, 1), (1, 1), wx.ALIGN_CENTER)

	# Controls
		sizer.Add(wx.StaticLine(self, -1), (7, 0), (1, 3), wx.EXPAND|wx.TOP|wx.BOTTOM, 10)
		sizer2 = wx.BoxSizer(wx.HORIZONTAL)
		sizer.Add(sizer2, (8, 0), (1, 3), wx.EXPAND|wx.BOTTOM, 10)
		sizer2.Add(wx.Button(self, wx.ID_OK))
		sizer2.AddStretchSpacer()
		sizer2.Add(wx.Button(self, wx.ID_CANCEL))

		# Add the sizer into the dialog with a space
		box = wx.BoxSizer(wx.VERTICAL)
		box.Add(sizer, 1, wx.EXPAND|wx.LEFT|wx.RIGHT, 20)
		self.SetSizerAndFit(box)

		result = self.ShowModal()
		if result == wx.ID_CANCEL: return
		
		cfg_set('ratingBias', self.uiSliderBias.GetValue()*.1)
		
	def event_choice_update(self, event):
		cfg_set('updateFrequency', event.EventObject.choiceMap[event.Int])
		self.plugin.timer.Stop()
		if event.EventObject.choiceMap[event.Int] is not None:
			self.plugin.timer.Start(cfg_get('updateFrequency')*1000)
		
	def event_check_whole_stars(self, event):
		cfg_set('useWholeStarRatings', event.Int)
		
	def event_button_update(self, event):
		cfg_set('ratingBias', self.uiSliderBias.GetValue()*.1)
		
		rate_libraries()

class Plugin:
	def plu_settings(self, parent):
		SettingsDialog(self, parent)
	
	def plu_start(self):
		self.timer = wx.Timer(None)
		self.timer.Bind(wx.EVT_TIMER, self.event_timer)
		if cfg_get('updateFrequency') is not None:
			self.timer.Start(cfg_get('updateFrequency')*1000)
		
		if time.time()-cfg_get('lastRan') > cfg_get('updateFrequency'):
			rate_libraries()

	def plu_stop(self, shutdown):
		self.timer.Stop()
		
	def event_timer(self, event):
		rate_libraries()

