import pympe, gst, wx
from pm_player import AudioBin

class RGAnalysisBin(AudioBin):
	def setup(self):
		self.rgAnalysis = gst.element_factory_make('rganalysis')

		# Settings
			## Prefer ReplayGain's per-album correction over per-track correction.
		#self.rgVolume.set_property("album-mode", _config['album_mode'])
			## Additional amplification to apply to all files
		#self.rgVolume.set_property("pre-amp", _config['preamp'])
			## Fallback correction for files that lack ReplayGain information
		#self.rgVolume.set_property("fallback-gain", _config["fallback_gain"])

		self.elements = [self.rgAnalysis]

class RGVolumeBin(AudioBin):
	def setup(self):
		self.rgAnalysis = gst.element_factory_make('rganalysis')
		self.rgVolume = gst.element_factory_make('rgvolume')

		# Settings
			# Prefer ReplayGain's per-album correction over per-track correction.
		self.rgVolume.set_property("album-mode", _config['album_mode'])
			# Additional amplification to apply to all files
		self.rgVolume.set_property("pre-amp", _config['preamp'])
			# Fallback correction for files that lack ReplayGain information
		self.rgVolume.set_property("fallback-gain", _config["fallback_gain"])

		self.elements = [gst.element_factory_make('audioconvert'), self.rgAnalysis, self.rgVolume]

class RGLimiterBin(AudioBin):
	def setup(self):
		self.rgLimiter = gst.element_factory_make("rglimiter")

		# Settings
			# Protects from noise caused by over amplification
		self.rgLimiter.set_property("enabled", _config["limiter_enabled"])

		self.elements = [gst.element_factory_make('audioconvert'), self.rgLimiter]


class ReplayGainDialog(wx.Dialog):
	def __init__(self, plugin, parent):
		wx.Dialog.__init__(self, parent, -1, "ReplayGain Settings")
		self.plugin = plugin

		sizer = wx.GridBagSizer(5, 5)

		self.checkPerAlbum = wx.Choice(self, -1, choices=["Radio (Equal Loudness For All Tracks", "Album (Ideal Loudness For All Tracks"])
		self.checkClipping = wx.CheckBox(self, -1, "Apply Compression To Prevent Clipping")
		self.spinPreamp = wx.Slider(self, 0, cfg_get("preamp"), -15.0, 15.0, style=wx.SL_AUTOTICKS|wx.SL_LABELS)
		self.spinFallback = wx.Slider(self, 1, cfg_get("fallback_gain"), -15.0, 15.0, style=wx.SL_AUTOTICKS|wx.SL_LABELS)

		self.checkPerAlbum.SetSelection(cfg_get("album_mode"))
		self.checkClipping.SetValue(cfg_get("limiter_enabled"))

		sizer.Add(wx.StaticText(self, -1, "ReplayGain Mode:"), (0, 0), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer.Add(self.checkPerAlbum, (0, 1), (1, 1))

		sizer.Add(wx.StaticText(self, -1, "Pre-Amp (dB):"), (1, 0), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer.Add(self.spinPreamp, (1, 1), (1, 2), wx.EXPAND)
		sizer.Add(wx.StaticText(self, -1, "Fallback correction level (dB):"), (2, 0), flag=wx.ALIGN_CENTER_VERTICAL)
		sizer.Add(self.spinFallback, (2, 1), flag=wx.EXPAND)
		sizer.Add(self.checkClipping, (3, 0), (1, 2))

		s2 = wx.BoxSizer()
		s2.AddStretchSpacer()
		s2.Add(wx.Button(self, wx.ID_CLOSE))
		sizer.Add(s2, (4, 0), (1, 2), flag=wx.EXPAND)

		self.Bind(wx.EVT_BUTTON, self.event_button)
		self.checkClipping.Bind(wx.EVT_CHECKBOX, self.event_check)
		self.checkPerAlbum.Bind(wx.EVT_CHOICE, self.event_choice)
		self.spinPreamp.Bind(wx.EVT_SCROLL, self.event_slider)
		self.spinFallback.Bind(wx.EVT_SCROLL, self.event_slider)

		# Finish up
		box = wx.BoxSizer()
		box.Add(sizer, 1, wx.ALL|wx.EXPAND, 10)
		self.SetSizerAndFit(box)

		# Show it
		self.CenterOnScreen()
		self.Show()

	def event_check(self, event):
		cfg_set("limiter_enabled", self.checkClipping.GetValue())
		self.plugin.rgLimiter.rgLimiter.set_property("enabled", self.checkClipping.GetValue())

	def event_choice(self, event):
		cfg_set("album_mode", self.checkPerAlbum.GetSelection())
		self.plugin.rgVolume.rgVolume.set_property("album-mode", self.checkPerAlbum.GetSelection())

	def event_slider(self, event):
		if event.Id == 0:
			cfg_set("preamp", self.spinPreamp.GetValue())
			self.plugin.rgVolume.rgVolume.set_property("pre-amp", self.spinPreamp.GetValue())
		elif event.Id == 1:
			cfg_set("fallback_gain", self.spinFallback.GetValue())
			self.plugin.rgVolume.rgVolume.set_property("fallback-gain", self.spinFallback.GetValue())

	def event_button(self, event):
		if event.Id == wx.ID_CLOSE:
			self.Destroy()
		else:
			event.Skip()


class Plugin:
	def plu_start(self):
		#self.rgAnalysis = RGAnalysisBin('RGAnalysisBin')
		self.rgVolume = RGVolumeBin('RGVolumeBin')
		self.rgLimiter = RGLimiterBin('RGLimiterBin')
		#pympe.player.audioBin.add_element(self.rgAnalysis)
		pympe.player.audioBin.add_element(self.rgVolume)
		pympe.player.audioBin.add_element(self.rgLimiter)

	def plu_stop(self, shutdown):
		if shutdown: return
		#pympe.player.audioBin.remove_element(self.rgAnalysis)
		pympe.player.audioBin.remove_element(self.rgVolume)
		pympe.player.audioBin.remove_element(self.rgLimiter)


	def plu_settings(self, parent):
		ReplayGainDialog(self, parent)
