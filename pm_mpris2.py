#import pympe, dbus, indicate, tempfile, os, pm_mediafile
import pympe, pm_mediafile
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
from pm_helpers import make_display_tag
#from hashlib import md5

# 'b' bool
# 'i' integer
# 's' string
# 'ay' array?
# 'o.' none or integer
# 'x' float

class Mpris2Interface(dbus.service.Object):
	def __init__(self):
		self.object_path = '/org/mpris/MediaPlayer2'
		self.bus = dbus.service.BusName('org.mpris.MediaPlayer2.pympe', bus=dbus.SessionBus(mainloop=DBusGMainLoop()))
		dbus.service.Object.__init__(self, self.bus, self.object_path)

		self.dbusProperties = {'org.mpris.MediaPlayer2':(\
									('CanQuit', 'b', 'read'),
									('Fullscreen', 'b', 'readwrite'),
									('CanSetFullscreen', 'b', 'read'),
									('CanRaise', 'b', 'read'),
									('HasTrackList', 'b', 'read'),
									('Identity', 's', 'read'),
									('DesktopEntry', 's', 'read'),
									('SupportedUriSchemes', 'as', 'read'),
									('SupportedMimeTypes', 'as', 'read')),
								'org.mpris.MediaPlayer2.Player':(\
									('PlaybackStatus', 's', 'read'),
									('LoopStatus', 's', 'readwrite'),
									('Rate', 'd', 'readwrite'),
									('Shuffle', 'b', 'readwrite'),
									('Metadata', 'a{sv}', 'read'),
									('Volume', 'd', 'readwrite'),
									('Position', 'x', 'read'),
									('MinimumRate', 'd', 'read'),
									('MaximumRate', 'd', 'read'),
									('CanGoNext', 'b', 'read'),
									('CanGoPrevious', 'b', 'read'),
									('CanPlay', 'b', 'read'),
									('CanPause', 'b', 'read'),
									('CanSeek', 'b', 'read'),
									('CanControl', 'b', 'read'))}

	def module_start(self):
		pass

	def module_stop(self):
		pass

	#######################################################
	# org.freedesktop.DBus.Properties
	#######################################################
	@dbus.service.method(dbus.INTROSPECTABLE_IFACE, '', 's')
	def Introspect(self, *args):
		"""Return a string of XML encoding this object's supported interfaces,
		methods and signals.
		"""
		reflection_data = '<!DOCTYPE node PUBLIC "-//freedesktop//DTD D-BUS Object Introspection 1.0//EN"\n                      "http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd">\n'
		reflection_data += '<node name="%s">\n' % self.object_path

		interfaces = self._dbus_class_table[self.__class__.__module__ + '.' + self.__class__.__name__]
		for (name, funcs) in interfaces.items():
			reflection_data += '  <interface name="%s">\n' % (name)

			for func in funcs.values():
				if getattr(func, '_dbus_is_method', False):
					reflection_data += self.__class__._reflect_on_method(func)
				elif getattr(func, '_dbus_is_signal', False):
					reflection_data += self.__class__._reflect_on_signal(func)

			# If the interface name is in the property list
			if name in self.dbusProperties:
				for prop in self.dbusProperties[name]:
					reflection_data += '<property name="{}" type="{}" access="{}"/>'.format(*prop)

			reflection_data += '  </interface>\n'

		reflection_data += '</node>\n'

		return reflection_data

	#######################################################
	# org.freedesktop.DBus.Properties
	#######################################################
	def handleProperties(self, interface, prop=None, value=None):
		# For getting properties
		if value == None:
			if interface == 'org.mpris.MediaPlayer2':
				if prop == 'CanQuit':
					return True
				elif prop == 'Fullscreen':
					return pympe.ui.uiMain.IsFullScreen()
				elif prop == 'CanSetFullscreen':
					return True
				elif prop == 'CanRaise':
					return True
				elif prop == 'HasTrackList':
					return True
				elif prop == 'Identity':
					return 'Pympe Media Player'
				elif prop == 'DesktopEntry':
					return 'pympe'
				elif prop == 'SupportedUriSchemes':
					return ['file', 'http']
				elif prop == 'SupportedMimeTypes':
					return ['audio/mpeg']
			elif interface == 'org.mpris.MediaPlayer2.Player':
				if prop == 'PlaybackStatus':
					return ('Stopped' if not pympe.player.playing else ('Paused' if pympe.player.paused else 'Playing'))
				elif prop == 'LoopStatus':
					loop = pympe.config.get("player", "loop")
					return 'None' if loop == 'LOOP OFF' else ('Track' if loop == 'LOOP ONE' else 'Playlist')
				elif prop == 'Rate':
					return 1.0
				elif prop == 'Shuffle':
					return (True if pympe.config.get('player', 'shuffle') else False)
				elif prop == 'Metadata':
					return self.generate_mpris_metadata(pympe.player.currentInfo)
				elif prop == 'Volume':
					return pympe.player.volume()
				elif prop == 'Position':
					return pympe.player.position()*1000
				elif prop == 'MinimumRate':
					return 1.0
				elif prop == 'MaximumRate':
					return 1.0
				elif prop == 'CanGoNext':
					return (True if pympe.playlists.current.get_next() else False)
				elif prop == 'CanGoPrevious':
					return (True if pympe.playlists.current.get_previous() else False)
				elif prop == 'CanPlay':
					return (True if pympe.playlists.current.get_length() else False)
				elif prop == 'CanPause':
					return True
				elif prop == 'CanSeek':
					return True
				elif prop == 'CanControl':
					return True

		# For setting properties
		else:
			if interface == 'org.mpris.MediaPlayer2':
				if prop == 'Fullscreen':
					pympe.event.ui_change_view_mode('theatre' if value else 'normal')
			elif interface == 'org.mpris.MediaPlayer2.Player':
				if prop == 'LoopStatus':
					pympe.events.player_set_repeat(0 if value == 'None' else (1 if value == 'Track' else 2))
				#elif prop == 'Rate': # Set playback rate (not implemented)
				elif prop == 'Shuffle':
					pympe.events.player_set_shuffle(True if value else False)
				elif prop == 'Volume':
					pympe.player.volume(volume if volume > 0 else 0)


	@dbus.service.method(dbus.PROPERTIES_IFACE, 'ss', 'v')
	def Get(self, interface, prop):
		return self.handleProperties(interface, prop)

	@dbus.service.method(dbus.PROPERTIES_IFACE, 'ssv', None)
	def Set(self, interface, prop, value):
		return self.handleProperties(interface, prop, value)

	@dbus.service.method(dbus.PROPERTIES_IFACE, 's', 'a{sv}')
	def GetAll(self, interface=None):
		results = {}

		if not interface in self.dbusProperties: return {}

		for propertyName in self.dbusProperties[interface]:
			results[propertyName[0]] = self.handleProperties(interface, propertyName[0])

		return results

#######################################################
# org.mpris.MediaPlayer2
#######################################################
	@dbus.service.method('org.mpris.MediaPlayer2')
	def Raise(self): pympe.ui.uiMain.bring_to_front()
	@dbus.service.method('org.mpris.MediaPlayer2')
	def Quit(self): pympe.stop()

#######################################################
# org.mpris.MediaPlayer2.Player
#######################################################
	# Methods
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def Next(self): pympe.events.player_next()
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def Previous(self): pympe.events.player_previous()
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def Pause(self): pympe.events.player_pause()
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def PlayPause(self):
		if pympe.player.playing: pympe.events.player_pause()
		else: pympe.events.player_play()
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def Stop(self): pympe.events.player_stop()
	@dbus.service.method('org.mpris.MediaPlayer2.Player')
	def Play(self): pympe.events.player_play()
	@dbus.service.method('org.mpris.MediaPlayer2.Player', 'x')
	def Seek(self, microseconds):
		pympe.player.seek(microseconds*1000)
	@dbus.service.method('org.mpris.MediaPlayer2.Player', 'ox')
	def SetPosition(self, trackId, microseconds):
		if trackId == self.trackId: pympe.player.position(microseconds*1000)
	@dbus.service.method('org.mpris.MediaPlayer2.Player', 's')
	def OpenUri(self, uri): pympe.player.play(uri)

	# Signals
	@dbus.service.signal("org.mpris.MediaPlayer2.Player", 'x')
	def Seeked(self, microseconds): pass

############################################
# Helper Functions
############################################3
	def generate_mpris_metadata(self, currentInfo):
		if currentInfo is None: return dbus.types.Dictionary({}, signature='sv')

		## mpris2.0 meta map, defined at http://xmms2.org/wiki/MPRIS_Metadata
		meta = {'mpris:trackid':currentInfo['trackId'],
				'mpris:length':currentInfo['length']*1000000,
				'xesam:album':make_display_tag(currentInfo['album'], False, False),
				'xesam:artist':currentInfo['artist'],
				'xesam:title':make_display_tag(currentInfo['title'], False, False),
				'xesam:url':currentInfo['uri'],
		}

		if 'albumartist' in currentInfo:
			meta['xesam:albumArtist'] = currentInfo['albumartist']
		if 'lyrics' in currentInfo:
			meta['xesam:asText'] = currentInfo['lyrics']
		if 'rating' in currentInfo:
			meta[('xesam:userRating' if currentInfo['hasCustomRating'] else 'xesam:autoRating')] = currentInfo['rating']*.2
		if 'comment' in currentInfo:
			meta['xesam:comment'] = currentInfo['comment']
		if 'composer' in currentInfo:
			meta['xesam:composer'] = currentInfo['composer']
		if 'discnumber' in currentInfo:
			meta['xesam:discNumber'] = currentInfo['discnumber']
		if 'genre' in currentInfo:
			meta['xesam:genre'] = currentInfo['genre']
		if 'timeplayed' in currentInfo:
			meta['xesam:lastUsed'] = currentInfo['timeplayed']
		if 'tracknumber' in currentInfo:
			meta['xesam:trackNumber'] = currentInfo['tracknumber']
		if 'playcount' in currentInfo:
			meta['xesam:useCount'] = currentInfo['playcount']


		# meta['xesam:title'] = mediaFile.display('title', True)
		# meta['xesam:album'] = mediaFile.display('album', True)
		# meta['xesam:artist'] = dbus.types.Array(mediaFile['artist'], signature='s')
		# meta['mpris:length'] = dbus.types.Int64(int(mediaFile['length'] or 0)*1000)

		cacheFile = pympe.cache.has(currentInfo['uri'])
		if cacheFile is None and currentInfo['art']:
			cacheFile = pympe.cache.set(currentInfo['uri'], currentInfo['art'])
		if cacheFile is not None: meta['mpris:artUrl'] = 'file://'+cacheFile

		#print meta
		# meta['mpris:trackid'] = mediaFile['uri']
		# meta['xesam:url'] = mediaFile['uri']

		return dbus.types.Dictionary(meta, signature='sv')
