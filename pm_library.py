import os, pm_helpers, pympe, re, pm_mediafile, time, shutil, hashlib, struct, itertools
from hashlib import sha1
import cPickle as pickle
import threading, Queue

import threading
class Library:
	def __init__(self, dbFilename=None, userConfig={}):
		#print 'Is self.name a good name for time.time() the library was initalized and do we need it? if so then do we need the library.uid'
		#self.name = time.time()
		self.version = 3
		self.uid, pympe.libraryUid = pympe.libraryUid, pympe.libraryUid+1
		pympe.libraries[self.uid] = self
		self.dbFilename = None # Keep this as None
		self.scanning = False
		self.device = None

		# Threading variables
		self.running = False
		self.lock = threading.RLock()
		self.queue = Queue.Queue()
		self.worker = threading.Thread(target=self.start_worker, name='Library %i Worker Thread'%self.uid)

		self.searchSanitizerRegex = '''[^a-z0-9\- ]*'''

		self.multiTagsLocal = ['title','comment']

		self.singleTagsLocal = ['tracknumber', 'tracktotal', 'discnumber', 'disctotal',
			'musicbrainzid', 'uri', 'mimetype', 'filesize', 'filename', 'bitrate', 'samplerate', 'length', 'rating',
			'compilation', 'playcount', 'skipcount', 'timeplayed', 'timemodified', 'timeadded', 'lyrics', 'hasCustomRating',
			'usePlaybackPosition', 'playbackPosition', 'useStartTime', 'startTime', 'useStopTime', 'stopTime',
			'skipOnShuffle', 'useEqualizerPreset', 'equalizerPreset', 'hash', 'episodenumber', 'episodetotal', 'bookmarks']

		self.multiTagsShared = ['artist', 'album', 'year', 'genre', 'albumartist', 'composer', 'tags', 'series', 'season']

		self.blankTrack = {'trackId':(0, int),
							'title':([], list),
							'comment':([], list),
							'tracknumber':(0, int),
							'tracktotal':(0, int),
							'discnumber':(0, int),
							'disctotal':(0, int),
							'musicbrainzid':('', str),
							'uri':('', str),
							'mimetype':('', str),
							'filesize':(0, int),
							'bitrate':(0, int),
							'samplerate':(0, int),
							'length':(0, int),
							'rating':(0, int),
							'compilation':(False, bool),
							'playcount':(0, int),
							'skipcount':(0, int),
							'timeplayed':(0, int),
							'timemodified':(0, int),
							'timeadded':(0, int),
							'lyrics':('', str),
							'hasCustomRating':(False, bool),
							'usePlaybackPosition':(False, bool),
							'playbackPosition':(0, int),
							'useStartTime':(False, bool),
							'startTime':(0, int),
							'useStopTime':(False, bool),
							'stopTime':(0, int),
							'skipOnShuffle':(False, bool),
							'useEqualizerPreset':(False, bool),
							'equalizerPreset':(0, int),
							'hash':('', str),
							'episodenumber':(0, int),
							'episodetotal':(0, int),
							'bookmarks':([], list),
							'artist':([], list),
							'album':([], list),
							'year':([], list),
							'genre':([], list),
							'albumartist':([], list),
							'composer':([], list),
							'tags':([], list),
							'series':([], list),
							'season':([], list)}

		self.dbKeys = self.blankTrack.keys()

		if dbFilename != None:
			self.start(dbFilename, userConfig)
		else:
			self.blank_database(sendEvent=False)

		# Subscribe
		pympe.events.player_previous_changed.subscribe(self.mpevent_player_previous_changed)
		pympe.events.player_current_updated.subscribe(self.mpevent_player_current_updated)
		pympe.events.player_current_changed.subscribe(self.mpevent_player_current_changed)
		pympe.events.config_key_changed.subscribe(self.mpevent_config_key_changed)

#for some reason on a blank database this is fucking up but it won't work when loading either it has the wrong tag ids
	def cleanup(self):
		for tag in self.sharedTags:
			print tag # DEBUG
			break

	def initalize_config(self, userConfig={}):
		for key, value in userConfig.items():
			pympe.config.set_first(self.configKey, key, value)

		# Library Settings
		pympe.config.set_first(self.configKey, 'searchPaths', [])
		pympe.config.set_first(self.configKey, 'defaultPath', '')
		pympe.config.set_first(self.configKey, 'watchForChanges', False)
		pympe.config.set_first(self.configKey, 'keepOrganized', False)
		pympe.config.set_first(self.configKey, 'writeFolderImage', False)
		pympe.config.set_first(self.configKey, 'saveMissingTags', False)

		# Library Interface Settings
		pympe.config.set_first(self.configKey, "ui_filters", [[0, "genre"], [0, "year"], [1, "albumartist"], [0, "artist"], [1, "album"], [0, 'composer']]) # [enabled, tag]
		pympe.config.set_first(self.configKey, "ui_columns", [[1, 'tracknumber', 50], [1, 'title', 270], [1, 'artist', 200], [1, 'album', 200], [0, 'composer', 200], [1, 'year', 45], [1, 'genre', 200], [0, "discnumber", 30], [0, "length", 45], [0, "bitrate", 30], [0, "rating", 50], [0, "playcount", 35], [0, "compilation", 30], [0, "timemodified", 130], [0, "timeplayed", 130], [0, "timeadded", 130], [0, 'uri', 300]]) # [enabled, tag, width]
		pympe.config.set_first(self.configKey, "ui_vertical_layout", False)
		pympe.config.set_first(self.configKey, "ui_panelSizeV", 200)
		pympe.config.set_first(self.configKey, "ui_panelSizeH", 200)

		pympe.config.set_first(self.configKey, 'sortOrder', {'artist':["artist", "album", "discnumber", "tracknumber"],
															'albumartist':["albumartist", "album", "discnumber", "tracknumber"],
															'tracknumber':["albumartist", "album", "discnumber", "tracknumber"],
															'album':["album", "discnumber", "tracknumber"],
															'composer':['composer', 'album', 'discnumber', 'artist', 'tracknumber'],
															None:["albumartist", "album", "discnumber", "tracknumber"]})

		pympe.config.set_first(self.configKey, 'defaultColumn', 'tracknumber')

	def start(self, dbFilename=None, userConfig={}):
		# Database here?
		if dbFilename:
			self.dbFilename = pympe.dataDirectory+os.sep+dbFilename+'.pmd'
			self.configKey = 'library_%s'%pm_helpers.sanitize_path(self.dbFilename.rsplit(os.sep, 1)[-1])
			self.initalize_config(userConfig)

		self.worker.start()
		self.load_database()

	def shutdown(self):
		del pympe.libraries[self.uid]
		self.save_database()

		self.running = False
		self.queue.put(None)
		self.queue.join()
		self.worker.join()

	# Worker thread
	def start_worker(self):
		if self.running: return
		self.running = True

		while self.running:
			task = self.queue.get()

			if task == None or not self.running:
				self.queue.task_done()
				break

			with self.lock:
				try:
					task[0](*task[1])
				except:
					pympe.error('There has been a library worker thread error.', True, False)

			self.queue.task_done()

		[self.queue.task_done() for x in range(self.queue.qsize())]

	def blank_database(self, sendEvent=True):
		# Db skeleton
		self.tracks   = {}
		self.tracksByUri = {}
		self.sharedTagsByName = {"artist":{}, "album":{}, "year":{}, "genre":{}, "albumartist":{}, "composer":{}, "tags":{}, "series":{}, 'season':{}}
		self.sharedTags   = {"artist":{}, "album":{}, "year":{}, "genre":{}, "albumartist":{}, "composer":{}, "tags":{}, "series":{}, 'season':{}}
		self.sharedTagIds = {"artist":0, "album":0, "year":0, "genre":0, "albumartist":0, "composer":0, "tags":0, "series":0, 'season':0}

		self.nextTrackId = 0
		#self.currentTagIds = [0, 0, 0, 0, 0, 0, 0, 0, 0]


		if sendEvent: pympe.events.library_cleared(self.uid)

	def save_database(self):
		if pympe.arguments['no_library']:
			return

		if self.dbFilename == None:
			print 'Cannot Save, No Database Filename Set'
			return

		with self.lock:
			tmpFilename = self.dbFilename+".bkup"

			startTime = time.time()
			libraryLength = len(self.tracks)
			sharedTagNames = ('artist', 'album', 'year', 'genre', 'albumartist', 'composer', 'tags', 'series', 'season')

			entries = []

		#### self.version, self.nextTrackId, self.currentTagIds
			entries.append(struct.pack('=HH9H', self.version, self.nextTrackId, *(self.sharedTagIds[tag] for tag in sharedTagNames)))


		#### self.tracks
			entries.append(struct.pack('=H', len(self.tracks)))

			for trackId, track in self.tracks.iteritems():
				tempValues = []

				packString = 'H16HI3f7?'
				packValues = [trackId]+[track[key] for key in ('tracknumber', 'tracktotal', 'discnumber', 'disctotal', 'episodenumber', \
					'episodetotal', 'bitrate', 'samplerate', 'length', 'rating', 'playcount', 'skipcount', 'equalizerPreset', 'stopTime', \
					'startTime', 'playbackPosition', 'filesize', 'timeplayed', 'timemodified', 'timeadded', 'compilation', 'hasCustomRating', \
					'usePlaybackPosition', 'useStartTime', 'useStopTime', 'skipOnShuffle', 'useEqualizerPreset')]

				# Bookmarks
				# [numberBookmarks, bookmarkSeconds, bookmarkSeconds, bookmarkSeconds]
				#
				#packString += 'H%iH'%len(track['bookmarks'])
				#packValues.append(len(track['bookmarks']))

				#for bookmark in track['bookmarks']:
					#encoded = bookmark[1].encode('UTF-8')
					#tempString += '%is'%len(encoded)

				# More
				for key in ('hash', 'musicbrainzid', 'uri', 'mimetype', 'lyrics'):
					encoded = track[key].encode('UTF-8')
					packString += '%is'%len(encoded)
					packValues.append(encoded)

				packString += '9H'
				for key in sharedTagNames:
					packValues.append(len(track[key]))

					if len(track[key]):
						packString += '%iH'%len(track[key])
						tempValues += track[key]

				packValues += tempValues




				packString += 'H'
				packValues.append(len(track['title']))
				for encoded in (value.encode('UTF-8') for value in track['title']):
					packString += '%is'%len(encoded)
					packValues.append(encoded)

				packString += 'H'
				packValues.append(len(track['comment']))
				for encoded in (value.encode('UTF-8') for value in track['comment']):
					packString += '%is'%len(encoded)
					packValues.append(encoded)

				try:
					entries.append(struct.pack('=50s', packString))
					entries.append(struct.pack('='+packString, *packValues))
				except struct.error:
					print packString
					print packValues
					print trackId
					print track
					pympe.error('Unable to save database', True)
					return

		#### self.sharedTags
			#{tagName:{tagId:[tagId, tagName, [trackId, trackId, trackId, etc..]]}}
			for tagName in ("artist", "album", "year", "genre", "albumartist", "composer", "tags", "series", 'season'):
				packValues = [len(self.sharedTags[tagName])]
				packString = 'H%iH'%(packValues[0]*3) # sharedTags, tagNameLength, tagTrackIdsLength

				tempValues = []
				tempValues2 = []
				tempValues3 = []
				tempValues4 = []

				for tag in self.sharedTags[tagName].values():
					encodedTag = tag[1].encode('UTF-8')
					packValues.append(tag[0])
					tempValues.append(len(encodedTag))
					tempValues2.append(len(tag)-3)
					tempValues3.append(encodedTag)
					tempValues4 += tag[3:]
					packString += '%is'%len(encodedTag)

				entries.append(struct.pack('='+packString+'%iH'%len(tempValues4), *packValues+tempValues+tempValues2+tempValues3+tempValues4))

			if os.path.exists(tmpFilename):
				print 'Lock file exists cannot save database at this time'

			with open(tmpFilename, 'wb') as db:
				db.write(struct.pack('=L', len(entries)))
				db.write(struct.pack('=%iL'%len(entries), *[len(x) for x in entries]))
				db.write(struct.pack('=H', self.version))
				for entry in entries: db.write(entry)

			pympe.debug('Saved Database [%s] in %f seconds'%(self.dbFilename.rsplit(os.sep, 1)[-1], time.time()-startTime))

			# Ok now that it's saved delete the old and rename it
			if os.path.exists(self.dbFilename): os.remove(self.dbFilename)
			os.rename(tmpFilename, self.dbFilename)


	def load_database(self, sendEvent=False):
		if pympe.arguments['no_library']: return self.blank_database()

		if self.dbFilename == None:
			print 'Cannot Load, No Database Filename Set'
			return self.blank_database()

		if not os.path.exists(self.dbFilename):
			#print 'Failed loading db loading backup'
			#if os.path.exists(self.dbFilename+".bkup"):
			#	dbFilename = self.dbFilename+".bkup"
			#else:
				pympe.error('Failed loading database: {}'.format(self.dbFilename.rsplit(os.sep, 1)[-1]))
				self.blank_database(sendEvent)
				self.save_database()
				return
		else: dbFilename = self.dbFilename



		startTime = time.time()

		self.blank_database(False)
		sharedTagNames = ('artist', 'album', 'year', 'genre', 'albumartist', 'composer', 'tags', 'series', 'season')

		with open(self.dbFilename, 'rb') as db:
			entryCount = struct.unpack('=L', db.read(4))[0]
			entryLengths = list(reversed(struct.unpack('=%iL'%entryCount, db.read(entryCount*4))))
			databaseVersion = struct.unpack('=H', db.read(2))[0]

	#### self.version, self.nextTrackId, self.currentTagIds
			entryLengths.pop()
			version, trackId = struct.unpack('=HH', db.read(4))
			currentIds = struct.unpack('=9H', db.read(18))

			self.version, self.nextTrackId = version, trackId
			self.sharedTagIds = dict(zip(sharedTagNames, currentIds))

	#### self.tracks
			trackCount = struct.unpack('=H', db.read(entryLengths.pop()))[0]

			# Track
			for x in range(trackCount):
				packString = struct.unpack('=50s', db.read(entryLengths.pop()))[0]
				try:
					unpacked = struct.unpack('='+packString, db.read(entryLengths.pop()))
				except struct.error:
					print 'Problem loading Track, Skipping.'
					continue

				track = {}
				trackId, track['tracknumber'], track['tracktotal'], track['discnumber'], track['disctotal'], track['episodenumber'], \
				track['episodetotal'], track['bitrate'], track['samplerate'], track['length'], track['rating'], track['playcount'], \
				track['skipcount'], track['equalizerPreset'], track['stopTime'], track['startTime'], track['playbackPosition'], \
				track['filesize'], track['timeplayed'], track['timemodified'], track['timeadded'], track['compilation'], \
				track['hasCustomRating'], track['usePlaybackPosition'], track['useStartTime'], track['useStopTime'], track['skipOnShuffle'], \
				track['useEqualizerPreset'], track['hash'], track['musicbrainzid'], track['uri'], track['mimetype'], \
				track['lyrics'] = unpacked[:33]

				track['trackId'] = trackId
				track['hash'] = track['hash'].decode('UTF-8')
				track['musicbrainzid'] = track['musicbrainzid'].decode('UTF-8')
				track['uri'] = track['uri'].decode('UTF-8')
				track['mimetype'] = track['mimetype'].decode('UTF-8')
				track['lyrics'] = track['lyrics'].decode('UTF-8')

				lengths = unpacked[33:42]
				unpackedIndex = 42

				for index, key in enumerate(sharedTagNames):
					if lengths[index]:
						track[key] = list(unpacked[unpackedIndex:unpackedIndex+lengths[index]])
						unpackedIndex += lengths[index]
					else:
						track[key] = []

				count = unpacked[unpackedIndex]
				end = unpackedIndex+1+count
				track['title'] = [value.decode('UTF-8') for value in unpacked[unpackedIndex+1:end]]
				count = unpacked[end]
				track['comment'] = [value.decode('UTF-8') for value in unpacked[end+1:-1]]

				self.tracks[trackId] = track
				self.tracksByUri[track['uri']] = track

	#### self.sharedTags
			#{tagName:{tagId:[tagId, tagName, [trackId, trackId, trackId, etc..]]}}
			for tagName in sharedTagNames:
				packed = db.read(entryLengths.pop())
				count = struct.unpack('=H', packed[:2])[0]
				if not count: continue
				integers = struct.unpack('='+('%iH'%count*3), packed[2:2+count*6])#packString = 'H%iH'%(packValues[0]*3) # sharedTags, tagNameLength, tagTrackIdsLength
				sharedTags, tagNameLengths, tagTrackIdLengths = integers[:count], integers[count:count*2], integers[count*2:]
				packedIndex = 2+count*6

				split = struct.unpack('='+('s'.join(str(x) for x in tagNameLengths)+'s%iH'%sum(tagTrackIdLengths)), packed[packedIndex:])
				sharedTagsByName, trackIds = [value.decode('UTF-8') for value in split[:count]], split[count:]

				currentIndex = 0

				for index in range(count):
					entry = [sharedTags[index], sharedTagsByName[index], sharedTagsByName[index].lower()]+list(trackIds[currentIndex:currentIndex+tagTrackIdLengths[index]])
					self.sharedTagsByName[tagName][sharedTagsByName[index]] = entry
					self.sharedTags[tagName][sharedTags[index]] = entry
					currentIndex += tagTrackIdLengths[index]

			pympe.debug('Loaded Database [%s] in %f seconds'%(self.dbFilename.rsplit(os.sep, 1)[-1], time.time()-startTime))

			# Check for invalid entries
			#print sorted(self.sharedTags['albumartist'].keys())

		if sendEvent == True:
			pympe.events.library_replaced(self.uid)

# Get Data
	def get_single_track_single_value(self, trackId, key, returnType='first'):
		return self.get_multi_track_multi_values([trackId], [key], returnType)[0][0]

	def get_single_track_multi_values(self, trackId, keys, returnType='first'):
		return self.get_multi_track_multi_values([trackId], keys, returnType)[0]

	def get_multi_track_single_value(self, trackIds, key, returnType='first'):
		return [x[0] for x in self.get_multi_track_multi_values(trackIds, [key], returnType)]

	def get_multi_track_multi_values(self, trackIds, keys, returnType='first'):
		# DEBUG TODO # print 'get_multi_track_multi_values Used to return None now returns ""'
		trackResult = []

		for trackId in trackIds:
			keyResult = []

			for key in keys:
				if key == 'trackId':
					value = [trackId]
				else:
					# Invalid key or trackId
					try: value = self.tracks[trackId][key]
					except:
						if returnType == 'list': keyResult += [['']]
						else: keyResult += ['']
						continue

					# This makes them both lists because the latter functions assume a list input unless unmodified then return unmolested
					if key in self.multiTagsShared:
						value = [self.sharedTags[key][tagId][1] for tagId in value]
					elif key in self.singleTagsLocal:
						if returnType == 'unmodified' or 'dict':
							keyResult.append(value)
							continue
						value = [value]

					# No result make it a list
					if not value:
						if returnType == 'list': keyResult += [['']]
						else: keyResult += ['']
						continue

				if returnType == 'first': keyResult.append(value[0])
				elif returnType == 'list': keyResult.append(value[:])
				elif returnType == 'joined': keyResult.append('; '.join([str(x) for x in value]))
				elif returnType == 'unmodified' or 'dict': keyResult.append(value)

			if returnType == 'dict':
				trackResult.append(dict(zip(keys, keyResult)))
			else:
				trackResult.append(keyResult)

		return trackResult


# More getters
	def get_trackids(self, filters=None):
		if not filters:
			return self.tracks.keys()

		# To find the common items grouped by key
		sets = [set(itertools.chain(*[self.sharedTagsByName[key][value][3:] for value in values])) for key, values in filters.iteritems()]

		# To find the common items
		#sets = list(itertools.chain(*[[set(pympe.library.sharedTagsByName[key][value][3:]) for value in values] for key, values in filters.iteritems()]))

		# To find the unique items
		#set(pympe.c(*pympe.c(*[[pympe.library.sharedTagsByName[key][value][3:] for value in values] for key, values in pympe.f.iteritems()])))

		if len(sets) == 1:
			return list(sets[0])

		return list(sets[0].intersection(*sets[1:]))

	def get_tag_keys(self, tag, filters=None, sort=False):
		if filters == None: return self.sharedTagsByName[tag].keys()

		tagIds = set(itertools.chain.from_iterable(((self.tracks[trackId][tag] if self.tracks[trackId][tag] else [None]) for trackId in self.get_trackids(filters))))
		nullTag = None in tagIds
		if nullTag: tagIds.remove(None)

		if sort:
			return nullTag, (x[1] for x in sorted((self.sharedTags[tag][tagId] for tagId in tagIds), key=lambda tag: tag[2]))
		else:
			return nullTag, (self.sharedTags[tag][tagId][1] for tagId in tagIds)

	def get_trackid_from_uri(self, uri):
		result = self.tracksByUri.get(uri)

		if result:
			return result['trackId']

	#def get_tracks_by_filter(self, filter):
		#if not filter: return self.tracks.keys()

		#trackIdSets = {}
		#for key in filter.iterkeys():
			#trackIdSets[key] = set()
			#for value in filter[key]:
				#trackIdSets[key].update(self.sharedTags[key][self.sharedTagsByName[key][value]][2])

		## Get the intersection of the sets
		#trackIds = trackIdSets.pop(trackIdSets.keys()[0])
		#for trackIdSet in trackIdSets.values():
			#trackIds = trackIds.intersection(trackIdSet)

		#return list(trackIds)

	#def get_trackid(self, uri):
		#try: return self.tracksByUri[uri][1]
		#except: return None

	#def get_tag_keys(self, tag):
		#return self.sharedTagsByName[tag].keys()

	#def get_filtered_trackids(self, tag, filters):
		## TODO Improve this make it fast instead of just work
		## Get all the trackIds of the items that match each filter
		#trackIdSets = {}
		#for key in filters.keys():
			#trackIdSets[key] = set()
			#for value in filters[key]:
				#trackIdSets[key].update(self.sharedTags[key][self.sharedTagsByName[key][value]][2])

		## Get the intersection of the sets (What's common to them both)
		#trackIds = trackIdSets.pop(trackIdSets.keys()[0])
		#for trackIdSet in trackIdSets.values():
			#trackIds = trackIds.intersection(trackIdSet)

		#return trackIds

	#def get_filtered_tag_keys(self, tag, filters):
		#trackIds = self.get_filtered_trackids(tag, filters)

		## Get the needed tagId from all the trackIds
		#sharedTags = []
		#for trackId in set(trackIds):
			#sharedTags += self.tracks[trackId][tag]
		#sharedTags = set(sharedTags)

		## Return the name of the tag for each
		#return [self.sharedTags[tag][tagId][1] for tagId in sharedTags]

# Set Data
	def set_single_track_single_value(self, trackId, key, value, writeMediaFile=True):
		self.queue.put((self.setter, ([trackId], dict(zip([key], [value])), writeMediaFile)))
	def set_multi_track_single_value(self, trackIds, key, value, writeMediaFile=True):
		self.queue.put((self.setter, (trackIds, dict(zip([key], [value])), writeMediaFile)))
	def set_single_track_multi_values(self, trackId, keys, values, writeMediaFile=True):
		self.queue.put((self.setter, ([trackId], dict(zip(keys, values)),writeMediaFile)))
	def set_multi_track_multi_values(self, trackIds, keys, values, writeMediaFile=True):
		self.queue.put((self.setter, (trackIds, dict(zip(keys, values)), writeMediaFile)))

	def setter(self, trackIds, values, writeToFile, fromInsertTrack=False):
		''' [{'artist':['Bob Marley', 'Lil Wayne']}] '''
		changedTrackIds = []
		changedKeys = []
		removedKeys = {}
		newSharedTagKeys = set()

		# Split the keys if they aren't already
		for key in values.iterkeys():
			if key in self.sharedTags.iterkeys():
				if type(values[key]) != list:
					values[key] = values[key].split(';')


		# For all the tracks that are being changed
		for trackId in trackIds:
			try: existingTrackInfo = self.tracks[trackId]
			except KeyError: # The track doesn't exist
				pympe.error('Tried to set track that doesn\'t exist: %i'%trackId, False, False)
				continue
			changedTags = []

			for key, newValueList in values.iteritems():
				# key:artist, value:['artistA', 'artistB']

				# If it's a shared key we need special attention
				if key in self.sharedTags.iterkeys():
					try:
						oldValueList = [self.sharedTags[key][i][1] for i in existingTrackInfo[key]]
					except KeyError:
						#print 'Error finding existing value for',trackId,key, i
						continue

					# If we haven't changed them or the order then skip this; else mark it as changed
					if newValueList == oldValueList: continue
					changedTags.append(key)

					#print 'Before:',existingTrackInfo[key]

					# Remove all the old ones
					for value in (x for x in oldValueList if not x in newValueList):
						entry = self.sharedTagsByName[key][value]
						if not trackId in entry: continue

						index = entry.index(trackId, 3)
						del entry[index]

						#print 'Removing track from',entry[1]

						# If this was the last track using the tag then remove it entirely
						if len(entry) == 3:
							if not key in removedKeys: removedKeys[key] = [self.sharedTags[key][entry[0]][1]]
							else: removedKeys[key].append(self.sharedTags[key][entry[0]][1])

							#print 'Removing %s completely'%entry[1]
							del self.sharedTags[key][entry[0]]
							del self.sharedTagsByName[key][entry[1]]


					# Add or get existing values
					existingTrackInfo[key] = []
					for value in newValueList:
						existingInfo = self.sharedTagsByName[key].get(value)

						if existingInfo:
							valueId = existingInfo[0]
							self.sharedTags[key][valueId].append(trackId)
							#print 'Existing tag %s has id of %s'%(existingInfo[1], existingInfo[0])
						else:
							#print 'Creating new tag %s id of %s'%(value, self.sharedTagIds[key])
							newInfo = [self.sharedTagIds[key], value, value.lower(), trackId]
							self.sharedTags[key][self.sharedTagIds[key]] = newInfo
							self.sharedTagsByName[key][value] = newInfo
							self.sharedTagIds[key] += 1
							newSharedTagKeys.update([key])
							valueId = newInfo[0]

						existingTrackInfo[key].append(valueId)

					#print 'After:',existingTrackInfo[key]

				# For keys that exist but are unique to the track just overwrite if they are changed
				elif key in self.blankTrack.iterkeys() and existingTrackInfo[key] != newValueList:
					changedTags.append(key)
					existingTrackInfo[key] = newValueList

			if removedKeys:
				pympe.events.library_keys_removed(self.uid, removedKeys)

			if changedTags:
				changedTrackIds.append(trackId)
				changedKeys.append(changedTags)

				# Write the media file if needed
				if writeToFile:
					mediaFile = pm_mediafile.MediaFile(existingTrackInfo['uri'])
					result = mediaFile.read()

					if result == None:
						pympe.error('Library Track:%s Missing File:%s'%(trackId, existingTrackInfo['uri']), False, False)
					elif result:
						mediaFile.write(values)

						# Calculate the new hash
						with open(existingTrackInfo["uri"].split('//', 1)[1]) as f: # Only hash the first 16k
							existingTrackInfo['hash'] = sha1('%s%s'%(struct.pack("=L", os.path.getsize(f.name)), f.read(16384))).hexdigest()

		if fromInsertTrack:
			return newSharedTagKeys

		elif changedTrackIds:
			pympe.events.library_entries_updated(self.uid, changedTrackIds, changedKeys)

			if newSharedTagKeys:
				pympe.events.library_keys_added(self.uid, newSharedTagKeys)

			if pympe.config.get(self.configKey, 'keepOrganized') and self is pympe.library:
				self.organize(changedTrackIds)

# Add/Remove Entries
	def insert_single_track(self, values, indicator=None, callback=None):
		self.queue.put((self.inserter, ([values], indicator, callback)))

	def insert_multi_tracks(self, values, indicator=None, callback=None):
		self.queue.put((self.inserter, (values, indicator, callback)))

	def inserter(self, values, indicator, callback=None):
		trackIds = []
		updatedSharedTagKeys = set()


		for index, values in enumerate(values):
			existing = self.tracksByUri.get(values['uri'])

			#del values['art']

			if existing == None:
				newTrack = dict((key, value[1](value[0])) for key, value in self.blankTrack.iteritems())
				trackId = self.nextTrackId
				self.nextTrackId += 1

				newTrack['trackId'] = trackId
				newTrack['timeadded'] = time.time()
				newTrack['art'] = None

				with open(values["uri"].split('//', 1)[1]) as f: # Only hash the first 16k
					newTrack['hash'] = sha1('%s%s'%(struct.pack("=L", os.path.getsize(f.name)), f.read(16384))).hexdigest()

				self.tracks[trackId] = newTrack
				self.tracksByUri[values['uri']] = newTrack
				trackIds.append(trackId)
			else:
				trackId = existing['trackId']

			# Set the entry info
			updatedSharedTagKeys.update(self.setter([trackId], values, writeToFile=False, fromInsertTrack=True))

			# Write the artwork if it exists
			if 'art' in values:
				pympe.cache.set('art://%i'%trackId, values['art'])

			if indicator:
				indicator.update()

		if trackIds:
			pympe.events.library_entries_added(self.uid, trackIds)

		if updatedSharedTagKeys:
			#print 'inserter'
			pympe.events.library_keys_added(self.uid, updatedSharedTagKeys)

		if callback: callback(trackIds)

	def remove_single_track(self, trackId, deleteFile=False):
		self.queue.put((self.remover, ([trackId], deleteFile)))

	def remove_multi_tracks(self, trackIds, deleteFile=False):
		self.queue.put((self.remover, (trackIds, deleteFile)))

	def remover(self, trackIds, deleteFile):
		removedKeys = {}

		for trackId in trackIds:
			# Try and pull the track info
			try:
				trackInfo = self.tracks[trackId]
			except KeyError:
				#pympe.error('Invalid TrackId [%s] in library [%s] removal attempted.'%(trackId, self.dbFilename), False, False)
				continue

			# Iterate through the track information and remove it
			for key, value in trackInfo.iteritems():
				if key in self.multiTagsShared:
					for sharedTagId in value:
						try:
							sharedTagEntry = self.sharedTags[key][sharedTagId]
							index = sharedTagEntry.index(trackId, 2)
						except KeyError:
							pympe.error('Missing shared tag [%s][%s], continuing.'%(key, sharedTagId), False, False)
							continue
						except ValueError:
							pympe.error('Missing shared tag linkback missing [%s][%s] for track %s, continuing.'%(key, sharedTagId, trackId), False, False)
							continue

						# Remove the link back to the track
						del self.sharedTags[key][sharedTagId][index]

						# If there are no more tracks using the shared tag then delete it
						if len(self.sharedTags[key][sharedTagId]) == 3:
							if not key in removedKeys: removedKeys[key] = [sharedTagEntry[1]]
							else: removedKeys[key].append(sharedTagEntry[1])

							del self.sharedTagsByName[key][sharedTagEntry[1]]
							del self.sharedTags[key][sharedTagId]
							del sharedTagEntry

			try: del self.tracksByUri[trackInfo['uri']]
			except KeyError: print 'Uri did not exist must have been a duplicate: %s'%uri

			del self.tracks[trackId]

			if deleteFile:
				os.remove(trackInfo['uri'].split("://", 1)[1])

		pympe.events.library_keys_removed(self.uid, removedKeys)
		pympe.events.library_entries_removed(self.uid, trackIds)



# Helpers
	def sort_by_tag(self, trackIds, sortOrder, reverse):
		trackIds = sorted(trackIds, key=lambda x: self.get_single_track_multi_values(x, sortOrder))
		if reverse: trackIds.reverse()
		return trackIds

	def get_artwork_path(self, trackId):
		return pympe.cache.filename('art://%i'%trackId)

# Needs removed and replaced
	def get_stats(self, indices=None):
		try:
			if indices != None:
				return "Tracks: {:,} | Playtime: {} | Size: {}".format(len(indices), pm_helpers.make_display_time(sum([self.tracks[x]["length"] for x in indices]), showDays=True), pm_helpers.make_display_filesize(sum([self.tracks[x]["filesize"] for x in indices])))
			return "Tracks: {:,} | Playtime: {}".format(len(self.tracks), pm_helpers.make_display_time(sum([x["length"] for x in self.tracks.values()]), showDays=True))
		except KeyError:
			# Between the call to set title and this it was probably changed
			return ''

#####################################################################################
###########################          Library Search       ###########################
#####################################################################################
	def build_search_index(self):
		searchIndex = {}

		# The search shared tags
		for key, entries in self.sharedTags.iteritems():
			for entry in entries.itervalues():
				sanitized = re.sub(self.searchSanitizerRegex , "", entry[2]).split()

				for word in sanitized:
					if not word in searchIndex: searchIndex[word] = set(entry[3:])
					else: searchIndex[word].update(entry[3:])

		# Titles
		for key, value in self.tracks.iteritems():
			for title in value["title"]:
				sanitized = re.sub(self.searchSanitizerRegex , "", title.lower()).split()

				for entry in sanitized:
					if not entry in searchIndex: searchIndex[entry] = set([key])
					else: searchIndex[entry].add(key)

		return searchIndex

	def __search_helper_search_in_phrase(self, searchString, searchIndex):
		searchResults = []

		special = ["==", "<=", ">=", "<", ">", "=", ":"]
		funcs   = [lambda x, n: (x == searchValue) == n, lambda x, n: (x <= searchValue) == n, lambda x, n: (x >= searchValue) == n, lambda x, n: (x < searchValue) == n, lambda x, n: (x > searchValue) == n, lambda x, n: (x.startswith(searchValue)) == n, lambda x, n: (searchValue in x) == n]

		# For the - not
		if searchString.startswith('-'):
			searchString = searchString[1:]
			positiveResults = False
		else:
			positiveResults = True


		specialSearch = False
		for searchType, searchFunc in zip(special, funcs):
			if searchType in searchString:
				(searchKey, searchValue) = searchString.split(searchType, 1)
				searchValue = re.sub(self.searchSanitizerRegex, "", searchValue)
				specialSearch = True
				break

		if specialSearch:
			if searchKey in self.multiTagsShared:
				[searchResults.extend(entry[3:]) for entry in self.sharedTagsByName[searchKey].itervalues() if searchFunc(re.sub(self.searchSanitizerRegex, "", entry[2]), positiveResults)]
			elif searchKey in self.singleTagsLocal:
				searchResults.extend([trackId for trackId, track in self.tracks.iteritems() if searchFunc(re.sub(self.searchSanitizerRegex, "", str(track[searchKey]).lower()), positiveResults)])
			elif searchKey in self.multiTagsLocal:
				searchResults.extend([trackId for trackId, track in self.tracks.iteritems() if True in [searchFunc(re.sub(self.searchSanitizerRegex, "", str(x).lower()), positiveResults) for x in track[searchKey]]])
			elif searchKey == 'trackid' or searchKey == 'id':
				try: searchValue = int(searchValue)
				except ValueError: return []

				# For ':' func which is startswith
				try: searchResults.extend([trackId for trackId in self.tracks.keys() if searchFunc(trackId, positiveResults)])
				except TypeError:
					searchFunc = funcs[0]
					searchResults.extend([trackId for trackId in self.tracks.keys() if searchFunc(trackId, positiveResults)])
				#searchResults.extend(int(searchValue) if int(searchValue) in self.tracks)
			else: return []
		else:
			# Then find the best matches that have at least as many matches as there are words being searched for
			sanitized = re.sub(self.searchSanitizerRegex, "", searchString).split()
			wordCount = len(sanitized)
			results = []

			# This is for words that start with it
			if wordCount == 1:
				for word in sanitized:
					for key in [(w if w.startswith(word) else None) for w in searchIndex]:
						if key:
							results += list(searchIndex[key])
			# This only searches for whole words
			else:
				for word in sanitized:
					if word in searchIndex:
						results += list(searchIndex[word])

			searchResults.extend([x for x in set(results) if results.count(x) >= wordCount])

		return set(searchResults)

	def search(self, searchString, searchIds=None):
		"""
		The search index is a key/value lookup
		each key is a single word and the value is a list of all the entries in the database that contain that word
		{"some":[5, 2, 62]
		"title":[5, 2, 63]}
		"""

		###
		# First build the search index
		searchIndex = self.build_search_index()

		# Lower the search string
		searchString = searchString.lower()

		# Split it into sections
		sections = [[orSection.strip() for orSection in andSection.split("|")] for andSection in searchString.split("&")]

		# Search per section
		searchResults = set()
		for section in sections:
			# OR the group between itself
			sectionResults = set()
			for phrase in section:
				sectionResults.update(self.__search_helper_search_in_phrase(phrase, searchIndex))

			# AND it to the rest of them
			if not searchResults: searchResults.update(sectionResults)
			else: searchResults.intersection_update(sectionResults)

		return searchResults

#####################################################################################
###########################         Library  Helpers      ###########################
#####################################################################################
	def organize_threaded(self, trackIds, callback=None, newDirectory=None, keepOriginal=False, winSafe=False):
		self.threadOrganize = pm_helpers.run_thread(self.organize, trackIds, callback, newDirectory, keepOriginal, winSafe)

	def organize(self, trackIds, indicator=None, newDirectory=None, keepOriginal=False, winSafe=False):
		#if callback == None:
		#	def callback(x,y='',z=''):
		#		pympe.output('%s, %s, %s'%(x, y, z))

		if indicator == None:
			indicator = pympe.progress.new("Organizing Library" , 0, len(trackIds))
		else:
			indicator.update(0, 'Organizing Library', len(trackIds))

		newFormat = pympe.config.get("import", "renamingRules")
		if newDirectory == None: newDirectory = pympe.config.get(self.configKey, "defaultPath")
		#self.organizing = True
		#if callback: callback(0, "", len(self.tracks))

		#index = 0
		for index, trackId in enumerate(trackIds):
			track = self.tracks[trackId]
			#if not self.organizing: break

			#if callback:
			#	callback(index, "Moving: "+track["uri"].rsplit(os.sep, 1)[-1])
			#	index += 1
			indicator.update(index, 'Moving: %s'%track["uri"])

			# Get the new filename and if it exists continue
			path = newDirectory+os.sep+pm_helpers.sanitize_path(pm_helpers.get_info_format_string(dict(zip(self.dbKeys, self.get_single_track_multi_values(trackId, self.dbKeys))), newFormat)+'.'+(track['uri'].rsplit('.', 1)[1].lower() if '.' in track['uri'] else ''))

			# Remove dots so no hidden directories for some reason?
			# Also strip individial paths so no beginning or ending spaces
			path = os.sep.join([(x[1:] if x.startswith('.') else x).strip() for x in path.split(os.sep)])

			#\:;*?"<>|

			if track['uri'][7:] == path: continue

			## If the file exists then add or increment the number
			#number = re.match('^(.+\.)([0-9]+)(\..+)$', path)
			#if number is not None:
			#	path = '%s%s%s'%(number.groups()[0], int(number.groups()[1])+1, number.groups()[2])

			# If it exists then skip it and print an error
			if os.path.exists(path):
				pympe.error('Path exists not moving file: %s'%path, showUiError=False)
				continue

			# Make the directory tree as needed
			directory = ""
			for d in path.split(os.sep)[:-1]:
				directory += d+os.sep
				if not os.path.exists(directory):
					os.mkdir(directory)

			# Copy the file
			if keepOriginal:
				shutil.copy(track['uri'][7:], path)
			# Move the file
			else:
				shutil.move(track['uri'][7:], path)

				# Update it in the library
				del self.tracksByUri[track['uri']]
				track['uri'] = track['uri'].split('://', 1)[0]+'://'+path
				self.tracksByUri[track['uri']] = self.tracks[trackId]

			# Delete the old path if needed
			oldPath = track['uri'][7:].rsplit(os.sep, 1)[0]
			dirListing = os.listdir(oldPath)
			emptyDirectory = True if (dirListing == ['folder.jpg'] or not dirListing) else False
			if oldPath.startswith(pympe.config.get(self.configKey, 'defaultPath')) and os.path.exists(oldPath) and emptyDirectory:
				print 'Need to Delete Directory %s'%oldPath[len(pympe.config.get(self.configKey, 'defaultPath')):]

		#if callback: callback(-1, "Finished Organizing Library", 0)
		indicator.finish('Finished Organizing Library')

	def insert_track_from_uri(self, uri):
		filename = uri.split('://', 1)[1]
		root = filename.rsplit(os.sep, 1)[0]

		# If it's already in the library and has not been modified then skip it
		modificationTime = os.path.getmtime(filename)

		try:
			if self.tracksByUri[uri]['timemodified'] == modificationTime:
				return
		except KeyError:
			pass

		# Open the file to read and add it
		mediaFile = pm_mediafile.MediaFile(uri)
		info = mediaFile.read()
		if not info: return

		# Write the art if needed
		if pympe.config[self.configKey]["writeFolderImage"]:
			pm_helpers.write_folder_art(mediaFile, root=root)

		# Insert it
		self.insert_single_track(info)

	@pm_helpers.threaded
	def scan_multi_directories_threaded(self, directories, indicator=None, updateDelta=237):
		self.scanning = True
		self.scan_multi_directories(directories, indicator, updateDelta)


	def scan_multi_directories(self, directories, indicator=None, updateDelta=237):
		if indicator == None:
			indicator = pympe.progress.new("Scanning For New Files" , 0, 0)
		else:
			indicator.update(0, 'Scanning For New Files', 0)

		writeArtToDirectory = pympe.config.get(self.configKey, "writeFolderImage")
		trackBuffer = []
		trackCount = 0

		directoryCount = directoryTotal = 0
#		self.scanning = True

		#if callback: callback(0, "Scanning For New Files", 0)
		lastUpdateTime = time.time()

		for directory in directories:
			if self.scanning == False: break
			for root, dirs, files in os.walk(str(directory)):
				if self.scanning == False: break
				directoryTotal += len(dirs)
				directoryCount += 1


				#if callback: callback(directoryCount, root, directoryTotal)
				indicator.update(directoryCount, root, directoryTotal)

				for scanningFile in files:
					if self.scanning == False: break
					scanningFile = u'%s%s%s'%(root, os.sep, scanningFile)
					uri = "file://%s"%scanningFile

					# If it's already in the library and has not been modified then skip it
					modificationTime = os.path.getmtime(scanningFile)

					try:
						if self.tracksByUri[uri]['timemodified'] == modificationTime:
							continue
					except KeyError:
						pass

					# Open the file to read and add it
					mediaFile = pm_mediafile.MediaFile(uri)
					info = mediaFile.read()
					if not info: continue

					trackCount += 1

					# Write the art if needed
					if writeArtToDirectory: pm_helpers.write_folder_art(mediaFile, root=root)

					# Add the track to the write buffer
					trackBuffer.append(info)

					## Every updateDelta directories update the library
					#if trackCount%updateDelta == 0:
					if time.time()-lastUpdateTime > .3:
						lastUpdateTime = time.time()
						self.insert_multi_tracks(trackBuffer)
						trackBuffer = []


		# Update again if needed
		#if trackCount%updateDelta != 0:
		if trackBuffer:
			self.insert_multi_tracks(trackBuffer)

		#if callback: callback(-1, 'Finished Scanning', 0)
		indicator.finish('Finished Scanning')

	def scan_single_directory_threaded(self, directory, indicator=None, updateDelta=237):
		self.scan_multi_directories_threaded([directory], indicator, updateDelta)

	def scan_single_directory(self, directory, indicator=None, updateDelta=237):
		self.scan_multi_directories([directory], indicator, updateDelta)

	@pm_helpers.threaded
	def remove_dead_links_threaded(self, indicator=None, updateDelta=173, trackIds=[]):
		self.scanning = True
		self.remove_dead_links(indicator, updateDelta, trackIds)

	def remove_dead_links(self, indicator=None, updateDelta=173, trackIds=[]):
		total = len(self.tracksByUri)

		if indicator == None:
			indicator = pympe.progress.new("Removing Dead Links" , 0, total)
		else:
			indicator.update(0, 'Removing Dead Links', total)

		#if callback: callback(0, 'Removing Dead Links', total)

		for index, uri in enumerate(self.tracksByUri.keys() if not trackIds else self.get_multi_track_single_value(trackIds, 'uri')):
			if self.scanning == False: break

			if index%updateDelta == 0:
				indicator.update(index, 'Checking Entry')
				#if callback: callback(index, "Checking Entry %s/%s"%(index, total))

			if not os.path.exists(uri[7:]):
				self.remove_single_track(self.tracksByUri[uri]['trackId'])

		indicator.finish('Finished Scanning')
		#if callback: callback(0, 'Finished Scanning', 0)

	def remove_duplicates(self):
		pass

	def update_library(self, directories, indicator=None):
		@pm_helpers.threaded
		def run_thread(directories, indicator):
			self.scanning = True
			self.remove_dead_links()
			self.scan_multi_directories(directories)
		run_thread(directories, indicator)

	def stop_scanning(self):
		self.scanning = False

	def mpevent_player_current_updated(self, event, tags):
		if pympe.player.currentLibrary != self or not pympe.config.get(self.configKey, 'saveMissingTags'):
			return


		modifiedTags = []


		for tag in tags:
			if tag == 'length' and self.tracks[pympe.player.currentId]['length'] == 0:
				self.tracks[pympe.player.currentId]['length'] = pympe.player.currentInfo[tag]
				modifiedTags.append(tag)
			elif not tag in self.tracks[pympe.player.currentId]:
				self.tracks[pympe.player.currentId][tag] = pympe.player.currentInfo[tag]
				modifiedTags.append(tag)

		if modifiedTags:
			#print 'Saved missing tag%s to Id:%s %s'%('s' if len(modifiedTags) > 1 else '', pympe.player.currentId, ', '.join(['%s:%s'%(k,v) for k,v in zip(modifiedTags[0], modifiedTags[1])]))
			pympe.events.library_entries_updated(self.uid, [pympe.player.currentId], [modifiedTags])

	def mpevent_player_current_changed(self, event, currentInfo, currentId, currentLibrary):
		if not currentLibrary is self: return

		self.tracks[currentId]['timeplayed'] = time.time()
		pympe.events.library_entries_updated(self.uid, [currentId], [['timeplayed']])

	def mpevent_player_previous_changed(self, event, previousInfo, previousId, previousLibrary, position, playTime):
		if not previousId: return
		if not previousLibrary is self: return

		if playTime >= 240 or playTime >= previousInfo['length']*.5:
			self.tracks[previousId]['playcount'] += 1
			pympe.events.library_entries_updated(self.uid, [previousId], [['playcount']])
		elif playTime >= 30:
			self.tracks[previousId]['skipcount'] += 1
			pympe.events.library_entries_updated(self.uid, [previousId], [['skipcount']])

	def mpevent_config_key_changed(self, event, section, key, value):
		if self != pympe.library: return

		if section == 'database' and key == 'locations':
			self.update_library(value)
